package com.instalively.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.instalively.android.R;
import com.instalively.android.customViews.LetterTileRoundedTransformation;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.models.RoomsModel;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by deepankur on 11/4/16.
 */
public class SliderMessageListAdapter extends RecyclerView.Adapter<SliderMessageListAdapter.ViewHolder> implements FireBaseKEYIDS {

    private final Context mContext;
    private RecyclerViewClickInterface mClickInterface;
    private LinkedHashMap<String,SliderMessageModel> messageList;
    private final String TAG = this.getClass().getSimpleName();
    private ViewControlsCallback viewControlsCallback;

    public SliderMessageListAdapter(Context context, LinkedHashMap<String,SliderMessageModel> messageList, RecyclerViewClickInterface clickInterface) {
        this.mClickInterface = clickInterface;
        this.messageList = messageList;
        this.mContext = context;
        viewControlsCallback = (ViewControlsCallback) context;
    }

    private SliderMessageModel getMessageDetailsIndex(int index) {
        String[] keySet = messageList.keySet().toArray(new String[messageList.size()]);
        return messageList.get(keySet[index]);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_message_list, parent, false);
        return new ViewHolder(v);
    }

    public void setMessagesList(LinkedHashMap<String,SliderMessageModel> messageList) {
        this.messageList = messageList;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SliderMessageModel messageModel = getMessageDetailsIndex(position);
        holder.nameTv.setText(messageModel.displayName);
        if (messageModel.imageUrl != null && !messageModel.imageUrl.isEmpty()) {
            Picasso.with(mContext).load(messageModel.imageUrl).transform(new RoundedTransformation()).placeholder(R.drawable.error_profile_picture).
                    into(holder.profileImageIv);
        } else {
            Transformation t = new LetterTileRoundedTransformation(mContext, messageModel.displayName);
            Picasso.with(mContext).load(R.drawable.transparent_image).
                    transform(t).into(holder.profileImageIv);
        }

        holder.timeTv.setText(messageModel.updatedAtText);
        if (AppLibrary.checkStringObject(messageModel.status) != null && messageModel.messageType > 4){
            holder.statusTv.setTextColor(R.color.black);
            holder.statusTv.setTypeface(null, Typeface.BOLD);
        }
        holder.statusTv.setText(messageModel.status);
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageModel.status.equals(SENDING_FAILED_TEXT)) {
                    viewControlsCallback.onResumeUpload(messageModel.roomId);
                } else {
                    if (mClickInterface != null)
                        mClickInterface.onItemClick(0, messageModel);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return messageList == null ? 0 : messageList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profileImageIv;
        TextView nameTv, timeTv, statusTv;
        View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            profileImageIv = (ImageView) itemView.findViewById(R.id.profile_picIV);
            nameTv = (TextView) itemView.findViewById(R.id.nameTV);
            timeTv = (TextView) itemView.findViewById(R.id.timeTV);
            statusTv = (TextView) itemView.findViewById(R.id.statusTV);
        }
    }

    public interface ViewControlsCallback {
        void onResumeUpload(String roomId);
    }
}
