package com.instalively.android.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.modelView.MediaModelView;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.LinkedHashMap;

/**
 * Created by deepankur on 27/4/16.
 */
public class SliderMyMomentAdapter extends RecyclerView.Adapter<SliderMyMomentAdapter.ViewHolder> implements FireBaseKEYIDS {

    private Context context;
    private LinkedHashMap<String, MediaModelView> mediaList;
    private ViewControlsCallback viewControlsCallback;

    public SliderMyMomentAdapter(Context context, LinkedHashMap<String, MediaModelView> mediaArrayList) {
        this.context = context;
        this.mediaList = mediaArrayList;
        viewControlsCallback = (ViewControlsCallback) context;
    }

    private MediaModelView getMessageDetailsIndex(int index) {
        String[] keySet = mediaList.keySet().toArray(new String[mediaList.size()]);
        return mediaList.get(keySet[index]);
    }

    public void setMediaList(LinkedHashMap<String, MediaModelView> mediaList) {
        this.mediaList = mediaList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_new_my_moment_slider, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onViewRecycled(final ViewHolder holder) {
        if (holder.imageTask != null) {
            if (holder.imageTask.getStatus() == AsyncTask.Status.RUNNING || holder.imageTask.getStatus() == AsyncTask.Status.PENDING)
                ((AsyncTask) holder.imageTask).cancel(true);
            else
                holder.imageTask.cleanup();

            holder.imageTask = null;
        } else {
            Picasso.with(context).cancelRequest(holder.mediaIv);
        }

        holder.mediaIv.setImageResource(0);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final MediaModelView mediaModelView = getMessageDetailsIndex(position);
        if (mediaModelView.status == MEDIA_UPLOADING_COMPLETE || mediaModelView.status == DOWNLOADED_MY_MOMENT_MEDIA) {
            if (AppLibrary.getMediaType(mediaModelView.url) == AppLibrary.MEDIA_TYPE_VIDEO) {
                holder.imageTask = new FetchImageTask(mediaModelView.url, holder.mediaIv);
                holder.imageTask.execute();
            } else {
                holder.imageTask = null;
                Picasso.with(context).load(new File(mediaModelView.url)).transform(new RoundedTransformation()).fit().centerCrop().error(R.drawable.error_profile_picture).into(holder.mediaIv);
            }
        } else {
            Picasso.with(context).load(R.drawable.error_profile_picture).transform(new RoundedTransformation()).fit().centerCrop().error(R.drawable.error_profile_picture).into(holder.mediaIv);
        }
        if (mediaModelView.status == MEDIA_UPLOADING_STARTED) {
            holder.updatedAtTv.setText(MEDIA_UPLOADING_TEXT);
        } else if (mediaModelView.status == MEDIA_UPLOADING_FAILED) {
            holder.updatedAtTv.setText(MEDIA_UPLOADING_FAILED_TEXT);
        } else if (mediaModelView.status == MEDIA_UPLOADING_COMPLETE) {
            holder.updatedAtTv.setText(AppLibrary.timeAccCurrentTime(mediaModelView.createdAt));
        } else if (mediaModelView.status == DOWNLOADING_MY_MOMENT_MEDIA) {
            //TODO update image with a placeholder
            holder.updatedAtTv.setText(MEDIA_DOWNLOADING_TEXT);
        } else if (mediaModelView.status == DOWNLOAD_MY_MOMENT_MEDIA_FAILED) {
            holder.updatedAtTv.setText(MEDIA_DOWNLOADING_FAILED_TEXT);
        } else if (mediaModelView.status == DOWNLOADED_MY_MOMENT_MEDIA) {
            holder.updatedAtTv.setText(AppLibrary.timeAccCurrentTime(mediaModelView.createdAt));
        }
        if (mediaModelView.mediaText != null) {
            holder.mediaNameTv.setText(mediaModelView.mediaText);
            holder.mediaNameTv.setVisibility(View.VISIBLE);
        } else {
            holder.mediaNameTv.setVisibility(View.INVISIBLE);
        }
        if (mediaModelView.totalViews == 0) {
            holder.viewTv.setVisibility(View.GONE);
            holder.viewsIv.setVisibility(View.GONE);
        } else {
            holder.viewTv.setText(String.valueOf(mediaModelView.totalViews));
        }
        if (mediaModelView.screenShots == 0) {
            holder.screenShotsTv.setVisibility(View.GONE);
            holder.screenShotsIv.setVisibility(View.GONE);
        } else
            holder.screenShotsTv.setText(String.valueOf(mediaModelView.screenShots));
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaModelView.status == MEDIA_UPLOADING_FAILED) {
                    viewControlsCallback.onUploadRetryClicked(mediaModelView.mediaId);
                } else if (mediaModelView.status == DOWNLOAD_MY_MOMENT_MEDIA_FAILED) {
                    FireBaseHelper.getInstance(context).onRetryMyMomentMediaDownload(mediaModelView.mediaId);
                } else if (mediaModelView.status == DOWNLOADING_MY_MOMENT_MEDIA) {
                    // do nothing
                } else {
                    // launch view media fragment
                    ((CameraActivity) context).loadViewMyMediaFragment(mediaModelView.mediaId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View rootView;
        TextView mediaNameTv, updatedAtTv, screenShotsTv, viewTv;
        ImageView mediaIv, screenShotsIv, viewsIv;
        FetchImageTask imageTask;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            mediaNameTv = (TextView) itemView.findViewById(R.id.headerTV);
            updatedAtTv = (TextView) itemView.findViewById(R.id.subHeaderTV);
            screenShotsTv = (TextView) itemView.findViewById(R.id.screenShotTV);
            viewTv = (TextView) itemView.findViewById(R.id.viewsTV);
            mediaIv = (ImageView) itemView.findViewById(R.id.card_mainIV);

            screenShotsIv = (ImageView) itemView.findViewById(R.id.screenShotIV);
            viewsIv = (ImageView) itemView.findViewById(R.id.viewsIV);

        }
    }

    public interface ViewControlsCallback {
        void onUploadRetryClicked(String mediaId);
    }

    private class FetchImageTask extends AsyncTask<String, Void, RoundedBitmapDrawable> {

        String imageUrl;
        ImageView imageView;
        Bitmap bitmap;

        public FetchImageTask(String url, ImageView imageView) {
            this.imageUrl = url;
            this.imageView = imageView;
        }

        @SuppressLint("LongLogTag")
        @Override
        protected RoundedBitmapDrawable doInBackground(String... params) {
            bitmap = ThumbnailUtils.createVideoThumbnail(new File(imageUrl).getAbsolutePath(), MediaStore.Images.Thumbnails.MICRO_KIND);
            if (bitmap != null) {
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
                drawable.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()) / 2.0f);
                return drawable;
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(RoundedBitmapDrawable drawable) {
            super.onPostExecute(drawable);
            if (drawable != null)
                imageView.setImageDrawable(drawable);
            else if (!isCancelled())
                imageView.setImageResource(R.drawable.error_profile_picture);
        }

        @Override
        protected void onCancelled() {
            if (bitmap != null) {
                bitmap.recycle();
                bitmap = null;
            }
        }

        private void cleanup() {
            if (bitmap != null) {
                bitmap.recycle();
                bitmap = null;
            }
        }
    }
}
