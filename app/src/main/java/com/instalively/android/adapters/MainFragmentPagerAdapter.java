package com.instalively.android.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.instalively.android.fragments.DashBoardFragment;

/**
 * Created by abc on 3/8/2016.
 */
public class MainFragmentPagerAdapter extends FragmentPagerAdapter {

    private static final int mCount = 1;
    private boolean fromIntent;

    public MainFragmentPagerAdapter(FragmentManager fm, boolean fromIntent) {
        super(fm);
        this.fromIntent = fromIntent;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0 :
                fragment = new DashBoardFragment();
                break;
//            case 1 :
//                Bundle bundle = new Bundle();
//                fragment = new CameraFragment();
//                bundle.putBoolean("fromIntent",fromIntent);
//                fragment.setArguments(bundle);
//                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mCount;
    }
}
