package com.instalively.android.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.instalively.android.fragments.UpcomingMomentsFragment;
import com.instalively.android.fragments.ViewMomentDetailsFragment;

/**
 * Created by user on 5/12/2016.
 */
public class ViewMomentAdapter extends FragmentPagerAdapter{

    private int mCount = 2;

    public ViewMomentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new ViewMomentDetailsFragment();
                break;
            case 1:
                fragment = new UpcomingMomentsFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mCount;
    }
}