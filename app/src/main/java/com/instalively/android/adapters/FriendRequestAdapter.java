package com.instalively.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.instalively.android.R;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.models.SocialModel;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

/**
 * Created by deepankur on 16/4/16.
 */
public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.ViewHolder> {

    private HashMap<String, SocialModel.RequestReceived> mRequestReceivedMap;
    private Context mContext;
    private FireBaseHelper mFireBaseHelper;
    private RecyclerViewClickInterface mClickInterface;
    private String myUserId;
    private String[] requestKeySet;

    public FriendRequestAdapter(HashMap<String, SocialModel.RequestReceived> requestReceivedMap, Context mContext, RecyclerViewClickInterface clickInterface) {
        this.mRequestReceivedMap = requestReceivedMap;
        this.requestKeySet = mRequestReceivedMap.keySet().toArray(new String[mRequestReceivedMap.size()]);
        this.mContext = mContext;
        this.mFireBaseHelper = FireBaseHelper.getInstance(mContext);
        this.myUserId = mFireBaseHelper.getMyUserId();
        this.mClickInterface = clickInterface;
    }

    public void refreshData() {
        this.requestKeySet = mRequestReceivedMap.keySet().toArray(new String[mRequestReceivedMap.size()]);
        notifyDataSetChanged();
    }

    @Override
    public FriendRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_add_friend, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        SocialModel.RequestReceived request = mRequestReceivedMap.get(requestKeySet[position]);
        holder.getAdapterPosition();
        holder.nameIv.setText(request.name);
        Picasso.with(mContext).load(request.imageUrl).transform(new RoundedTransformation()).into(holder.profileImageIv);

        holder.acceptRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sendRequestTo = ((requestKeySet[holder.getAdapterPosition()]));
                mFireBaseHelper.acceptFriendRequest(myUserId, sendRequestTo);
                mRequestReceivedMap.remove(requestKeySet[holder.getAdapterPosition()]);
                if (mClickInterface != null)
                    mClickInterface.onItemClick(position, false);

            }
        });

        holder.ignoreRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFireBaseHelper.ignoreFriendRequest(myUserId, (mRequestReceivedMap.get(requestKeySet[0]).UID));
                if (mClickInterface != null)
                    mClickInterface.onItemClick(position, true);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mRequestReceivedMap.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameIv;
        ImageView profileImageIv, addFriendIv;
        TextView handleTv;
        View rootView;
        LinearLayout confirm_ignoreLl;
        Button acceptRequest, ignoreRequest;


        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            nameIv = (TextView) itemView.findViewById(R.id.friend_name_TV);
            profileImageIv = (ImageView) itemView.findViewById(R.id.friend_pic_IV);
            confirm_ignoreLl = (LinearLayout) itemView.findViewById(R.id.request_accept_reject_LL);
            confirm_ignoreLl.setVisibility(View.VISIBLE);
            addFriendIv = (ImageView) itemView.findViewById(R.id.add_friend_IV);
            handleTv = (TextView) itemView.findViewById(R.id.friend_handle_TV);

            addFriendIv.setVisibility(View.GONE);
            handleTv.setVisibility(View.GONE);

            acceptRequest = (Button) itemView.findViewById(R.id.acceptRequestBTN);
            ignoreRequest = (Button) itemView.findViewById(R.id.ignoreRequestBTN);

        }
    }
}
