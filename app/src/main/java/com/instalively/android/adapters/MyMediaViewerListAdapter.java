package com.instalively.android.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.firebase.FireBaseKEYIDS;

/**
 * Created by user on 6/22/2016.
 */
public class MyMediaViewerListAdapter extends RecyclerView.Adapter<MyMediaViewerListAdapter.ItemViewHolder> implements FireBaseKEYIDS {

    private String[] viewerArray;

    public MyMediaViewerListAdapter(String[] objects){
        viewerArray = objects;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView viewerNameTv;

        public ItemViewHolder(View itemView) {
            super(itemView);
            viewerNameTv = (TextView) itemView.findViewById(R.id.viewsTV);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_my_moment_slider, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.viewerNameTv.setText(viewerArray[position]);
    }

    @Override
    public int getItemCount() {
        return viewerArray.length;
    }
}
