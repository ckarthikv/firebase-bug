package com.instalively.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.customViews.LetterTileRoundedTransformation;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.FontPicker;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

/**
 * Created by deepankur on 30/4/16.
 */
public class AllChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FireBaseKEYIDS {

    private Context context;
    private ArrayList<SliderMessageModel> allChatsModels;// here each null item will represent headerType;ie switch of alphabet
    private static final int TYPE_HEADER = 11111;
    private static final int TYPE_ITEM = 55555;
    private RecyclerViewClickInterface recyclerViewClickInterface;
    public FontPicker fontPicker;

    public AllChatListAdapter(Context context, ArrayList<SliderMessageModel> allChatsModels, RecyclerViewClickInterface recyclerViewClickInterface) {
        this.context = context;
        this.allChatsModels = allChatsModels;
        this.recyclerViewClickInterface = recyclerViewClickInterface;
        fontPicker = FontPicker.getInstance(context);
    }

    public ArrayList<SliderMessageModel> getAllChatsModels() {
        return allChatsModels;
    }

    public void setAllChatsModels(ArrayList<SliderMessageModel> allChatsModels) {
        this.allChatsModels = allChatsModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM)
            return new VHItem(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_all_chat_list, parent, false));

        if (viewType == TYPE_HEADER)
            return new VHHeader(LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_chat_list_header, parent, false));

        throw new RuntimeException("there is no type that matches the type " + viewType + "  wtf ");
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            ((VHItem) holder).name.setText(allChatsModels.get(position).displayName);
            int roomType = allChatsModels.get(position).roomType;
            if (roomType == FRIEND_ROOM) {
                Picasso.with(context).load(allChatsModels.get(position).imageUrl).
                        transform(new RoundedTransformation()).into(((VHItem) holder).imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        Picasso.with(context).load(R.drawable.error_profile_picture).
                                transform(new RoundedTransformation()).into(((VHItem) holder).imageView);
                    }
                });
            } else {
                Transformation t = new LetterTileRoundedTransformation(context, allChatsModels.get(position).displayName);
                Picasso.with(context).load(R.drawable.transparent_image).
                        transform(t).into(((VHItem) holder).imageView);
            }
            ((VHItem) holder).rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewClickInterface.onItemClick(0, allChatsModels.get(holder.getAdapterPosition()));
                }
            });
        }
        if (holder instanceof VHHeader) {
            //cast holder to HeaderVH and set data for header.
            if (position == 0) {
                ((VHHeader) holder).alphabetTv.setText("New Group");
                ((VHHeader) holder).imageView.setVisibility(View.VISIBLE);
                ((VHHeader) holder).alphabetTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ((RelativeLayout.LayoutParams) ((VHHeader) holder).alphabetTv.getLayoutParams()).leftMargin = AppLibrary.convertDpToPixels(context, 0);
                ((VHHeader) holder).alphabetTv.setAlpha(0.87f);
                ((VHHeader) holder).rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //using null we indicate create group action
                        recyclerViewClickInterface.onItemClick(0, null);

                    }
                });
            } else {
                ((VHHeader) holder).imageView.setVisibility(View.GONE);
                char displayAlphabet = allChatsModels.get(position + 1).displayName.charAt(0);
                ((VHHeader) holder).alphabetTv.setText(String.valueOf(Character.toUpperCase(displayAlphabet)));
                ((VHHeader) holder).alphabetTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                ((RelativeLayout.LayoutParams) ((VHHeader) holder).alphabetTv.getLayoutParams()).leftMargin = AppLibrary.convertDpToPixels(context, 16);
                ((VHHeader) holder).alphabetTv.setAlpha(0.54f);
            }
        }
    }


    @Override
    public int getItemCount() {
        return (allChatsModels == null ? 0 : allChatsModels.size());
    }

    /**
     * @param position the position from 0 to array list size
     * @return header type if the array List item at that position ==null
     */
    @Override
    public int getItemViewType(int position) {
//        if (position == 0) return TYPE_SUPER_HEADER;
        //--position to adjust super header
        return (allChatsModels.get(position) == null ? TYPE_HEADER : TYPE_ITEM);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }


    class VHItem extends RecyclerView.ViewHolder {
        TextView name;
        ImageView imageView;
        View rootView;

        public VHItem(View itemView) {
            super(itemView);
            rootView = itemView;
            name = (TextView) itemView.findViewById(R.id.cardTV);
            imageView = (ImageView) itemView.findViewById(R.id.cardIV);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        TextView alphabetTv;
        View rootView;
        ImageView imageView;

        public VHHeader(View itemView) {
            super(itemView);
            rootView = itemView;
            alphabetTv = (TextView) itemView.findViewById(R.id.alphabetTv);
            imageView = (ImageView) itemView.findViewById(R.id.groupIV);
            alphabetTv.setTypeface(fontPicker.getMontserratRegular());
        }
    }
}
