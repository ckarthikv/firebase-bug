package com.instalively.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.instalively.android.R;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.models.UserModel;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by deepankur on 16/4/16.
 */
public class AddFriendFromFBAdapter extends RecyclerView.Adapter<AddFriendFromFBAdapter.ViewHolder> {

    private ArrayList<UserModel> userModelArrayList;
    private Context mContext;
    private FireBaseHelper mFireBaseHelper;

    public AddFriendFromFBAdapter(ArrayList<UserModel> userModelArrayList, Context mContext) {
        this.userModelArrayList = userModelArrayList;
        this.mContext = mContext;
        this.mFireBaseHelper = FireBaseHelper.getInstance(mContext);
    }

    @Override
    public AddFriendFromFBAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_add_friend, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final UserModel userModel = userModelArrayList.get(position);
        holder.nameIv.setText(userModel.name);
        holder.handleTv.setText(userModel.handle);
        Picasso.with(mContext).load(userModel.imageUrl).transform(new RoundedTransformation()).into(holder.profileImageIv);
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFireBaseHelper.sendFriendRequest(mFireBaseHelper.getMyUserId(), userModel.KEY_UID);
                userModelArrayList.remove(userModelArrayList.get(holder.getAdapterPosition()));

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return userModelArrayList==null?0:userModelArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameIv;
        ImageView profileImageIv, addFriendIv;
        TextView handleTv;
        View rootView;
        //  LinearLayout confirm_ignoreLl;
        //  Button acceptRequest,ignoreRequest;


        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            nameIv = (TextView) itemView.findViewById(R.id.friend_name_TV);
            profileImageIv = (ImageView) itemView.findViewById(R.id.friend_pic_IV);
            addFriendIv = (ImageView) itemView.findViewById(R.id.add_friend_IV);
            handleTv = (TextView) itemView.findViewById(R.id.friend_handle_TV);

        }
    }
}
