package com.instalively.android.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.models.MomentModel;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.util.Map;

/**
 * Created by user on 4/22/2016.
 */
public class MomentListAdapter  extends HeaderRecyclerViewAdapterV2{

    // View Type for Separators
    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    // View Type for Regular rows
    private static final int ITEM_VIEW_TYPE_REGULAR = 1;
    private static final int ITEM_VIEW_TYPE_COUNT = 2;
    private static final String TAG = "MomentListAdapter";
    private Context mContext;
    private String[] momentModelArray;
    private Map<String,MomentModel> modelMap;
    private ViewControlsCallback viewControlsCallback;

    public class MomentListViewHolder extends RecyclerView.ViewHolder{

        private ImageView momentImage;
        private TextView momentName;
        private RadioButton radioButton;
        private View itemView;
        private TextView settingView;

        public MomentListViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            momentImage = (ImageView) itemView.findViewById(R.id.momentImage);
            momentName = (TextView) itemView.findViewById(R.id.momentName);
            radioButton = (RadioButton) itemView.findViewById(R.id.itemRadio);
            settingView = (TextView) itemView.findViewById(R.id.settingTextView);
        }
    }

    public MomentListAdapter(Fragment fragment,Context context, Map<String,MomentModel> modelMap){
        mContext = context;
        this.modelMap = modelMap;
        momentModelArray = this.modelMap.keySet().toArray(new String[this.modelMap.keySet().size()]);
        viewControlsCallback = (ViewControlsCallback)fragment;
    }

    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public boolean useFooter() {
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.moment_list_item, parent, false);
        return new MomentListViewHolder(view);
    }

    @Override
    public void onBindBasicItemView(final RecyclerView.ViewHolder holder, final int position) {
        if (modelMap.get(momentModelArray[position]).source == 1){
            ((MomentListViewHolder)holder).momentName.setText("My Moments");
        } else {
            ((MomentListViewHolder)holder).momentName.setText(modelMap.get(momentModelArray[position]).name);
        }

        ((MomentListViewHolder)holder).settingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMomentSelected(holder,position);
                viewControlsCallback.onSettingsSelected(v);
            }
        });
        Map<String,MomentModel.Media> mediaMap = modelMap.get(momentModelArray[position]).media;
        if (mediaMap != null) {
            Map.Entry<String, MomentModel.Media> entrySet = mediaMap.entrySet().iterator().next();
            Picasso.with(mContext).load(entrySet.getValue().url)
                    .fit().transform(new RoundedTransformation())
                    .into(((MomentListViewHolder) holder).momentImage);
        } else {
            Picasso.with(mContext).load(R.drawable.error_profile_picture)
                    .fit().transform(new RoundedTransformation())
                    .into(((MomentListViewHolder) holder).momentImage);
        }
        ((MomentListViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onMomentSelected(holder,position);
            }
        });
    }

    private void onMomentSelected(RecyclerView.ViewHolder holder,int position){
        if (((MomentListViewHolder) holder).radioButton.isChecked()){
            ((MomentListViewHolder) holder).radioButton.setChecked(false);
        } else {
            ((MomentListViewHolder) holder).radioButton.setChecked(true);
        }
        viewControlsCallback.onMomentSelected(momentModelArray[position],modelMap.get(momentModelArray[position]).source);
    }

    @Override
    public int getBasicItemCount() {
        return momentModelArray.length;
    }

    @Override
    public int getBasicItemType(int position) {
        return ITEM_VIEW_TYPE_REGULAR;
    }

    public interface ViewControlsCallback{
        void onMomentSelected(String momentId,int source);

        void onSettingsSelected(View view);
    }

}
