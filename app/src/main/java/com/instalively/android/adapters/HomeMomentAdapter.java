package com.instalively.android.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;


import com.instalively.android.R;
import com.instalively.android.customViews.LetterTileRoundedTransformation;
import com.instalively.android.customViews.TintRoundedTransformation;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.modelView.HomeMomentViewModel;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

/**
 * Created by deepankur on 22/4/16.
 */
public class HomeMomentAdapter extends RecyclerView.Adapter<HomeMomentAdapter.ViewHolder> implements FireBaseKEYIDS {

    private final String myUserId;
    private Context context;
    private RecyclerViewClickInterface recyclerViewClickInterface;
    private final int MOMENT_VIEW_TYPE;
    private ArrayList<HomeMomentViewModel> momentViewModels;
    private HomeMomentViewModel clickedMoment;
    private GestureDetector gestureDetector;
    private final String TAG = this.getClass().getSimpleName();
    private final static float STROKE_WIDTH = 3;

    public HomeMomentAdapter(String myUserId, Context context, ArrayList<HomeMomentViewModel> momentViewModels, int momentViewType,
                             RecyclerViewClickInterface recyclerViewClickInterface) {
        this.myUserId = myUserId;
        this.context = context;
        this.momentViewModels = momentViewModels;
        this.MOMENT_VIEW_TYPE = momentViewType;
        this.recyclerViewClickInterface = recyclerViewClickInterface;
        this.gestureDetector = new GestureDetector(context, new GestureListener());

    }

    public void setMomentModelArrayList(ArrayList<HomeMomentViewModel> momentViewModels) {
        this.momentViewModels = momentViewModels;
    }

    public ArrayList<HomeMomentViewModel> getMomentModelArrayList() {
        return this.momentViewModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_friends_grid, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HomeMomentViewModel homeMoment = momentViewModels.get(position);
        if (homeMoment.momentStatus == DOWNLOADING_MOMENT || homeMoment.momentStatus == SEEN_BUT_DOWNLOADING) {
            holder.ripple1.setVisibility(View.VISIBLE);
            holder.ripple2.setVisibility(View.VISIBLE);
        } else {
            holder.ripple1.setVisibility(View.GONE);
            holder.ripple2.setVisibility(View.GONE);
        }

        Transformation t;
        holder.nameTv.setText(homeMoment.name);
        holder.timeTv.setText(homeMoment.updatedAtText);

        if (MOMENT_VIEW_TYPE == AROUND_YOU_MOMENT_RECYCLER) {
//            holder.nameTv.setMaxWidth(AppLibrary.convertDpToPixels(context,70));
            holder.timeTv.setVisibility(View.GONE);

            if (homeMoment.momentStatus == UNSEEN_MOMENT || homeMoment.momentStatus == READY_TO_VIEW_MOMENT) {
                if (homeMoment.imageUrl != null)
                    Picasso.with(context).load(homeMoment.imageUrl).transform(new RoundedTransformation()).
                            into(holder.profilePicIv);
                else Picasso.with(context).load(R.drawable.transparent_image).
                        transform(new LetterTileRoundedTransformation(context, homeMoment.name)).
                        into(holder.profilePicIv);
            }
            if (homeMoment.momentStatus == DOWNLOADING_MOMENT || homeMoment.momentStatus == SEEN_BUT_DOWNLOADING) {
                if (homeMoment.imageUrl != null)
                    Picasso.with(context).load(homeMoment.imageUrl).transform(new RoundedTransformation()).
                            into(holder.profilePicIv);
                else Picasso.with(context).load(R.drawable.transparent_image).
                        transform(new LetterTileRoundedTransformation(context, homeMoment.name)).
                        into(holder.profilePicIv);
                doRippling(holder);
            }
         /*   if (homeMoment.momentStatus == READY_TO_VIEW_MOMENT) {
                if (homeMoment.imageUrl != null)
                    Picasso.with(context).load(homeMoment.imageUrl).transform(new RoundedTransformation()).
                            into(holder.profilePicIv);
                else Picasso.with(context).load(R.drawable.transparent_image).
                        transform(new LetterTileRoundedTransformation(context, homeMoment.name)).
                        into(holder.profilePicIv);
            }*/
            if (homeMoment.momentStatus == SEEN_MOMENT) {
                if (homeMoment.imageUrl != null)
                    Picasso.with(context).load(homeMoment.imageUrl).transform(new RoundedTransformation(true)).
                            into(holder.profilePicIv);
                else {
                    Picasso.with(context).load(R.drawable.transparent_image).
                            transform(new LetterTileRoundedTransformation(context, homeMoment.name, true)).
                            into(holder.profilePicIv);
                }
            }
        }


        if (MOMENT_VIEW_TYPE == UNSEEN_FRIEND_MOMENT_RECYCLER) {
            drawProgress(holder.timerIv, homeMoment.angle);
            if (homeMoment.momentStatus == UNSEEN_MOMENT) {
                t = new LetterTileRoundedTransformation(context, homeMoment.name);
                Picasso.with(context).load(homeMoment.imageUrl).
                        transform(t).into(holder.profilePicIv, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        Log.d(TAG, " onError for " + homeMoment.name);
                    }
                });
            }
            if (homeMoment.momentStatus == SEEN_MOMENT) {
                t = new TintRoundedTransformation(context, homeMoment.name, true);
                Picasso.with(context).load(homeMoment.imageUrl).
                        transform(t).into(holder.profilePicIv);
            }
            if (homeMoment.momentStatus == READY_TO_VIEW_MOMENT) {
                t = new TintRoundedTransformation(context, homeMoment.name, false);
                Picasso.with(context).load(homeMoment.imageUrl).
                        transform(t).into(holder.profilePicIv);
            }
            if (homeMoment.momentStatus == DOWNLOADING_MOMENT) {
                t = new TintRoundedTransformation(context, homeMoment.name, false);
                Picasso.with(context).load(R.drawable.transparent_image).
                        transform(t).into(holder.profilePicIv);
                doRippling(holder);
/*                holder.ripple1.setVisibility(View.VISIBLE);
                holder.ripple2.setVisibility(View.VISIBLE);
                ScaleAnimation animation1, animation2;
                animation1 = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animation2 = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);


                animation1.setRepeatCount(Animation.INFINITE);
                animation2.setRepeatCount(Animation.INFINITE);
                animation1.setDuration(1000);
                animation2.setDuration(1000);
                animation2.setStartOffset(500);
                holder.ripple1.startAnimation(animation1);
                holder.ripple2.startAnimation(animation2);*/
            }
        }


        if (MOMENT_VIEW_TYPE == SEEN_FRIEND_MOMENT_RECYCLER) {
            if (homeMoment.momentStatus == SEEN_BUT_DOWNLOADING) {
                t = new TintRoundedTransformation(context, homeMoment.name, false);
                Picasso.with(context).load(homeMoment.imageUrl).
                        transform(t).into(holder.profilePicIv);
                doRippling(holder);
/*                holder.ripple1.setVisibility(View.VISIBLE);
                holder.ripple2.setVisibility(View.VISIBLE);
                ScaleAnimation animation1, animation2;
                animation1 = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animation2 = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);


                animation1.setRepeatCount(Animation.INFINITE);
                animation2.setRepeatCount(Animation.INFINITE);
                animation1.setDuration(1000);
                animation2.setDuration(1000);
                animation2.setStartOffset(500);
                holder.ripple1.startAnimation(animation1);
                holder.ripple2.startAnimation(animation2);*/
            } else {
                Picasso.with(context).load(homeMoment.imageUrl).
                        transform(new RoundedTransformation()).
                        into(holder.profilePicIv);
            }
        }

        holder.profilePicIv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                clickedMoment = homeMoment;
                return true;
            }
        });
        int diameter;
        switch (MOMENT_VIEW_TYPE) {
            case SEEN_FRIEND_MOMENT_RECYCLER:
                diameter = 70;
                break;
            case UNSEEN_FRIEND_MOMENT_RECYCLER:
                diameter = 96;
                break;
            case FAVOURITE_MOMENT_RECYCLER:
                diameter = 90;
                break;
            case AROUND_YOU_MOMENT_RECYCLER:
                diameter = 56;
                break;
            default:
                diameter = 70;
                break;
        }
        diameter = AppLibrary.convertDpToPixels(context, diameter);
        holder.profilePicIv.getLayoutParams().height = diameter;
        holder.profilePicIv.getLayoutParams().width = diameter;

    }

    private void doRippling(ViewHolder holder) {
//        holder.ripple1.setVisibility(View.VISIBLE);
//        holder.ripple2.setVisibility(View.VISIBLE);
        ScaleAnimation animation1, animation2;
        animation1 = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation2 = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);


        animation1.setRepeatCount(Animation.INFINITE);
        animation2.setRepeatCount(Animation.INFINITE);
        animation1.setDuration(1000);
        animation2.setDuration(1000);
        animation2.setStartOffset(500);
        holder.ripple1.startAnimation(animation1);
        holder.ripple2.startAnimation(animation2);
    }

    private void drawProgress(ImageView imageView, float angle) {

        int diameter = AppLibrary.convertDpToPixels(context, 92);
        Bitmap myBitmap = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888);

        Bitmap tempBitmap = Bitmap.createBitmap(myBitmap.getWidth(), myBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(tempBitmap);

        tempCanvas.drawBitmap(myBitmap, 0, 0, null);

        if (angle != 0) {
            int padding = 2;
            RectF rectF = new RectF(padding, padding, diameter - padding, diameter - padding);
            Paint p = new Paint();
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeCap(Paint.Cap.ROUND);
            p.setAntiAlias(true);
            p.setStrokeWidth(STROKE_WIDTH);
            p.setColor(Color.WHITE);
            tempCanvas.drawArc(rectF, -90, angle, false, p);
        }

        imageView.setImageDrawable(new BitmapDrawable(context.getResources(), tempBitmap));
    }


    @Override
    public int getItemCount() {
        return momentViewModels == null ? 0 : momentViewModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profilePicIv, timerIv;
        TextView nameTv, timeTv;
        ImageView ripple1, ripple2;

        public ViewHolder(View itemView) {
            super(itemView);
            profilePicIv = (ImageView) itemView.findViewById(R.id.profileIV);
            timerIv = (ImageView) itemView.findViewById(R.id.timerIV);
            nameTv = (TextView) itemView.findViewById(R.id.nameTV);
            timeTv = (TextView) itemView.findViewById(R.id.updatedAtTV);
            ripple1 = (ImageView) itemView.findViewById(R.id.rippleIv1);
            ripple2 = (ImageView) itemView.findViewById(R.id.rippleIv2);
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.d(TAG, "Single Tap " + "Tapped at: (" + e.getX() + "," + e.getY() + ")");
            clickedMoment.clickType = HomeMomentViewModel.ClickType.SINGLE_TAP;
            if (recyclerViewClickInterface != null)
                recyclerViewClickInterface.onItemClick(0, clickedMoment);

            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.d(TAG, "Double Tap " + "Tapped at: (" + e.getX() + "," + e.getY() + ")");
            clickedMoment.clickType = HomeMomentViewModel.ClickType.DOUBLE_TAP;
            if (recyclerViewClickInterface != null)
                recyclerViewClickInterface.onItemClick(0, clickedMoment);
            return true;
        }
    }
}
