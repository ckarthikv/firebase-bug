package com.instalively.android;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.HandlerThread;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.appsflyer.AppsFlyerLib;
import com.facebook.FacebookSdk;
import com.instalively.android.apihandling.RequestManager;
import com.instalively.android.services.KillAppService;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.InstallationController;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.EventBusBuilder;
/**
 * Created by admin on 1/21/2015.
 */

//@ReportsCrashes(formKey = "", // will not be used
//        formUri = "http://mandrillapp.com/api/1.0/messages/send.json"
//)
public class MasterClass extends Application{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.

    private static EventBus mEventBus;
    private static SharedPreferences prefs;
    private static String deviceId;
    public static TransferUtility transferUtility;
    private static AmazonS3Client s3Client;
    private static CognitoCachingCredentialsProvider sCredProvider;
    private static HandlerThread uploadWaiter = new HandlerThread("uploadWaiter");
    public static android.os.Handler uploadHandler;

    @Override
    public void onCreate() {
        super.onCreate();
//        ACRA.init(this);
        prefs = getSharedPreferences(AppLibrary.APP_SETTINGS, 0);
//        CameraFragment.mVisible = true;

        AppsFlyerLib.setAppsFlyerKey("HAGYCQzWF8zBz9BYvk3Xvk");
        AppsFlyerLib.sendTracking(getApplicationContext());
        AppsFlyerLib.setUseHTTPFalback(true);
        deviceId = InstallationController.id(this);

        FacebookSdk.sdkInitialize(getApplicationContext());

        uploadWaiter.start();
        uploadHandler = new android.os.Handler(uploadWaiter.getLooper());
//        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//        Fabric.with(this, new Twitter(authConfig), new Crashlytics(),new CrashlyticsNdk());

        /* start Event bus for signaling */
        buildEventBus();
        
        /* initialize context loaders/Managers */
        RequestManager.initialize(this);

//        /* initialize parse */
//        Parse.enableLocalDatastore(getApplicationContext());
//        initializeParse(this);

        /* initialize service to track app killing */
        startService(new Intent(getApplicationContext(), KillAppService.class));

        /* initialize Cognito for Uploads */
        sCredProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),    // get the context for the current activity
                "us-east-1:5d48bd03-d736-4a6c-9f0b-c4761abe73f5",    /* Identity Pool ID */
                Regions.US_EAST_1           /* Region for your identity pool*/
        );
        s3Client = new AmazonS3Client(sCredProvider);
//        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

        transferUtility = new TransferUtility(s3Client,getApplicationContext());

        //todo remove in production
        // Enable all thread strict mode policies
        StrictMode.ThreadPolicy.Builder threadPolicyBuilder = new StrictMode.ThreadPolicy.Builder();

        // Detect everything that's potentially suspect
        threadPolicyBuilder.detectAll();

        // Crash the whole process on violation
        threadPolicyBuilder.penaltyDeath();

        // Log detected violations to the system log
        threadPolicyBuilder.penaltyLog();

        // Crash the whole process on any network usage.
        threadPolicyBuilder.penaltyDeathOnNetwork();


        StrictMode.ThreadPolicy threadPolicy = threadPolicyBuilder.build();
        StrictMode.setThreadPolicy(threadPolicy);

        // Enable all VM strict mode policies
        StrictMode.VmPolicy.Builder vmPolicyBuilder = new StrictMode.VmPolicy.Builder();

        // Detect everything that's potentially suspect
        vmPolicyBuilder.detectAll();

        // Log detected violations to the system log
        vmPolicyBuilder.penaltyLog();

        StrictMode.VmPolicy vmPolicy = vmPolicyBuilder.build();
        StrictMode.setVmPolicy(vmPolicy);

    }

    public static EventBus getEventBus() {
        if (mEventBus == null)
            buildEventBus();
        return mEventBus;
    }

    public static TransferUtility getTransferUtility() {
        return transferUtility;
    }

//    private void initializeParse(Context context) {
//        Parse.initialize(context, parseAppId, parseClientKey);
//    }
//
//    public static void saveParseObject() {
//        String userId = prefs.getString(AppLibrary.USER_LOGIN, null);
//        String userEmail = prefs.getString(AppLibrary.USER_LOGIN_EMAIL, null);
//        String userName = prefs.getString(AppLibrary.USER_NAME, null);
//        String deviceName = android.os.Build.MODEL;
//        String appVersion = RequestManager.APP_VERSION_CODE;
//
//        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
//
//        if (AppLibrary.checkStringObject(userId) != null)
//            installation.put("userId", userId);
//
//        if (AppLibrary.checkStringObject(userEmail) != null)
//            installation.put("email", userEmail);
//
//        if (AppLibrary.checkStringObject(userName) != null)
//            installation.put("fullName", userName);
//
//        if (AppLibrary.checkStringObject(deviceName) != null)
//            installation.put("deviceModel", deviceName);
//
//        if (AppLibrary.checkStringObject(appVersion) != null)
//            installation.put("app_version", appVersion);
//
//        if (AppLibrary.checkStringObject(deviceId) != null)
//            installation.put("deviceId", deviceId);
//
//        installation.saveInBackground();
//    }
//
//    public static Object getParseObject(String key) {
//        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
//        return installation.get(key);
//    }
//
//    public static void saveParseObject(String key, Object value) {
//        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
//        installation.put(key, value);
//        installation.saveInBackground();
//    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static void buildEventBus() {
        EventBusBuilder builder = EventBus.builder();
        builder.eventInheritance(true);
        builder.logNoSubscriberMessages(true);
        builder.logSubscriberExceptions(true);
        builder.sendNoSubscriberEvent(true);
        builder.sendSubscriberExceptionEvent(true);
        builder.throwSubscriberException(true);

        mEventBus = builder.build();
    }

}
