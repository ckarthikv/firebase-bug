package com.instalively.android.chatsetup;

/**
 * Created by Karthik on 5/19/16.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.ArrayMap;
import android.util.TypedValue;

//import com.firebase.client.DataSnapshot;
//import com.firebase.client.Firebase;
//import com.firebase.client.FirebaseError;
//import com.firebase.client.ValueEventListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FirebaseMessageService extends FirebaseMessagingService implements FireBaseKEYIDS {

    private static final String TAG = "MyFirebaseMsgService";
    private Context context;
    private SharedPreferences prefs;
    private String userId;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        AppLibrary.log_d(TAG,"onMessageReceived called -"+remoteMessage.getData().toString());
        context = getApplicationContext();
        prefs = context.getSharedPreferences(AppLibrary.APP_SETTINGS, 0);
        userId = prefs.getString(AppLibrary.USER_LOGIN, "");

        String roomId,messageId = null;
        boolean isAppKilled = false;
        final Bundle bundle = new Bundle();

        if (AppLibrary.checkStringObject(remoteMessage.getData().toString()) == null) return;
        if (userId.isEmpty()) return;

        ArrayMap<String,String> jsonData = (ArrayMap<String, String>) remoteMessage.getData();

//        Firebase.setAndroidContext(this);
//        if (FirebaseApp.getDefaultConfig() == null) {
//            Firebase.getDefaultConfig().setPersistenceEnabled(true);
//            isAppKilled = true; //default config is null when app is killed
//        } else {
//            isAppKilled = false;
//        }
        if (!FirebaseApp.getApps(context).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }

        try {
            //Compulsory fields
            bundle.putString("title", jsonData.get("title"));
            bundle.putString("body",jsonData.get("body"));
            bundle.putString("senderName",jsonData.get("senderName"));
            bundle.putInt("type" ,Integer.parseInt(jsonData.get("type")));
            bundle.putString("image",jsonData.get("image"));
            bundle.putString("senderId",jsonData.get("senderId"));

            //Optional fields
            bundle.putString("roomId",jsonData.get("roomId"));
            bundle.putString("groupName",jsonData.get("groupName"));
            bundle.putString("mediaId",jsonData.get("mediaId"));
            bundle.putString("mediaUrl",jsonData.get("mediaUrl"));
            bundle.putString("messageId",jsonData.get("messageId"));
            bundle.putBoolean("isAppKilled",isAppKilled);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        roomId = bundle.getString("roomId");
        messageId = bundle.getString("messageId");

        if (AppLibrary.checkStringObject(roomId) != null && AppLibrary.checkStringObject(messageId) != null) { //Its a new chat/media message, check first if already seen

            DatabaseReference firebase = FireBaseHelper.getInstance(this).getNewFireBase("rooms", new String[]{roomId, "messages", messageId, "viewers"});
            firebase.child(userId).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null)
                        return; //Don't do anything if message already seen

                    sendNotification(bundle);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            sendNotification(bundle);
        }

    }

    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     */

    private void sendNotification(Bundle bundle) {
        final String title,body, senderName,groupName, roomId,senderId,from = null;
        int type = 0;
        boolean isAppKilled;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        Notification notification;
        Bitmap picture = null;
        SliderMessageModel model;

        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                64,
                context.getResources().getDisplayMetrics()
        );

        title = bundle.getString("title");
        body = bundle.getString("body");
        senderName = bundle.getString("senderName");
        type = bundle.getInt("type");
        String image = bundle.getString("image"); //imageUrl
        senderId = bundle.getString("senderId");

        //Optional fields
        roomId = bundle.getString("roomId");
        groupName = bundle.getString("groupName");
        final String mediaId = bundle.getString("mediaId");
        final String mediaUrl = bundle.getString("mediaUrl");
        isAppKilled = bundle.getBoolean("isAppKilled");

        if (AppLibrary.checkStringObject(image)!=null) { //Downloading the picture for notification first
            try {
                if (android.os.Build.VERSION.SDK_INT > 20) {
                    if (AppLibrary.checkStringObject(image) != null) {
                        if (image.contains("https://"))
                            image = image.replace("https://", "http://"); //Avoid https SSL handshake error in Notifications
                        picture = Picasso.with(context).load(image).resize(px, px).transform(new RoundedTransformation()).get();
                    } else {
                        picture = Picasso.with(context).load("http://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50").resize(px, px).transform(new RoundedTransformation()).get();
                    }
                } else {
                    if (AppLibrary.checkStringObject(image) != null) {
                        if (image.contains("https://"))
                            image = image.replace("https://", "http://"); //Avoid https SSL handshake error in Notifications
                        picture = Picasso.with(context).load(image).resize(px, px).get();

                    } else {
                        picture = Picasso.with(context).load("http://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50").resize(px, px).get();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                picture = null; //ToDo: Put Default Picture
            }
        }

        //Start showing notifications parallely while updating firebase if any
        switch (type) {
            case 1: //Friend request sent
                Intent intent1 = new Intent(this, CameraActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent1.putExtra("action","friendRequestReceived");
                PendingIntent pendingIntent1 = PendingIntent.getActivity(this, 0 /* Request code */, intent1, PendingIntent.FLAG_ONE_SHOT);

                notification = mBuilder.setSmallIcon(R.drawable.stop_button_normal)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setTicker(title)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                            .setLargeIcon(picture)
                            .setColor(context.getResources().getColor(R.color.notification_icon_background))
                            .setContentText(body).setContentIntent(pendingIntent1).build();

                notificationManager.notify((int) System.currentTimeMillis(), notification);
                break;

            case 2: //Friends requested accepted
                Intent intent2 = new Intent(this, CameraActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent2.putExtra("action","friendRequestAccepted");
                PendingIntent pendingIntent2 = PendingIntent.getActivity(this, 0 /* Request code */, intent2, PendingIntent.FLAG_ONE_SHOT);

                notification = mBuilder.setSmallIcon(R.drawable.stop_button_normal)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setTicker(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                        .setLargeIcon(picture)
                        .setColor(context.getResources().getColor(R.color.notification_icon_background))
                        .setContentText(body).setContentIntent(pendingIntent2).build();

                notificationManager.notify((int) System.currentTimeMillis(), notification);
                break;

            case 3: //Group request sent
                Intent intent3 = new Intent(this, CameraActivity.class);
                intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent3.putExtra("action","groupRequestReceived");
                PendingIntent pendingIntent3 = PendingIntent.getActivity(this, 0 /* Request code */, intent3, PendingIntent.FLAG_ONE_SHOT);

                notification = mBuilder.setSmallIcon(R.drawable.stop_button_normal)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setTicker(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                        .setLargeIcon(picture)
                        .setColor(context.getResources().getColor(R.color.notification_icon_background))
                        .setContentText(body).setContentIntent(pendingIntent3).build();

                notificationManager.notify((int) System.currentTimeMillis(), notification);

                break;

            case 4: //New message

                if(!mediaId.isEmpty()) { //Media message

                    FireBaseHelper.getInstance(this).getNewFireBase("rooms", new String[]{roomId}).keepSynced(true); //Sync the room instantly using this internet connection

                    //Start downloading the media
                    final DatabaseReference firebase = FireBaseHelper.getInstance(this).getNewFireBase("localData", new String[]{userId, "mediaDownload"});
                    firebase.child(mediaId).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                        @Override
                        public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) { //Media already exists
                                int status = (int)dataSnapshot.child("status").getValue();
                                if (status != 2) { //Update downloading status and start download
                                    dataSnapshot.child("status").getRef().setValue(1);
                                    dataSnapshot.child("roomId").getRef().setValue(roomId);

                                    downloadFile(mediaUrl, mediaId, firebase);
                                } else {
                                    //already downloaded, relax!
                                }
                            } else {
                                //Create new data entry and start download
                                Map<String, Object> hashMap = new HashMap<String, Object>();
                                hashMap.put("roomId", roomId);
                                hashMap.put("status", 1);
                                hashMap.put("url", mediaUrl);
                                dataSnapshot.getRef().setValue(hashMap);

                                downloadFile(mediaUrl, mediaId, firebase);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }

                    });

                    //Show notification
                    Intent intent4 = new Intent(this, CameraActivity.class);
                    intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    if (groupName.isEmpty())
                        model = new SliderMessageModel(senderName, image, roomId, senderId, FRIEND_ROOM, 000, null, null);
                    else
                        model = new SliderMessageModel(groupName, image, roomId, senderId,GROUP_ROOM, 000, null, null);

                    intent4.putExtra("action","mediaMessage");
                    intent4.putExtra("model",model);
                    PendingIntent pendingIntent4 = PendingIntent.getActivity(this, 0 /* Request code */, intent4, PendingIntent.FLAG_ONE_SHOT);

                    notification = mBuilder.setSmallIcon(R.drawable.stop_button_normal)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setTicker(title)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                            .setLargeIcon(picture)
                            .setColor(context.getResources().getColor(R.color.notification_icon_background))
                            .setContentText(body).setContentIntent(pendingIntent4).build();

                    notificationManager.notify((int) System.currentTimeMillis(), notification);

                } else { //Normal chat message

                    FireBaseHelper.getInstance(this).getNewFireBase("rooms", new String[]{roomId}).keepSynced(true); //Sync the room instantly using this internet connection

                    Intent intent4 = new Intent(this, CameraActivity.class);
                    intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (groupName.isEmpty())
                        model = new SliderMessageModel(senderName, image, roomId, senderId, FRIEND_ROOM, 000, null, null);
                    else
                        model = new SliderMessageModel(groupName, image, roomId, senderId,GROUP_ROOM, 000, null, null);

                    intent4.putExtra("action","chatMessage");
                    intent4.putExtra("model",model);
                    PendingIntent pendingIntent4 = PendingIntent.getActivity(this, 0 /* Request code */, intent4, PendingIntent.FLAG_ONE_SHOT);

                    notification = mBuilder.setSmallIcon(R.drawable.stop_button_normal)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setTicker(title)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                            .setLargeIcon(picture)
                            .setColor(context.getResources().getColor(R.color.notification_icon_background))
                            .setContentText(body).setContentIntent(pendingIntent4).build();

                    notificationManager.notify((int) System.currentTimeMillis(), notification);
                }
                break;
        }

    }

    private void downloadFile(final String mediaUrl, final String mediaId, final DatabaseReference firebase) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        final PowerManager.WakeLock mWakeLock; mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
        mWakeLock.acquire(); //Acquire wakelock to prevent CPU from dozing off

        ThinDownloadManager downloadManager = new ThinDownloadManager();

        String filename = null;
        if(mediaUrl.endsWith(".jpg"))
                filename = mediaId + ".jpg";
        else if (mediaUrl.endsWith(".webp"))
                filename = mediaId + ".webp";
        else if (mediaUrl.endsWith(".mp4"))
                filename = mediaId + ".mp4";

        if(filename == null) return;

        String mediaStoragePath = AppLibrary.getFilesDirectory(context);
        File mediaDir = new File(mediaStoragePath);
        if (!mediaDir.exists())
            mediaDir.mkdirs();

        mediaStoragePath = mediaStoragePath + filename;

        DownloadStatusListenerV1 downloadListener = new DownloadStatusListenerV1() {
            @Override
            public void onDownloadComplete(DownloadRequest downloadRequest) {

                firebase.child(mediaId).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            dataSnapshot.child("status").getRef().setValue(2);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });
                mWakeLock.release();
            }

            @Override
            public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {

                firebase.child(mediaId).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            dataSnapshot.child("status").getRef().setValue(0);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });
                mWakeLock.release();
            }

            @Override
            public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {

            }

        };

        DownloadRequest req = new DownloadRequest(Uri.parse(mediaUrl))
                .setDownloadListener((DownloadStatusListener) downloadListener)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(Uri.parse(mediaStoragePath))
                .setPriority(DownloadRequest.Priority.HIGH);

        long mDownloadedFileID = downloadManager.add(req);

//        // Function is called once download completes.
//        BroadcastReceiver onComplete = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                // Prevents the occasional unintentional call. I needed this.
//                if (mDownloadedFileID == -1)
//                    return;
//                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
//
//                // Grabs the Uri for the file that was downloaded.
//                Uri mostRecentDownload =
//                        mDownloadManager.getUriForDownloadedFile(mDownloadedFileID);
//                // DownloadManager stores the Mime Type. Makes it really easy for us.
//                String mimeType =
//                        mDownloadManager.getMimeTypeForDownloadedFile(mDownloadedFileID);
//
//                //update on firebase
//
//
//                // Sets up the prevention of an unintentional call. I found it necessary. Maybe not for others.
//                mDownloadedFileID = -1;
//            }
//        };
//        // Registers function to listen to the completion of the download.
//        registerReceiver(onComplete, new
//                IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

//        InputStream input = null;
//        OutputStream output = null;
//        HttpURLConnection connection = null;
//        boolean successfulDownload = false;
//
//        try {
//            URL url = new URL(mediaUrl);
//            connection = (HttpURLConnection) url.openConnection();
//            connection.connect();
//
//            // expect HTTP 200 OK, so we don't mistakenly save error report
//            // instead of the file
//            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                return; //Don't download, some error
//            }
//
//            // this will be useful to display download percentage
//            // might be -1: server did not report the length
//            int fileLength = connection.getContentLength();
//
//            // download the file
//            input = new BufferedInputStream(connection.getInputStream());
//            output = new FileOutputStream("/sdcard/file_name.extension");
//
//            byte data[] = new byte[4096];
//            long total = 0;
//            int count;
//            while ((count = input.read(data)) != -1) {
//                total += count;
//                // publishing the progress....
//                if (fileLength > 0) { // only if total length is known
//                    //If we need to keep track of progress in the future
////                    publishProgress((int) (total * 100 / fileLength));
//                }
//                output.write(data, 0, count);
//            }
//            successfulDownload = true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            successfulDownload = false;
//        } finally {
//            try {
//                if (output != null) {
//                    output.flush();
//                    output.close();
//                }
//                if (input != null) {
//                    input.close();
//                }
//            } catch (IOException ignored) {
//            }
//
//            if (connection != null)
//                connection.disconnect();
//
//            mWakeLock.release();
//        }

//        if(successfulDownload) {
//            //Update firebase
//        }
    }

}