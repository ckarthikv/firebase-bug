package com.instalively.android.models;

import java.util.HashMap;

/**
 * Created by user on 5/4/2016.
 */
public class SettingsModel {

    public HashMap<String,CustomFriendListDetails> customFriendList;
    public HashMap<String,MemberDetails> ignoredList;

    public SettingsModel(){}

    public SettingsModel(HashMap<String,CustomFriendListDetails> customFriendList,HashMap<String,MemberDetails> ignoredList){
        this.customFriendList = customFriendList;
        this.ignoredList = ignoredList;
    }

    public static class CustomFriendListDetails{
        public String name;
        public HashMap<String,MemberDetails> members;

        public CustomFriendListDetails(){}

        public CustomFriendListDetails(String name,HashMap<String,MemberDetails> members){
            this.name = name;
            this.members = members;
        }
    }

    public static class MemberDetails{
        public String roomId;
        public String name;

        public MemberDetails(){}

        public MemberDetails(String roomId,String name){
            this.roomId = roomId;
            this.name = name;
        }
    }
}
