package com.instalively.android.models;

/**
 * Created by deepankur on 6/1/16.
 */
public class StickerCategoryModel {
    public String categoryId; //note: fixme do not use this; it is here just because its in server
    public String imageUrl;
    public boolean isActive;
    public String title;
    public int type;
    public boolean imagePresentInAssets;
    public String localUri;//for image

    public StickerCategoryModel() {
    }
}
