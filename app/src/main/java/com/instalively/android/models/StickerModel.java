package com.instalively.android.models;

/**
 * Created by deepankur on 6/2/16.
 */
public class StickerModel {

    public String stickerId;//for Internal reference
    public int degree;
    public float height;
    public float marginLeft;
    public float marginTop;
    public float width;
    public String categoryId;
    public String name;
    public String pictureUrl;
    public int status;
    public int type;
    //local uri will represent asset in case the image was shipped with app and internal directory otherwise
    public String localUri;//for internal reference only
    public boolean stickerPresentInAssets;//for internal reference

    public StickerModel() {
    }
}
