package com.instalively.android.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Karthik on 3/19/16.
 */
public class UserModel {


    public String KEY_UID;

    public String name;
    public String gender;
    public String handle;
    public String facebookId;
    public String verifiedPhone;
    public String momentId;
    public int favourited;
    public long createdAt;
    public long modifiedAt;
    public String imageUrl;
    public List<Object> childUser;
    public int accountType;
    public boolean isVerified;
    public String accountCategory;
    public HashMap<String, Rooms> rooms;
    public List<String> blockedList;
    public HashMap<String, Integer> blocked;

    public HashMap<String, Rooms> messageRooms;
    public HashMap<String, Rooms> momentRooms;

    public UserModel() {
    }

//    /**
//     * Only default constructor required.
//     * <p/>
//     * Setters are not needed in this data structure as user rooms need not be creates
//     * on the basis of user object
//     */
//
//    public ArrayList<String> getRoomsList() {
//        ArrayList<String> roomIds = new ArrayList();
//        for (Map.Entry<String, Rooms> hash : rooms.entrySet()) {
//            roomIds.add(hash.getKey());
//        }
//        return roomIds;
//    }

    //For person
    public void createAccount(int type, boolean verified) {
        this.accountType = type;
        this.isVerified = verified;
        this.accountCategory = null;
    }

    //For page
    public void createAccount(int type, boolean verified, String category) {
        this.accountType = type;
        this.isVerified = verified;
        this.accountCategory = category;
    }

    public static class Rooms {
        public String roomId;
        public long updatedAt;
        public int type;
        public boolean interacted;
        public boolean deletionChecked;

        public Rooms() {
        }

        public Rooms(long updatedAt, int type) {
            this.updatedAt = updatedAt;
            this.type = type;
        }


        /**
         * new Constructor
         */
        public Rooms(int type, long updatedAt, boolean interacted) {
            this.updatedAt = updatedAt;
            this.type = type;
            this.interacted = interacted;
        }

        public Rooms(long updatedAt, boolean interacted, boolean deletionChecked) {
            this.updatedAt = updatedAt;
            this.interacted = interacted;
            this.deletionChecked = deletionChecked;
        }
    }
}



