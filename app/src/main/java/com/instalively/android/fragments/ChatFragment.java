package com.instalively.android.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.adapters.ChatAdapter;
import com.instalively.android.adapters.RecyclerViewClickInterface;
import com.instalively.android.customViews.CustomFrameLayout;
import com.instalively.android.customViews.LetterTileRoundedTransformation;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.modelView.ChatMediaModel;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.models.MediaModel;
import com.instalively.android.models.RoomsModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.OnSwipeTouchListener;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by deepankur on 14/4/16.
 */
public class ChatFragment extends BaseFragment implements ChatAdapter.ChatViewController,
        FireBaseHelper.onUploadStatusChanges {

    private final String TAG = this.getClass().getSimpleName();
    private RecyclerView mRecyclerView;
    private ImageView chatImageIv;
    private TextView chatNameTv;
    private String roomId;
    private EditText messageEditText;
    private ChatAdapter chatAdapter;
    private RelativeLayout chatRl;
    private ImageView tintLayout;
    private String displayName, displayImageUrl;
    private int roomType;
    @Nullable //in  case of group room friend id would be null
    private String friendId;
    private LinkedHashMap<String, ChatMediaModel> downloadedChatMediaMap = new LinkedHashMap<>();
    private ViewControlsCallback viewControlsCallback;

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGesture();
        mFireBaseHelper.setOnUploadStatusChangesCallback(this);
        SliderMessageModel sliderMessageModel = CameraActivity.currentRoomBeingOpened;
        this.isInDeletionMode = false;
        roomId = sliderMessageModel.roomId;
        roomType = sliderMessageModel.roomType;
        displayName = sliderMessageModel.displayName;
        displayImageUrl = sliderMessageModel.imageUrl;
        if (roomId == null) throw new RuntimeException("Cannot open this Fragment without room Id");
        friendId = sliderMessageModel.friendId;
        chatAdapter = new ChatAdapter(this, isInDeletionMode, getActivity(), null, recyclerViewClickInterface, downloadedChatMediaMap);//initially, no messages in the room
        chatAdapter.setChatBoxDisplayName(displayName);
        chatFireBase = mFireBaseHelper.getFireBaseReference(ANCHOR_ROOMS, new String[]{ roomId, MESSAGES });
        chatFireBase.addChildEventListener(chatListener);
        Log.d(TAG, " opened chat room " + sliderMessageModel.toString());
        mFireBaseHelper.setChatMediaStatusChangeListener(new FireBaseHelper.ChatMediaStatusChangeListener() {
            @Override
            public void onMediaChanged() {
                if (chatAdapter != null)
                    chatAdapter.notifyDataSetChanged();
            }

            @Override
            public void deleteMediaMessage(String messageId) {
                if (messageLinkedMap.containsKey(messageId)){
                    messageLinkedMap.remove(messageId);
                    mFireBaseHelper.removeViewOnceMessage(roomId,messageId);
                }
                if (chatAdapter != null)
                    chatAdapter.notifyDataSetChanged();
            }
        });
        viewControlsCallback.onRoomOpen(roomId);
    }

    CustomFrameLayout customFrameLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        final View rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        customFrameLayout = (CustomFrameLayout) rootView.findViewById(R.id.customFrame);
        initActionBar(rootView.findViewById(R.id.action_bar));
        initViewObjects(rootView);
        mFireBaseHelper.roomOpen(myUserId, roomId, roomType);
        messageEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (mRecyclerView != null && messageLinkedMap != null)
                    mRecyclerView.scrollToPosition(messageLinkedMap.size() - 1);
            }
        });
        final ImageView sendMessageIv = (ImageView) rootView.findViewById(R.id.send_messageIV);
        sendMessageIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageEditText.getText().toString();
                if (messageText.trim().length() == 0) {
                    messageEditText.setText("");
                    return;
                }

                RoomsModel.Messages message = new RoomsModel.Messages(myUserId, null, MESSAGE_TYPE_TEXT,
                        messageEditText.getText().toString(), System.currentTimeMillis(), null, 2, null);
                if (roomType == FRIEND_ROOM)
                    mFireBaseHelper.sendMessageToFriend(myUserId, friendId, roomId, message, NEW_TEXT, null, null);
                if (roomType == GROUP_ROOM)
                    mFireBaseHelper.sendMessageToGroup(myUserId, roomId, message, NEW_TEXT, null, null);

                messageEditText.setText("");
            }
        });
        initRecyclerView(rootView);


        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            //not changing drawable everyTime ; only when editText length toggles bwn 0 and 1
            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, " changing text , new size " + s.length());

                boolean isEmpty = s.length() < 1;
                if (isEmpty != isEditTextEmpty)
                    sendMessageIv.setImageResource(isEmpty ?
                            android.R.drawable.ic_menu_gallery : android.R.drawable.btn_plus);
                isEditTextEmpty = isEmpty;
            }
        });
        customFrameLayout.setInterceptTouchListener(new CustomFrameLayout.InterceptTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(MotionEvent event) {
                Log.d(TAG, " customFrameLayout intercept " + event);
                swipeDetectorView.dispatchTouchEvent(event);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //reset everything
                        currentScrollDirection = UNDETERMINED;
                        notifyDeletionAllowed();
                        numberOfMoves = 0;
                        swipeDetected = false;
                        firstDownPoints[0] = event.getRawX();
                        firstDownPoints[1] = event.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (currentScrollDirection == UNDETERMINED)
                            determineDirection(event);
                        if (currentScrollDirection == HORIZONTAL)
                            translate(event);
                        break;
                    case MotionEvent.ACTION_UP:
                        currentScrollDirection=UNDETERMINED;
                        notifyDeletionAllowed();
                        Log.d(TAG, "current direction " + currentScrollDirection + "swipeDetected " + swipeDetected);
                        if (!swipeDetected)
                            settleDownTheView();
                        break;
                }
                return false;
            }
        });

        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return currentScrollDirection == HORIZONTAL;
            }
        });
        customFrameLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //currentScrollDirection = Horizontal now
                Log.d(TAG, " customFrameLayout onTouch " + event);
                return true;
            }
        });
        return rootView;
    }

    float[] firstDownPoints = new float[2];//0 for x; 1 for y


    public void determineDirection(MotionEvent event) {
        Log.d(TAG, " determineDirection newX " + event.getRawX() + " newY " + event.getRawY());
        numberOfMoves++;
        if (currentScrollDirection == UNDETERMINED && numberOfMoves >= 3) {
            float deltaX = Math.abs(firstDownPoints[0] - event.getRawX());
            float deltaY = Math.abs(firstDownPoints[1] - event.getRawY());
            currentScrollDirection = deltaX > deltaY ? HORIZONTAL : VERTICAL;
            if (deltaX > deltaY) {
                currentScrollDirection = HORIZONTAL;
                deltaFirstTouch = event.getX();
                toggleSoftKeyboard(context, messageEditText, false);
                messageEditText.clearFocus();
                Log.d(TAG, " assigning HORIZONTAL direction ");
                notifyDeletionAllowed();
            } else if (deltaX < deltaY) {
                Log.d(TAG, " assigning VERTICAL direction ");
                currentScrollDirection = VERTICAL;
                notifyDeletionAllowed();
            } else {//deltaX equals deltaY
                Log.d(TAG, "unable to assign direction ");
                currentScrollDirection = UNDETERMINED;
            }
        }
    }

    private boolean isEditTextEmpty = true;
    private boolean swipeDetected;

    private void initViewObjects(View rootView) {
        tintLayout = (ImageView) rootView.findViewById(R.id.tintFrame);
        chatRl = (RelativeLayout) rootView.findViewById(R.id.chatRL);

        messageEditText = (EditText) rootView.findViewById(R.id.messageEditText);
        chatNameTv.setText(displayName);
        if (this.roomType == FRIEND_ROOM)
            Picasso.with(getActivity()).load(displayImageUrl).
                    transform(new RoundedTransformation()).into(chatImageIv);
        else {
            Transformation t = new LetterTileRoundedTransformation(context, this.displayName);
            Picasso.with(context).load(R.drawable.transparent_image).
                    transform(t).into(chatImageIv);
        }
    }

    private ImageView backIv, deleteIv;

    void initActionBar(View actionBar) {
        actionBar.findViewById(R.id.action_bar_IV_4).setVisibility(View.GONE);
        deleteIv = (ImageView) actionBar.findViewById(R.id.action_bar_IV_3);
        ((RelativeLayout.LayoutParams) deleteIv.getLayoutParams()).addRule(RelativeLayout.ALIGN_PARENT_END);
        deleteIv.setVisibility(View.GONE);
        deleteIv.setImageResource(R.drawable.delete_svg);
        deleteIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final HashMap<String, Boolean> deletionListMap = chatAdapter.getDeletionListMap();
                for (Map.Entry<String, Boolean> entry : deletionListMap.entrySet())
                    if (entry.getValue())
                        mFireBaseHelper.getFireBaseReference(ANCHOR_ROOMS, new String[]{roomId, MESSAGES, entry.getKey()}).setValue(null);
                toggleDeletionMode();

            }
        });

        chatImageIv = (ImageView) actionBar.findViewById(R.id.action_bar_IV_2);
        int diameter = (int) getActivity().getResources().getDimension(R.dimen.profile_pic_diameter_chat);
        Log.d(TAG, " hard setting profile pic dimens to " + diameter);
        chatImageIv.getLayoutParams().height = diameter;
        chatImageIv.getLayoutParams().width = diameter;
        backIv = (ImageView) actionBar.findViewById(R.id.action_bar_IV_1);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isInDeletionMode)
                    popFragmentFromBackStack();
                else toggleDeletionMode();
            }
        });
        backIv.setImageResource(R.drawable.back_svg);

        chatNameTv = ((TextView) actionBar.findViewById(R.id.titleTV));
        culpritView = actionBar.findViewById(R.id.status_bar_background);
        culpritView.getLayoutParams().height = 0 /** AppLibrary.getStatusBarHeight(getActivity())*/;
//        culpritView.requestLayout();

    }

    /**
     * @return true if want on consue backPress ;
     * false otherWise
     */
    public boolean onBackPressed() {
        if (isInDeletionMode) {
            toggleDeletionMode();
            Log.d(TAG, " exiting deletion mode on backPress");
            return true;
        }
        return false;
    }

    View culpritView;
    int numberOfMessagesUnderDeletion;
    private RecyclerViewClickInterface recyclerViewClickInterface = new RecyclerViewClickInterface() {
        @Override
        public void onItemClick(int extras, Object data) {
            numberOfMessagesUnderDeletion = extras;
            if (numberOfMessagesUnderDeletion > 0 && !isInDeletionMode)
                toggleDeletionMode();
            if (numberOfMessagesUnderDeletion == 0 && isInDeletionMode)
                toggleDeletionMode();
        }
    };

    private Boolean isInDeletionMode;

    private void toggleDeletionMode() {
        isInDeletionMode = !isInDeletionMode;
//        chatImageIv.setVisibility(isInDeletionMode ? View.GONE : View.VISIBLE);
        deleteIv.setVisibility(isInDeletionMode ? View.VISIBLE : View.GONE);
        chatNameTv.setVisibility(isInDeletionMode ? View.GONE : View.VISIBLE);
        if (!isInDeletionMode)
            chatAdapter.clearDeletionList();

    }

    private void initRecyclerView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.chat_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ViewTreeObserver viewTreeObserver = mRecyclerView.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    displayWidth = mRecyclerView.getMeasuredWidth();

                }
            });
        }
        mRecyclerView.setAdapter(chatAdapter);
        mRecyclerView.addOnScrollListener(scrollListener);
    }


    private float deltaFirstTouch;
    private int numberOfMoves;

    /**
     * tell the chat adapter whether deletion can be done or not .
     */
   private void notifyDeletionAllowed() {
//       Log.d(TAG, buildStackTraceString(Thread.currentThread().getStackTrace()));
        if (chatAdapter != null) {
            chatAdapter.setDeletionAllowed(this.currentScrollDirection != HORIZONTAL);
        }
    }

    private final int HORIZONTAL = 1111, VERTICAL = 2222, UNDETERMINED = 0;//0 is the default
    private int currentScrollDirection = UNDETERMINED;

    private void translate(MotionEvent event) {
//        Log.d(TAG, " translate: " + chatRl.getX());
        float displacement = event.getRawX() - deltaFirstTouch;
        if (displacement > 0)
            chatRl.setTranslationX(displacement);
        else
            chatRl.setTranslationX(0);

        resetAlpha(displacement);
    }

    private void resetAlpha(float displacement) {
        float currentFactor = (1 - (displacement / displayWidth));

        if (currentFactor >= 0.99f)
            tintLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        else if (currentFactor == 0f)
            tintLayout.setLayerType(View.LAYER_TYPE_NONE, null);

        tintLayout.setAlpha((currentFactor));
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            //Log.d(TAG, " recycler Scroll " + dx + " " + dy);
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback) context;
    }

    private ObjectAnimator dismissAnimator, settleAnimator;
    int displayWidth;
    final int MAXIMUM_FRAGMENT_DISMISS_DURATION = 150;
    boolean inAnimation;

    private void settleDownTheView() {
        Log.d(TAG, "settling down the view");
//        if (settleAnimator == null) {
        settleAnimator = ObjectAnimator.ofFloat(chatRl, "translationX", 0);
        settleAnimator.removeAllListeners();
        settleAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                inAnimation = true;
                chatRl.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                tintLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                inAnimation = false;
                chatRl.setLayerType(View.LAYER_TYPE_NONE, null);
                tintLayout.setLayerType(View.LAYER_TYPE_NONE, null);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                inAnimation = false;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
//        }

//        float effectiveDuration = (chatRl.getTranslationX() / displayWidth) * MAXIMUM_FRAGMENT_DISMISS_DURATION;
        settleAnimator.setDuration((long) MAXIMUM_FRAGMENT_DISMISS_DURATION);
        settleAnimator.setFloatValues(chatRl.getTranslationX(), 0);
        settleAnimator.start();
    }

    private void dismissChatFragment() {
//        if (dismissAnimator == null) {
        dismissAnimator = ObjectAnimator.ofFloat(chatRl, "translationX", /*chatRl.getTranslationX(),*/ displayWidth);
        dismissAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                chatRl.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                tintLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);

                inAnimation = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                chatRl.setLayerType(View.LAYER_TYPE_NONE, null);
                tintLayout.setLayerType(View.LAYER_TYPE_NONE, null);

//                getActivity().onBackPressed();
                popFragmentFromBackStack();
                inAnimation = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                inAnimation = false;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
//        }
//        float dismissEndPoint = currentDisplacement > 0 ? displayWidth : -displayWidth;
//        dismissAnimator.setFloatValues(currentDisplacement < 0 ? chatRl.getTranslationX() : dismissEndPoint);
//
//        float effectiveDuration = (chatRl.getTranslationX() / displayWidth) * MAXIMUM_FRAGMENT_DISMISS_DURATION;
//        dismissAnimator.setDuration((long) effectiveDuration);
        dismissAnimator.setDuration(MAXIMUM_FRAGMENT_DISMISS_DURATION);
        dismissAnimator.setFloatValues(displayWidth);
        dismissAnimator.start();

        ObjectAnimator.ofFloat(tintLayout, "alpha", 0)
                .setDuration(MAXIMUM_FRAGMENT_DISMISS_DURATION)
                .start();
    }

    @Override
    public void onDestroyView() {
        context.setTheme(R.style.AppTheme);
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        chatFireBase.removeEventListener(chatListener);
        CameraActivity.currentRoomBeingOpened = null;//manually nullify static objects
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    private LinkedHashMap<String, RoomsModel.Messages> messageLinkedMap;
    DatabaseReference chatFireBase;

    private ChildEventListener chatListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if (messageLinkedMap == null)
                messageLinkedMap = new LinkedHashMap<>();
            if (dataSnapshot == null || dataSnapshot.getValue() == null) return;
            RoomsModel.Messages message = dataSnapshot.getValue(RoomsModel.Messages.class);
            String messageKey = dataSnapshot.getKey();

            message.messageId = messageKey;
            if (message.expiryType == REMOVED_UPON_ROOM_OPEN) {
                mFireBaseHelper.removeViewOnceMessage(roomId, message.messageId);
            } else {
                if (message.type == MESSAGE_TYPE_MEDIA && message.expiryType != VIEW_ONCE_AND_VIEWED) {
                    if (message.mediaUploadingStatus != MEDIA_UPLOADING_COMPLETE) {
                        if (message.memberId.equals(mFireBaseHelper.getMyUserId())) {
                            // retry upload option , status upload failed
                            messageLinkedMap.put(messageKey, message);
                            mFireBaseHelper.checkChatMediaDownloadStatus(message);
                        }
                    } else {
                        // uploaded but not downloaded
                        messageLinkedMap.put(messageKey, message);
                        mFireBaseHelper.checkChatMediaDownloadStatus(message);
                    }
                } else {
                    messageLinkedMap.put(messageKey, message);
                }
//                checkAndValidatePendingMessagesStatus(message);
                if (chatAdapter.getMessagesLinkedHashMap() == null)
                    chatAdapter.setMessagesLinkedHashMap(messageLinkedMap);
                chatAdapter.notifyDataSetChanged();
                chatAdapter.notifyItemInserted(messageLinkedMap.size() - 1);
                mRecyclerView.scrollToPosition(messageLinkedMap.size() - 1);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            RoomsModel.Messages message = dataSnapshot.getValue(RoomsModel.Messages.class);
            if (message.expiryType == VIEW_ONCE_AND_VIEWED){
                message.messageId = dataSnapshot.getKey();
                messageLinkedMap.put(dataSnapshot.getKey(), message);
                chatAdapter.setMessagesLinkedHashMap(messageLinkedMap);
                chatAdapter.notifyDataSetChanged();
                chatAdapter.notifyItemInserted(messageLinkedMap.size() - 1);
                mRecyclerView.scrollToPosition(messageLinkedMap.size() - 1);
            } else {
                if ((message.type == MESSAGE_TYPE_MEDIA) && message.mediaUploadingStatus == MEDIA_UPLOADING_COMPLETE && !message.memberId.equals(mFireBaseHelper.getMyUserId())) {
                    message.messageId = dataSnapshot.getKey();
                    mFireBaseHelper.checkChatMediaDownloadStatus(message);
                    messageLinkedMap.put(dataSnapshot.getKey(), message);
                    chatAdapter.setMessagesLinkedHashMap(messageLinkedMap);
                    chatAdapter.notifyDataSetChanged();
                    chatAdapter.notifyItemInserted(messageLinkedMap.size() - 1);
                    mRecyclerView.scrollToPosition(messageLinkedMap.size() - 1);
                } /*else if ((message.type == MESSAGE_TYPE_MEDIA) && message.memberId.equals(mFireBaseHelper.getMyUserId())) {
                    if (messageLinkedMap.containsKey(dataSnapshot.getKey())) {
                        messageLinkedMap.get(dataSnapshot.getKey()).status = message.status;
                        mFireBaseHelper.checkChatMediaDownloadStatus(message);
                        chatAdapter.setMessagesLinkedHashMap(messageLinkedMap);
                        chatAdapter.notifyDataSetChanged();
                        chatAdapter.notifyItemInserted(messageLinkedMap.size() - 1);
                        mRecyclerView.scrollToPosition(messageLinkedMap.size() - 1);
                    }
                }*/
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            messageLinkedMap.remove(dataSnapshot.getKey());
            chatAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

//    public void checkAndValidatePendingMessagesStatus(RoomsModel.Messages message){
//        String mediaId = message.mediaId;
//        if (pendingMediaInMessageRoom != null) {
//            for (int i = 0; i < pendingMediaInMessageRoom.size(); i++) {
//                if (pendingMediaInMessageRoom.get(i).mediaId.equals(mediaId)) {
//                    messageLinkedMap.get(message.messageId).mediaUploadingStatus = MEDIA_UPLOADING_FAILED;
//                }
//            }
//        }
//    }

    @Override
    public void onMediaClicked(RoomsModel.Messages messages) {
        viewControlsCallback.onLoadViewChatMediaFragment(roomId,roomType ,messages.mediaId);
    }

    public LinkedHashMap<String, ChatMediaModel> getDownloadedChatMediaMap(String mediaId) {
        LinkedHashMap<String, ChatMediaModel> localDownloadedMediaMap = new LinkedHashMap<>();
        if (downloadedChatMediaMap.size() > 0 && downloadedChatMediaMap.containsKey(mediaId)) {
            int counter = 0;
            int foundIndex = -1;
            for (Map.Entry<String, ChatMediaModel> entry : downloadedChatMediaMap.entrySet()) {
                if (entry.getKey().equals(mediaId)) {
                    localDownloadedMediaMap.put(entry.getKey(), entry.getValue());
                    foundIndex = counter;
                } else if (foundIndex != -1) {
                    localDownloadedMediaMap.put(entry.getKey(), entry.getValue());
                }
                counter++;
            }
            counter = 0;
            for (Map.Entry<String, ChatMediaModel> entry : downloadedChatMediaMap.entrySet()) {
                if (counter < foundIndex) {
                    localDownloadedMediaMap.put(entry.getKey(), entry.getValue());
                }
                counter++;
            }
        } else {
            LinkedHashMap<String, ChatMediaModel> chatMediaStatusMap = mFireBaseHelper.getChatMediaStatusMap();
            localDownloadedMediaMap.put(mediaId, chatMediaStatusMap.get(mediaId));
        }
        return localDownloadedMediaMap;
    }

    public void onOpenMedia(String mediaId) {
        if (downloadedChatMediaMap != null && downloadedChatMediaMap.containsKey(mediaId)) {
            ChatMediaModel model = downloadedChatMediaMap.get(mediaId);
            if (model.expiryType == VIEW_ONCE) {
                model.expiryType = VIEW_ONCE_AND_VIEWED;
                messageLinkedMap.get(model.messageId).expiryType = VIEW_ONCE_AND_VIEWED;
                chatAdapter.notifyDataSetChanged();
            }
            downloadedChatMediaMap.remove(mediaId);
        }
    }

    @Override
    public void onUploadingStatusModified(String messageId, int status) {
        if (messageLinkedMap != null && messageLinkedMap.containsKey(messageId)) {
            messageLinkedMap.get(messageId).mediaUploadingStatus = status;
            chatAdapter.notifyDataSetChanged();
        }
    }

    private View swipeDetectorView;

    private void initGesture() {
        swipeDetectorView = new View(context);
        swipeDetectorView.setOnTouchListener(new OnSwipeTouchListener(context) {

            public void onSwipeRight() {
                swipeDetected = true;
                Log.d(TAG, "onSwipeRight currentScrollDirection: " + currentScrollDirection);
                if (currentScrollDirection == HORIZONTAL) {
                    notifyDeletionAllowed();
                    Log.d(TAG, " on SwipeRight dismissing with current X as : " + chatRl.getX());
                    dismissChatFragment();
                }
            }
        });
    }

    public interface ViewControlsCallback {
        void onLoadViewChatMediaFragment(String roomId,int roomType,String mediaId);
        void onRoomOpen(String roomId);
    }
    void popFragmentFromBackStack(){
        if (getFragmentInBackStack(ChatFragment.class.getSimpleName()) != null) {
            boolean popped = getActivity().getSupportFragmentManager().popBackStackImmediate();
            Log.d(TAG, "popFragmentFromBackStack: popped " + popped);
        } else Log.d(TAG, "popFragmentFromBackStack: fragment is null");
    }

    /**
     * @param fragmentTag the String supplied while fragment transaction
     * @return null if not found
     */
    private Fragment getFragmentInBackStack(String fragmentTag) {
        return getActivity().getSupportFragmentManager().findFragmentByTag(fragmentTag);
    }
}
