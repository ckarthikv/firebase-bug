package com.instalively.android.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.instalively.android.R;
import com.instalively.android.adapters.FriendListAdapter;
import com.instalively.android.adapters.MomentListAdapter;
import com.instalively.android.adapters.RecentFriendListAdapter;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.modelView.ListRoomView;
import com.instalively.android.models.MomentModel;
import com.instalively.android.models.SettingsModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.SettingsModelLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by abc on 4/12/2016.
 */
public class ShareMediaFragment extends BaseFragment implements View.OnClickListener,
        FriendListAdapter.ViewControlsCallback, RecentFriendListAdapter.ViewControlsCallback, FireBaseHelper.MomentCallback
        , MomentListAdapter.ViewControlsCallback, FireBaseHelper.CustomFriendListCallBack
        , RemoveFriendsFragment.LoadDataCallBack, RemoveFriendsFragment.ViewControlsCallback
        , CustomFriendListFragment.LoadDataCallBack,
        CustomFriendListFragment.ViewControlsCallback,
        SettingsModelLayout.ViewControlsCallback, CreateManageGroupFragment.ViewControlsCallback {

    private static final String TAG = "ShareMediaFragment";

    private static final int MEDIA_TYPE_IMAGE = 0;
    private static final int MEDIA_TYPE_VIDEO = 1;

    private static final int CURRENT_ACTIVE_VIEW_NORMAL = 0;
    private static final int CURRENT_ACTIVE_VIEW_SETTING_POPUP = 1;
    private static final int CURRENT_ACTIVE_VIEW_ALL_FRIEND_LIST = 2;
    private static final int CURRENT_ACTIVE_VIEW_EXCEPT_FRIEND_LIST = 3;
    private static final int CURRENT_ACTIVE_VIEW_CUSTOM_FRIEND_LIST = 4;

    private static final int ACTION_TYPE_ALL_FRIEND = 1;
    private static final int ACTION_TYPE_EXCEPT_FRIEND = 2;
    private static final int ACTION_TYPE_CUSTOM_FRIEND = 3;

    private LinearLayout sliderIv;
    private ImageView option1, option2, option3;
    private ObjectAnimator anim;
    private FrameLayout touchDetector;
    private int sliderWidth = -1;
    private int MEDIA_TYPE = 1;
    private float endPoint, interBreakPointDistance;
    private float option2X, option3X = -1;
    private boolean mInAnimation;
    private RecyclerView recentRoomListRecyclerView, momentRecyclerView, roomListRecyclerView;
    private LinearLayout newGroupButton;
    private ViewControlsCallback viewControlsCallback;
    private List<ListRoomView> friendsList;
    private List<ListRoomView> recentFriendList,allFriendList;
    private Map<String, MomentModel> modelMap;
    private int shiftMargin;
    private int optionWidth = -1;
    private int leftMargin;
    private View rootView;
    private String mediaPath;
    private HashMap<String, Integer> selectedMomentList;
    private HashMap<String, Integer> selectedRoomList;

    private int expiryType = 1;
    private String mediaText = null;
    private int actionType, currentActiveView;

    private SettingsModel settingsModel;
    private View settingPopupView;
    private LinearLayout settingsListLayout;
    private HashMap<String, String> selectedRoomForMyMomentShare;
    private SettingsModelLayout exceptFriendLayout;
    private TextView activeMomentTextView;
    private List<ListRoomView> roomViewList;
    private List<ListRoomView> recentRoomViewList;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback) context;
    }

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mediaPath = bundle.getString("mediaPath");
        mediaText = bundle.getString(AppLibrary.MEDIA_TEXT);
        selectedRoomForMyMomentShare = new HashMap<>();
        actionType = ACTION_TYPE_ALL_FRIEND;
        allFriendList = mFireBaseHelper.getListRoomView();
        recentFriendList = new ArrayList<>();
        friendsList = new ArrayList<>();
        if (allFriendList != null) {
            int maxRecentCount = (allFriendList.size() > 5) ? 5 :allFriendList.size();
            if (maxRecentCount > 5){
                recentFriendList = allFriendList.subList(0,4);
                friendsList = allFriendList.subList(4,allFriendList.size());
            } else {
                recentFriendList = allFriendList;
            }
        }
        mFireBaseHelper.setMomentCallback(this);
        mFireBaseHelper.setCustomFriendListCallback(this);
        mFireBaseHelper.fetchMomentList();
        mFireBaseHelper.getCustomFriendList();

        leftMargin = AppLibrary.convertDpToPixels(getActivity(), 50);
        selectedMomentList = new HashMap<>();
        selectedRoomList = new HashMap<>();
        createCustomizeFriendList();
        createCustomRecentFriendList();
    }

    private void createCustomRecentFriendList() {
//        recentRoomViewList = new ArrayList<>();
        Collections.sort(recentFriendList, new Comparator<ListRoomView>() {
            @Override
            public int compare(ListRoomView lhs, ListRoomView rhs) {
                return lhs.name.compareToIgnoreCase(rhs.name);
            }
        });

//        boolean isSeparator = false;
//        int previousIndex = 0;
//        for (int i = 0 ;i < recentFriendList.size();i++){
//            isSeparator = false;
//            char[] nameArray;
//            String name = recentFriendList.get(i).name;
//            // If it is the first item then need a separator
//            if (i == 0) {
//                isSeparator = true;
//                nameArray = name.toCharArray();
//            } else {
//                char[] previousNameArray = recentFriendList.get(previousIndex).name.toCharArray();
//                nameArray = name.toCharArray();
//                if (nameArray[0] != previousNameArray[0]) {
//                    isSeparator = true;
//                }
//            }
//
//            previousIndex = i;
//
//            if (isSeparator){
//                ListRoomView roomView = new ListRoomView();
//                roomView.hasHeader = true;
//                roomView.name = String.valueOf(nameArray[0]);
//                recentRoomViewList.add(roomView);
//            }
//
//            recentRoomViewList.add(recentFriendList.get(i));
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.share_media_fragment, container, false);
        initializeViewObjects(rootView);
        return rootView;
    }

    private void createCustomizeFriendList() {
        roomViewList = new ArrayList<>();
        Collections.sort(friendsList, new Comparator<ListRoomView>() {
            @Override
            public int compare(ListRoomView lhs, ListRoomView rhs) {
//                return lhs.name.compareToIgnoreCase(rhs.name);
                return lhs.normalizedFirstChar - rhs.normalizedFirstChar;
            }
        });

//        boolean isSeparator = false;
//        int previousIndex = 0;
//        for (int i = 0 ;i < friendsList.size();i++){
//            isSeparator = false;
//            char[] nameArray;
//            String name = friendsList.get(i).name;
//            // If it is the first item then need a separator
//            if (i == 0) {
//                isSeparator = true;
//                nameArray = name.toCharArray();
//            } else {
//                char[] previousNameArray = friendsList.get(previousIndex).name.toCharArray();
//                nameArray = name.toCharArray();
//                if (nameArray[0] != previousNameArray[0]) {
//                    isSeparator = true;
//                }
//            }
//
//            previousIndex = i;
//
//            if (isSeparator){
//                ListRoomView roomView = new ListRoomView();
//                roomView.hasHeader = true;
//                roomView.name = String.valueOf(nameArray[0]);
//                roomView.setFirstChar(roomView.name);
//                roomViewList.add(roomView);
//            }
//
//            roomViewList.add(friendsList.get(i));
//        }
        int currentChar = -1111;//ascii
        for (ListRoomView roomView : friendsList) {//single pass iteration; nullify redundant objects
            if (roomView.normalizedFirstChar != currentChar) {
                roomViewList.add(null);//null indicating the header view; else normal shit
                currentChar = roomView.normalizedFirstChar;//resetting isSeparator  here
            }
            roomViewList.add(roomView);//adding always
        }

    }

    private void initializeViewObjects(View rootView) {
        settingsListLayout = (LinearLayout) rootView.findViewById(R.id.settingsListLayout);
        newGroupButton = (LinearLayout) rootView.findViewById(R.id.newGroupLayout);
        touchDetector = (FrameLayout) rootView.findViewById(R.id.touchDetector);
        option1 = (ImageView) rootView.findViewById(R.id.option1);
        option2 = (ImageView) rootView.findViewById(R.id.option2);
        option3 = (ImageView) rootView.findViewById(R.id.option3);
        sliderIv = (LinearLayout) rootView.findViewById(R.id.selectedIv);
        rootView.findViewById(R.id.rootPopupView).setOnClickListener(this);
        settingPopupView = rootView.findViewById(R.id.settings_popup);
        initSettingsList();
        roomListRecyclerView = (RecyclerView) rootView.findViewById(R.id.friendListRecyclerView);
        recentRoomListRecyclerView = (RecyclerView) rootView.findViewById(R.id.recentRecyclerView);
        momentRecyclerView = (RecyclerView) rootView.findViewById(R.id.momentRecyclerView);

        roomListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        momentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recentRoomListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (friendsList != null) {
            recentRoomListRecyclerView.setAdapter(new RecentFriendListAdapter(this, getActivity(), recentFriendList));
            roomListRecyclerView.setAdapter(new FriendListAdapter(this, getActivity(), roomViewList));
        }
        if (MEDIA_TYPE == MEDIA_TYPE_IMAGE) {
            option1.setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.option1View).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.quickPeekText).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.quickPeekDivider).setVisibility(View.VISIBLE);
        } else if (MEDIA_TYPE == MEDIA_TYPE_VIDEO) {
            option1.setVisibility(View.GONE);
            rootView.findViewById(R.id.option1View).setVisibility(View.GONE);
            rootView.findViewById(R.id.quickPeekText).setVisibility(View.GONE);
            rootView.findViewById(R.id.quickPeekDivider).setVisibility(View.GONE);
        }

        touchDetector.setOnTouchListener(touchListener);
        sliderIv.setClickable(false);
        sliderIv.setOnTouchListener(null);
        newGroupButton.setOnClickListener(this);
        rootView.findViewById(R.id.shareLayout).setOnClickListener(this);

        addViewTreeObserver(sliderIv);
        addViewTreeObserver(option2);
        addViewTreeObserver(option3);
    }

    private void initSettingsList() {
        for (int i = 0; i < 3; i++) {
            SettingsModelLayout layout = new SettingsModelLayout(getActivity());
            if (i == 0) {
                layout.initializeViewObjects(getResources().getString(R.string.all_friends_text), "allFriendsButton");
                layout.getUpdateView().setVisibility(View.GONE);
            } else if (i == 1) {
                layout.initializeViewObjects(getResources().getString(R.string.except_friends_text), "exceptFriendsButton");
                exceptFriendLayout = layout;
            } else if (i == 2)
                layout.initializeViewObjects(getResources().getString(R.string.custom_friend_list_text), "customListButton");
            layout.setViewControlsCallback(this);
            settingsListLayout.addView(layout);
        }
        settingPopupView.findViewById(R.id.doneButton).setOnClickListener(this);
    }

    private void removeCustomListFragment() {
        currentActiveView = CURRENT_ACTIVE_VIEW_SETTING_POPUP;
        Fragment fragment = getChildFragmentManager().findFragmentByTag("customListFragment");
        getChildFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
    }

    private void removeExceptFriendListFragment() {
        currentActiveView = CURRENT_ACTIVE_VIEW_SETTING_POPUP;
        Fragment fragment = getChildFragmentManager().findFragmentByTag("removeListFragment");
        getChildFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
    }

    private void showSettingPopup() {
        currentActiveView = CURRENT_ACTIVE_VIEW_SETTING_POPUP;
        rootView.findViewById(R.id.rootPopupView).setVisibility(View.VISIBLE);
        settingPopupView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideInUp).duration(500).playOn(settingPopupView);
    }

    private void updateIgnoredList() {
        selectedRoomForMyMomentShare.clear();
        if (settingsModel != null && settingsModel.ignoredList != null) {
            HashMap<String, SettingsModel.MemberDetails> details = settingsModel.ignoredList;
            boolean isValueUpdated = false;
            for (Map.Entry<String, SettingsModel.MemberDetails> entry : details.entrySet()) {
                if (activeMomentTextView != null && !isValueUpdated) {
                    activeMomentTextView.setText(getResources().getString(R.string.except_friends_text) + " " + entry.getValue().name);
                    isValueUpdated = true;
                }
                selectedRoomForMyMomentShare.put(entry.getKey(), entry.getValue().roomId);
            }
        }
    }

    private void updateSelectedRoomsList(String customListId) {
        selectedRoomForMyMomentShare.clear();
        if (settingsModel != null && settingsModel.customFriendList != null) {
            if (activeMomentTextView != null)
                activeMomentTextView.setText(settingsModel.customFriendList.get(customListId).name);
            HashMap<String, SettingsModel.MemberDetails> details = settingsModel.customFriendList.get(customListId).members;
            for (Map.Entry<String, SettingsModel.MemberDetails> entry : details.entrySet()) {
                selectedRoomForMyMomentShare.put(entry.getKey(), entry.getValue().roomId);
            }
        }
    }

    private void loadCustomFriendListFragment(String id) {
        Bundle arguments = new Bundle();
        if (id != null) {
            arguments.putString("activeListId", id);
        }
        CustomFriendListFragment fragment = new CustomFriendListFragment();
        fragment.setArguments(arguments);
        getChildFragmentManager().beginTransaction()
                .add(R.id.roomListContainer, fragment, "customListFragment")
                .commitAllowingStateLoss();
    }

    private void loadRemoveFriendsFragment() {
        getChildFragmentManager().beginTransaction()
                .add(R.id.roomListContainer, new RemoveFriendsFragment(), "removeListFragment")
                .commitAllowingStateLoss();
    }

    private void hideSettingsPopup() {
        currentActiveView = CURRENT_ACTIVE_VIEW_NORMAL;
        rootView.findViewById(R.id.rootPopupView).setVisibility(View.GONE);
        YoYo.with(Techniques.SlideOutDown).duration(500).withListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(com.nineoldandroids.animation.Animator animation) {

            }

            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                settingPopupView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(com.nineoldandroids.animation.Animator animation) {

            }

            @Override
            public void onAnimationRepeat(com.nineoldandroids.animation.Animator animation) {

            }
        }).playOn(settingPopupView);
    }

    private void addViewTreeObserver(final View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onGlobalLayout() {
                    if (view.getId() == R.id.selectedIv) {
                        sliderWidth = view.getMeasuredWidth();
                        if (optionWidth != -1) {
                            calculateShiftMarginToCenter();
                        }
                    } else if (view.getId() == R.id.option2) {
                        option2X = option2.getLeft();
                        optionWidth = option2.getMeasuredWidth();
                        if (option3X != -1) {
                            interBreakPointDistance = option3X - option2X;
                        }
                        if (sliderWidth != -1) {
                            calculateShiftMarginToCenter();
                        }
                    } else if (view.getId() == R.id.option3) {
                        option3X = option3.getLeft();
                        if (option2X != -1)
                            interBreakPointDistance = option3X - option2X;
                    }
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    private void calculateShiftMarginToCenter() {
        shiftMargin = sliderWidth / 2 - optionWidth / 2;
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) sliderIv.getLayoutParams();
        params.leftMargin = leftMargin - shiftMargin;
        sliderIv.setLayoutParams(params);
    }

    private View.OnTouchListener touchListener = new View.OnTouchListener() {

        int prevX = 0;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int touchX = (int) event.getX() - sliderWidth / 2;
            if (touchX >= 0 && touchX <= option3X) {
                AppLibrary.log_d(TAG, "Event getX is-" + event.getX() + ", Action is -" + event.getAction());
                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                    prevX = touchX;
                    startTranslation((int) event.getX());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    settlingAnimation(touchX);
                } else {
                    settlingAnimation(prevX);
                }
            }
            return true;
        }
    };

    private void settlingAnimation(int touchX) {
        if (MEDIA_TYPE == MEDIA_TYPE_IMAGE) {
            if (touchX > option1.getLeft() && touchX <= (option1.getLeft() + interBreakPointDistance / 2)) {
                endPoint = option1.getX();
                expiryType = QUICK_PEEK;
            } else if (touchX < option2.getLeft() && touchX > (option1.getLeft() + interBreakPointDistance / 2)) {
                endPoint = option2.getX();
                expiryType = VIEW_ONCE;
            } else if (touchX > option2.getLeft() && touchX <= (option2.getLeft() + interBreakPointDistance / 2)) {
                endPoint = option2.getX();
                expiryType = VIEW_ONCE;
            } else {
                endPoint = option3.getX();
                expiryType = VIEW_FOR_A_DAY;
            }
        } else {
            if (touchX > option2.getLeft() && touchX <= (option2.getLeft() + interBreakPointDistance / 2)) {
                endPoint = option2.getX();
                expiryType = VIEW_ONCE;
            } else {
                endPoint = option3.getX();
                expiryType = VIEW_FOR_A_DAY;
            }
        }
        startAnimator();
    }

    private void startTranslation(int touchX) {
        sliderIv.setTranslationX(touchX - sliderWidth / 2);
    }

    private void startAnimator() {
        if (mInAnimation) return;
        if (anim == null) {
            anim = ObjectAnimator.ofFloat(sliderIv, "translationX", endPoint);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    mInAnimation = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mInAnimation = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mInAnimation = false;
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else {
            anim.cancel();
        }
        anim.setFloatValues(endPoint);
        anim.setDuration(200);
        anim.start();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newGroupLayout:
                launchNewGroupFragment(AppLibrary.GROUP_ACTION_CREATE);
                break;
            case R.id.shareLayout:
                uploadAndShareMedia();
                break;
            case R.id.rootPopupView:
                hideSettingsPopup();
                break;
            case R.id.doneButton:
                saveSettingChanges();
                break;
        }
    }

    private void saveSettingChanges() {
        hideSettingsPopup();
    }

    private void launchNewGroupFragment(int groupAction) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppLibrary.GROUP_ACTION, groupAction);
        CreateManageGroupFragment fragment = new CreateManageGroupFragment();
        fragment.setArguments(bundle);
        getChildFragmentManager().beginTransaction()
                .add(R.id.roomListContainer, fragment, "newGroupFragment").addToBackStack(null).commitAllowingStateLoss();
    }

    private void uploadAndShareMedia() {
        rootView.findViewById(R.id.shareLayout).setVisibility(View.GONE);
        if (selectedRoomList.size() == 0) {
            expiryType = 3;
        }
        viewControlsCallback.uploadMediaToFireBase(actionType, selectedRoomForMyMomentShare, mediaPath, selectedMomentList, selectedRoomList, expiryType, mediaText);
    }

    @Override
    public void onFriendItemClicked(ListRoomView friendItem, boolean isChecked) {

        if (isChecked) {
            if (friendItem.type == FRIEND_ROOM)
                selectedRoomList.put(friendItem.roomId, FRIEND_ROOM);
            else
                selectedRoomList.put(friendItem.roomId, GROUP_ROOM);
        } else {
            selectedRoomList.remove(friendItem.roomId);
        }

        if ((selectedMomentList.size() > 0 || selectedRoomList.size() > 0))
            rootView.findViewById(R.id.shareLayout).setVisibility(View.VISIBLE);
        else
            rootView.findViewById(R.id.shareLayout).setVisibility(View.GONE);
    }

    @Override
    public void onRecentFriendItemClicked(ListRoomView friendItem, boolean isChecked) {
        if (isChecked) {
            if (friendItem.type == FRIEND_ROOM)
                selectedRoomList.put(friendItem.roomId, FRIEND_ROOM);
            else
                selectedRoomList.put(friendItem.roomId, GROUP_ROOM);
        } else {
            selectedRoomList.remove(friendItem.roomId);
        }

        if ((selectedMomentList.size() > 0 || selectedRoomList.size() > 0))
            rootView.findViewById(R.id.shareLayout).setVisibility(View.VISIBLE);
        else
            rootView.findViewById(R.id.shareLayout).setVisibility(View.GONE);
    }

    @Override
    public void onMomentListLoaded(Map<String, MomentModel> modelMap) {
        this.modelMap = modelMap;
        momentRecyclerView.setAdapter(new MomentListAdapter(this, getActivity(), this.modelMap));
    }

    @Override
    public void onMomentSelected(String momentId, int source) {
        if (!selectedMomentList.containsKey(momentId))
            selectedMomentList.put(momentId, source);
        else
            selectedMomentList.remove(momentId);

        if ((selectedMomentList.size() > 0 || selectedRoomList.size() > 0))
            rootView.findViewById(R.id.shareLayout).setVisibility(View.VISIBLE);
        else
            rootView.findViewById(R.id.shareLayout).setVisibility(View.GONE);
    }

    @Override
    public void onSettingsSelected(View view) {
        activeMomentTextView = (TextView) view;
        showSettingPopup();
    }

    @Override
    public void onCustomFriendListLoaded(SettingsModel settingsModel) {
        if (settingsModel != null) {
            this.settingsModel = settingsModel;
            HashMap<String, SettingsModel.CustomFriendListDetails> customListDetails = settingsModel.customFriendList;
            if (customListDetails != null) {
                for (Map.Entry<String, SettingsModel.CustomFriendListDetails> entry : customListDetails.entrySet()) {
                    SettingsModelLayout layout = new SettingsModelLayout(getActivity());
                    layout.initializeViewObjects(entry.getValue().name, entry.getKey());
                    layout.setViewControlsCallback(this);
                    settingsListLayout.addView(layout);
                }
            }

            // ignored list
            HashMap<String, SettingsModel.MemberDetails> ignoredMap = settingsModel.ignoredList;
            if (ignoredMap != null) {
                for (Map.Entry<String, SettingsModel.MemberDetails> entry : ignoredMap.entrySet()) {
                    exceptFriendLayout.setDescText(entry.getValue().name);
                    break;
                }
            }
        }
    }

    @Override
    public List<ListRoomView> getLoadedFriendList() {
        return friendsList;
    }

    public void updateCurrentActiveView() {
        switch (currentActiveView) {
            case CURRENT_ACTIVE_VIEW_ALL_FRIEND_LIST:
                break;
            case CURRENT_ACTIVE_VIEW_CUSTOM_FRIEND_LIST:
                removeCustomListFragment();
                break;
            case CURRENT_ACTIVE_VIEW_EXCEPT_FRIEND_LIST:
                removeExceptFriendListFragment();
                break;
            case CURRENT_ACTIVE_VIEW_NORMAL:
                break;
            case CURRENT_ACTIVE_VIEW_SETTING_POPUP:
                hideSettingsPopup();
                break;
        }
    }

    public int getCurrentActiveView() {
        return currentActiveView;
    }

    @Override
    public void onCreateShareListClicked(HashMap<String, SettingsModel.MemberDetails> selectedList) {
        mFireBaseHelper.updateIgnoredList(selectedList);
        updateShareListInSettingsLayout(selectedList);
    }

    @Override
    public SettingsModel onRemoveFriendsFragmentCreated() {
        return settingsModel;
    }

    private void updateShareListInSettingsLayout(HashMap<String, SettingsModel.MemberDetails> selectedList) {
        if (settingsModel == null)
            settingsModel = new SettingsModel();
        if (settingsModel.ignoredList == null)
            settingsModel.ignoredList = new HashMap<>();
        boolean exceptFriendTextUpdated = false;
        for (Map.Entry<String, SettingsModel.MemberDetails> entry : selectedList.entrySet()) {
            if (!exceptFriendTextUpdated) {
                exceptFriendLayout.setDescText(entry.getValue().name);
                for (int i = 0; i < settingsListLayout.getChildCount(); i++) {
                    ((SettingsModelLayout) settingsListLayout.getChildAt(i)).setRadioChecked(false);
                }
                exceptFriendLayout.setRadioChecked(true);
                exceptFriendTextUpdated = true;
            }
            settingsModel.ignoredList.put(entry.getKey(), entry.getValue());
        }
        updateIgnoredList();
        removeExceptFriendListFragment();
    }

    @Override
    public void onCreateCustomListClicked(HashMap<String, SettingsModel.MemberDetails> selectedList, String name) {
        SettingsModel.CustomFriendListDetails details = new SettingsModel.CustomFriendListDetails(name, selectedList);
        String tag = mFireBaseHelper.createCustomFriendList(details);
        updateCustomListInSettingsLayout(tag, details, name);
    }

    @Override
    public SettingsModel onCustomFragmentCreated() {
        return settingsModel;
    }

    private void updateCustomListInSettingsLayout(String tag, SettingsModel.CustomFriendListDetails selectedList, String name) {
        if (settingsModel == null)
            settingsModel = new SettingsModel();
        if (settingsModel.customFriendList == null)
            settingsModel.customFriendList = new HashMap<>();
        settingsModel.customFriendList.put(tag, selectedList);
        for (int i = 0; i < settingsListLayout.getChildCount(); i++) {
            ((SettingsModelLayout) settingsListLayout.getChildAt(i)).setRadioChecked(false);
        }
        SettingsModelLayout layout = new SettingsModelLayout(getActivity());
        layout.setRadioChecked(true);
        layout.setViewControlsCallback(this);
        layout.initializeViewObjects(name, tag);
        settingsListLayout.addView(layout);
        updateSelectedRoomsList(tag);
        removeCustomListFragment();
    }

    @Override
    public void onUpdateListClicked(String id) {
        onSettingRowItemClicked(id);
        if (id.equals("exceptFriendsButton")) {
            loadRemoveFriendsFragment();
        } else if (id.equals("customListButton")) {
            loadCustomFriendListFragment(null);
        } else {
            loadCustomFriendListFragment(id);
        }
    }

    @Override
    public void onSettingRowItemClicked(String id) {
        for (int i = 0; i < settingsListLayout.getChildCount(); i++) {
            ((SettingsModelLayout) settingsListLayout.getChildAt(i)).setRadioChecked(false);
        }
        if (id.equals("allFriendsButton")) {
            currentActiveView = CURRENT_ACTIVE_VIEW_ALL_FRIEND_LIST;
            actionType = ACTION_TYPE_ALL_FRIEND;
            if (activeMomentTextView != null)
                activeMomentTextView.setText(getResources().getString(R.string.all_friends_text));
        } else if (id.equals("exceptFriendsButton")) {
            currentActiveView = CURRENT_ACTIVE_VIEW_EXCEPT_FRIEND_LIST;
            actionType = ACTION_TYPE_EXCEPT_FRIEND;
            updateIgnoredList();
        } else if (id.equals("customListButton")) {
            currentActiveView = CURRENT_ACTIVE_VIEW_CUSTOM_FRIEND_LIST;
            actionType = ACTION_TYPE_CUSTOM_FRIEND;
        } else {
            actionType = ACTION_TYPE_CUSTOM_FRIEND;
            updateSelectedRoomsList(id);
        }
    }

    @Override
    public void onCreateGroupClicked(ListRoomView listRoomView) {
        friendsList.add(0, listRoomView);
//        int maxRecentListIndex = friendsList.size() > 5 ? 5 : friendsList.size();
        recentFriendList.add(0, listRoomView);
        recentRoomListRecyclerView.getAdapter().notifyDataSetChanged();
        roomListRecyclerView.getAdapter().notifyDataSetChanged();
        Fragment fragment = getChildFragmentManager().findFragmentByTag("newGroupFragment");
        if (fragment != null) {
            getChildFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    public interface ViewControlsCallback {
        void onNewGroupButtonClicked();

        void uploadMediaToFireBase(int action_type, HashMap<String, String> selectedRoomsForMoment, String mediaPath, HashMap<String, Integer> momentList
                , HashMap<String, Integer> roomList, int expiryType, String mediaText);
    }
}