package com.instalively.android.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.activities.OnBoardingActivity;
import com.instalively.android.apihandling.RequestManager;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepankur on 6/23/16.
 */
public class ChooseUserNameFragment extends BaseFragment {

    private String userName;
    private EditText userHandleEt;
    private String TAG = this.getClass().getSimpleName();
    private View progressView;
    private TextView nextPageTV;
    private SharedPreferences preferences;
    private int displayWidth;

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userName = FireBaseHelper.getInstance(context).getMyUserModel().name;
        preferences = getActivity().getSharedPreferences(AppLibrary.APP_SETTINGS, 0);
        displayWidth = AppLibrary.getDeviceParams(getActivity(), "width");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_choose_username, container, false);
        String s = "Hello " + userName + ", welcome to Pulse.";

        Picasso.with(context).load(mFireBaseHelper.getMyUserModel().imageUrl).transform(new RoundedTransformation(20)).
                into((ImageView) rootView.findViewById(R.id.chooseUserNameIV));

        ((TextView) rootView.findViewById(R.id.helloUserNameTV)).setText(s);
        ((TextView) rootView.findViewById(R.id.helloUserNameTV)).setTypeface(fontPicker.getMuseo700());
        ((TextView) rootView.findViewById(R.id.chooseUserNameTV)).setTypeface(fontPicker.getMuseo500());

        userHandleEt = (EditText) rootView.findViewById(R.id.chooseUserNameET);
        progressView = (ProgressBar) rootView.findViewById(R.id.progressBar);
        userHandleEt.setTypeface(fontPicker.getMuseo500());
        progressView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        progressView.setVisibility(View.GONE);

        nextPageTV = (TextView) rootView.findViewById(R.id.nextPageTV);
        ObjectAnimator.ofFloat(nextPageTV, "translationX", 0, displayWidth).setDuration(20).start();

        nextPageTV.setVisibility(View.GONE);
        nextPageTV.setTypeface(fontPicker.getMontserratRegular());
        nextPageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userHandleEt.getText().toString().trim().isEmpty()) {
                    userHandleEt.clearFocus();
                    userHandleEt.setFocusable(false);
                    hitApiForHandle(mFireBaseHelper.getMyUserId(), userHandleEt.getText().toString().trim());
                    progressView.setVisibility(View.VISIBLE);
                } else {
                    showShortToastMessege("Please enter valid UserName");
                }
            }
        });
        userHandleEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d(TAG, "onEditorAction: " + " actionId " + actionId + " event " + event);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    Log.d(TAG, "done");
//                    if (!userHandleEt.getText().toString().trim().isEmpty()) {
//                        userHandleEt.clearFocus();
//                        userHandleEt.setFocusable(false);
//                        hitApiForHandle(mFireBaseHelper.getMyUserId(), userHandleEt.getText().toString().trim());
//                        progressView.setVisibility(View.VISIBLE);
//                    } else showShortToastMessege("Please enter valid UserName");
                }
                return false;
            }
        });

        userHandleEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                nextPageTV.setVisibility(s.toString().trim().length() > 0 ? View.VISIBLE : View.GONE);
                if (s.toString().trim().length() > 0)
                    slideInAnimation();
                else slideOutAnimation();
            }
        });

        return rootView;
    }

    private void slideOutAnimation() {
        if (nextPageTV.getX() == 0) {
            ObjectAnimator anim = ObjectAnimator.ofFloat(nextPageTV, "translationX", 0, displayWidth).setDuration(200);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    nextPageTV.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            anim.start();
        }

    }

    private void slideInAnimation() {
        nextPageTV.setVisibility(View.VISIBLE);
        if (nextPageTV.getX() == displayWidth)
            ObjectAnimator.ofFloat(nextPageTV, "translationX", displayWidth, 0).setDuration(200).start();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userHandleEt.requestFocus();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toggleSoftKeyboard(context, userHandleEt, true);
            }
        }, 200);

    }

    @SuppressWarnings("deprecation")
    private void hitApiForHandle(String userId, String handle) {

        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("userId", userId));
        pairs.add(new BasicNameValuePair("handle", handle));

        RequestManager.makePostRequest(getActivity(), RequestManager.ADD_USER_HANDLE_REQUEST, RequestManager.ADD_USER_HANDLE_RESPONSE, null, pairs, usrHandleCallBack);
    }

    private boolean isDestroyed;
    private RequestManager.OnRequestFinishCallback usrHandleCallBack = new RequestManager.OnRequestFinishCallback() {
        @SuppressLint("CommitPrefEdits")
        @Override
        public void onBindParams(boolean success, Object response) {
            try {
                Log.d(TAG, " server says " + response);
                final JSONObject object = (JSONObject) response;
                if (success) {
                    boolean error = object.getBoolean("error");
                    if (!error) {
                        progressView.setVisibility(View.VISIBLE);
//                        userHandleEt.setFocusable(false);
//                        userHandleEt.clearFocus();
                        toggleSoftKeyboard(context, userHandleEt, false);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt(AppLibrary.USER_ONBOARDING_STATUS, USER_NAME_SELECTION_DONE);
                        editor.commit();
//                        toggleSoftKeyboard(context, userHandleEt, false);

                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(userHandleEt.getApplicationWindowToken(), 0);
                        ((OnBoardingActivity) getActivity()).onUserNameSelectedSuccessfully();
                    } else {
                        progressView.setVisibility(View.GONE);
                        showShortToastMessage("An error accured please try again; with different username ");
                    }
                } else {
                    Log.e(TAG, "usrHandleCallBack Error, response -" + object);
                    showShortToastMessege(object.getString("value"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "usrHandleCallBack JsonException " + e);

            }
        }

        @Override
        public boolean isDestroyed() {
            return isDestroyed;
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
    }
}
