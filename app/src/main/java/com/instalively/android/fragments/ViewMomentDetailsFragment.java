package com.instalively.android.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.instalively.android.ExoPlayer.EventLogger;
import com.instalively.android.ExoPlayer.MediaPlayer;
import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.models.MomentModel;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.OnScreenshotTakenListener;
import com.instalively.android.util.RoundedTransformation;
import com.instalively.android.util.SnapshotDetector;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by user on 5/12/2016.
 */
public class ViewMomentDetailsFragment extends Fragment implements FireBaseKEYIDS, View.OnClickListener, OnScreenshotTakenListener ,EventLogger.OnPlayerStateChanged{

    private static final String TAG = "ViewMomentDetailsFragment";
    private ViewControlsCallback viewControlsCallback;
    private ListIterator<Map.Entry<String, MomentModel.Media>> mediaIterator;
    private View rootView;
    private MomentModel currentMomentModel;
    private MomentModel previousMomentModel;
    private ArrayList<Map.Entry<String, MomentModel.Media>> currentMediaList;
    private ImageView momentImageView;
    private ListIterator<Map.Entry<String, MomentModel>> momentIterator;
    private int currentMediaType;
    private int momentCounter = 0;
    private MomentModel.Media currentMediaModel;
    private MomentModel.Media previousMediaModel;
    private SnapshotDetector snapshotDetector;
    private boolean isCurrentMediaPaused = false;
    private boolean isNearByMomentOpened = false;
    private boolean isFirstTime = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isNearByMomentOpened = ((CameraActivity) getActivity()).isNearByMoment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.moment_details_fragment, container, false);
        initializeViewObjects(rootView);
        return rootView;
    }

    private void initializeViewObjects(View rootView) {
        rootView.findViewById(R.id.closeButton).setOnClickListener(this);
        if (!isNearByMomentOpened)
            snapshotDetector = new SnapshotDetector(this);
        momentImageView = (ImageView) rootView.findViewById(R.id.momentImage);
        final GestureDetector gestureDetector = new GestureDetector(getActivity(), new CustomGestureListener());
        momentIterator = viewControlsCallback.getMomentListIterator();
        viewControlsCallback.setGestureDetector(gestureDetector);
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean touchResult = true;
                boolean result = gestureDetector.onTouchEvent(event);

                if (isCurrentMediaPaused && event.getActionMasked()==MotionEvent.ACTION_UP) {
                    resumeCurrentMedia();
                }

                return touchResult || result;
            }
        });
        playNextMedia();
    }

    private void switchToNextMoment() {
        momentIterator = viewControlsCallback.getMomentListIterator();
        currentMomentModel = null;
        previousMomentModel = null;
        if (momentIterator.hasNext()) {
            currentMomentModel = momentIterator.next().getValue();
            currentMediaList = new ArrayList<>(currentMomentModel.media.entrySet());
            mediaIterator = currentMediaList.listIterator();
            initializeMomentViewObjects();
            momentCounter++;
        } else {
            while (momentIterator.hasPrevious()) {
                currentMomentModel = momentIterator.previous().getValue();
            }
            currentMediaList = new ArrayList<>(currentMomentModel.media.entrySet());
            mediaIterator = currentMediaList.listIterator();
        }
    }

    private void initializeMomentViewObjects() {
        for (Map.Entry<String, MomentModel.Media> entry : currentMomentModel.media.entrySet()) {
            Picasso.with(getActivity()).load(new File(entry.getValue().url)).fit().transform(new RoundedTransformation())
                    .into((ImageView) rootView.findViewById(R.id.createrImage));
            break;
        }
        ((TextView) rootView.findViewById(R.id.createrName)).setText(currentMomentModel.name);
        ((TextView) rootView.findViewById(R.id.createdTime)).setText(currentMomentModel.updatedDate);
        AppLibrary.log_d(TAG, "Currently Watching Moment Id -" + currentMomentModel.momentId);
    }

    private void switchToPreviousMoment() {
        momentIterator = viewControlsCallback.getMomentListIterator();
        if (momentIterator.previousIndex() >= 0) {
            currentMomentModel = momentIterator.previous().getValue();
            currentMediaList = new ArrayList<>(currentMomentModel.media.entrySet());
            mediaIterator = currentMediaList.listIterator();
            initializeMomentViewObjects();
        } else {
            // switch to last unseen moment in the list
            while (momentIterator.hasNext()) {
                currentMomentModel = momentIterator.next().getValue();
            }
            currentMediaList = new ArrayList<>(currentMomentModel.media.entrySet());
            mediaIterator = currentMediaList.listIterator();
        }
    }

    private void pauseCurrentMedia() {
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
            MediaPlayer.getInstance().pauseCurrentMedia();
        }else if (currentMediaType == AppLibrary.MEDIA_TYPE_IMAGE){
            viewControlsCallback.stopAutoPlay();
        }
        isCurrentMediaPaused = true;
    }

    private void resumeCurrentMedia() {
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
            MediaPlayer.getInstance().resumeCurrentMedia();
        } else if (currentMediaType == AppLibrary.MEDIA_TYPE_IMAGE){
            viewControlsCallback.resumeAutoPlay();
        }
        isCurrentMediaPaused = false;
    }

    @SuppressLint("LongLogTag")
    private String getNextMediaUrl() {
        String url = null;
        if (mediaIterator == null) {
            switchToNextMoment();
        }
        previousMediaModel = currentMediaModel;
        if (mediaIterator.hasNext()) {
            currentMediaModel = mediaIterator.next().getValue();
            if (viewControlsCallback.getMomentStatus() == READY_TO_VIEW_MOMENT && currentMediaModel.status == MEDIA_VIEWED) {
                url = getNextMediaUrl();
            } else {
                if (previousMediaModel != null && currentMomentModel != null) {
                    if (isNearByMomentOpened) {
                        FireBaseHelper.getInstance(getActivity()).openNearbyMedia(currentMomentModel.momentId, previousMediaModel.mediaId);
                    } else
                        FireBaseHelper.getInstance(getActivity()).openMedia(currentMomentModel.momentId, previousMediaModel.mediaId, null, null, -1,FRIEND_ROOM);
                }
                url = currentMediaModel.url;
            }
        } else {
            if (viewControlsCallback.getMomentStatus() == READY_TO_VIEW_MOMENT && currentMediaModel.status == MEDIA_VIEWED) {

            } else {
                // update seen media to fire base for the last media item in a moment
                if (previousMediaModel != null && currentMomentModel != null) {
                    if (isNearByMomentOpened)
                        FireBaseHelper.getInstance(getActivity()).openNearbyMedia(currentMomentModel.momentId, previousMediaModel.mediaId);
                    else
                        FireBaseHelper.getInstance(getActivity()).openMedia(currentMomentModel.momentId, previousMediaModel.mediaId, null, null, -1,FRIEND_ROOM);
                }
            }

            if (momentIterator.hasNext()) {
                // last media in the current moment so transit to upcoming fragment
                if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                    MediaPlayer.getInstance().onPause();
                }
                currentMediaModel = null;
                FireBaseHelper fireBaseHelper = FireBaseHelper.getInstance(getActivity());
                fireBaseHelper.viewCompleteMoment(currentMomentModel.momentId);
                viewControlsCallback.transitToUpcomingMoments(momentCounter);
            } else {
                FireBaseHelper fireBaseHelper = FireBaseHelper.getInstance(getActivity());
                fireBaseHelper.viewCompleteMoment(currentMomentModel.momentId);
                if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                    MediaPlayer.getInstance().onDestroy();
                }
                Log.d(TAG, " finishing activity on all media seen ");
                viewControlsCallback.onCloseMomentsFragment();
//                getActivity().finish();
            }
        }
        return url;
    }

//    private String getPreviousUrl(){
//        String url = null;
//        if (mediaIterator.hasNext()) {
//            currentMediaModel = mediaIterator.next().getValue();
//            url = currentMediaModel.url;
//        }
//        return url;
//    }

    public void playNextMedia() {
        String url = getNextMediaUrl();
        if (url != null) {
            if (previousMediaModel != null && AppLibrary.getMediaType(previousMediaModel.url) == AppLibrary.MEDIA_TYPE_VIDEO){
                MediaPlayer.getInstance().onPlayerRelease();
            }
            int mediaType = AppLibrary.getMediaType(url);
            if (mediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                viewControlsCallback.stopAutoPlay();
                currentMediaType = AppLibrary.MEDIA_TYPE_VIDEO;
                File file = new File(url);
                MediaPlayer.getInstance().initializePlayer(this,getActivity(), rootView, file.getAbsolutePath());
            } else if (mediaType == AppLibrary.MEDIA_TYPE_IMAGE) {
                currentMediaType = AppLibrary.MEDIA_TYPE_IMAGE;
                AppLibrary.log_d(TAG,"Currently displaying media with mediaId -"+currentMediaModel.mediaId);
                momentImageView.setImageURI(Uri.fromFile(new File(url)));
                viewControlsCallback.startAutoPlay();
                if (momentImageView.getVisibility() == View.GONE) {
                    momentImageView.setVisibility(View.VISIBLE);
                }
//                new FetchImageTask(url).execute();
            } else {
                // error
                AppLibrary.log_e(TAG, "Error playing next Media");
            }
        }
    }

//    public void replayCurrentMedia() {
//        currentMediaModel = null;
//        currentMediaList = new ArrayList<>(currentMomentModel.media.entrySet());
//        mediaIterator = currentMediaList.listIterator();
//        playNextMedia();
//    }

    public void hideImageView() {
        if (momentImageView != null && momentImageView.getVisibility() == View.VISIBLE)
            momentImageView.setVisibility(View.GONE);
    }

    private void playPreviousMoment() {
        currentMediaModel = null;
        currentMomentModel = null;
        switchToPreviousMoment();
        String url = getNextMediaUrl();
        if (url != null) {
            int mediaType = AppLibrary.getMediaType(url);
            if (mediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                viewControlsCallback.stopAutoPlay();
                currentMediaType = AppLibrary.MEDIA_TYPE_VIDEO;
                File file = new File(url);
                MediaPlayer.getInstance().initializePlayer(this,getActivity(), rootView, file.getAbsolutePath());
            } else if (mediaType == AppLibrary.MEDIA_TYPE_IMAGE) {
                currentMediaType = AppLibrary.MEDIA_TYPE_IMAGE;
                momentImageView.setImageURI(Uri.fromFile(new File(url)));
                viewControlsCallback.startAutoPlay();
                if (momentImageView.getVisibility() == View.GONE) {
                    momentImageView.setVisibility(View.VISIBLE);
                    if (previousMediaModel != null && AppLibrary.getMediaType(previousMediaModel.url) == AppLibrary.MEDIA_TYPE_VIDEO){
                        MediaPlayer.getInstance().onPause();
                    }
                }
            } else {
                // error
            }
        } else {
            if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                MediaPlayer.getInstance().onDestroy();
            }
//            getActivity().finish();
            viewControlsCallback.onCloseMomentsFragment();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback) getParentFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isNearByMomentOpened)
            snapshotDetector.start();
        if (!isFirstTime){
            isFirstTime = false;
            if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                MediaPlayer.getInstance().onResume();
            }
            viewControlsCallback.resumeAutoPlay();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isNearByMomentOpened)
            snapshotDetector.stop();
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO)
            MediaPlayer.getInstance().onPause();
        viewControlsCallback.stopAutoPlay();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onSwipeRightAction() {
        playPreviousMoment();
    }

    public void updatePlaylist() {
        momentImageView.setImageBitmap(null);
        switchToNextMoment();
        playNextMedia();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.closeButton:
//                getActivity().finish();
                viewControlsCallback.onCloseMomentsFragment();
                break;
        }
    }

    @Override
    public void onStateEnded() {
        MediaPlayer.getInstance().onPause();
        playNextMedia();
    }

    @Override
    public void onStateReady() {
        hideImageView();
    }

    private class CustomGestureListener implements GestureDetector.OnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;


        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            onSingleTap();
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            onLongHold();
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                            result = true;
                        } else {
                            onSwipeLeft();
                        }
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                }

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }

        public void onSwipeRight() {
        }

        public void onSwipeLeft() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }

        public void onSingleTap() {
            if (isCurrentMediaPaused) {
                resumeCurrentMedia();
            } else
                playNextMedia();
        }

        public void onLongHold() {
            if (!isCurrentMediaPaused)
                pauseCurrentMedia();
        }
    }

    public interface ViewControlsCallback {
        void setGestureDetector(GestureDetector gestureDetector);

        ListIterator<Map.Entry<String, MomentModel>> getMomentListIterator();

        void startAutoPlay();

        void transitToUpcomingMoments(int position);

        int getMomentStatus();

        void stopAutoPlay();

        void resumeAutoPlay();

        void onCloseMomentsFragment();
    }

    private class FetchImageTask extends AsyncTask<String, Void, Bitmap> {

        String imageUrl;

        public FetchImageTask(String url) {
            this.imageUrl = url;
        }

        @SuppressLint("LongLogTag")
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                if (imageUrl == null)
                    Log.e(TAG, " imageUrl null ");
                bitmap = Picasso.with(getActivity())
                        .load(new File(imageUrl))
                        .centerCrop()
                        .resize(AppLibrary.getDeviceParams(getActivity(), "width"),
                                AppLibrary.getDeviceParams(getActivity(), "height")).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                momentImageView.setImageBitmap(bitmap);
                if (momentImageView.getVisibility() == View.GONE) {
                    momentImageView.setVisibility(View.VISIBLE);
                    viewControlsCallback.startAutoPlay();
                    if (previousMediaModel != null && AppLibrary.getMediaType(previousMediaModel.url) == AppLibrary.MEDIA_TYPE_VIDEO){
                        MediaPlayer.getInstance().onPause();
                    }
                }
            }
        }
    }

    @Override
    public void onScreenshotTaken(Uri uri) {
        if (!isNearByMomentOpened) {
            AppLibrary.log_d(TAG,"Screen Shot taken media is -"+currentMediaModel.mediaId);
            FireBaseHelper fireBaseHelper = FireBaseHelper.getInstance(getActivity());
            fireBaseHelper.updateOnScreenShotTaken(currentMomentModel.userId, currentMomentModel.roomId, currentMediaModel.mediaId);
        }
    }
}