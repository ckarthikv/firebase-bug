package com.instalively.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.instalively.android.R;
import com.instalively.android.adapters.AddFriendFromFBAdapter;
import com.instalively.android.adapters.FriendRequestAdapter;
import com.instalively.android.adapters.RecyclerViewClickInterface;
import com.instalively.android.models.SocialModel;
import com.instalively.android.models.UserModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deepankur on 16/4/16.
 */
public class AddFriendFragment extends BaseFragment {
    private ArrayList<UserModel> mFacebookUserList;
    private RecyclerView mFriendRequestRecycler, mAddFriendRecycler;
    private TextView viewMoreRequestTV, viewMoreFriendsTV;
    private FrameLayout groupRequestFrame;

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFacebookUserList = mFireBaseHelper.getSuggestedFriends(/*mFireBaseHelper.getMyUserId()*/);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_friend, container, false);
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //else dashboard consumes touch and open
                //this fragment again due to touch
                return true;
            }
        });
        if (mFacebookUserList == null || mFacebookUserList.size() == 0)
            rootView.findViewById(R.id.addFromFacebookLL).setVisibility(View.GONE);

        initActionBar(rootView.findViewById(R.id.action_bar));
        loadGroupRequestFragment();
        mFriendRequestRecycler = (RecyclerView) rootView.findViewById(R.id.friend_request_recycler);
        initFacebookRecycler(rootView);
        initFriendRequestRecycler(rootView);
        viewMoreRequestTV = (TextView) rootView.findViewById(R.id.view_more_requestTV);
        viewMoreFriendsTV = (TextView) rootView.findViewById(R.id.viewMoreFriendsTV);
        groupRequestFrame = (FrameLayout) rootView.findViewById(R.id.groupRequestFrame);
        return rootView;
    }

    private void initActionBar(View actionBar) {
        actionBar.findViewById(R.id.action_bar_IV_1).setVisibility(View.GONE);
        actionBar.findViewById(R.id.action_bar_IV_2).setVisibility(View.GONE);
        actionBar.findViewById(R.id.action_bar_IV_3).setVisibility(View.GONE);
        actionBar.findViewById(R.id.action_bar_IV_4).setVisibility(View.GONE);

        actionBar.findViewById(R.id.action_bar_IV_1).setVisibility(View.VISIBLE);
        actionBar.findViewById(R.id.action_bar_IV_4).setVisibility(View.VISIBLE);
        ((ImageView) actionBar.findViewById(R.id.action_bar_IV_1)).setImageResource(R.drawable.back_svg);
        (actionBar.findViewById(R.id.action_bar_IV_1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        (actionBar.findViewById(R.id.action_bar_IV_4)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchShareIntent();
            }
        });
        ((ImageView) actionBar.findViewById(R.id.action_bar_IV_4)).setImageResource(android.R.drawable.ic_menu_share);
        ((TextView) actionBar.findViewById(R.id.titleTV)).setText("ADD FRIENDS");
        View topView = actionBar.findViewById(R.id.status_bar_background);
        topView.getLayoutParams().height = AppLibrary.getStatusBarHeight(getActivity());
        topView.requestLayout();

    }

    private void launchShareIntent() {
        String shareBody = "Here is the share content body";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));

    }

    private AddFriendFromFBAdapter addFriendFromFBAdapter;

    private void initFacebookRecycler(View rootView) {
        if (addFriendFromFBAdapter == null)
            addFriendFromFBAdapter = new AddFriendFromFBAdapter(mFacebookUserList, getActivity());
        mAddFriendRecycler = (RecyclerView) rootView.findViewById(R.id.add_from_facebook_recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mAddFriendRecycler.setLayoutManager(mLayoutManager);
        mAddFriendRecycler.setAdapter(addFriendFromFBAdapter);

    }

    private FriendRequestAdapter adapter;

    private void initFriendRequestRecycler(View rootView) {
        mFriendRequestRecycler = (RecyclerView) rootView.findViewById(R.id.friend_request_recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mFriendRequestRecycler.setLayoutManager(mLayoutManager);
        final HashMap<String, SocialModel.RequestReceived> friendRequestMap = mFireBaseHelper.getFriendRequestMap();
        if (friendRequestMap==null||friendRequestMap.size()==0){
            rootView.findViewById(R.id.friendRequestLL).setVisibility(View.GONE);
        }
        if (friendRequestMap == null) return;
        adapter = new FriendRequestAdapter(friendRequestMap, getActivity(), new RecyclerViewClickInterface() {
            @Override
            public void onItemClick(int position, Object optionalInfo) {
                adapter.refreshData();
                addFriendFromFBAdapter.notifyDataSetChanged();
            }
        });
        mFriendRequestRecycler.setAdapter(adapter);
    }


    private GroupRequestFragment groupRequestFragment;

    public void loadGroupRequestFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.groupRequestFrame, new GroupRequestFragment(), "GroupRequestFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }


}
