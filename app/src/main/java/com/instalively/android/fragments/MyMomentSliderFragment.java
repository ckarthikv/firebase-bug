package com.instalively.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.adapters.RecyclerViewClickInterface;
import com.instalively.android.adapters.SliderMyMomentAdapter;

import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.modelView.MediaModelView;
import com.instalively.android.models.MediaModel;
import com.instalively.android.models.MomentModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by deepankur on 26/4/16.
 */
public class MyMomentSliderFragment extends BaseFragment implements FireBaseHelper.onMyMomentMediaDownloadStatusModified{

    private RecyclerView recyclerView;
    private LinkedHashMap<String,MediaModelView> mediaArrayList;
    private String TAG = this.getClass().getSimpleName();
    private OnStreamListModified onStreamListModified;


    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onStreamListModified = (OnStreamListModified)getParentFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mediaArrayList = new LinkedHashMap<>();
        mFireBaseHelper.setOnMyMomentMediaDownloadStatusModified(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_moment, container, false);
        initRecyclerView(rootView);
        return rootView;
    }

    public void setMediaList(LinkedHashMap<String,MediaModelView> mediaList) {
        this.mediaArrayList = mediaList;
        if (myMomentAdapter != null) {
            myMomentAdapter.setMediaList(mediaArrayList);
            myMomentAdapter.notifyDataSetChanged();
        }
    }

    private SliderMyMomentAdapter myMomentAdapter;

    private void initRecyclerView(View rootView) {
        myMomentAdapter = new SliderMyMomentAdapter(getActivity(), mediaArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) rootView.findViewById(R.id.myMomentSliderRecycler);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(myMomentAdapter);
        myMomentAdapter.notifyDataSetChanged();
    }

    public void updateMediaUploadingStatus(MediaModel mediaModel,int uploadStatus,String mediaId) {
        switch (uploadStatus){
            case MEDIA_UPLOADING_STARTED:
                if (mediaArrayList.containsKey(mediaId)){
                    mediaArrayList.get(mediaId).status = uploadStatus;
                } else {
                    MediaModelView mediaModelView = new MediaModelView(mediaModel.url,mediaModel.createdAt,mediaModel.type,mediaModel.totalViews,
                            mediaModel.viewers,0,null,mFireBaseHelper.getMyUserModel().momentId,mediaId,uploadStatus,mediaModel.mediaText);
                    mediaArrayList.put(mediaId,mediaModelView);
                    mFireBaseHelper.getMyStreams().put(mediaId,mediaModelView);
                }
                onStreamListModified.onStreamListChanged();
                break;
            case MEDIA_UPLOADING_COMPLETE:
                updateMediaStatus(mediaId,uploadStatus);
                break;
            case MEDIA_UPLOADING_FAILED:
                updateMediaStatus(mediaId,uploadStatus);
                break;
        }
    }

    private void updateMediaStatus(String mediaId,int uploadStatus){
        if (mediaArrayList.containsKey(mediaId)){
            mediaArrayList.get(mediaId).status = uploadStatus;
        }
        myMomentAdapter.setMediaList(mediaArrayList);
        myMomentAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDownloadStatusChanges(String mediaId, int status) {
        // update my moment media download status
        if (mediaArrayList.containsKey(mediaId)){
            mediaArrayList.get(mediaId).status = status;
        }
        myMomentAdapter.setMediaList(mediaArrayList);
        myMomentAdapter.notifyDataSetChanged();
    }

    public interface OnStreamListModified{
        void onStreamListChanged();
    }

}