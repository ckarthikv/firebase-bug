package com.instalively.android.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.stickers.ScribbleView;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.EditTextBackEvent;

import java.lang.reflect.Field;

/**
 * Created by abc on 3/9/2016.
 */

public class CanvasFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = "CameraActivity";
    private ImageView undoIv;
    public EditTextBackEvent editText;//editText extension to detect backKey press;
    private InputMethodManager imm;
    private FrameLayout relativeLayout;
    private View bitmapView, backGroundColorView;
    private ViewControlsCallback viewControlsCallback;
    private View rootView;
    private FrameLayout revealView;
    private boolean defaultTextState;
    private int backgroundCount = 0;
    private static final int PERMISSION_ACCESS_EXTERNAL_STORAGE = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback)getParentFragment();
    }

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        defaultTextState = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        editText.clearFocus();
        viewControlsCallback.notifyFocus(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.canvas_fragment, container, false);
        revealView = (FrameLayout) rootView.findViewById(R.id.revealView);
        revealView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                showRevealAnimation(false,revealView);
                revealView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        bitmapView = rootView.findViewById(R.id.bitmapLayout);
        bitmapView.setDrawingCacheEnabled(true);
        bitmapView.setDrawingCacheQuality(FrameLayout.DRAWING_CACHE_QUALITY_HIGH);
        backGroundColorView = rootView.findViewById(R.id.backgroundColorLayout);
        rootView.findViewById(R.id.addToMoment_IV).setOnClickListener(this);
        rootView.findViewById(R.id.addToStream).setOnClickListener(this);
        rootView.findViewById(R.id.closeButton).setOnClickListener(this);
        undoIv = (ImageView) rootView.findViewById(R.id.undo_IV);
        editText = (EditTextBackEvent) rootView.findViewById(R.id.textEditLayout);
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        editText.setTypeface(Typeface.SANS_SERIF);
//        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                Rect r = new Rect();
//                //r will be populated with the coordinates of your view that area still visible.
//                rootView.getWindowVisibleDisplayFrame(r);
//
//                int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
//                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
//                    Log.d(TAG, "Height difference is " + heightDiff);
//                }
//            }
//        });
        editText.setOnEditTextImeBackListener(new EditTextBackEvent.EditTextImeBackListener() {
            @Override
            public void onImeBack(EditTextBackEvent ctrl, String text) {
                editText.clearFocus();
                editText.setCursorVisible(false);
                viewControlsCallback.notifyFocus(false);
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                editText.setCursorVisible(hasFocus);
                editText.setSelection(editText.getText().length());
                if (hasFocus)
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                else
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                viewControlsCallback.notifyFocus(hasFocus);
            }
        });

//        editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (
//                        actionId == EditorInfo.IME_ACTION_UNSPECIFIED
//                        || actionId == EditorInfo.IME_ACTION_NEXT
//                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
//                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
//                    editText.clearFocus();
//                    editText.setCursorVisible(false);
//                    viewControlsCallback.notifyFocus(false);
//                    return true;
//                }
//                return false;
//            }
//        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    editText.setHint("");
                } else {
                    editText.setHint("What's up?");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateCanvasViews(s.length());
                viewControlsCallback.notifyFocus(true);
            }
        });

        try {
            // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(editText, R.drawable.cursor);
        } catch (Exception ignored) {
        }

        final GestureDetector gestureDetector = new GestureDetector(getContext(),new CustomGestureListener());
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = false;
                if (!isColorPickerOpen())
                    return false;
                else
                    return true;

//                return result;
            }
        });
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = false;
                if(!isColorPickerOpen())
                    result = gestureDetector.onTouchEvent(event);

                return result;
            }
        });
        frameLayout = (FrameLayout) rootView.findViewById(R.id.touchDetector);
        scribbleView = (ScribbleView) rootView.findViewById(R.id.scribbleView);
        scribbleView.setDrawingEnabled(false);
        scribbleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {}
        });
        scribbleView.setScribbleListener(new ScribbleView.ScribbleListener() {
            @Override
            public void onScribbleStart() {
                undoIv.setVisibility(View.VISIBLE);
                updateCanvasViews(1);
                viewControlsCallback.notifyFocus(true);
            }

            @Override
            public void onScribbleEnd() {

            }

            @Override
            public void onTap(MotionEvent event) {

            }

            @Override
            public void onUndoScribble() {
                    if (isCanvasLocked())
                        updateCanvasViews(1);
                    else
                        updateCanvasViews(0);

                viewControlsCallback.notifyFocus(true);
            }
        });

        rootView.findViewById(R.id.hideCanvas).setOnClickListener(this);
        initColorEditor(rootView);
        relativeLayout = (FrameLayout) rootView;
        return rootView;
    }

    public void dispatchTouchEvent(MotionEvent event) {
        rootView.dispatchTouchEvent(event);
    }

    private void updateCanvasViews(int length) {
        if (length == 0){
            if (rootView.findViewById(R.id.addToMoment_IV).getVisibility() == View.VISIBLE)
                rootView.findViewById(R.id.addToMoment_IV).setVisibility(View.GONE);
            if (rootView.findViewById(R.id.addToStream).getVisibility() == View.VISIBLE)
                rootView.findViewById(R.id.addToStream).setVisibility(View.GONE);
            if (rootView.findViewById(R.id.hideCanvas).getVisibility() == View.GONE)
                rootView.findViewById(R.id.hideCanvas).setVisibility(View.VISIBLE);
            if (rootView.findViewById(R.id.save_IV).getVisibility() == View.VISIBLE)
                rootView.findViewById(R.id.save_IV).setVisibility(View.GONE);
        } else {
            if (rootView.findViewById(R.id.addToMoment_IV).getVisibility() == View.GONE)
                rootView.findViewById(R.id.addToMoment_IV).setVisibility(View.VISIBLE);
            if (rootView.findViewById(R.id.addToStream).getVisibility() == View.GONE)
                rootView.findViewById(R.id.addToStream).setVisibility(View.VISIBLE);
            if (rootView.findViewById(R.id.hideCanvas).getVisibility() == View.VISIBLE)
                rootView.findViewById(R.id.hideCanvas).setVisibility(View.GONE);
            if (rootView.findViewById(R.id.save_IV).getVisibility() == View.GONE)
                rootView.findViewById(R.id.save_IV).setVisibility(View.VISIBLE);
        }
    }

    FrameLayout frameLayout;
    ScribbleView scribbleView;

    /**
     * @return true if the back button was handled successfully
     * false otherwise
     */
    public boolean handleBack() {
        if (!isColorPickerOpen())
            return false;
        else toggleColorPicker();
        return true;
    }

    public boolean isColorPickerOpen() {
        return colorLayout.getVisibility() == View.VISIBLE;
    }

    public View getCloseButtonReference(){
        return rootView.findViewById(R.id.closeButton);
    }

    public boolean isCanvasLocked(){
        if (editText.getText().toString().length() != 0 || scribbleView.getPathListSize() != 0)
            return true;
        else return false;
    }

    public void toggleColorPicker() {
        if (isColorPickerOpen())
            colorLayout.setVisibility(View.GONE);
        else colorLayout.setVisibility(View.VISIBLE);
        scribbleView.setDrawingEnabled(isColorPickerOpen());
    }

    private void initColorEditor(View rootView) {
        colorLayout = (LinearLayout) rootView.findViewById(R.id.colorLayout);
        rootView.findViewById(R.id.greenBox).setOnClickListener(colorListener);
        rootView.findViewById(R.id.redBox).setOnClickListener(colorListener);
        rootView.findViewById(R.id.blueBox).setOnClickListener(colorListener);
        rootView.findViewById(R.id.purpleBox).setOnClickListener(colorListener);
        addTickMark((rootView.findViewById(R.id.redBox)));
        rootView.findViewById(R.id.undo_IV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pathSize = scribbleView.undo();
                if (pathSize < 1) undoIv.setVisibility(View.GONE);
            }
        });

        rootView.findViewById(R.id.save_IV).setOnClickListener(this);
    }

    LinearLayout colorLayout;
    int GREEN_COLOR = Color.parseColor("#00ff00"), BLUE_COLOR = Color.parseColor("#ff00ddff"),
            RED_COLOR = Color.parseColor("#ff0000"), PURPLE_COLOR = Color.parseColor("#ffaa66cc");
    private View.OnClickListener colorListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.greenBox:
                    scribbleView.setPaintColor(GREEN_COLOR);
                    break;
                case R.id.purpleBox:
                    scribbleView.setPaintColor(PURPLE_COLOR);
                    break;
                case R.id.blueBox:
                    scribbleView.setPaintColor(BLUE_COLOR);
                    break;
                case R.id.redBox:
                    scribbleView.setPaintColor(RED_COLOR);
                    break;

            }
            addTickMark(v);
        }
    };

    private void addTickMark(View view) {
        ((ImageView) rootView.findViewById(R.id.greenBox)).setImageDrawable(null);
        ((ImageView) rootView.findViewById(R.id.redBox)).setImageDrawable(null);
        ((ImageView) rootView.findViewById(R.id.blueBox)).setImageDrawable(null);
        ((ImageView) rootView.findViewById(R.id.purpleBox)).setImageDrawable(null);
        ((ImageView) view).setImageResource(R.drawable.add);
    }

    public Bitmap getBitmap() {
        bitmapView.buildDrawingCache(true);
        Bitmap bmp = Bitmap.createBitmap(bitmapView.getDrawingCache());
        bitmapView.setDrawingCacheEnabled(false);
        return bmp;
    }

    public void toggleEditTextProperties() {
        defaultTextState = !defaultTextState;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) editText.getLayoutParams();
        editText.setGravity(defaultTextState ? Gravity.LEFT : Gravity.CENTER);
//        editText.setTypeface(defaultTextState? Typeface.SANS_SERIF : Typeface.SERIF);
        if(!defaultTextState)
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
        else
            params.removeRule(RelativeLayout.CENTER_IN_PARENT);

        editText.setLayoutParams(params);
    }

    public void applyRandomBackground(boolean next) {
        if (next)
            backgroundCount = ((backgroundCount+1)%5);
        else
            backgroundCount = ((backgroundCount-1)%5);

        if (backgroundCount < 0)
            backgroundCount = backgroundCount + 5;

        int color;
        switch (backgroundCount) {
            case 0:
                color = Color.parseColor("#ffaa66cc"); //Should be the default purple gradient
                break;
            case 1:
                color = Color.argb(255, 50, 100, 23);
                break;
            case 2:
                color = Color.argb(255, 100, 20, 80);
                break;
            case 3:
                color = Color.argb(255, 150, 180, 20);
                break;
            case 4:
                color = Color.argb(255, 100, 30, 20);
                break;
            default:
                color = Color.argb(255, 200,30,70);
                break;
        }

        backGroundColorView.setBackgroundColor(color);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeButton:
                undoEditChanges();
                break;
            case R.id.addToMoment_IV:
                sendEditedImage();
                break;
            case R.id.addToStream:
                sendEditedImage();
                break;
            case R.id.hideCanvas:
                undoEditChanges();
                showRevealAnimation(true,revealView);
                viewControlsCallback.onHideCanvasFragment();
                break;
            case R.id.save_IV:
                requestSaveEditedMedia();
                break;
        }
    }

    private void requestSaveEditedMedia() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSION_ACCESS_EXTERNAL_STORAGE);
        } else {
            saveBitmap(getBitmap(),"_editedCanvas",getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestSaveEditedMedia(); //Now save the file
                } else {
                    showShortToastMessage(" Please provide access to save to Gallery");
                }
                break;
        }
    }

    public void startRevealAnimation(){
        showRevealAnimation(false,revealView);
    }

    private void sendEditedImage() {
        if (editedFile == null)
            saveBitmap(getBitmap(),"_editedCanvas",getActivity());
        viewControlsCallback.launchShareFragment(editedFile.getAbsolutePath(),AppLibrary.checkStringObject(editText.getText().toString()));
    }

    public void undoEditChanges() {
        if (isColorPickerOpen())
            toggleColorPicker();
        scribbleView.undoCompleteCanvas();
        if(!defaultTextState)
            toggleEditTextProperties();
        editText.setText("");
        editText.setHint("What's up?");
        viewControlsCallback.notifyFocus(false);
        editText.clearFocus();
        editText.setCursorVisible(false);
        hideKeyboard();
    }

    public void hideKeyboard() {
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    EditText tutorialText;

    private void addTutorialText() {
        tutorialText = new EditText(getActivity());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 0;
        params.topMargin = 400;
        relativeLayout.addView(tutorialText, params);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                relativeLayout.removeView(editText);
            }
        }, 2000);
    }

    public interface ViewControlsCallback{
        void notifyFocus(boolean hasFocus);
        void onHideCanvasFragment();
        void launchShareFragment(String absolutePath, String s);
    }

    private class CustomGestureListener implements GestureDetector.OnGestureListener {

        private static final int SWIPE_THRESHOLD = 70;
        private static final int SWIPE_VELOCITY_THRESHOLD = 30;


        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (editText.isFocused()) {
                viewControlsCallback.notifyFocus(true);
                Rect outRect = new Rect();
                editText.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) e.getRawX(), (int) e.getRawY())) {
                    editText.clearFocus();
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                } else
                    return false; //Pass onto edit text to handle it - for editing, etc
            } else {
                if (!((CameraActivity) getActivity()).isScrolling) {
                    editText.requestFocus();
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                }
            }
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                            result = false;
                        } else {
                            result = false;
                            onSwipeLeft();
                        }
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                }

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }

        public void onSwipeRight() {
            if(isCanvasLocked())
                applyRandomBackground(false);
        }

        public void onSwipeLeft() {
            if(isCanvasLocked())
                applyRandomBackground(true);
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }
    }
}
