package com.instalively.android.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.instalively.android.ExoPlayer.EventLogger;
import com.instalively.android.ExoPlayer.MediaPlayer;
import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.adapters.MyMediaViewerListAdapter;
import com.instalively.android.modelView.MediaModelView;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.OnSwipeTouchListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by deepankur on 6/9/16.
 */

public class ViewMyMediaFragment extends BaseFragment implements View.OnClickListener, EventLogger.OnPlayerStateChanged {

    private View rootView;
    private View slidingView;
    private RelativeLayout sliderHeaderView;
    private MotionEvent swipeEvent;
    private View headerSwipeDetectorView, bodySwipeDetectorView;
    private String TAG = getClass().getSimpleName();
    private GestureDetector sGestureListener;
    private int displayHeight;
    private int sliderHeaderHeight;
    private int MAXIMUM_DISPLACEMENT_DISTANCE;
    boolean swipeDetected;
    private String mediaId;
    private MediaModelView currentMediaModelView;
    private int currentMediaType;
    private Iterator<Map.Entry<String, MediaModelView>> mediaIterator;
    private String nextMediaUrl;
    private MediaModelView previousMediaModelView;
    private ViewControlsCallback viewControlsCallback;
    private ImageView mediaImageView;
    private boolean isFirstTime = true;
    private boolean isCurrentMediaPaused = false;
    private android.os.Handler autoPlayHandler = new Handler();
    private static final int AUTO_PLAY_THRESHOLD_TIME = 7;
    private CountDownTimer timer;
    private long timerTime;
    private Runnable autoPlayRunnable = new Runnable() {
        @Override
        public void run() {
            playNextMedia();
        }
    };
    private RecyclerView viewersRecyclerView;
    private ImageView arrowIv;

    public void startAutoPlay() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        autoPlayHandler.removeCallbacksAndMessages(null);
        timer = new CountDownTimer(AUTO_PLAY_THRESHOLD_TIME * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                timerTime = millisUntilFinished;
            }

            public void onFinish() {
                autoPlayHandler.post(autoPlayRunnable);
            }
        }.start();
    }

    public void stopAutoPlay() {
        autoPlayHandler.removeCallbacksAndMessages(null);

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mediaId = bundle.getString(AppLibrary.MEDIA_ID);
        if (mediaId != null) {
            // fetch media list containing only this mediaId
            HashMap<String, MediaModelView> hashMap = new HashMap<>();
            hashMap.put(mediaId, mFireBaseHelper.getMyStreams().get(mediaId));
            mediaIterator = hashMap.entrySet().iterator();
        } else {
            // fetch all the media list
            mediaIterator = mFireBaseHelper.getMyDownloadedStreams().entrySet().iterator();
        }
        displayHeight = AppLibrary.getDeviceParams((CameraActivity) context)[1];
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback) context;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isFirstTime) {
            isFirstTime = false;
            if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                MediaPlayer.getInstance().onResume();
            }
            resumeAutoPlay();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO)
            MediaPlayer.getInstance().onPause();
        stopAutoPlay();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_view_my_media, container, false);
        arrowIv = (ImageView) rootView.findViewById(R.id.arrowIV);
        mediaImageView = (ImageView) rootView.findViewById(R.id.mediaImage);
        viewersRecyclerView = (RecyclerView) rootView.findViewById(R.id.viewersRecyclerView);
        viewersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rootView.findViewById(R.id.closeButton).setOnClickListener(this);
        rootView.findViewById(R.id.deleteIV).setOnClickListener(this);
        rootView.findViewById(R.id.downloadIV).setOnClickListener(this);
        ((CameraActivity) getActivity()).toggleFullScreen(true);
        sliderHeaderView = (RelativeLayout) rootView.findViewById(R.id.sliderHeaderRL);
        slidingView = rootView.findViewById(R.id.sliderFrame);


        sliderHeaderView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        sliderHeaderView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        sliderHeaderHeight = sliderHeaderView.getHeight();
                        MAXIMUM_DISPLACEMENT_DISTANCE = displayHeight - sliderHeaderHeight;
                        slidingView.setTranslationY(MAXIMUM_DISPLACEMENT_DISTANCE);
                        arrowIv.setScaleY(-1);
                        Log.d(TAG, "VTO sliderHeaderHeight " + sliderHeaderHeight + " MAXIMUM_DISPLACEMENT_DISTANCE " + MAXIMUM_DISPLACEMENT_DISTANCE);
                    }
                });

        handleSwipeGestureOnHeader();
        handleSwipeGestureOnBody();
        sGestureListener = new GestureDetector(context, new CustomGestureListener());


        final GestureDetector detector = new GestureDetector(context, new MyGestureListener());
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean touchResult = true;
                boolean result = detector.onTouchEvent(event);

                if (isCurrentMediaPaused && event.getActionMasked() == MotionEvent.ACTION_UP) {
                    resumeCurrentMedia();
                }

                return touchResult || result;
            }
        });
        sliderHeaderView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                {//swipe thing
                    swipeEvent = MotionEvent.obtain(event);
                    swipeEvent.offsetLocation(swipeEvent.getRawX(), swipeEvent.getRawY());
                    headerSwipeDetectorView.dispatchTouchEvent(swipeEvent);
                }
                //translation thing
                handleHeaderTranslation(event);

                //tap thing
                sGestureListener.onTouchEvent(event);
                return true;
            }
        });

        slidingView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                {//swipe thing
                    swipeEvent = MotionEvent.obtain(event);
                    swipeEvent.offsetLocation(swipeEvent.getRawX(), swipeEvent.getRawY());
                    bodySwipeDetectorView.dispatchTouchEvent(swipeEvent);
                }

                handleBodyTranslation(event);
                return true;
            }
        });
        playNextMedia();
        return rootView;
    }

    public void resumeAutoPlay() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        autoPlayHandler.removeCallbacksAndMessages(null);
        timer = new CountDownTimer(timerTime, 1000) {

            public void onTick(long millisUntilFinished) {
                timerTime = millisUntilFinished;
            }

            public void onFinish() {
                autoPlayHandler.post(autoPlayRunnable);
            }
        }.start();
    }

    private void resumeCurrentMedia() {
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
            MediaPlayer.getInstance().resumeCurrentMedia();
        } else if (currentMediaType == AppLibrary.MEDIA_TYPE_IMAGE) {
            resumeAutoPlay();
        }
        isCurrentMediaPaused = false;
    }

    private void pauseCurrentMedia() {
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
            MediaPlayer.getInstance().pauseCurrentMedia();
        } else if (currentMediaType == AppLibrary.MEDIA_TYPE_IMAGE) {
            stopAutoPlay();
        }
        isCurrentMediaPaused = true;
    }

    public void playNextMedia() {
        String url = getNextMediaUrl();
        if (url != null) {
            if (previousMediaModelView != null && AppLibrary.getMediaType(previousMediaModelView.url) == AppLibrary.MEDIA_TYPE_VIDEO) {
                MediaPlayer.getInstance().onPlayerRelease();
            }
            int mediaType = AppLibrary.getMediaType(url);
            if (mediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                stopAutoPlay();
                currentMediaType = AppLibrary.MEDIA_TYPE_VIDEO;
                File file = new File(url);
                MediaPlayer.getInstance().initializePlayer(this, getActivity(), rootView, file.getAbsolutePath());
            } else if (mediaType == AppLibrary.MEDIA_TYPE_IMAGE) {
                currentMediaType = AppLibrary.MEDIA_TYPE_IMAGE;
                mediaImageView.setImageURI(Uri.fromFile(new File(url)));
                startAutoPlay();
                if (mediaImageView.getVisibility() == View.GONE) {
                    mediaImageView.setVisibility(View.VISIBLE);
                }
            }
        }
        ((TextView) rootView.findViewById(R.id.viewsText)).setText(String.valueOf(currentMediaModelView.totalViews));
        ((TextView) rootView.findViewById(R.id.screenShotText)).setText(String.valueOf(currentMediaModelView.screenShots));
        if (currentMediaModelView.viewers == null || currentMediaModelView.viewers.isEmpty()) {
            viewersRecyclerView.setAdapter(new MyMediaViewerListAdapter(new String[]{}));
        } else {
            viewersRecyclerView.setAdapter(new MyMediaViewerListAdapter(currentMediaModelView.viewers.keySet().toArray(new String[currentMediaModelView.viewers.size()])));
        }
    }

    private float firstDownRawY;

    private void handleHeaderTranslation(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            firstDownRawY = event.getRawY() - slidingView.getTranslationY();
            Log.d(TAG, "handleHeaderTranslation ACTION_DOWN");
        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
//            Log.d(TAG, "handleHeaderTranslation ACTION_MOVE");
            slidingView.setTranslationY(event.getRawY() - firstDownRawY);
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d(TAG, "handleHeaderTranslation ACTION_UP");
            if (!swipeDetected)
                settleDownTheView();
            swipeDetected = false;
        }
    }

    private float previousRawY;

    private void handleBodyTranslation(MotionEvent event) {

        float eventRawY = event.getRawY();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            previousRawY = eventRawY;
            Log.d(TAG, "handleHeaderTranslation ACTION_DOWN");
        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
//            Log.d(TAG, "handleHeaderTranslation ACTION_MOVE");

            float sliderTranslationY = slidingView.getTranslationY();
            if (sliderTranslationY + eventRawY - previousRawY >= 0 &&
                    sliderTranslationY + eventRawY - previousRawY <= MAXIMUM_DISPLACEMENT_DISTANCE) {
                slidingView.setTranslationY(sliderTranslationY + eventRawY - previousRawY);
            }
            previousRawY = eventRawY;

        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d(TAG, "handleHeaderTranslation ACTION_UP");
            if (!swipeDetected)
                settleDownTheView();
            swipeDetected = false;
        }
    }

    private void settleDownTheView() {
        float dis = slidingView.getTranslationY();
        if (dis > Math.abs(dis - MAXIMUM_DISPLACEMENT_DISTANCE)) {
            closeDrawer();
        } else openDrawer();
    }


    private void handleSwipeGestureOnHeader() {
        headerSwipeDetectorView = new View(context);
        headerSwipeDetectorView.setOnTouchListener(new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeTop() {
                Log.d(TAG, "onSwipeTop");
                swipeDetected = true;
                openDrawer();
            }

            @Override
            public void onSwipeRight() {
            }

            @Override
            public void onSwipeLeft() {
            }

            @Override
            public void onSwipeBottom() {
                Log.d(TAG, "onSwipeBottom");
                swipeDetected = true;
                closeDrawer();
            }
        });
    }


    private void handleSwipeGestureOnBody() {
        bodySwipeDetectorView = new View(context);
        bodySwipeDetectorView.setOnTouchListener(new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeTop() {
                Log.d(TAG, "onSwipeTop");
                swipeDetected = true;
                openDrawer();
            }

            @Override
            public void onSwipeRight() {
            }

            @Override
            public void onSwipeLeft() {
            }

            @Override
            public void onSwipeBottom() {
                Log.d(TAG, "onSwipeBottom");
                swipeDetected = true;
                closeDrawer();
            }
        });
    }

    private int ANIMATION_DURATION = 150;
    private boolean inAnimation;

    private void openDrawer() {
        if (inAnimation) return;
        inAnimation = true;
        Log.d(TAG, "openDrawer");
        rootView.findViewById(R.id.closeButton).setVisibility(View.GONE);
        stopAutoPlay();
        ObjectAnimator o = ObjectAnimator.ofFloat(slidingView, "translationY", slidingView.getTranslationY(), 0);
        o.setDuration(ANIMATION_DURATION);
        o.addListener(animatorListener);
        o.start();
        ObjectAnimator.ofFloat(arrowIv, "scaleY", arrowIv.getScaleY(), 1).setDuration(ANIMATION_DURATION).start();
    }

    private void closeDrawer() {
        if (inAnimation) return;
        inAnimation = true;
        Log.d(TAG, "closeDrawer");
        rootView.findViewById(R.id.closeButton).setVisibility(View.VISIBLE);
        resumeAutoPlay();
        ObjectAnimator o = ObjectAnimator.ofFloat(slidingView, "translationY", slidingView.getTranslationY(), MAXIMUM_DISPLACEMENT_DISTANCE).setDuration(ANIMATION_DURATION);
        o.addListener(animatorListener);
        o.start();
        ObjectAnimator.ofFloat(arrowIv, "scaleY", arrowIv.getScaleY(), -1).setDuration(ANIMATION_DURATION).start();

    }

    private final int TOP = 111, BOTTOM = 222, RUNNING = 333;

    private int getCurrentSliderPosition() {
        Log.d(TAG, "getCurrentSliderPosition " + slidingView.getTranslationY());
        if (slidingView.getTranslationY() == 0) return TOP;
        if (slidingView.getTranslationY() == MAXIMUM_DISPLACEMENT_DISTANCE) return BOTTOM;
        return RUNNING;
    }

    private Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            Log.d(TAG, "onAnimationStart");
            inAnimation = true;
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            inAnimation = false;
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            Log.d(TAG, "onAnimationCancel");
            inAnimation = false;
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO)
            MediaPlayer.getInstance().onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((CameraActivity) getActivity()).toggleFullScreen(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deleteIV:
                deleteMedia();
                break;
            case R.id.downloadIV:
                downloadMedia();
                break;
            case R.id.closeButton:
                stopAutoPlay();
                viewControlsCallback.onCloseViewMyMediaFragment();
                break;
        }
    }

    private void downloadMedia() {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        String name = date + "_" + today.hour + today.minute + today.second + ".jpg";
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "/InstaLively/");
        if (!dir.exists())
            dir.mkdirs();
        File sourceFile = new File(currentMediaModelView.url);
        File destFile = new File(dir, name);
        try {
            if (sourceFile.exists()) {

                InputStream in = new FileInputStream(sourceFile);
                OutputStream out = new FileOutputStream(destFile);

                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();

                Log.v(TAG, "Copy file successful.");
                Intent intent =
                        new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(Uri.fromFile(destFile));
                getActivity().sendBroadcast(intent);
            } else {
                Log.v(TAG, "Copy file failed. Source file missing.");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteMedia() {
        playNextMedia();
        mFireBaseHelper.deleteMyMediaFromMyMoments(currentMediaModelView.mediaId);
    }

    public String getNextMediaUrl() {
        String url = null;
        previousMediaModelView = currentMediaModelView;
        if (mediaIterator.hasNext()) {
            currentMediaModelView = mediaIterator.next().getValue();
            url = currentMediaModelView.url;
        } else {
            // exit the fragment
            if (currentMediaType == AppLibrary.MEDIA_TYPE_VIDEO) {
                MediaPlayer.getInstance().onDestroy();
            }
            stopAutoPlay();
            viewControlsCallback.onCloseViewMyMediaFragment();
        }
        return url;
    }

    @Override
    public void onStateEnded() {
        MediaPlayer.getInstance().onPause();
        playNextMedia();
    }

    @Override
    public void onStateReady() {
        if (mediaImageView != null && mediaImageView.getVisibility() == View.VISIBLE)
            mediaImageView.setVisibility(View.GONE);
    }

    private class CustomGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            int x = (int) e.getX();
            int y = (int) e.getY();
            Log.d(TAG, "onSingleTapConfirmed at " + x + " " + y);
            final int pos = getCurrentSliderPosition();
            Log.d(TAG, " pos " + pos);
            if (pos == TOP)
                closeDrawer();
            else if (pos == BOTTOM)
                openDrawer();

            return super.onSingleTapConfirmed(e);
        }
    }

    public interface ViewControlsCallback {
        void onCloseViewMyMediaFragment();
    }

    private class MyGestureListener implements OnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;


        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            onSingleTap();
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            onLongHold();
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                            result = false;
                        } else {
                            onSwipeLeft();
                        }
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                }

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }

        public void onSwipeRight() {
        }

        public void onSwipeLeft() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }

        public void onSingleTap() {
            if (isCurrentMediaPaused) {
                resumeCurrentMedia();
            } else
                playNextMedia();
        }

        public void onLongHold() {
            if (!isCurrentMediaPaused)
                pauseCurrentMedia();
        }
    }

    private class FetchImageTask extends AsyncTask<String, Void, Bitmap> {

        String imageUrl;

        public FetchImageTask(String url) {
            this.imageUrl = url;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                bitmap = Picasso.with(getActivity())
                        .load(new File(imageUrl))
                        .centerCrop()
                        .resize(AppLibrary.getDeviceParams(getActivity(), "width"),
                                AppLibrary.getDeviceParams(getActivity(), "height")).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                mediaImageView.setImageBitmap(bitmap);
            }
        }
    }
}
