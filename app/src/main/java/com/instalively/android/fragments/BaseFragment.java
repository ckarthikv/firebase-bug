package com.instalively.android.fragments;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.instalively.android.MasterClass;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.FontPicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

/**
 * Created by deepankur on 04-12-2015.
 * <p/>
 * <p/>
 * Fragment used for all common functions of children
 */

public abstract class BaseFragment extends Fragment implements FireBaseKEYIDS {

    private boolean registerForEvents = false;
    boolean isInternetPresent = false;
    private static String TAG = "BaseFragment";
    protected String myUserId;
    protected FireBaseHelper mFireBaseHelper;
    protected Context context;
    protected FontPicker fontPicker;

    public abstract void onEvent(BroadCastSignals.BaseSignal eventSignal);

    protected void registerForInAppSignals(boolean flag) {
        this.registerForEvents = flag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        if (this.registerForEvents) {
            MasterClass.getEventBus().register(this);
        }
        mFireBaseHelper = FireBaseHelper.getInstance(getActivity());
        myUserId = mFireBaseHelper.getMyUserId();
        if (fontPicker==null)
            fontPicker = FontPicker.getInstance(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.registerForEvents) {
            MasterClass.getEventBus().unregister(this);
        }
    }

    public static void setViewAndChildrenVisibility(View view, boolean enabled) {
        if (enabled)
            view.setVisibility(View.VISIBLE);
        else view.setVisibility(View.GONE);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenVisibility(child, enabled);
            }
        }
    }


    public static void enableTheseViews(View[] views) {

    }

    public static void toggleVisibility(View view, Boolean isVisible) {
        if (isVisible)
            view.setVisibility(View.VISIBLE);
        else if (!isVisible) view.setVisibility(View.GONE);

    }

    public void showShortToastMessage(String object) {
        Toast.makeText(getContext(), object, Toast.LENGTH_SHORT).show();
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isInternetAvailable(boolean showNoInternetToast) {
        try {
            isInternetPresent = isNetworkAvailable(getActivity());
            if (!isInternetPresent) {
                if (showNoInternetToast)
                    Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isInternetPresent;
    }

    private static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public void showShortToastMessege(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public static String getActionName(final int action) {

        switch (action) {
            case 0:
                return "ACTION_DOWN";
            case 1:
                return "ACTION_UP";
            case 2:
                return "ACTION_MOVE";
            case 3:
                return "ACTION_CANCEL";
            case 4:
                return "ACTION_OUTSIDE";
            case 5:
                return "ACTION_POINTER_DOWN";
            case 6:
                return "ACTION_POINTER_UP";
            case 7:
                return "ACTION_HOVER_MOVE";
            default:
                Log.w(TAG, "Warning:  Define this action above ");
                return null;
        }

    }

    /**
     * @param view the View Under Inspection
     * @param rx   x Coordinate for eg. event.getX()
     * @param ry   y Coordinate for eg. event.getY()
     * @return whether the point lies inside the bounding box of the view
     */
    public boolean isViewContains(View view, int rx, int ry) {
        int[] l = new int[2];
        view.getLocationOnScreen(l);
        int x = l[0];
        int y = l[1];
        int w = view.getWidth();
        int h = view.getHeight();

        if (rx < x || rx > x + w || ry < y || ry > y + h) {
            return false;
        }
        return true;
    }

    /**
     * Compute the dot product AB . AC
     */
    private double DotProduct(double[] pointA, double[] pointB, double[] pointC) {
        double[] AB = new double[2];
        double[] BC = new double[2];
        AB[0] = pointB[0] - pointA[0];
        AB[1] = pointB[1] - pointA[1];
        BC[0] = pointC[0] - pointB[0];
        BC[1] = pointC[1] - pointB[1];
        double dot = AB[0] * BC[0] + AB[1] * BC[1];

        return dot;
    }

    /**
     * Compute the cross product AB x AC
     */
    private double CrossProduct(double[] pointA, double[] pointB, double[] pointC) {
        double[] AB = new double[2];
        double[] AC = new double[2];
        AB[0] = pointB[0] - pointA[0];
        AB[1] = pointB[1] - pointA[1];
        AC[0] = pointC[0] - pointA[0];
        AC[1] = pointC[1] - pointA[1];
        double cross = AB[0] * AC[1] - AB[1] * AC[0];

        return cross;
    }

    /**
     * Compute the distance from A to B
     */
    double Distance(double[] pointA, double[] pointB) {
        double d1 = pointA[0] - pointB[0];
        double d2 = pointA[1] - pointB[1];

        return Math.sqrt(d1 * d1 + d2 * d2);
    }

    /**
     * Compute the distance from AB to C
     * if isSegment is true, AB is a segment, not a line.
     */
    public double LineToPointDistance2D(double[] pointA, double[] pointB, double[] pointC,
                                        boolean isSegment) {
        double dist = CrossProduct(pointA, pointB, pointC) / Distance(pointA, pointB);
        if (isSegment) {
            double dot1 = DotProduct(pointA, pointB, pointC);
            if (dot1 > 0)
                return Distance(pointB, pointC);

            double dot2 = DotProduct(pointB, pointA, pointC);
            if (dot2 > 0)
                return Distance(pointA, pointC);
        }
        return Math.abs(dist);
    }

    Bitmap scaleBitmap(Bitmap bitmap, float outputResolutionWidth) {
        float scaleX = 1;
        float width = bitmap.getWidth();
        float height = bitmap.getHeight();
        if (width > outputResolutionWidth)
            scaleX = outputResolutionWidth / width;
        Bitmap b = Bitmap.createScaledBitmap(bitmap, (int) (width * scaleX), (int) (height * scaleX), false);
        Log.d(TAG, " scaled bitmap height " + b.getHeight() + " width " + b.getWidth());
        return b;
    }

    File editedFile = null;
    public void saveBitmap(Bitmap bitmap,String suffix,Context mContext) {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        String name = date + "_" + today.hour + today.minute + today.second +suffix +".jpg";
        String mediaStoragePath = AppLibrary.getFilesDirectory(mContext);
        File mediaDir = new File(mediaStoragePath);
        if (!mediaDir.exists())
            mediaDir.mkdirs();
        editedFile = new File(mediaDir, name);
        writeFile(bitmap, editedFile,true);
    }

    public void setImageFilePath(boolean justSave, Context mContext, int MEDIA_TYPE, String suffix) {
        if(MEDIA_TYPE == AppLibrary.MEDIA_TYPE_IMAGE) {
            if (!justSave) {
                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                Time today = new Time(Time.getCurrentTimezone());
                today.setToNow();
                String name = date + "_" + today.hour + today.minute + today.second + suffix + ".jpg";

                String mediaStoragePath = AppLibrary.getCreatedMediaDirectory(mContext); //Uses local files directory

                File mediaDir = new File(mediaStoragePath);
                if (!mediaDir.exists())
                    mediaDir.mkdirs();
                editedFile = new File(mediaDir, name);
            } else {

                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                Time today = new Time(Time.getCurrentTimezone());
                today.setToNow();
                String name = date + "_" + today.hour + today.minute + today.second + suffix + ".jpg";

                String mediaStoragePath = AppLibrary.setupOutputDirectoryForRecordedFile(); //Uses external SD card

                File mediaDir = new File(mediaStoragePath);
                if (!mediaDir.exists())
                    mediaDir.mkdirs();
                editedFile = new File(mediaDir, name);
            }
        }
    }

    public void writeFile(Bitmap bmp, File f,boolean justSave) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(f);
            if (justSave)
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            else
                bmp.compress(Bitmap.CompressFormat.JPEG, 80, out); //ToDo - Use WebP here
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) out.close();
                bmp.recycle();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        bmp.recycle();
    }

    public static int getDIP(Context context, int value) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, r.getDisplayMetrics());
    }

    public long utcToEpoch(String utcTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.CHINA);
        Date date = null;
        try {
            date = sdf.parse(utcTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long mills = 0;//date.getTime();
        TimeZone t = TimeZone.getDefault();
        Log.d(TAG, " time zone" + t);
        Log.d(TAG, " returning " + mills + " for " + utcTime);
        return System.currentTimeMillis();
    }

    protected String getUtcFromEpoch(long unixMilliSeconds, boolean accountServerOffset) {
        if (accountServerOffset)
            unixMilliSeconds -= (mFireBaseHelper.getServerOffsetTime());
        Date date = new Date(unixMilliSeconds);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    public void showRevealAnimation(final boolean reverse, final View revealView) {

        int cx = revealView.getRight() - AppLibrary.convertDpToPixels(context,40);
        int cy = revealView.getBottom() - AppLibrary.convertDpToPixels(context,40);

        int radius = Math.max(revealView.getWidth(), revealView.getHeight());
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            SupportAnimator animator, animator_reverse;
            animator = ViewAnimationUtils.createCircularReveal(revealView, cx, cy, 0, radius);
            if (reverse)
                animator = animator.reverse();
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(400);
            animator.addListener(new SupportAnimator.AnimatorListener() {
                @Override
                public void onAnimationStart() {
                    if (!reverse)
                        revealView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd() {
                    if (reverse)
                        revealView.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel() {

                }

                @Override
                public void onAnimationRepeat() {

                }
            });

            animator.start();
        } else {
            Animator animator;
            if (reverse) {
                animator = android.view.ViewAnimationUtils.createCircularReveal(revealView, cx, cy, radius, 0);
            } else {
                animator = android.view.ViewAnimationUtils.createCircularReveal(revealView, cx, cy, 0, radius);
            }
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (!reverse)
                        revealView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (reverse) {
                        revealView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(400);
            animator.start();
        }
    }

    protected void toggleSoftKeyboard(Context context, EditText editText, boolean showKeyBoard) {

        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (showKeyBoard) {
            mgr.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
            return;
        }

        // check if no view has focus:
        View v = ((Activity) context).getCurrentFocus();
        if (v == null)
            return;

        mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @return A float value to represent dp equivalent to px value
     */
    public float pixelsToDp(float px) {
        Resources resources = this.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    protected void traverse(@Nullable String TAG, File dir) {
        if (TAG == null) TAG = this.TAG;
        Log.d(TAG, " transversing");
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; ++i) {
                File file = files[i];
                if (file.isDirectory()) {
                    Log.d(TAG, " directory: " + file);
                    traverse(TAG, file);
                } else {
                    Log.d(TAG, " file: " + file);
                }
            }
        }
    }
    public static String buildStackTraceString(final StackTraceElement[] elements) {
        StringBuilder sb = new StringBuilder();
        if (elements != null && elements.length > 0) {
            for (StackTraceElement element : elements) {
                sb.append(element.toString());
            }
        }
        return sb.toString();
    }

}
