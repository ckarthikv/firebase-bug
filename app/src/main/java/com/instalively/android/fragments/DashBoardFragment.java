package com.instalively.android.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.adapters.RecyclerViewClickInterface;
import com.instalively.android.adapters.SliderMessageListAdapter;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.floatingbutton.FloatingActionButton;
import com.instalively.android.modelView.DummyDataGenerator;
import com.instalively.android.modelView.HomeMomentViewModel;
import com.instalively.android.modelView.MediaModelView;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.models.MediaModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.EditTextBackEvent;
import com.instalively.android.util.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by deepankur on 14/4/16.
 */
public class DashBoardFragment extends BaseFragment implements FireBaseHelper.OnMyStreamsLoaded, MyMomentSliderFragment.OnStreamListModified {

    private final int BOTTOM = 0, MIDDLE = 1, TOP = 2, MOVING = -1;
    private final int MAXIMUM_ANIMATION_DURATION = 150;
    private final float MAXIMUM_TINT_ALPHA = 0.5f;
    private boolean mRecyclerTouchDownTriggered;//control variable ; touch down is sometimes consumed
    private long tintFingerDownTime;
    private float mFirstHitPoint;
    private ObjectAnimator anim;
    private boolean mDragLockGained;
    private View swipeDetectorView;
    private RecyclerView mRecyclerView;
    private LinearLayout noMessagesLayout;
    private LinearLayout sliderLayout;
    private TextView myStreamsTextView;
    private TextView messageTextView;
    private FloatingActionButton messageFAB;
    private ImageView sliderArrowIv, myMomentSliderArrowIv;
    private ImageView friendFragmentIv;
    private ScrollView scrollView;
    private EditTextBackEvent mSearchEditText;
    private int displayHeight;
    private FrameLayout tintedFrame;
    private RelativeLayout contentFrame, myMomentHeaderRl;
    private boolean isAnimating;
    private String TAG = this.getClass().getSimpleName();
    private RelativeLayout topBar;
    private int barHeight;
    private int mCurrentSliderPosition = BOTTOM;
    private float bottomBoundary, middleBoundary, topBoundary;//fixme always Zero bottom boundary ??
    private Context mContext;
    private long sliderAnimationDuration = MAXIMUM_ANIMATION_DURATION;
    private Handler mTintHandler;
    private boolean mPauseHandler = true;
    private long mTimeFingerDown;
    private Runnable mTintRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mPauseHandler) {
                mTintHandler.postDelayed(this, 30);
                resetTint();
            }
        }
    };

    private ViewControlsCallback viewControlsCallback;
    private LinkedHashMap<String, MediaModel> pendingMediaInMessageRoom;
    private String sendTo;
    private ImageView myMomentsIV;

    @Override
    public void onPause() {
        super.onPause();
        mPauseHandler = true;
        if (mTintHandler != null) {
            mTintHandler.removeCallbacksAndMessages(null);
        }
    }

    public void dismissSliderOnChatOpen() {
        mSearchEditText.clearFocus();
        mSearchEditText.setText("");
//        sliderAnimationDuration = 0;
        animateSlider(middleBoundary);
        scaleFAB(1f);
    }

    /**
     * @return true if this fragment can consume back press;
     * false otherwise
     */
    public boolean onBackPressed() {
        if (getRunTimeSliderPosition() == MIDDLE) {
            animateSlider(true);
            return true;
        }

        return false;
    }

    private float mPositionFingerDown;
    private Animator.AnimatorListener animationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            isAnimating = true;
            Log.d(TAG, "onAnimationStart");
            mPauseHandler = false;

            sliderLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            tintedFrame.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mTintHandler.post(mTintRunnable);

            if ((mCurrentSliderPosition == BOTTOM || mCurrentSliderPosition == TOP) && messageFAB.getVisibility() == View.VISIBLE)
                scaleFAB(0f);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            isAnimating = false;
            Log.d(TAG, "onAnimationEnd");
            mPauseHandler = true;
            int pos = getRunTimeSliderPosition();
            if (pos == BOTTOM) {
                tintedFrame.setAlpha(0);
                sliderArrowIv.setScaleY(1);
                updateMessageFooterText(true);  // bottom pos
            } else {
                sliderArrowIv.setScaleY(-1);    // middle pos
                updateMessageFooterText(false);
            }

            sliderLayout.setLayerType(View.LAYER_TYPE_NONE, null);
            tintedFrame.setLayerType(View.LAYER_TYPE_NONE, null);

            if (mCurrentSliderPosition == MIDDLE && messageFAB.getVisibility() == View.INVISIBLE)
                scaleFAB(1f);
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            isAnimating = false;
            Log.d(TAG, "onAnimationCancel");
            mPauseHandler = true;
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    private void updateMessageFooterText(boolean atBottom) {
//        if (atBottom) {
//            if (pendingMediaInMessageRoom != null && pendingMediaInMessageRoom.size() > 0) {
//                if (sendTo != null) {
//                    messageTextView.setText(sendTo);
//                } else {
//                    messageTextView.setText(getString(R.string.message_sending_failed_text));
//                }
//            } else {
//                messageTextView.setText(getString(R.string.message_footer_text));
//            }
//        } else {
//            messageTextView.setText(getString(R.string.message_footer_text));
//        }
    }

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (v.getId()) {
                case R.id.message_list_recycler_view:
                    Log.d(TAG, "recycler x " + event.getX() + " y " + event.getY());
                    mRecyclerView.onTouchEvent(event);
                    swipeDetectorView.dispatchTouchEvent(event);
                    return true;
                case R.id.tintedFrameLayout:
                    if (getRunTimeSliderPosition() != BOTTOM)
                        handleTintTouch(event);
                    else contentFrame.dispatchTouchEvent(event);
                    return true;
                default:
                    throw new RuntimeException();
            }
        }
    };
    private float mRecyclerRecordedDownY;
    private int mRecyclerDisplacement;
    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            mRecyclerDisplacement += dy;
        }
    };
    private boolean mTranslatingFirstTime;
    private boolean mEditTextHasFocus;

    private void handleTintTouch(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            tintFingerDownTime = System.currentTimeMillis();
            Log.d(TAG, "tintTime down " + tintFingerDownTime);
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d(TAG, " tintTime up" + (System.currentTimeMillis() - tintFingerDownTime));
            animateSlider(true);
        }
        Log.d(TAG, " tintTouch" + event.getAction());
    }

    //    ArrayList<RoomsModel> mMessageRoomArrayList;
    LinkedHashMap<String, SliderMessageModel> messageArrayList;

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate called");
        mContext = getActivity();
        mTintHandler = new Handler();
        swipeDetectorView = new View(getActivity());
//        loadNearByFragment();
//        loadMoments();
        mFireBaseHelper.setMyStreamListener(this);
        messageArrayList = new LinkedHashMap<>();
        sliderMessageListAdapter = new SliderMessageListAdapter(getActivity(), null, new RecyclerViewClickInterface() {
            @Override
            public void onItemClick(int position, Object data) {
                //close keyboard and then open fragment
                toggleSoftKeyboard(getActivity(), mSearchEditText, false);
                ((CameraActivity) getActivity()).loadChatFragment((SliderMessageModel) data);
            }
        });

        initSliderData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        Log.d(TAG, "onCreateView called");
        initActionBar(rootView.findViewById(R.id.action_bar));
        addViewTreeObserver(rootView.findViewById(R.id.action_bar));

        friendFragmentIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity) getActivity()).loadAddFriendFragment();
                loadAddFriendFriendFragment();
            }
        });
        View noMessageView = rootView.findViewById(R.id.include_no_message_layout);
        noMessageView.findViewById(R.id.add_friendBTN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadAddFriendFriendFragment();
            }
        });
        myStreamsTextView = (TextView) rootView.findViewById(R.id.myStreamsText);
        myMomentsIV = (ImageView) rootView.findViewById(R.id.myMomentsIV);
        messageTextView = (TextView) rootView.findViewById(R.id.sliderTV);
        initRecyclerView(rootView);
        toggleMessagesEmptyStates();
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
        handleSearchEditText(rootView);


        sliderLayout = (LinearLayout) rootView.findViewById(R.id.sliderLayout);
        sliderLayout.setVisibility(View.VISIBLE);

        messageFAB = (FloatingActionButton) rootView.findViewById(R.id.message_FAB);
        messageFAB.setScaleX(0);
        messageFAB.setScaleY(0);
        messageFAB.setVisibility(View.INVISIBLE);
        messageFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CameraActivity) getActivity()).openFriendListFragment();
            }
        });

        sliderArrowIv = (ImageView) rootView.findViewById(R.id.sliderArrowIV);
        tintedFrame = (FrameLayout) rootView.findViewById(R.id.tintedFrameLayout);
        initMyMomentSlider(rootView);
        tintedFrame.setVisibility(View.VISIBLE);
        tintedFrame.setOnTouchListener(touchListener);
        contentFrame = (RelativeLayout) rootView.findViewById(R.id.mainContent);
        topBar = (RelativeLayout) rootView.findViewById(R.id.topBarLayout);
        addViewTreeObserver(tintedFrame);
        addViewTreeObserver(topBar);
        addViewTreeObserver(myMomentHeaderRl);
        addViewTreeObserver(scrollView);

        topBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, " currentHeight " + sliderLayout.getY());
                Log.d(TAG, " touching top bar action" + event.getAction());
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (messageFAB.getVisibility() == View.VISIBLE)
                        scaleFAB(0f);

                    mSearchEditText.clearFocus();
                    toggleSoftKeyboard(getActivity(), mSearchEditText, false);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mPauseHandler = false;
                    mTintHandler.post(mTintRunnable);
                    mDragLockGained = true;
                    mTimeFingerDown = System.currentTimeMillis();
                    mPositionFingerDown = pixelsToDp(sliderLayout.getY());
                    mTranslatingFirstTime = true;
                    Log.d(TAG, " topBarDown" + event.getAction() + " y " + event.getY());
                }
                if (mDragLockGained && event.getAction() == MotionEvent.ACTION_MOVE) {
                    Log.d(TAG, " topBarMove" + event.getAction() + " y " + event.getY());
                    if (mTranslatingFirstTime) {
                        mFirstHitPoint = event.getY();
                        Log.d(TAG, " first hit point " + mFirstHitPoint);
                    }
                    doTranslation(event);
                    mTranslatingFirstTime = false;
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.d(TAG, " topBarUp" + event.getAction() + " y " + event.getY());
                    mPauseHandler = true;
                    mDragLockGained = false;
                    float distance = pixelsToDp(sliderLayout.getY()) - mPositionFingerDown;
                    if (Math.abs(distance) < 20) {//dp
                        getRunTimeSliderPosition();
                        if (messageTextView.getText().toString().equals(context.getResources().getString(R.string.message_sending_failed_text))
                                && getRunTimeSliderPosition() == BOTTOM) {
                            retrySendingMessage();
                        } else
                            animateSlider(true);
                        return true;
                    }
                    float speed = (distance / (System.currentTimeMillis() - mTimeFingerDown)) * 1000;
                    Log.d(TAG, " distance " + distance + " speed " + speed + " dp/second");

                    long mDownUpTime = System.currentTimeMillis() - mTimeFingerDown;
                    if (mDownUpTime < 500)
                        settlingAction(speed, event);
                    else delayedSettlingAction(speed, event);
                }
                return false;
            }
        });

        topBar.setOnClickListener(null);

        return rootView;
    }

    private void retrySendingMessage() {
        sendTo = "Sending to ";
        if (pendingMediaInMessageRoom != null && pendingMediaInMessageRoom.size() > 0) {
            for (Map.Entry<String, MediaModel> entry : pendingMediaInMessageRoom.entrySet()) {
                MediaModel mediaModel = entry.getValue();
                for (Map.Entry<String, Integer> roomEntry : mediaModel.addedTo.rooms.entrySet()) {
                    if (messageArrayList.containsKey(roomEntry.getKey())) {
                        sendTo += messageArrayList.get(roomEntry.getKey()).displayName + ",";
                        break;
                    }
                }
            }
            messageTextView.setText(sendTo);
        }
        viewControlsCallback.onUploadRetryClickedForAllMediaMessages();
    }

    void initActionBar(View actionBar) {
        actionBar.findViewById(R.id.action_bar_IV_2).setVisibility(View.GONE);
        ((ImageView) actionBar.findViewById(R.id.action_bar_IV_4)).setImageResource(R.drawable.camera_svg);
        actionBar.findViewById(R.id.action_bar_IV_4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CameraActivity) getActivity()).scrollToCameraFragment();
            }
        });

        ((ImageView) actionBar.findViewById(R.id.action_bar_IV_3)).setImageResource(R.drawable.search_svg);
        friendFragmentIv = (ImageView) actionBar.findViewById(R.id.action_bar_IV_1);
        (friendFragmentIv).setImageResource(R.drawable.add_friends_svg);
        ((TextView) actionBar.findViewById(R.id.titleTV)).setText("PULSE");
        View topView = actionBar.findViewById(R.id.status_bar_background);
        topView.getLayoutParams().height = AppLibrary.getStatusBarHeight(getActivity());
        topView.requestLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPauseHandler = false;
    }

    private void handleSearchEditText(View rootView) {
        mSearchEditText = (EditTextBackEvent) rootView.findViewById(R.id.searchET);
        mSearchEditText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        mSearchEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mSearchEditText.setGravity(Gravity.NO_GRAVITY);
                mSearchEditText.setFocusableInTouchMode(true);
                boolean focusGained = mSearchEditText.requestFocus();
                mSearchEditText.setCursorVisible(focusGained);
                toggleSoftKeyboard(getActivity(), mSearchEditText, true);
                if (mEditTextHasFocus)
                    animateETonSearchIntended();
                return true;
            }
        });

        mSearchEditText.setOnEditTextImeBackListener(new EditTextBackEvent.EditTextImeBackListener() {
            @Override
            public void onImeBack(EditTextBackEvent ctrl, String text) {
                sliderAnimationDuration = (long) (MAXIMUM_ANIMATION_DURATION * ((topBoundary - middleBoundary) / bottomBoundary));
                sliderAnimationDuration = sliderAnimationDuration < 0 ? -sliderAnimationDuration : sliderAnimationDuration;
                mSearchEditText.setText("");
                mSearchEditText.clearFocus();
                toggleSoftKeyboard(context, mSearchEditText, false);
                animateSlider(middleBoundary);
                scaleFAB(1);
            }
        });
        mSearchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mEditTextHasFocus = hasFocus;
                mSearchEditText.setGravity(hasFocus ? Gravity.NO_GRAVITY : Gravity.CENTER_VERTICAL);
                float reqDelta = hasFocus ? Math.abs(sliderLayout.getY() - topBoundary) :
                        Math.abs(sliderLayout.getY() - middleBoundary);

                sliderAnimationDuration = 3 * (long) (MAXIMUM_ANIMATION_DURATION *
                        reqDelta / (bottomBoundary - middleBoundary));

                if (hasFocus) {
                    animateETonSearchIntended();
                }
            }
        });
        mSearchEditText.addTextChangedListener(textWatcher);
    }

    private void animateETonSearchIntended() {
        mCurrentSliderPosition = TOP;
        animateSlider(topBoundary);
    }

    private void doTranslation(MotionEvent event) {
        sliderLayout.setTranslationY(sliderLayout.getTranslationY() + event.getY() - mFirstHitPoint);
    }

    private void resetTint() {
        float alpha = (MAXIMUM_TINT_ALPHA * (bottomBoundary - sliderLayout.getY())) / bottomBoundary;
        if (alpha > MAXIMUM_TINT_ALPHA || alpha < 0) {
            Log.e(TAG, "Alpha gone bonkers with alpha: " + alpha);
        }

        alpha = alpha < 0 ? 0 : alpha;
        alpha = alpha > MAXIMUM_TINT_ALPHA ? MAXIMUM_TINT_ALPHA : alpha;

        tintedFrame.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        tintedFrame.setAlpha(alpha);
    }

    private void settlingAction(float speed, MotionEvent event) {

        float deltaNeededToSettle = speed < 0 ? middleBoundary - sliderLayout.getY() :
                bottomBoundary - sliderLayout.getY();

        Log.d(TAG, " newSettling method : delta " + deltaNeededToSettle);
        sliderAnimationDuration = (long) (MAXIMUM_ANIMATION_DURATION * ((deltaNeededToSettle / (bottomBoundary - middleBoundary))));
        sliderAnimationDuration = sliderAnimationDuration < 0 ? -sliderAnimationDuration : sliderAnimationDuration;
        mCurrentSliderPosition = speed < 0 ? BOTTOM : MIDDLE;
        animateSlider(false);
    }

    private void delayedSettlingAction(float speed, MotionEvent event) {

        float middleBoundaryDistance = sliderLayout.getY() - middleBoundary;
        float bottomBoundaryDistance = sliderLayout.getY() - bottomBoundary;

        middleBoundaryDistance = middleBoundaryDistance < 0 ? -middleBoundaryDistance : middleBoundaryDistance;
        bottomBoundaryDistance = bottomBoundaryDistance < 0 ? -bottomBoundaryDistance : bottomBoundaryDistance;

        float deltaNeededToSettle = middleBoundaryDistance < bottomBoundaryDistance ? middleBoundaryDistance : bottomBoundaryDistance;

        sliderAnimationDuration = (long) (MAXIMUM_ANIMATION_DURATION * ((deltaNeededToSettle / (bottomBoundary - middleBoundary))));
        sliderAnimationDuration = sliderAnimationDuration < 0 ? -sliderAnimationDuration : sliderAnimationDuration;
        mCurrentSliderPosition = middleBoundaryDistance < bottomBoundaryDistance ? BOTTOM : MIDDLE;
        animateSlider(false);
    }

    //while listing

    private void animateSlider(boolean defaultAnimationDuration) {
        Log.d(TAG, " top" + sliderLayout.getY());
        sliderAnimationDuration = sliderAnimationDuration < 0 ?
                -sliderAnimationDuration : sliderAnimationDuration;
        if (defaultAnimationDuration)
            sliderAnimationDuration = MAXIMUM_ANIMATION_DURATION;
        switch (mCurrentSliderPosition) {
            case BOTTOM:
                mCurrentSliderPosition = MIDDLE;
                Log.d(TAG, "@@@@@@@@@  88888");
                animateSlider(middleBoundary);
                return;
            case MIDDLE:
                mCurrentSliderPosition = BOTTOM;
                Log.d(TAG, "@@@@@@@@  666666");
                animateSlider(bottomBoundary);
                return;
            case TOP:
                mCurrentSliderPosition = BOTTOM;
                animateSlider(bottomBoundary);
                return;
            case MOVING:

                return;
            default:
                throw new RuntimeException();
        }
    }

    private void animateSlider(float endPoint) {
        if (anim == null) {
            anim = ObjectAnimator.ofFloat(sliderLayout, "translationY", endPoint);
            anim.addListener(animationListener);
//            anim.setInterpolator(new BounceInterpolator());
        } else {
            anim.cancel();
        }
        anim.setFloatValues(endPoint);
        anim.setDuration(sliderAnimationDuration);
        anim.start();
    }

    float fabScaleTo;

    private void scaleFAB(float scaleTo) {
        fabScaleTo = scaleTo;

        long animDuration;
        if (scaleTo >= 0.5f)
            animDuration = 100;
        else
            animDuration = 50;

        messageFAB.setVisibility(View.VISIBLE);
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(messageFAB, "scaleX", scaleTo);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(messageFAB, "scaleY", scaleTo);
        scaleDownX.setDuration(animDuration);
        scaleDownY.setDuration(animDuration);
        AnimatorSet scaleFab = new AnimatorSet();
        scaleFab.play(scaleDownX).with(scaleDownY);
        scaleFab.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                messageFAB.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                messageFAB.setVisibility(scaleTo == 0 ? View.GONE : View.VISIBLE);
                if (fabScaleTo == 0) messageFAB.setVisibility(View.INVISIBLE);

                messageFAB.setLayerType(View.LAYER_TYPE_NONE, null);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        scaleFab.start();
    }

    int myStreamsHeaderHeight;
    int scrollViewTopDistance;

    private void addViewTreeObserver(final View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    if (view.getId() == R.id.scrollView) {
                        scrollViewTopDistance = view.getTop();
                        resetMyMomentMargins(scrollViewTopDistance);
                        Log.d(TAG, "addViewTreeObserver scrollViewTopDistance: " + scrollViewTopDistance);
                        return;
                    }
                    if (view.getId() == R.id.myMomentHeaderRL) {
                        myStreamsHeaderHeight = view.getHeight();
                        Log.d(TAG, "addViewTreeObserver myStreamsHeaderHeight: " + myStreamsHeaderHeight);
                        return;
                    }

                    if (view.getId() == R.id.action_bar) {
                        // resetMyMomentMargins(view.getHeight());
                        return;
                    }

                    int height = view.getMeasuredHeight();
                    if (view.getId() == R.id.tintedFrameLayout) {
                        displayHeight = height;
                        middleBoundary = (float) (height * 0.3);
                        topBoundary = 0;
                        sliderLayout.getLayoutParams().height = (int) (0.7 * height);
                        Log.d(TAG, " vto frame");
                    }

                    if (view.getId() == R.id.topBarLayout) {
                        barHeight = height;
                        Log.d(TAG, " vto RL");
                        scrollView.setPadding(0, 0, 0, height);
                    }
                    bottomBoundary = displayHeight - barHeight;
                    sliderLayout.setTranslationY((bottomBoundary));//interpolator can work here instead so no hard translations
                    Log.d(TAG, "VTO " + middleBoundary + "<-middle bottom->" + bottomBoundary);
                    /*resetTint();*/
                    tintedFrame.setAlpha(0);
                }
            });
        }
    }

    private int getRunTimeSliderPosition() {
        float currentDelta = sliderLayout.getY();
        float dropDistanceFromMiddle = currentDelta - middleBoundary;
        if (dropDistanceFromMiddle < 0) dropDistanceFromMiddle = -dropDistanceFromMiddle;

        float dropDistanceFromBottom = currentDelta - bottomBoundary;
        if (dropDistanceFromBottom < 0) dropDistanceFromBottom = -dropDistanceFromBottom;

        return dropDistanceFromBottom > dropDistanceFromMiddle ? MIDDLE : BOTTOM;

    }

    private SliderMessageListAdapter sliderMessageListAdapter;

    private void initRecyclerView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.message_list_recycler_view);
        noMessagesLayout = (LinearLayout) rootView.findViewById(R.id.no_messages_layout);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(sliderMessageListAdapter);
        mRecyclerView.addOnScrollListener(scrollListener);
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mRecyclerView.onTouchEvent(event);
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mRecyclerRecordedDownY = event.getY();
                    mRecyclerTouchDownTriggered = true;
                }
                if (event.getAction() == MotionEvent.ACTION_MOVE && !mRecyclerTouchDownTriggered) {
                    mRecyclerRecordedDownY = event.getY();
                    mRecyclerTouchDownTriggered = true;
                }
                if (event.getAction() == MotionEvent.ACTION_UP &&
                        mRecyclerDisplacement == 0 &&
                        event.getY() > mRecyclerRecordedDownY) {
                    animateSlider(true);
                    mRecyclerTouchDownTriggered = false;
                }
                return true;
            }
        });
    }

    AroundYouFragment aroundYouFragment;

    // nearBy fragment
    private void loadNearByFragment() {
        if (aroundYouFragment != null) {
            Log.e(TAG, " ignoring multiple instancing of a fragment ");
            return;
        }
        aroundYouFragment = new AroundYouFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.nearByLL, aroundYouFragment, AroundYouFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    //Message room Code begins
    private void initSliderData() {
        mFireBaseHelper.setSliderMessageListListener(new FireBaseHelper.SliderMessageListListener() {
            @Override
            public void onSliderListChanged(LinkedHashMap<String, SliderMessageModel> roomsModels) {
                AppLibrary.log_d(TAG, "onSliderlistChanged size -" + roomsModels.size());
                messageArrayList = roomsModels;
                toggleMessagesEmptyStates();
                sliderMessageListAdapter.setMessagesList(messageArrayList);
                sliderMessageListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void toggleMessagesEmptyStates() {
        boolean isMessageListEmpty = messageArrayList == null || messageArrayList.size() == 0;
        if (noMessagesLayout == null || mRecyclerView == null)
            return;
        noMessagesLayout.setVisibility(isMessageListEmpty ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(isMessageListEmpty ? View.GONE : View.VISIBLE);
    }

    private LinkedHashMap<String, SliderMessageModel> filteredMessageList;

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            Log.d(TAG, " textWatcher beforeTextChanged : " + s + " count " + count + " after " + after);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.d(TAG, " textWatcher onTextChanged : " + s + " count " + count + " count " + count);
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d(TAG, " textWatcher afterTextChanged : " + s);
            Log.d(TAG, " textWatcher filtering started at " + System.currentTimeMillis());
            if (s.toString().isEmpty()) {
                sliderMessageListAdapter.setMessagesList(messageArrayList);
                sliderMessageListAdapter.notifyDataSetChanged();
                return;
            }
            if (filteredMessageList == null) filteredMessageList = new LinkedHashMap<>();
            filteredMessageList.clear();
            for (Map.Entry<String, SliderMessageModel> entry : messageArrayList.entrySet()) {
                if (entry.getValue().displayName.toLowerCase().contains(s.toString().toLowerCase())) {
                    filteredMessageList.put(entry.getKey(), entry.getValue());
                }
            }
            sliderMessageListAdapter.setMessagesList(filteredMessageList);
            sliderMessageListAdapter.notifyDataSetChanged();
            Log.d(TAG, " textWatcher filtering done @ " + System.currentTimeMillis());

        }
    };
    //Message Rooms code ends----------------->momentRoom Begins
    MomentListFragment unseenMomentFragment, seenMomentFragment, favouriteMomentFragment;

    private void loadMoments() {

        if (unseenMomentFragment == null)
            loadUnSeenMomentFragment();

        if (seenMomentFragment == null)
            loadSeenMomentFragment();

     /*   if (favouriteMomentFragment == null)//todo in future
            loadFavoriteMomentFragment();*/

        mFireBaseHelper.setMomentsDataListener(new FireBaseHelper.MomentsDataListener() {
            @Override
            public void onUnseenDataChanged(ArrayList<HomeMomentViewModel> rooms) {
                Log.d(TAG, " onUnseenDataChanged " + rooms);
                if (rooms != null) {
                    {
                        Log.d(TAG, " onUnseenDataChanged length " + rooms.size());
                        if (rooms.size() > 0)
                            Log.d(TAG, " onUnseenDataChanged moment status" + rooms.get(0).momentStatus);
                    }
                }
                unseenMomentFragment.setRoomsModelArrayList(rooms);
            }

            @Override
            public void onSeenDataChanged(ArrayList<HomeMomentViewModel> rooms) {
                if (rooms != null) {
                    {
                        Log.d(TAG, " onSeenDataChanged length " + rooms.size());
                        if (rooms.size() > 0)
                            Log.d(TAG, " onSeenDataChanged moment status" + rooms.get(0).momentStatus);
                    }
                }
                seenMomentFragment.setRoomsModelArrayList(rooms);
//                seenMomentFragment.setRoomsModelArrayList(dummyDataGenerator.getSeenFriendMoments());
            }

            @Override
            public void onFavouritesDataChanged(ArrayList<HomeMomentViewModel> rooms) {
//                favouriteMomentFragment.setRoomsModelArrayList(rooms);
//                   favouriteMomentFragment.setRoomsModelArrayList(dummyDataGenerator.getFavouriteMoments());
            }

            @Override
            public void onElementChanged(String momentId) {
                unseenMomentFragment.updateSeenRoomModelArrayList(momentId);
            }
        });

    }

    private DummyDataGenerator dummyDataGenerator = DummyDataGenerator.getInstance();


    public void loadAddFriendFriendFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, new AddFriendFragment(), AddFriendFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(AddFriendFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();

    }

    public void loadUnSeenMomentFragment() {
        unseenMomentFragment = new MomentListFragment();
        unseenMomentFragment.setAddFriendListener(new MomentListFragment.AddFriendListener() {
            @Override
            public void requestOpenAddFriendFragment() {
                loadAddFriendFriendFragment();
            }
        });
        Bundle data = new Bundle();
        data.putInt(MOMENT_VIEW_TYPE, UNSEEN_FRIEND_MOMENT_RECYCLER);
        unseenMomentFragment.setArguments(data);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.ll1, unseenMomentFragment, "UnseenMomentFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void loadFavoriteMomentFragment() {

        favouriteMomentFragment = new MomentListFragment();
        Bundle data = new Bundle();
        data.putInt(MOMENT_VIEW_TYPE, FAVOURITE_MOMENT_RECYCLER);
        favouriteMomentFragment.setArguments(data);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.ll2, favouriteMomentFragment, "FavouriteMomentFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void loadSeenMomentFragment() {
        seenMomentFragment = new MomentListFragment();
        Bundle data = new Bundle();
        data.putInt(MOMENT_VIEW_TYPE, SEEN_FRIEND_MOMENT_RECYCLER);
        seenMomentFragment.setArguments(data);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.ll3, seenMomentFragment, "SeenMomentFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    //My Moment Header code begins
    private FrameLayout layer1, layer2Container;
    private FrameLayout layer1Header, layer1Tint, layer2FragmentHolder;
    private MyMomentSliderFragment myMomentFragment;
    private boolean isSliderOpen;
    private final int TOP_BAR_HEIGHT = 50;//in dps

    private void resetMyMomentMargins(int scrollViewTopDistance) {
        ((FrameLayout.LayoutParams) layer2Container.getLayoutParams()).topMargin =
                (AppLibrary.convertDpToPixels(getActivity(), TOP_BAR_HEIGHT)) + scrollViewTopDistance;
        ((FrameLayout.LayoutParams) layer1.getLayoutParams()).topMargin = scrollViewTopDistance;
    }

    private void initMyMomentSlider(View rootView) {
        myMomentSliderArrowIv = (ImageView) rootView.findViewById(R.id.myMomentSliderIndicator);
        myMomentSliderArrowIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleMyMomentView();
            }
        });
        myMomentSliderArrowIv.setVisibility(myMomentMediaListSize > 0 ? View.VISIBLE : View.GONE);
        myMomentHeaderRl = (RelativeLayout) rootView.findViewById(R.id.myMomentHeaderRL);
        layer2Container = (FrameLayout) rootView.findViewById(R.id.layer_2_ContainerFrame);
        layer1 = (FrameLayout) rootView.findViewById(R.id.layer_1_Frame);
        layer1Header = (FrameLayout) rootView.findViewById(R.id.layer_1_HeaderFrame);
        layer1Tint = (FrameLayout) rootView.findViewById(R.id.layer_1_TintFrame);
        layer2FragmentHolder = (FrameLayout) rootView.findViewById(R.id.layer_2_FragmentFrame);

        myMomentHeaderRl.setOnClickListener(myMomentsClickListener);
        scrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isSliderOpen;
            }
        });
        layer1Header.setOnClickListener(myMomentsClickListener);
        layer1Tint.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                    toggleMyMomentView();
                return true;
            }
        });


        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        myMomentFragment = myMomentFragment == null ? new MyMomentSliderFragment() : myMomentFragment;
        myMomentFragment.setMediaList(myStreamsList);
        fragmentTransaction.add(R.id.layer_2_FragmentFrame, myMomentFragment, "MyMomentSliderFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    public View.OnClickListener myMomentsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.myMomentHeaderRL:
                    scrollView.setScrollY(0);
                    if (mFireBaseHelper.getMyStreams() != null && mFireBaseHelper.getMyStreams().size() > 0) {
                        if (myStreamsTextView.getText().toString().equals(getString(R.string.posting_media_failed_text)))
                            viewControlsCallback.onUploadRetryClickedForAllMediaInMoments();
                        else {
                            if (mFireBaseHelper.getMyDownloadedStreams().size() > 0) {
                                // launch view my media fragment
                                ((CameraActivity) context).loadViewMyMediaFragment(null);
                            }
                        }
                    } else ((CameraActivity) getActivity()).scrollToCameraFragment();
                    break;
                case R.id.layer_2_ContainerFrame:
                    break;
                case R.id.layer_1_Frame:
                    break;
                case R.id.layer_1_HeaderFrame:
                    toggleMyMomentView();
                    break;
            }
        }
    };

    private boolean isMomentFrameAnimating;
    private ObjectAnimator momentFrameAnimator;
    private final int MOMENT_SLIDER_ANIM_DURATION = 300;

    private void toggleMyMomentView() {
        if (isMomentFrameAnimating) return;
        layer2Container.setVisibility(View.VISIBLE);
        layer1.setVisibility(View.VISIBLE);
        if (momentFrameAnimator == null) {
            momentFrameAnimator = ObjectAnimator.ofFloat(layer2FragmentHolder, "translationY", -displayHeight, 0);
            momentFrameAnimator.setDuration(MOMENT_SLIDER_ANIM_DURATION);
            momentFrameAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    isMomentFrameAnimating = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    isMomentFrameAnimating = false;
                    isSliderOpen = !isSliderOpen;
                    layer2Container.setVisibility(isSliderOpen ? View.VISIBLE : View.GONE);
                    layer1.setVisibility(isSliderOpen ? View.VISIBLE : View.GONE);

                    layer1Tint.setLayerType(View.LAYER_TYPE_NONE, null);
                    layer2FragmentHolder.setLayerType(View.LAYER_TYPE_NONE, null);
                    myMomentSliderArrowIv.setLayerType(View.LAYER_TYPE_NONE, null);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }

        float[] from_to_Y;
        float[] from_to_Alpha;
        float[] from_to_Scale_Y;
        if (!isSliderOpen) {
            from_to_Y = new float[]{-displayHeight, 0};
            from_to_Alpha = new float[]{0, 1f};
            from_to_Scale_Y = new float[]{-1f, 1f};
        } else {
            from_to_Y = new float[]{0, -displayHeight};
            from_to_Alpha = new float[]{1f, 0};
            from_to_Scale_Y = new float[]{1f, -1f};
        }


        layer1Tint.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        layer2FragmentHolder.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        myMomentSliderArrowIv.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        momentFrameAnimator.setFloatValues(from_to_Y);
        momentFrameAnimator.start();

        ObjectAnimator.ofFloat(layer1Tint, "alpha", from_to_Alpha)
                .setDuration(MOMENT_SLIDER_ANIM_DURATION)
                .start();
        ObjectAnimator.ofFloat(myMomentSliderArrowIv, "scaleY", from_to_Scale_Y)
                .setDuration(MOMENT_SLIDER_ANIM_DURATION)
                .start();

    }

    private int myMomentMediaListSize = 0;

    LinkedHashMap<String, MediaModelView> myStreamsList;

    @Override

    public void onStreamsLoaded(LinkedHashMap<String, MediaModelView> list, LinkedHashMap<String, MediaModel> pendingMyMomentMedia, LinkedHashMap<String, MediaModel> pendingMediaInMessageRoom) {
        Map.Entry<String, MediaModelView> firstEntry = null;
        if (list != null && list.size() > 0) {
            List<Map.Entry<String, MediaModelView>> entryList =
                    new ArrayList<Map.Entry<String, MediaModelView>>(list.entrySet());
            firstEntry = entryList.get(0);
        }

        if (firstEntry != null) {
            if (AppLibrary.getMediaType(firstEntry.getValue().url) == AppLibrary.MEDIA_TYPE_IMAGE) {
                Picasso.with(getActivity()).load(new File(firstEntry.getValue().url)).transform(new RoundedTransformation()).centerCrop().fit().into(myMomentsIV);
            } else {
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(new File(firstEntry.getValue().url).getAbsolutePath(), MediaStore.Images.Thumbnails.MICRO_KIND);
                if (bitmap != null) {
                    RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
                    drawable.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()) / 2.0f);
                    myMomentsIV.setImageBitmap(bitmap);
                } else {
                    Picasso.with(getActivity()).load(R.drawable.error_profile_picture).transform(new RoundedTransformation()).centerCrop().fit().into(myMomentsIV);
                }
            }
        }
        if (pendingMyMomentMedia.size() > 0) {
            myStreamsTextView.setText(getString(R.string.posting_media_failed_text));
        } else if (firstEntry != null) {
            myStreamsTextView.setText(AppLibrary.timeAccCurrentTime(firstEntry.getValue().createdAt));
        } else {
            // display default image
        }
        if (pendingMediaInMessageRoom.size() > 0) {
            this.pendingMediaInMessageRoom = pendingMediaInMessageRoom;
            messageTextView.setText(getString(R.string.message_sending_failed_text));
            updateMessageSliderForPendingMedia(pendingMediaInMessageRoom);
        }
        this.myStreamsList = sortMyStreams(list);
        if (myMomentFragment != null)
            myMomentFragment.setMediaList(myStreamsList);
        myMomentMediaListSize = mFireBaseHelper.getMyStreams().size();
        if (myMomentSliderArrowIv != null)
            myMomentSliderArrowIv.setVisibility(myMomentMediaListSize > 0 ? View.VISIBLE : View.GONE);
    }

    private LinkedHashMap<String, MediaModelView> sortMyStreams(LinkedHashMap<String, MediaModelView> list) {
        List<Map.Entry<String, MediaModelView>> entryList = new LinkedList<>(list.entrySet());
        Collections.sort(entryList, new Comparator<Map.Entry<String, MediaModelView>>() {
            @Override
            public int compare(Map.Entry<String, MediaModelView> ele1,
                               Map.Entry<String, MediaModelView> ele2) {
                return (int) (ele2.getValue().createdAt - ele1.getValue().createdAt);
            }
        });
        LinkedHashMap<String, MediaModelView> newSortedList = new LinkedHashMap<>();
        for (Map.Entry<String, MediaModelView> entry : entryList) {
            newSortedList.put(entry.getKey(), entry.getValue());
        }
        return newSortedList;
    }

    @Override
    public void onMediaInMessageRoomsUpdated(LinkedHashMap<String, MediaModel> pendingMediaInMessageRoom) {
//        if (pendingMediaInMessageRoom.size() > 0) {
//            messageTextView.setText(getString(R.string.message_sending_failed_text));
//            updateMessageSliderForPendingMedia(pendingMediaInMessageRoom);
//        }
    }

    @Override
    public void onMediaInMomentUpdated(LinkedHashMap<String, MediaModel> pendingMyMomentMedia) {
//        if (pendingMyMomentMedia.size() > 0)
//            myStreamsTextView.setText(getString(R.string.posting_media_failed_text));
    }

    public void updateMessageSliderForPendingMedia(LinkedHashMap<String, MediaModel> pendingMediaInMessageRoom) {
        for (Map.Entry<String, MediaModel> entry : pendingMediaInMessageRoom.entrySet()) {
            MediaModel mediaModel = entry.getValue();
            if (mediaModel.addedTo.rooms.size() > 0) {
                for (String room : mediaModel.addedTo.rooms.keySet()) {
                    if (messageArrayList != null && messageArrayList.size() > 0) {
                        if (messageArrayList.containsKey(room)) {
                            messageArrayList.get(room).status = messageArrayList.get(room).setStatus(SENDING_FAILED);
                        }
                    }
                }
            }
        }
        sliderMessageListAdapter.notifyDataSetChanged();
    }

    public void updateMediaUploadStatus(MediaModel mediaModel, String mediaId, int uploadStatus) {
        if (mediaModel.addedTo.moments != null && !mediaModel.addedTo.moments.isEmpty()) {
            myMomentFragment.updateMediaUploadingStatus(mediaModel, uploadStatus, mediaId);
            if (uploadStatus == MEDIA_UPLOADING_STARTED)
                myStreamsTextView.setText(getString(R.string.posting_media_text));
            else if (uploadStatus == MEDIA_UPLOADING_COMPLETE)
                myStreamsTextView.setText(getString(R.string.add_photo_video));
            else if (uploadStatus == MEDIA_UPLOADING_FAILED) {
                myStreamsTextView.setText(getString(R.string.posting_media_failed_text));
            }
        }

        if (mediaModel.addedTo.rooms != null && !mediaModel.addedTo.rooms.isEmpty()) {
            for (Map.Entry<String, Integer> entry : mediaModel.addedTo.rooms.entrySet()) {
                updateSliderMessageModelForRoom(entry.getKey(), uploadStatus);
            }
            sliderMessageListAdapter.notifyDataSetChanged();
        }
    }

    private void updateSliderMessageModelForRoom(String roomId, int status) {
        sendTo = "Sending to ";
        if (messageArrayList.containsKey(roomId)) {
            if (status == MEDIA_UPLOADING_STARTED) {
                messageArrayList.get(roomId).status = messageArrayList.get(roomId).setStatus(SENDING_MEDIA);
                sendTo += messageArrayList.get(roomId).displayName + ",";
                messageTextView.setText(sendTo);
            } else if (status == MEDIA_UPLOADING_COMPLETE) {
                sendTo = null;
                messageArrayList.get(roomId).status = messageArrayList.get(roomId).setStatus(SENT_MEDIA);
                messageTextView.setText(getString(R.string.sent_message_text));
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        messageTextView.setText(getString(R.string.message_footer_text));
                    }
                }, 3000);
            } else if (status == MEDIA_UPLOADING_FAILED) {
                messageArrayList.get(roomId).status = messageArrayList.get(roomId).setStatus(SENDING_FAILED);
                messageTextView.setText(getString(R.string.message_sending_failed_text));
            }
        }
    }

    @Override
    public void onStreamListChanged() {

        LinkedHashMap<String, MediaModelView> myStreams = sortMyStreams(mFireBaseHelper.getMyStreams());
        this.myStreamsList = myStreams;
        myMomentMediaListSize = myStreams.size();
        if (myMomentMediaListSize > 0) {
            List<Map.Entry<String, MediaModelView>> entryList =
                    new ArrayList<Map.Entry<String, MediaModelView>>(myStreams.entrySet());
            Map.Entry<String, MediaModelView> firstEntry =
                    entryList.get(0);
            if (firstEntry != null) {
                myStreamsTextView.setText(AppLibrary.timeAccCurrentTime(firstEntry.getValue().createdAt));
                if (AppLibrary.getMediaType(firstEntry.getValue().url) == AppLibrary.MEDIA_TYPE_IMAGE) {
                    Picasso.with(getActivity()).load(new File(firstEntry.getValue().url)).transform(new RoundedTransformation()).centerCrop().fit().into(myMomentsIV);
                } else {
                    Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(new File(firstEntry.getValue().url).getAbsolutePath(), MediaStore.Images.Thumbnails.MICRO_KIND);
                    if (bitmap != null) {
                        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
                        drawable.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()) / 2.0f);
                        myMomentsIV.setImageBitmap(bitmap);
                    } else {
                        Picasso.with(getActivity()).load(R.drawable.error_profile_picture).transform(new RoundedTransformation()).centerCrop().fit().into(myMomentsIV);
                    }
                }
            }
        }
        if (myMomentSliderArrowIv != null)
            myMomentSliderArrowIv.setVisibility(myMomentMediaListSize > 0 ? View.VISIBLE : View.GONE);
        if (myMomentFragment != null)
            myMomentFragment.setMediaList(myStreamsList);
    }

    public void updateOnRoomOpen(String roomId) {
        messageArrayList.get(roomId).status = messageArrayList.get(roomId).setStatus(NO_STATUS);
    }

    public interface ViewControlsCallback {
        void onUploadRetryClickedForAllMediaInMoments();

        void onUploadRetryClickedForAllMediaMessages();
    }
}