package com.instalively.android.fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instalively.android.R;
import com.instalively.android.activities.OnBoardingActivity;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.OverRideBackEventEditText;

import java.security.SecureRandom;
import java.util.ArrayList;

/**
 * Created by deepankur on 6/23/16.
 */
public class SelectInstitutionFragment extends BaseFragment {

    private OverRideBackEventEditText institutionET;
    private View rootView;
    private String TAG = getClass().getSimpleName();
    private LinearLayout suggestionsLayout;
    private TextView instituteSelectedTv;
    private ImageView schoolIv;
    private TextView schollTv;
    private HorizontalScrollView scrollView;

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generateDummyData();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_select_institution, container, false);
        convertDpToPixel();
        scrollView = (HorizontalScrollView) rootView.findViewById(R.id.scrollView);
        schollTv = (TextView) rootView.findViewById(R.id.school_1_TV);
        selectedLL = (LinearLayout) rootView.findViewById(R.id.instituteSelectedLL);
        schoolIv = (ImageView) rootView.findViewById(R.id.schoolIV);
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        institutionET = (OverRideBackEventEditText) rootView.findViewById(R.id.schoolET);
//        institutionET.requestFocus();
        toggleSoftKeyboard(context, institutionET, true);
        suggestionsLayout = (LinearLayout) rootView.findViewById(R.id.suggestionLL);
        instituteSelectedTv = (TextView) rootView.findViewById(R.id.instituteSelectedTV);
        institutionET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d(TAG, "onEditorAction: " + " actionId " + actionId + " event " + event);
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //translateDown();
                    Log.d(TAG, "starting search @ " + System.currentTimeMillis());
                    startPartialSearch(institutionET.getText().toString());
                }
                return true;
            }
        });
//        institutionET.setFocusable(true);
//        institutionET.setFocusableInTouchMode(true);
//        institutionET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                Log.d(TAG, "onFocusChange " + hasFocus);
//                if (!hasFocus) {
//                    // code to execute when EditText loses focus
//                }
//            }
//        });

        institutionET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch " + event);
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    if (institutionET.getTranslationY() == 0)
                        translateUP();
                return false;
            }
        });

        institutionET.setOnKeyPreImeListener(new OverRideBackEventEditText.OnKeyPreImeListener() {
            @Override
            public void onKeyPreImePressed(int keyCode, KeyEvent event) {
                Log.d(TAG, "onKeyPreIme: keycode " + keyCode + " event " + event);
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    translateDown();
                }
            }
        });
        institutionET.setTypeface(fontPicker.getMuseo500());
        ((TextView) rootView.findViewById(R.id.school_1_TV)).setTypeface(fontPicker.getMuseo700());
        ((TextView) rootView.findViewById(R.id.skipTV)).setTypeface(fontPicker.getMontserratRegular());

        ((TextView) rootView.findViewById(R.id.instituteSelectedTV)).setTypeface(fontPicker.getMuseo500());

        rootView.findViewById(R.id.nextPageTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(institutionET.getApplicationWindowToken(), 0);

                if (currentlySelectedData != null)
                    ((OnBoardingActivity) getActivity()).onInstitutionSelectionComplete(true, currentlySelectedData.ID, institutionET.getText().toString());
                else
                    ((OnBoardingActivity) getActivity()).onInstitutionSelectionComplete(false, -1, institutionET.getText().toString());
            }
        });

        rootView.findViewById(R.id.cancelIV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentlySelectedData = null;
                toggleSelectionState(false);
            }
        });

        rootView.findViewById(R.id.skipTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OnBoardingActivity) getActivity()).onInstitutionSelectionComplete(false, -1, institutionET.getText().toString());
            }
        });
        return rootView;
    }

    private ArrayList<InstitutionData> filteredData = new ArrayList<>();
    private int animationState;
    private final int TRANSLATED_DOWN = 0, TRANSLATED_UP = 1;
    private int IMAGE_OFFEST = -314;
    private int TEXT_VIEW_OFFSET = 108 - 314;
    private int EDIT_TEXT_OFFSET = 150 - 356;
    private boolean inAnimation = false;

    void convertDpToPixel() {
        IMAGE_OFFEST = AppLibrary.convertDpToPixels(context, IMAGE_OFFEST);
        TEXT_VIEW_OFFSET = AppLibrary.convertDpToPixels(context, TEXT_VIEW_OFFSET);

    }

    void translateUP() {
        ObjectAnimator.ofFloat(schollTv, "translationY", 0, TEXT_VIEW_OFFSET).setDuration(200).start();
        ObjectAnimator.ofFloat(schoolIv, "translationY", 0, IMAGE_OFFEST).setDuration(200).start();
        ObjectAnimator.ofFloat(selectedLL, "translationY", 0, TEXT_VIEW_OFFSET).setDuration(200).start();
        ObjectAnimator.ofFloat(scrollView, "translationY", 0, TEXT_VIEW_OFFSET).setDuration(200).start();
        ObjectAnimator.ofFloat(institutionET, "translationY", 0, TEXT_VIEW_OFFSET).setDuration(200).start();
    }

    void translateDown() {
        ObjectAnimator.ofFloat(schollTv, "translationY", TEXT_VIEW_OFFSET, 0).setDuration(200).start();
        ObjectAnimator.ofFloat(schoolIv, "translationY", IMAGE_OFFEST, 0).setDuration(200).start();
        ObjectAnimator.ofFloat(selectedLL, "translationY", TEXT_VIEW_OFFSET, 0).setDuration(200).start();
        ObjectAnimator.ofFloat(scrollView, "translationY", TEXT_VIEW_OFFSET, 0).setDuration(200).start();
        ObjectAnimator.ofFloat(institutionET, "translationY", TEXT_VIEW_OFFSET, 0).setDuration(200).start();

    }

    private void startPartialSearch(String query) {
        int match = 0;
        boolean broken = false;
        filteredData.clear();
        for (InstitutionData data : allInstitutesData) {
            if (data.name.toLowerCase().contains(query.toLowerCase())) {
                filteredData.add(data);
                match++;
                if (match == 10) {
                    broken = true;
                    break;
                }
            }
        }
        if (!broken)
            Log.d(TAG, " iterated through " + allInstitutesData.size() + " elements @ " + System.currentTimeMillis());

        filteredData.add(null);
        suggestionsLayout.removeAllViews();

        for (int i = 0; i < filteredData.size(); i++) {
            InstitutionData data = filteredData.get(i);
            TextView textView = new TextView(context);
            suggestionsLayout.addView(textView);
            suggestionsLayout.setVisibility(View.VISIBLE);
            textView.setBackgroundResource(R.drawable.institute_suggestion_background);
            textView.setTextColor(Color.parseColor("#FFFFFF"));
            textView.setTypeface(fontPicker.getMontserratRegular());
            ((LinearLayout.LayoutParams) textView.getLayoutParams()).leftMargin = AppLibrary.convertDpToPixels(context, 8);
            textView.setTag(data);
            if (data != null) {
                textView.setText(data.name);
            } else textView.setText("Unable to find?");
            textView.setOnClickListener(suggestionClickedListener);
        }
    }

    InstitutionData currentlySelectedData;

    private View.OnClickListener suggestionClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InstitutionData data = (InstitutionData) v.getTag();
            if (data != null) {
                currentlySelectedData = data;
                instituteSelectedTv.setText(data.name);
                toggleSelectionState(true);
            } else {
                saveUnableToFindInstituteInFireBase();
            }
        }
    };

    private void saveUnableToFindInstituteInFireBase() {
        //// TODO: 6/24/16
        ((OnBoardingActivity) getActivity()).onInstitutionSelectionComplete(false, -1, institutionET.getText().toString());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        institutionET.setVisibility(View.VISIBLE);
    }

    LinearLayout selectedLL;

    void toggleSelectionState(boolean selected) {


        selectedLL.setVisibility(selected ? View.VISIBLE : View.GONE);
        rootView.findViewById(R.id.nextPageTV).setVisibility(selected ? View.VISIBLE : View.GONE);

        institutionET.setVisibility(selected ? View.GONE : View.VISIBLE);
        suggestionsLayout.setVisibility(selected ? View.GONE : View.VISIBLE);
    }

    private ArrayList<InstitutionData> allInstitutesData = new ArrayList<>();

    class InstitutionData {
        int ID;
        String name;

        public InstitutionData(int ID, String name) {
            this.ID = ID;
            this.name = name;
        }
    }

    void generateDummyData() {
        for (int i = 0; i < 500; i++) {
            InstitutionData data = new InstitutionData(i, randomString(10));
            allInstitutesData.add(data);
        }
    }

    final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private SecureRandom rnd = new SecureRandom();

    private String randomString(final int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
            if (i == 5) sb.append(" ");
        }
        return sb.toString();
    }
}
