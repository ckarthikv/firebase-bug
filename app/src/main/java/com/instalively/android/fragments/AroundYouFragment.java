package com.instalively.android.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.instalively.android.R;
import com.instalively.android.adapters.HomeMomentAdapter;
import com.instalively.android.adapters.RecyclerViewClickInterface;
import com.instalively.android.apihandling.RequestManager;
import com.instalively.android.downloader.NearbyMomentDownloader;
import com.instalively.android.modelView.HomeMomentViewModel;
import com.instalively.android.models.MomentModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.util.AppLibrary;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by deepankur on 27/4/16.
 */
public class AroundYouFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private String TAG = getClass().getSimpleName();
    private boolean isDestroyed;
    private View rootView;
    private static Location knownLocation;
    private GoogleApiClient googleApiClient;
    private ProgressBar progressBar;
    private ViewControlsCallback viewControlsCallback;

    @Override
    public void onEvent(BroadCastSignals.BaseSignal eventSignal) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewControlsCallback = (ViewControlsCallback)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googleApiClient = new GoogleApiClient.Builder(context, connectionCallbacks, connectionFailedListener).addApi(LocationServices.API).build();
        Log.d(TAG, " onCreateCalled @ " + System.currentTimeMillis());
        NearbyMomentDownloader.setMomentDownloadListener(momentDownloadListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_around_you, container, false);
        this.rootView = rootView;
        this.progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        return rootView;
    }


    private void startLocationUpdate() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) { //Will check permissions in Marshmallow
            Log.e(TAG, "Request location permissions");{
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
//                    PERMISSION_ACCESS_COARSE_LOCATION);
                //not explicitly asking for
                hitApiOnHavingLocation(null, null);//hitting api although we don't have permissions itself
            }
        } else {
            if (knownLocation == null)
                knownLocation = getNewLocationFromFusedProvider();

            if (knownLocation != null) {
                double lat = knownLocation.getLatitude(), lon = knownLocation.getLongitude();
                Log.d(TAG, " getNewLocationFromFusedProvider: lat " + lat + " long " + lon);
                hitApiOnHavingLocation(lat, lon);
            } else {
                Log.d(TAG, "Unable to fetch location");
                hitApiOnHavingLocation(null, null);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResumeCalled");
    }


    public void notifyWholeMomentWatchedInCurrentSession(String watchedCompleteMomentId) {
        if (momentViewModels == null) {
            Log.e(TAG, "notifyCompleteMomentWatchedInThisSession failed momentViewModels null");
            return;
        }
        for (HomeMomentViewModel model : momentViewModels)
            if (model.momentId.equals(watchedCompleteMomentId)) {
                model.momentStatus = SEEN_MOMENT;
                momentViewModels.remove(model);//removing from wherever
                momentViewModels.add(model);//adding to last
                if (recyclerView != null && recyclerView.getAdapter() != null)
                    recyclerView.getAdapter().notifyDataSetChanged();
                return;
            }
        Log.e(TAG, "notifyCompleteMomentWatchedInThisSession: no moment found with supplied parameter");
    }

    @Override
    public void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.d(TAG, " onConnectionFailed " + connectionResult);
        }
    };
    private final short PERMISSION_ACCESS_COARSE_LOCATION = 32213;
    private GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Log.d(TAG, " onConnected " + bundle);
            startLocationUpdate();
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.d(TAG, " onConnectionSuspended " + i);

        }
    };

    private Location getNewLocationFromFusedProvider() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            Log.d(TAG, " getNewLocationFromFusedProvider " + lastLocation);
            return lastLocation;
        } else {
            Log.e(TAG, " getNewLocationFromFusedProvider failed");
            return null;
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        LinearLayoutManager layoutManager = new GridLayoutManager(context,2);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.nearbyRecycler);
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new HomeMomentAdapter(myUserId, context, momentViewModels, AROUND_YOU_MOMENT_RECYCLER, new RecyclerViewClickInterface() {
            @Override
            public void onItemClick(int extras, Object data) {
                HomeMomentViewModel model = (HomeMomentViewModel) data;
                if (model.clickType == HomeMomentViewModel.ClickType.SINGLE_TAP) {
                   // showShortToastMessege(model.momentId + " singleTapped  status: " + model.momentStatus);
                    if (model.momentStatus == UNSEEN_MOMENT) {
                        model.momentStatus = DOWNLOADING_MOMENT;
                        downloadMoment(model);
                    }
                    if (model.momentStatus == READY_TO_VIEW_MOMENT) {
                        //start activity
                        final MomentModel nearByMoment = NearbyMomentDownloader.getNearByMoment(model.momentId);
                        if (nearByMoment != null && nearByMoment.media != null) {
                            int totalMediaCount = nearByMoment.media.size();
                            int viewedCount = 0;
                            for (Map.Entry<String, MomentModel.Media> mediaEntry : nearByMoment.media.entrySet())
                                if (mediaEntry.getValue().status == MEDIA_VIEWED)
                                    viewedCount++;

//                            Intent mIntent = new Intent(context, ViewMomentActivity.class);
//                            mIntent.putExtra(AppLibrary.MOMENT_ID, model.momentId);
//                            mIntent.putExtra(AppLibrary.MOMENT_TYPE, "nearby");
                            if (viewedCount == totalMediaCount) {
//                                mIntent.putExtra(AppLibrary.MOMENT_STATUS, SEEN_MOMENT);
                                viewControlsCallback.onLoadViewMomentFragment(model.momentId,SEEN_MOMENT,"nearby");
                            } else {
//                                mIntent.putExtra(AppLibrary.MOMENT_STATUS, READY_TO_VIEW_MOMENT);
                                viewControlsCallback.onLoadViewMomentFragment(model.momentId,READY_TO_VIEW_MOMENT,"nearby");
                            }
//                            startActivity(mIntent);

                        }
                    }
                }
                if (model.clickType == HomeMomentViewModel.ClickType.DOUBLE_TAP) {
                    Log.d(TAG, " got double click on " + model.momentId);
                }
            }
        }));
    }


    private void downloadMoment(HomeMomentViewModel model) {
        String momentId = model.momentId;
        Log.d(TAG, " starting Download moment with id " + momentId);
        recyclerView.getAdapter().notifyDataSetChanged();

        if (aroundYouMap.get(momentId) != null) {
            if (NearbyMomentDownloader.isDownloaderReady(context)) {
                NearbyMomentDownloader downloader = NearbyMomentDownloader.getNearbyMediaDownloader(context, aroundYouMap.get(momentId));

                if (downloader.getDownloadState() == NearbyMomentDownloader.DownloaderState.LOADING_COMPLETE) {
                    Log.d(TAG, " already present in local data ");
                    recyclerView.getAdapter().notifyDataSetChanged();
                    model.momentStatus = READY_TO_VIEW_MOMENT;
                    return;
                }
                if (downloader.getDownloadState() == NearbyMomentDownloader.DownloaderState.LOADING_NOT_STARTED)
                    downloader.startNearByMomentDownload();
                else {
                    Log.e(TAG, " download already in progress for: " + model.momentId);
                }
            } else {//downloader not ready
                Log.e(TAG, " downloader not ready Queuing up");
                requestedMomentsToDownload.add(model);
            }
        } else {//linked map doesn't have the moment Queuing up
            Log.e(TAG, "nearbyMoments " + momentId + " not found in map; Queuing it");
            requestedMomentsToDownload.add(model);
        }

        if (model.momentStatus == READY_TO_VIEW_MOMENT || model.momentStatus == SEEN_MOMENT) {
            //OpenViewMoment Activity
        }
    }


    private ArrayList<HomeMomentViewModel> requestedMomentsToDownload = new ArrayList<>();

    private void checkAndDownloadQuedMoments() {
        Log.d(TAG, " pending list size " + requestedMomentsToDownload.size());

        if (requestedMomentsToDownload.size() > 0) {

            for (HomeMomentViewModel model : requestedMomentsToDownload) {
                if (aroundYouMap.get(model.momentId) != null) {//moment model exists
                    requestedMomentsToDownload.remove(model);
                    downloadMoment(model);
                }
            }
        } else Log.d(TAG, "onDownloaderReady requestedMomentsToDownload list empty");


    }


    final long API_CUT_OUT_TIME = 2000;

    @SuppressWarnings("deprecation")
    private void hitApiOnHavingLocation(Double lati, Double longi) {

        if (momentViewModels != null) {
            Log.w(TAG, " already have desired data; not hitting api again ");
            getMomentIdsFromFireBase();
            return;
        }

        if (!isInternetAvailable(false)) {
            Log.e(TAG, " not hitting api; no internet");
            getMomentIdsFromFireBase();
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getMomentIdsFromFireBase();
            }
        }, API_CUT_OUT_TIME);

        Log.d(TAG, "going to hit api @ " + System.currentTimeMillis());

        List<NameValuePair> pairs = new ArrayList<>();
        if (lati != null && longi != null) {
            pairs.add(new BasicNameValuePair("latitude", String.valueOf(lati)));
            pairs.add(new BasicNameValuePair("longitude", String.valueOf(longi)));
        }
        RequestManager.makePostRequest(context, RequestManager.MOMENTS_AROUND_YOU_REQUEST, RequestManager.MOMENTS_AROUND_YOU_RESPONSE,
                null, pairs, momentsAroundYouCallBack);
    }

    boolean gotDataFromApi;
    private static ArrayList<HomeMomentViewModel> momentViewModels;
    private RequestManager.OnRequestFinishCallback momentsAroundYouCallBack = new RequestManager.OnRequestFinishCallback() {
        @Override
        public void onBindParams(boolean success, Object response) {
            try {
                Log.d(TAG, " server says " + response);
                final JSONObject object = (JSONObject) response;
                if (success) {
                    if (gotDataFromApi && momentViewModels != null) {
                        Log.e(TAG, " already have data !! not listening to api callBack more than once, returning ");
                        return;
                    }
                    JSONArray momentArray = object.getJSONArray("value");
                    Log.d(TAG, "momentsAroundYouCallBack response " + momentArray);

                    writeMomentIdsToFireBase(momentArray);
                    gotDataFromApi = true;
                    Log.d(TAG, "got data from api @ " + System.currentTimeMillis());

                    getMomentIdsFromFireBase();

                } else {
                    Log.e(TAG, "momentsAroundYouCallBack Error, response -" + object);
                    // request failed
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "momentsAroundYouCallBack JsonException " + e);

            }
        }

        @Override
        public boolean isDestroyed() {
            return isDestroyed;
        }
    };

    private void writeMomentIdsToFireBase(JSONArray momentArray) throws JSONException {
        //clearing previous moments
        //  mFireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MOMENTS_AROUND_YOU_LOCAL_REF}).setValue(null);// TODO: 6/18/16

        DatabaseReference fireBase = mFireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MOMENTS_AROUND_YOU_LOCAL_REF});
        if (momentViewModels == null && momentArray.length() > 0)
            momentViewModels = new ArrayList<>();
        for (int i = 0; i < momentArray.length(); i++) {
            JSONObject momentObject = momentArray.getJSONObject(i);
            String momentId = momentObject.getString("_id");
            if (i == 0) momentId = "56f146d32ebd397811a290bd";
//            if (i == 1) momentId = "56f146d32ebd397811a290bd";
            fireBase.child(momentId).child("name").setValue(momentObject.getString("name"));
            fireBase.child(momentId).child(MOMENT_ID).setValue(momentId);
            fireBase.child(momentId).child("imageUrl").setValue(momentObject.getString("thumbnail"));
        }
    }

    boolean loadedMomentIdsFromFireBase;

    private void getMomentIdsFromFireBase() {
        if (loadedMomentIdsFromFireBase) {
            Log.d(TAG, "loadedMomentIdsFromFireBase; returning");
            return;
        }
        loadedMomentIdsFromFireBase = true;
        Log.d(TAG, "loadedMomentIdsFromFireBase");
        mFireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MOMENTS_AROUND_YOU_LOCAL_REF}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange " + dataSnapshot);
                if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                    Log.e(TAG, "getMomentIdsFromFireBase failed, dataSnapshot null ");
                    rootView.setVisibility(View.GONE);
                    return;
                }
                if (momentViewModels != null) momentViewModels.clear();
                for (DataSnapshot momentSnap : dataSnapshot.getChildren()) {
                    HomeMomentViewModel model = new HomeMomentViewModel();
                    model.momentId = momentSnap.getKey();
                    model.name = momentSnap.child("name").getValue(String.class);
                    model.imageUrl = momentSnap.child("imageUrl").getValue(String.class);
                    model.imageUrl = momentSnap.child("imageUrl").getValue(String.class);
                    model.momentStatus = UNSEEN_MOMENT;//unseen moment w.r.t. current session
                    if (momentViewModels == null) momentViewModels = new ArrayList<>();
                    momentViewModels.add(model);
                    loadMomentsDataFromFireBase(model.momentId);
                }
                initRecyclerView();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "OnCancelled " + databaseError);
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
    }

    private LinkedHashMap<String, MomentModel> aroundYouMap = new LinkedHashMap<>();
    private HashMap<String, MomentModel.Media> expiredMediaMap = new HashMap<>();


    public void loadMomentsDataFromFireBase(final String momentId) {
        Log.d(TAG, " load moment data form fireBase " + momentId);
        DatabaseReference mFireBase = mFireBaseHelper.getNewFireBase(ANCHOR_MOMENTS, new String[]{momentId});
        mFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                    Log.e(TAG, " null moment found in fireBase for id" + momentId + "returning");
                    return;
                }
                Log.d(TAG, " loaded !!!! moment data form fireBase " + momentId);
                int momentStatus = SEEN_MOMENT;
                LinkedHashMap<String, MomentModel.Media> medias = new LinkedHashMap<>();
                for (DataSnapshot media : dataSnapshot.child(MEDIA).getChildren()) {
                    MomentModel.Media mediaObj = media.getValue(MomentModel.Media.class);
                    mediaObj.mediaId = media.getKey();
                    mediaObj.momentId = momentId;
                    boolean isExpired = isExpiredMedia(media);
                    if (isExpired) {
                        if (expiredMediaMap == null)
                            expiredMediaMap = new HashMap<>();
                        expiredMediaMap.put(media.getKey(), mediaObj);
                    } else {
                        medias.put(media.getKey(), mediaObj);
                        if (!media.child(MEDIA_VIEWS_USERS).hasChild(myUserId)) {
                            momentStatus = UNSEEN_MOMENT;
                        }
                    }
                }

                MomentModel momentFinal = dataSnapshot.getValue(MomentModel.class);
//                if (false)//todo
                    momentFinal.media = medias;
                momentFinal.momentStatus = momentStatus;
                momentFinal.momentId = dataSnapshot.getKey();
                if (aroundYouMap == null) {
                    aroundYouMap = new LinkedHashMap<>();
                }
                momentFinal.momentId = dataSnapshot.getKey();
                aroundYouMap.put(dataSnapshot.getKey(), momentFinal);
                {//preparing moments before the first click
                    if (NearbyMomentDownloader.isDownloaderReady(context))
                        NearbyMomentDownloader.getNearbyMediaDownloader(context, aroundYouMap.get(momentId));
                    else Log.e(TAG, " downloader not ready ");
                }

                checkAndDownloadQuedMoments();
   /*             saveMoment(momentFinal, userId);
                if (mMoments.size() == finalMomentIndex) {
                    cleanExpiredMedias();
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public boolean isExpiredMedia(DataSnapshot media) {
        long now = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
        Long dtStart = (Long) media.child(CREATED_AT).getValue();
        if (dtStart == null) {
            Log.e(TAG, " crashing on  " + media);
            return true;
        }
        if (now > dtStart) {
            return true;
        }
        return false;
    }

    private NearbyMomentDownloader.NearbyMomentDownloadListener momentDownloadListener = new NearbyMomentDownloader.NearbyMomentDownloadListener() {
        @Override
        public void onMomentDownloaded(String momentId) {
            Log.d(TAG, "onMomentDownloaded " + momentId);
            for (HomeMomentViewModel model : momentViewModels)
                if (momentId.equals(model.momentId)) {
                    model.momentStatus = READY_TO_VIEW_MOMENT;
                    recyclerView.getAdapter().notifyDataSetChanged();
                    Log.w(TAG, " remove multiple loaded calls");//// TODO: 6/15/16
                    break;
                }
        }

        @Override
        public void onDownloaderReady() {
            Log.d(TAG, " onDownloaderReady");
            checkAndDownloadQuedMoments();
        }
    };

    private void writeNewMediaToFireBase(String mediaId, String momentId, String url) {
        DatabaseReference mediaDownloadReference = mFireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{MEDIA_DOWNLOAD, mediaId});
        mediaDownloadReference.child(MOMENT_ID).setValue(momentId);
        mediaDownloadReference.child(DOWNLOAD_STATUS).setValue(MEDIA_DOWNLOAD_NOT_STARTED);
        mediaDownloadReference.child(URL).setValue(url);
    }


    void cleanExpiredMedias() {
        if (expiredMediaMap == null) {
            Log.d(TAG, " cleanExpiredMedias() failed ; mExpiredMomentMedias null");
            return;
        }
        for (Map.Entry<String, MomentModel.Media> media : expiredMediaMap.entrySet()) {
            mFireBaseHelper.cleanMedia(expiredMediaMap.get(media.getKey()).mediaId);
        }
    }

    public interface ViewControlsCallback{
        void onLoadViewMomentFragment(String momentId,int momentStatus,String momentType);
    }
}
