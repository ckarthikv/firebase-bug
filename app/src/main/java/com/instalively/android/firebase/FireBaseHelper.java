package com.instalively.android.firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.instalively.android.downloader.MediaDownloader;
import com.instalively.android.downloader.NearbyMomentDownloader;
import com.instalively.android.fragments.BaseFragment;
import com.instalively.android.modelView.ChatMediaModel;
import com.instalively.android.modelView.HomeMomentViewModel;
import com.instalively.android.modelView.ListRoomView;
import com.instalively.android.modelView.MediaModelView;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.models.MediaModel;
import com.instalively.android.models.MomentModel;
import com.instalively.android.models.NotificationContent;
import com.instalively.android.models.RoomsModel;
import com.instalively.android.models.SettingsModel;
import com.instalively.android.models.SocialModel;
import com.instalively.android.models.UserModel;
import com.instalively.android.util.AppLibrary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by deepankur on 23/3/16.
 */
public class FireBaseHelper implements FireBaseKEYIDS {

//        public static String myUserId = "56f1458d8023876511e4d96f";//farhan
        public static String myUserId = null;
//    public static String myUserId = "56f150db54108da8110fe627";//deepankur
    private static FireBaseHelper mFireBaseHelper;
    private LinkedHashMap<String, RoomsModel> mMomentRoomHashMap;
    private LinkedHashMap<String, RoomsModel> mMessageRoomHashMap;
    private HashMap<String, UserModel.Rooms> mExpiredMomentRooms;
    public LinkedHashMap<String, MomentModel> mMoments;
    private DatabaseReference mFireBase;
    private String TAG = this.getClass().getSimpleName();
    private double serverOffsetTime;
    private static UserModel mUserModel;
    private static SocialModel mSocialModel;
    private static Context mContext;
    private HashMap<String, RoomsModel> mFriendRoomHashMap;
    private ArrayList<UserModel> mFacebookSuggestedFriendList;
    private HashMap<String, UserModel> mFacebookSuggestedMap;
    private MediaModelCallback mediaModelCallback;
    private MomentCallback momentCallback;
    private RoomDataLoadedCallbacks roomDataLoadedListener;
    private CustomFriendListCallBack customFriendListCallback;
    private OnMyStreamsLoaded onMyStreamsLoadedCallback;
    private onUploadStatusChanges onUploadStatusChangesCallback;
    private onMyMomentMediaDownloadStatusModified onMyMomentMediaDownloadStatusModified;
    public MomentModel myMoment;
    public LinkedHashMap<String,MediaModel> pendingMediaInMyMoments = new LinkedHashMap<>();
    public LinkedHashMap<String,MediaModel> pendingMediaInMessageRooms = new LinkedHashMap<>();
    LinkedHashMap<String,MediaModelView> myMedias = new LinkedHashMap();

    public void setFireBaseReadyListener(FireBaseReadyListener fireBaseReadyListener) {
        FireBaseHelper.fireBaseReadyListener = fireBaseReadyListener;
        fireBaseReadyListener.onDataLoaded(getSocialModel(),getMyUserModel());
    }

    public double getServerOffsetTime() {
        return this.serverOffsetTime;
    }

    private FireBaseHelper(Context context) {
        this.mContext = context;
        if (!FirebaseApp.getApps(context).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
//        getServerOffset();
        if (mFireBase == null)
            mFireBase = FirebaseDatabase.getInstance().getReferenceFromUrl(FireBaseKEYIDS.fireBaseURL);
        if (getMyUserId() == null) return;
        mFireBase.child(ANCHOR_SOCIALS).child(getMyUserId()).keepSynced(true);
        mFireBase.child(ANCHOR_USERS).child(getMyUserId()).keepSynced(true);
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).keepSynced(true);
        mFireBase.child(ANCHOR_STICKERS).keepSynced(true);
        mFireBase.child(ANCHOR_STICKER_CATEGORY).keepSynced(true);
        if (!AppLibrary.PRODUCTION_MODE)
           mFireBase.child("templates").keepSynced(true);//only for faster debugging  remove this line in production // TODO: 6/21/16
        checkNetworkAvailability();
        loadData();
        cleanData(getMyUserId());
    }

    static FireBaseReadyListener fireBaseReadyListener;

    public interface FireBaseReadyListener{
        void onDataLoaded(SocialModel socialModel,UserModel userModel);
    }

    private void checkNetworkAvailability(){
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Log.d(TAG, "checkNetworkAvailability connected @ "+System.currentTimeMillis());
                } else {
                    Log.d(TAG, "checkNetworkAvailability disconnected @ "+System.currentTimeMillis());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    public static synchronized FireBaseHelper getInstance(Context context) {
        String TAG="FireBaseHelper";
        if (mFireBaseHelper == null) {
            mFireBaseHelper = new FireBaseHelper(context.getApplicationContext());
            Log.i(TAG,"initialized fireBase ");
                   Log.d(TAG, BaseFragment.buildStackTraceString(Thread.currentThread().getStackTrace()));

        }
        return mFireBaseHelper;
    }

    private void loadMySocialModel(String myUserId) {
//        mSocialModel = new SocialModel();
        DatabaseReference socialFireBase = this.getNewFireBase(ANCHOR_SOCIALS, new String[]{myUserId});
        socialFireBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mSocialModel = dataSnapshot.getValue(SocialModel.class);
                if (mSocialModel != null)
                    getFriendList();
                else Log.e(TAG," mSocialModel null ");
                if (fireBaseReadyListener != null)
                    fireBaseReadyListener .onDataLoaded(mSocialModel,mUserModel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public SocialModel.Friends[] getFriendList() {
        if (mSocialModel.friends == null) {
            Log.d(TAG, " user " + getMyUserId() + " has no friends");
            return null;
        }
        HashMap<String, SocialModel.Friends> friendsHashMap = mSocialModel.friends;
        String[] friendKeys = friendsHashMap.keySet().toArray(new String[friendsHashMap.size()]);
        SocialModel.Friends[] friendList = new SocialModel.Friends[friendKeys.length];
        for (int i = 0; i < friendKeys.length; i++) {
            friendList[i] = friendsHashMap.get(friendKeys[i]);
            friendList[i].friendId = friendKeys[i];
        }
        return friendList;
    }

    private void loadFriendSuggestions() {
        final String TAG = this.TAG + "loadFriendSuggestions";
        DatabaseReference userFireBase = this.getNewFireBase(ANCHOR_SOCIALS, new String[]{getMyUserId(), FACEBOOK_FRIENDS});
        userFireBase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                userModel.KEY_UID = dataSnapshot.getKey();
                Log.d(TAG, " onChildAdded " + userModel);
                if (mFacebookSuggestedMap == null)
                    mFacebookSuggestedMap = new HashMap<>();
                mFacebookSuggestedMap.put(dataSnapshot.getKey(), userModel);

                if (mFacebookSuggestedFriendList == null)
                    mFacebookSuggestedFriendList = new ArrayList<>();

                mFacebookSuggestedFriendList.add(userModel);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, " onChildChanged " + dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.e(TAG, " onChildRemoved " + dataSnapshot);
                String key = dataSnapshot.getKey();
                UserModel userModel = mFacebookSuggestedMap.get(key);
                mFacebookSuggestedFriendList.remove(userModel);

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, s + "<<-string onChildMoved " + dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, " onCancelled error--> " + databaseError);
            }

        });
    }

    public ArrayList<UserModel> getSuggestedFriends() {
        return mFacebookSuggestedFriendList;
    }

    public SocialModel getSocialModel() {
        return mSocialModel;
    }

    public UserModel getMyUserModel() {
        return mUserModel;
    }

    /**
     * @param anchor   the outer  FireBase node ie. socials,moments,users,media
     * @param children subsequent nodes to query upon; supply null If you need the anchor nodes.
     *                 instead of children
     * @return the resolved FireBase
     */

    public DatabaseReference getNewFireBase(String anchor, String[] children) {
        if (mFireBase == null)
            mFireBase = FirebaseDatabase.getInstance().getReference();
        if (anchor == null) throw new NullPointerException("anchor cannot be null");
        DatabaseReference fireBase = this.mFireBase.child(anchor);
        if (children != null && children.length > 0) {
            for (int i = 0; i < children.length; i++) {
                fireBase = fireBase.child(children[i]);
            }
        }
        return fireBase;
    }

    public String getMyUserId() {
        if (myUserId == null) {
            //Start Login Activity;
        }
        return FireBaseHelper.myUserId;
    }

    public void setMyUserId(String userId) {
        myUserId = userId;
    }


    public long getUpdatedAt() {
        return ((long) (System.currentTimeMillis() + serverOffsetTime));
    }

    private void loadAllFriendRooms() {
        mFriendRoomHashMap = new HashMap<>();
        if (getMyUserModel().messageRooms == null) return;
        final HashMap<String, UserModel.Rooms> messageRoomsHashMap = getMyUserModel().messageRooms;
        String[] roomKeys = messageRoomsHashMap.keySet().toArray(new String[messageRoomsHashMap.size()]);
        for (int i = 0; i < roomKeys.length; i++) {
            DatabaseReference roomFireBase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomKeys[i]});
            roomFireBase.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (mFriendRoomHashMap == null)
                        mFriendRoomHashMap.put(dataSnapshot.getKey(), dataSnapshot.getValue(RoomsModel.class));
                    Log.d(TAG, " loadAllFriendRooms onChildAdded" + dataSnapshot.getKey());
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    Log.d(TAG, " loadAllFriendRooms onChildChanged " + dataSnapshot.getKey());
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Log.d(TAG, " loadAllFriendRooms onChildRemoved " + dataSnapshot);

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    Log.d(TAG, " loadAllFriendRooms onChildMoved " + dataSnapshot);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, " loadAllFriendRooms onCancelled " + databaseError);
                }
            });
        }
    }

    private long getPriorityTimeStamp() {
        return System.currentTimeMillis() + (long) serverOffsetTime;
    }

    long getCreatedAt() {
        return System.currentTimeMillis();
    }

    private void loadMyUserModel() {
        DatabaseReference fireBase = this.getNewFireBase(ANCHOR_USERS, new String[]{getMyUserId()});
        fireBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUserModel = dataSnapshot.getValue(UserModel.class);
                if (mUserModel!=null) {
                    mUserModel.KEY_UID = dataSnapshot.getKey();
                if (fireBaseReadyListener  != null)
                    fireBaseReadyListener .onDataLoaded(mSocialModel,mUserModel);
                if (mFriendRoomHashMap == null )
                    loadAllFriendRooms();
                loadMyMoment();
                } else Log.e(TAG,"userModel is null ");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * 1. Send Friend Request
     * STEP 1:
     * UPDATE socials/<reqUser._id>/requestSent with key <user._id> and value as 1
     * <p/>
     * STEP 2:
     * UPDATE socials/<user._id>/requestRecieved with key <reqUser._id> and value
     * as <reqUser> details
     * <p/>
     * STEP 3:
     * Check and remove socials/<reqUser._id>/requestIgnored/<user._id>
     * Check and remove socials/<reqUser._id>/facebookFriends/<user._id>
     *
     * @param reqUserId
     * @param userId
     */
    public void sendFriendRequest(String reqUserId, String userId) {
        Log.d(TAG, " sending Friend Request From: " + reqUserId + " to " + userId);
        //STEP 1
        DatabaseReference socialAnchorFireBase = getNewFireBase(ANCHOR_SOCIALS, null);
        socialAnchorFireBase.child(reqUserId).child(REQUEST_SENT).child(userId).setValue(1);

        //STEP 2
        SocialModel.RequestReceived requestReceived = new SocialModel.RequestReceived(mUserModel.name,
                mUserModel.handle, mUserModel.imageUrl, mUserModel.momentId, getUpdatedAt());
        socialAnchorFireBase.child(userId).child(REQUEST_RECEIVED).child(reqUserId).setValue(requestReceived);

        //STEP 3
        socialAnchorFireBase.child(reqUserId).child(REQUEST_IGNORED).child(userId).setValue(null);
        this.getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUserId, FACEBOOK_FRIENDS, userId}).setValue(null);
        this.getNewFireBase(ANCHOR_SOCIALS, new String[]{userId, FACEBOOK_FRIENDS, reqUserId}).setValue(null);
        NotificationContent content = new NotificationContent(reqUserId, getMyUserModel().name, userId, null, null, null, getMyUserModel().imageUrl, SEND_FRIEND_REQUEST, null, null);
        sendNotification(content);
    }

    /**
     * 2. List Friend Requests
     * INPUT : reqUser
     * STEP 1:
     * get value of key socials/<reqUser._id>/requestRecieved
     */
    public SocialModel.RequestReceived[] getFriendRequestList(String reqUser) {
        HashMap<String, SocialModel.RequestReceived> friendsRequestMap = getSocialModel().requestRecieved;
        String[] keys = friendsRequestMap.keySet().toArray(new String[friendsRequestMap.size()]);
        SocialModel.RequestReceived[] requests = new SocialModel.RequestReceived[keys.length];
        for (int i = 0; i < keys.length; i++) {
            requests[i] = friendsRequestMap.get(keys[i]);
        }
        return requests;
    }

    public HashMap<String, SocialModel.RequestReceived> getFriendRequestMap() {
        if (getSocialModel() == null) {
            Log.d(TAG, " getFriendRequestMap: socialModel is null, returning");
            return null;
        }
        return getSocialModel().requestRecieved;
    }

    /**
     * 3. Accept Friend Request
     * INPUT : reqUser, user
     * STEP 1: Create Friend Room
     * PUSH roomDetail with type 0 and each members as
     * <p/>
     * <userID> : {
     * name: String,
     * image: String,
     * handle: String,
     * momentId : String // in case of friend
     * type: 0 // screenshot etc
     * }
     * <p/>
     * STEP 2: Add each user to others friend list
     * STEP 2.1
     * UPDATE socials/<reqUser._id>/friends with user detail including roomKey
     * STEP 2.2
     * UPDATE socials/<user._id>/friends with reqUser detail including roomKey
     * <p/>
     * STEP 3: Add room ref to each user messageRooms child
     * - UPDATE users/<reqUser._id>/messageRooms with key as roomId and value
     * type: 0,
     * updatedAt: Date,
     * interacted: false
     * <p/>
     * - UPDATE users/<user._id>/messageRooms with key as roomId and value as JSON
     * type: 0,
     * updatedAt: Date,
     * interacted: true
     * STEP 4:
     * Set Priority with (-ve)timestamp for  users/<user._id>/messageRooms/<roomId>
     */
    //todo remove from facebook fromView viewItself dynamically
    public void acceptFriendRequest(String reqUserId, String userId) {
        this.getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUserId, FACEBOOK_FRIENDS, userId}).setValue(null);
        //STEP 1
        SocialModel.RequestReceived friendRequester = getFriendRequestMap().get(userId);
        RoomsModel roomsModel = new RoomsModel();
        //  name,   imageUrl,   handle,   lastSeenMessageId,   momentId, int msgCount, int momentCount,int type) {
        RoomsModel.Members roomMember1 = new RoomsModel.Members(
                mUserModel.name,
                mUserModel.imageUrl,
                mUserModel.handle,
                null,           //lastSeenMessageId
                mUserModel.momentId,
                0,              //msgCount
                0,              //momentCout
                0);             //type
        RoomsModel.Members roomMember2 = new RoomsModel.Members(
                friendRequester.name,
                friendRequester.imageUrl,
                friendRequester.handle,
                null,
                friendRequester.momentId,
                0,
                0,
                0);
        if (roomsModel.members == null)
            roomsModel.members = new HashMap<>();
        roomsModel.members.put(reqUserId, roomMember1);
        roomsModel.members.put(userId, roomMember2);

        roomsModel.type = FRIEND_ROOM;

        DatabaseReference roomAnchorFireBase = this.getNewFireBase(ANCHOR_ROOMS, null);
        DatabaseReference room = roomAnchorFireBase.push();
        room.setValue(roomsModel);
        String roomId = room.getKey();

        //STEP 2.1
        //friend1 is userItself
        SocialModel.Friends friend1 = new SocialModel.Friends(
                mUserModel.name,
                null,
                mUserModel.handle,
                mUserModel.imageUrl,
                mUserModel.momentId,
                getUpdatedAt(),
                roomId);

        //friend2 is the users friend
        SocialModel.Friends friend2 = new SocialModel.Friends(
                friendRequester.name,
                null,
                friendRequester.handle,
                friendRequester.imageUrl,
                friendRequester.momentId,
                getUpdatedAt(),
                roomId
        );
        DatabaseReference socialAnchorFireBase = this.getNewFireBase(ANCHOR_SOCIALS, null);
        socialAnchorFireBase.child(reqUserId).child(FRIENDS).child(userId).setValue(friend2);
        socialAnchorFireBase.child(userId).child(FRIENDS).child(reqUserId).setValue(friend1);

        //STEP 3
        DatabaseReference userAnchorFireBase = this.getNewFireBase(ANCHOR_USERS, null);
        userAnchorFireBase.child(reqUserId).child(MESSAGE_ROOM).child(roomId).setValue(new UserModel.Rooms(0, getUpdatedAt(), false));
        userAnchorFireBase.child(userId).child(MESSAGE_ROOM).child(roomId).setValue(new UserModel.Rooms(0, getUpdatedAt(), true));


        //  STEP 4:
        userAnchorFireBase.child(reqUserId).child(MESSAGE_ROOM).child(roomId).setPriority(-getPriorityTimeStamp());
        userAnchorFireBase.child(userId).child(MESSAGE_ROOM).child(roomId).setPriority(-getPriorityTimeStamp());

        //delete received Req.
        socialAnchorFireBase.child(reqUserId).child(REQUEST_RECEIVED).child(userId).setValue(null);
        NotificationContent content = new NotificationContent(reqUserId, getMyUserModel().name, userId, null, roomId, null, getMyUserModel().imageUrl, ACCEPT_FRIEND_REQUEST, null, null);
        sendNotification(content);
    }

    public void ignoreFriendRequest(String reqUserId, String userId) {
        DatabaseReference socialFireBase = this.getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUserId});
        socialFireBase.child(REQUEST_RECEIVED).child(userId).setValue(null);
        socialFireBase.child(REQUEST_IGNORED).child(userId).setValue(1);
    }

    public void removeOrBlockFriend(@Nullable String roomId, String reqUserId, String userId, boolean blockAlso) {
        //STEP 1
        if (roomId == null)
            roomId = getSocialModel().friends.get(userId).roomId;

        //STEP 2
        DatabaseReference socialAnchorFireBase = this.getNewFireBase(ANCHOR_SOCIALS, null);
        socialAnchorFireBase.child(reqUserId).child(FRIENDS).child(userId).setValue(null);
        socialAnchorFireBase.child(userId).child(FRIENDS).child(reqUserId).setValue(null);
        //     2.3
        DatabaseReference userAnchorFireBase = this.getNewFireBase(ANCHOR_USERS, null);
        userAnchorFireBase.child(reqUserId).child(MESSAGE_ROOM).child(roomId).setValue(null);
        userAnchorFireBase.child(reqUserId).child(MOMENT_ROOM).child(roomId).setValue(null);
        //     2.4
        userAnchorFireBase.child(userId).child(MESSAGE_ROOM).child(roomId).setValue(null);
        userAnchorFireBase.child(userId).child(MOMENT_ROOM).child(roomId).setValue(null);
        //STEP 3
        DatabaseReference roomAnchorFireBase = this.getNewFireBase(ANCHOR_ROOMS, null);
        roomAnchorFireBase.child(roomId).setValue(null);
        //STEP 4

        if (!blockAlso) return;
        userAnchorFireBase.child(reqUserId).child(BLOCKED_FRIEND).child(userId).setValue(1);
    }

    public void addFavorite(String reqUserId, String userId, UserModel favoriteModel) {

        //STEP 1
        DatabaseReference userFireBase = this.getNewFireBase(ANCHOR_USERS, new String[]{reqUserId});
        //        userFireBase.child(FAVORITES).child(userId)
        SocialModel.Favourites favourites = new SocialModel.Favourites(
                favoriteModel.name,
                favoriteModel.handle,
                favoriteModel.imageUrl,
                favoriteModel.momentId,
                getUpdatedAt()
        );
        userFireBase.child(FAVORITES).child(userId).setValue(favoriteModel);
        //STEP 2
        int favorited = getMyUserModel().favourited;
        ++favorited;
        userFireBase.child(FAVOURITED).setValue(favorited);
        /**
         *
         * STEP 3:
         * PUSH roomDetail with type 2 and members details
         */
        RoomsModel.Members roomMemberFav = new RoomsModel.Members(
                favoriteModel.name,
                favoriteModel.imageUrl,
                favoriteModel.handle,
                null,
                favoriteModel.momentId,
                0,
                0,
                0
        );

        UserModel myUserModel = getMyUserModel();
        RoomsModel.Members roomMemberMe = new RoomsModel.Members(favoriteModel.name,
                myUserModel.imageUrl,
                myUserModel.handle,
                null,
                myUserModel.momentId,
                0,
                0,
                0);


        HashMap<String, RoomsModel.Members> membersMap = new HashMap<>();
        membersMap.put(reqUserId, roomMemberMe);
        membersMap.put(userId, roomMemberFav);

        RoomsModel roomsModel = new RoomsModel(FAVORITE_ROOM, membersMap, null, null, null);

        DatabaseReference roomAnchorFireBase = this.getNewFireBase(ANCHOR_ROOMS, null);
        DatabaseReference newFireBase = roomAnchorFireBase.push();
        newFireBase.setValue(roomsModel);
        String pushedRoomId = newFireBase.getKey();

        //STEp 4
        DatabaseReference userAnchorFireBase = this.getNewFireBase(ANCHOR_USERS, null);
        UserModel.Rooms userRooms = new UserModel.Rooms(FAVORITE_ROOM, getUpdatedAt(), false);
        userAnchorFireBase.child(reqUserId).child(MESSAGE_ROOM).child(pushedRoomId).setValue(userRooms);
        userAnchorFireBase.child(userId).child(MESSAGE_ROOM).child(pushedRoomId).setValue(userRooms);
    }

    public void removeFavourite(String reqUserId, String userId, String roomId) {
        //STEP 1
        this.getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUserId, FAVORITES, userId}).setValue(null);

        //STEP 2
        final DatabaseReference favouritedFireBase = this.getNewFireBase(ANCHOR_USERS, new String[]{userId, FAVOURITED});
        favouritedFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int favourited = dataSnapshot.getValue(Integer.class);
                favouritedFireBase.setValue(--favourited);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //STEP 3

        DatabaseReference userAnchorFireBase = this.getNewFireBase(ANCHOR_USERS, null);
        userAnchorFireBase.child(reqUserId).child(MESSAGE_ROOM).child(roomId).setValue(null);
        userAnchorFireBase.child(reqUserId).child(MOMENT_ROOM).child(roomId).setValue(null);

        userAnchorFireBase.child(userId).child(MESSAGE_ROOM).child(roomId).setValue(null);
        userAnchorFireBase.child(userId).child(MOMENT_ROOM).child(roomId).setValue(null);

        //STEP 4
//        STEP 4:
//        Remove room ref rooms/<roomId>
        DatabaseReference roomAnchorFireBase = this.getNewFireBase(ANCHOR_ROOMS, null);
        roomAnchorFireBase.child(roomId).setValue(null);
    }

    /**
     * 10. Send Message To Friend
     * INPUT: reqUser, user, roomId, messageDetail
     * <p/>
     * STEP 1:
     * PUSH message Details rooms/<roomId>/messages
     * STEP 2:
     * STEP 2.1
     * IF msgCount is 0
     * INCREMENT rooms/<roomId>/members/<user._id>/msgCount
     * STEP 2.2
     * UPDATE rooms/<roomId>/members/<user._id>/type
     * UPDATE rooms/<roomId>/members/<reqUser._id>/type
     * STEP 3:
     * UPDATE users/<user._id>/messageRooms/<roomId>/updatedAt
     * UPDATE users/<user._id>/messageRooms/<roomId>/interacted as true
     * UPDATE users/<user._id>/messageRooms/<roomId>/deletionChecked as false
     * <p/>
     * STEP 4:
     * Set Priority of messageRooms with -ve (timestamp)
     * PATH is  : users/<userId>/messageRooms/<roomId>
     */
    public void sendMessageToFriend(final String reqUserId, final String userId, final String roomId, final RoomsModel.Messages message, final int messageType, String mediaId, String mediaUrl) {
        AppLibrary.log_d(TAG, "Sending Message To Friend-> " + userId);
        DatabaseReference roomFireBase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES});
        DatabaseReference pushedMessageFB = roomFireBase.push();
        pushedMessageFB.setValue(message);
        pushedMessageFB.setPriority(getPriorityTimeStamp());
        this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, userId, MESSAGE_COUNT}).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    mutableData.setValue(1);
                } else {
                    mutableData.setValue((Long) mutableData.getValue() + 1);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                Log.d(TAG, " sendFriendRequest onCompletion listener triggers ds " + dataSnapshot);
            }
        });
        //STEP 2.2
        getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, userId, MESSAGE_TYPE}).setValue(messageType);
        if (messageType == NEW_MEDIA) {
            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, reqUserId, MESSAGE_TYPE}).setValue(SENT_MEDIA);
        } else {
            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, reqUserId, MESSAGE_TYPE}).setValue(SENT_CHAT);
        }
        UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
        userRoom.type = FRIEND_ROOM;
        DatabaseReference receiverMessageReference = getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM, roomId});
        receiverMessageReference.setValue(userRoom);
        receiverMessageReference.setPriority(-getPriorityTimeStamp());
        DatabaseReference senderMessageReference = getNewFireBase(ANCHOR_USERS, new String[]{reqUserId, MESSAGE_ROOM, roomId});
        senderMessageReference.setValue(userRoom);
        senderMessageReference.setPriority(-getPriorityTimeStamp());
        NotificationContent content = new NotificationContent(reqUserId, getMyUserModel().name, userId, pushedMessageFB.getKey(), roomId, null, getMyUserModel().imageUrl, NEW_MESSAGE_REQUEST, mediaId, mediaUrl);
        sendNotification(content);
    }

    public void createMediaMessageForFriend(String roomId, RoomsModel.Messages message, String userId) {
        DatabaseReference roomFireBase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES});
        DatabaseReference pushedMessageFB = roomFireBase.push();
        pushedMessageFB.setValue(message);
        pushedMessageFB.setPriority(getPriorityTimeStamp());
        this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, userId, MESSAGE_COUNT}).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    mutableData.setValue(1);
                } else {
                    mutableData.setValue((Long) mutableData.getValue() + 1);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                Log.d(TAG, " sendFriendRequest onCompletion listener triggers ds " + dataSnapshot);
            }
        });
        UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
        userRoom.type = FRIEND_ROOM;
        DatabaseReference senderFirebase = getNewFireBase(ANCHOR_USERS, new String[]{getMyUserId(), MESSAGE_ROOM, roomId});
        senderFirebase.setValue(userRoom);
        senderFirebase.setPriority(-getPriorityTimeStamp());
    }

    public void updateMediaMessageRoomAndSendNotificationForFriend(String reqUserId, String roomId, String userId, int messageType, String messageId, String mediaId, String mediaUrl) {
        getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, userId, MESSAGE_TYPE}).setValue(messageType);
        if (messageType == NEW_MEDIA) {
            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, reqUserId, MESSAGE_TYPE}).setValue(SENT_MEDIA);
        } else {
            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, reqUserId, MESSAGE_TYPE}).setValue(SENT_CHAT);
        }
        UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
        userRoom.type = FRIEND_ROOM;
        getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM, roomId}).setValue(userRoom);
        getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM, roomId}).setPriority(-getPriorityTimeStamp());
        NotificationContent content = new NotificationContent(reqUserId, getMyUserModel().name, userId, messageId, roomId, null, getMyUserModel().imageUrl, NEW_MESSAGE_REQUEST, mediaId, mediaUrl);
        sendNotification(content);
    }

    public void updateMediaMessageRoomAndSendNotificationForGroup(final String reqUserId, final String roomId, final int messageType, final String messageId, final String mediaId, final String mediaUrl) {
        DatabaseReference firebaseRooms = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId});
        firebaseRooms.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                HashMap<String, RoomsModel.Members> membersHashMap = roomsModel.members;
                String sendTo = null;
                if (membersHashMap != null) {
                    final String[] membersKey = membersHashMap.keySet().toArray(new String[membersHashMap.size()]);
                    for (int i = 0; i < membersKey.length; i++) {
                        if (!membersKey[i].equals(reqUserId)) {
                            sendTo += membersKey[i] + ",";
                            final DatabaseReference roomMemberFireBase = getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS});
                            final int finalI = i;
                            roomMemberFireBase.child(membersKey[i]).child(MESSAGE_COUNT).runTransaction(new Transaction.Handler() {
                                @Override
                                public Transaction.Result doTransaction(MutableData mutableData) {
                                    if (mutableData.getValue() == null) {
                                        mutableData.setValue(1);
                                    } else {
                                        mutableData.setValue((Long) mutableData.getValue() + 1);
                                    }
                                    return Transaction.success(mutableData);
                                }

                                @Override
                                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                }
                            });

                            roomMemberFireBase.child(membersKey[finalI]).child(ROOM_TYPE).setValue(messageType);
                            UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
                            userRoom.type = GROUP_ROOM;
                            DatabaseReference receiverFirebase = getNewFireBase(ANCHOR_USERS, new String[]{membersKey[finalI], MESSAGE_ROOM, roomId});
                            receiverFirebase.setValue(userRoom);
                            receiverFirebase.setPriority(-getPriorityTimeStamp());
                        }
                    }
                }
                NotificationContent content = new NotificationContent(reqUserId, getMyUserModel().name, sendTo, messageId, roomId, null, getMyUserModel().imageUrl, NEW_MESSAGE_REQUEST, mediaId, mediaUrl);
                sendNotification(content);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void createMediaMessageForGroup(String roomId, RoomsModel.Messages message) {
        final DatabaseReference pushedMessageFireBase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES}).push();
        pushedMessageFireBase.setValue(message);
        pushedMessageFireBase.setPriority(getPriorityTimeStamp());
        UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
        userRoom.type = GROUP_ROOM;
        DatabaseReference senderFirebase = getNewFireBase(ANCHOR_USERS, new String[]{getMyUserId(), MESSAGE_ROOM, roomId});
        senderFirebase.setValue(userRoom);
        senderFirebase.setPriority(-getPriorityTimeStamp());
    }

    public void sendMessageToGroup(final String reqUserId, final String roomId, RoomsModel.Messages messageDetail, final int messageType, final String mediaId, final String mediaUrl) {
        //STEP 1
        final DatabaseReference pushedMessageFireBase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES}).push();
        pushedMessageFireBase.setValue(messageDetail);
        pushedMessageFireBase.setPriority(getPriorityTimeStamp());

        //STEP 2
        DatabaseReference firebaseRooms = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId});
        firebaseRooms.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                HashMap<String, RoomsModel.Members> membersHashMap = roomsModel.members;
                final String[] membersKey = membersHashMap.keySet().toArray(new String[membersHashMap.size()]);
                String sendTo = null;
                for (int i = 0; i < membersKey.length; i++) {
                    if (!membersKey[i].equals(reqUserId)) {
                        sendTo += membersKey[i] + ",";
                        final DatabaseReference roomMemberFireBase = getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS});
                        final int finalI = i;
                        roomMemberFireBase.child(membersKey[i]).child(MESSAGE_COUNT).runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {
                                if (mutableData.getValue() == null) {
                                    mutableData.setValue(1);
                                } else {
                                    mutableData.setValue((Long) mutableData.getValue() + 1);
                                }
                                return Transaction.success(mutableData);
                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                            }

                        });

                        roomMemberFireBase.child(membersKey[finalI]).child(ROOM_TYPE).setValue(messageType);
                        UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
                        userRoom.type = GROUP_ROOM;
                        DatabaseReference receiverFireBase = getNewFireBase(ANCHOR_USERS, new String[]{membersKey[finalI],MESSAGE_ROOM,roomId});
                        receiverFireBase.setValue(userRoom);
                        receiverFireBase.setPriority(-getPriorityTimeStamp());
                    }
                }
                UserModel.Rooms userRoom = new UserModel.Rooms(getUpdatedAt(), true, false);
                userRoom.type = GROUP_ROOM;
                DatabaseReference senderFirebase = getNewFireBase(ANCHOR_USERS, new String[]{reqUserId, MESSAGE_ROOM, roomId});
                senderFirebase.setValue(userRoom);
                senderFirebase.setPriority(-getPriorityTimeStamp());
                NotificationContent content = new NotificationContent(reqUserId, getMyUserModel().name, sendTo, pushedMessageFireBase.getKey(), roomId, null, getMyUserModel().imageUrl, NEW_MESSAGE_REQUEST, mediaId, mediaUrl);
                sendNotification(content);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * INPUT reqUser, roomId
     * STEP 1:
     * UPDATE rooms/<roomId>/members/<reqUser._id>/msgCount 0
     * STEP 2: Iterate over each message
     * STEP 2.1
     * -if reqUser != memberId and ( expiryType 6) and reqUser exists in viewers && rooms/<roomId>/type == FRIEND_ROOM
     * Remove message from room
     * -if roomType == GROUP_ROOM and ( expiryType 6)
     * Remove message from room
     * STEP 2.2
     * UPDATE viewers for message of type
     */
    public void roomOpen(final String reqUserId, final String roomId, final int roomType) {
        Log.d(TAG,"roomOpen: "+ roomId);
        final DatabaseReference roomFireBase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId});

        roomFireBase.child(MEMBERS).child(reqUserId).child(MESSAGE_COUNT).setValue(0);
        roomFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                if (roomsModel == null) {
                    Log.e(TAG, " openRoom failed : roomModel null returning");
                    return;
                }
                HashMap<String, RoomsModel.Messages> messages = roomsModel.messages;
                if (roomsModel.members == null) {
                    Log.e(TAG, " roomOpen failed : members are null returning");
                    return;
                }
                if (messages == null) {
                    Log.e(TAG, " roomOpen failed : messages are null returning");
                    return;
                }
                for (Map.Entry<String, RoomsModel.Messages> entry : messages.entrySet()) {
                    RoomsModel.Messages message = entry.getValue();
                    String messageKey = entry.getKey();
                    if (message.type == MESSAGE_TYPE_TEXT) {
                        if (!reqUserId.equals(message.memberId) && message.expiryType == REMOVED_UPON_ROOM_OPEN)
                            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES, messageKey}).setValue(null);
                        else {
                            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES, messageKey, VIEWERS, reqUserId}).setValue(1);
                            if (message.expiryType == MESSAGE_EXPIRED_BUT_NOT_VIEWED) {
                                message.expiryType = REMOVED_UPON_ROOM_OPEN;
                                getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES, messageKey, EXPIRY_TYPE}).setValue(REMOVED_UPON_ROOM_OPEN);
                            }
                        }
                    }
                }
                getNewFireBase(ANCHOR_ROOMS,new String[]{roomId,MEMBERS,reqUserId,TYPE}).setValue(0);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    public DatabaseReference getFireBaseReference(String anchor, String[] children) {
        return this.getNewFireBase(anchor, children);
    }

    public void loadExistingGroups(String groupId) {
        SocialModel.Groups groups = mSocialModel.groups.get(groupId);
        this.getNewFireBase(ANCHOR_ROOMS, new String[]{groups.roomId}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                roomDataLoadedListener.onGroupMemberLoaded(roomsModel.members);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateOnScreenShotTaken(String userId, String roomId, String mediaId) {
        RoomsModel.Messages message = new RoomsModel.Messages(userId, mediaId, MEDIA_WITH_TEXT,
                SCREENSHOT_TEXT, System.currentTimeMillis(), null, EXPIRY_TYPE_24_HOURS, null);
        sendMessageToFriend(myUserId, userId, roomId, message, MEDIA_WITH_TEXT, null, null);
    }

    public void updateOnFailedUpload(final Context context, String pendingMediaUploadId, final MediaModel mediaObject) {
        HashMap<String, Integer> rooms = mediaObject.addedTo.rooms;
        if (rooms == null || rooms.size() <= 0)
            return;
        for (final String room : rooms.keySet()) {
            mFireBase.child(ANCHOR_ROOMS).child(room).child(MESSAGES).orderByChild(MEDIA_ID).equalTo(pendingMediaUploadId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        RoomsModel.Messages messages = snapshot.getValue(RoomsModel.Messages.class);
                        if (messages.type == MESSAGE_TYPE_MEDIA) {
                            String messageId = snapshot.getKey();
                            DatabaseReference firebase = getNewFireBase(ANCHOR_ROOMS, new String[]{room, MESSAGES, messageId});
                            firebase.child(MEDIA_UPLOADING).setValue(MEDIA_UPLOADING_FAILED);
                            firebase.setPriority(getPriorityTimeStamp());
                            if (onUploadStatusChangesCallback != null)
                                onUploadStatusChangesCallback.onUploadingStatusModified(messageId,MEDIA_UPLOADING_FAILED);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void updateMediaMessageUploadStarting(final String roomId, String pendingMediaUploadId){
        mFireBase.child(ANCHOR_ROOMS).child(roomId).child(MESSAGES).orderByChild(MEDIA_ID).equalTo(pendingMediaUploadId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    RoomsModel.Messages messages = snapshot.getValue(RoomsModel.Messages.class);
                    if (messages.type == MESSAGE_TYPE_MEDIA) {
                        String messageId = snapshot.getKey();
                        DatabaseReference firebase = getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES, messageId});
                        firebase.child(MEDIA_UPLOADING).setValue(MEDIA_UPLOADING_STARTED);
                        firebase.setPriority(getPriorityTimeStamp());
                        if (onUploadStatusChangesCallback != null)
                            onUploadStatusChangesCallback.onUploadingStatusModified(messageId,MEDIA_UPLOADING_STARTED);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public LinkedHashMap<String,MediaModel> getPendingMediaMessagesList() {
        return pendingMediaInMessageRooms;
    }

    public void deleteMyMediaFromMyMoments(String mediaId) {
        if (mMoments != null && mMoments.containsKey(getMyUserModel().momentId) && mMoments.get(getMyUserModel().momentId).media != null && mMoments.get(getMyUserModel().momentId).media.containsKey(mediaId))
            mMoments.get(getMyUserModel().momentId).media.remove(mediaId);
        if (getMyStreams().containsKey(mediaId))
            getMyStreams().remove(mediaId);
        cleanMedia(mediaId);
        updateFriendsDataOnDelete(mediaId);
        if (onMyStreamsLoadedCallback != null)
            onMyStreamsLoadedCallback.onStreamsLoaded(myMedias, pendingMediaInMyMoments, pendingMediaInMessageRooms);
    }

    private void updateFriendsDataOnDelete(String mediaId){
        mFireBase.child(MEDIA).child(mediaId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    MediaModel mediaModel = dataSnapshot.getValue(MediaModel.class);
                    updateUserFriendsData(mediaModel, false, getCreatedAt());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public interface RoomDataLoadedCallbacks {
        void onGroupMemberLoaded(HashMap<String, RoomsModel.Members> memberMap);
    }

    public void setOnGroupMembersLoadedListener(Fragment fragment) {
        this.roomDataLoadedListener = (RoomDataLoadedCallbacks) fragment;
    }

    public String createCustomFriendList(SettingsModel.CustomFriendListDetails customFriendDetails) {
        DatabaseReference firebase = this.getNewFireBase(ANCHOR_SETTINGS, new String[]{myUserId, CUSTOM_FRIEND_LIST}).push();
        firebase.setValue(customFriendDetails);
        return firebase.getKey();
    }

    public String updateIgnoredList(HashMap<String, SettingsModel.MemberDetails> ignoredList) {
        DatabaseReference firebase = this.getNewFireBase(ANCHOR_SETTINGS, new String[]{myUserId, IGNORED_LIST});
        firebase.setValue(ignoredList);
        return firebase.getKey();
    }

    public void getCustomFriendList() {
        this.getNewFireBase(ANCHOR_SETTINGS, new String[]{myUserId}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                SettingsModel settingsModel = dataSnapshot.getValue(SettingsModel.class);
                customFriendListCallback.onCustomFriendListLoaded(settingsModel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCustomFriendListCallback(Fragment fragment) {
        this.customFriendListCallback = (CustomFriendListCallBack) fragment;
    }

    public interface CustomFriendListCallBack {
        void onCustomFriendListLoaded(SettingsModel settingsModel);
    }

    public String updateOnGroupCreated(HashMap<String, RoomsModel.Members> removeMembers,
                                       HashMap<String, RoomsModel.Members> addedMembers, String groupName, String roomId) {
        DatabaseReference firebase = this.getNewFireBase(ANCHOR_ROOMS, null).push();
        long createdAt = System.currentTimeMillis();
        HashMap<String,RoomsModel.Members> adminMemberMap = new HashMap<>();
        RoomsModel.Members adminMember = new RoomsModel.Members(myUserId, getMyUserModel().name, getMyUserModel().imageUrl, GROUP_CREATED);
        adminMemberMap.put(myUserId, adminMember);
        RoomsModel.Detail details = new RoomsModel.Detail(FireBaseHelper.myUserId, groupName, null, null, createdAt);
        details.roomType = GROUP_ROOM;
        String[] memberArray = addedMembers.keySet().toArray(new String[addedMembers.size()]);
        HashMap<String, RoomsModel.PendingMemberDetails> pendingMemberMap = new HashMap<>();
        for (int i = 0; i < memberArray.length; i++) {
            if (!myUserId.equals(memberArray[i])) {
                RoomsModel.Members members = addedMembers.get(memberArray[i]);
                pendingMemberMap.put(memberArray[i], new RoomsModel.PendingMemberDetails(members.memberId, members.name, members.imageUrl, members.handle, members.momentId));
                // update pending group request
                SocialModel.PendingGroupRequest groupRequest = new SocialModel.PendingGroupRequest(groupName, null, getMyUserModel().name);
                DatabaseReference pendingGroupFireBase = this.getNewFireBase(ANCHOR_SOCIALS, new String[]{memberArray[i], PENDING_GROUP_REQUEST, firebase.getKey()});
                pendingGroupFireBase.setValue(groupRequest);
                pendingGroupFireBase.setPriority(getPriorityTimeStamp());
            }
        }
        RoomsModel roomModel = new RoomsModel(GROUP_ROOM, adminMemberMap, null, pendingMemberMap, details);
        firebase.setValue(roomModel);
        updateUserMessageRoomOnGroupCreated(firebase.getKey(), createdAt, groupName);
        if (removeMembers != null && removeMembers.size() > 0) {
            if (roomId != null)
                removeUsersFromGroup(removeMembers, roomId);
        }

        String sendTo = TextUtils.join(",", addedMembers.keySet().toArray());
        NotificationContent content = new NotificationContent(getMyUserId(), getMyUserModel().name, sendTo, null, firebase.getKey(), groupName, getMyUserModel().imageUrl, SEND_GROUP_REQUEST, null, null);
        sendNotification(content);
        return firebase.getKey();
    }

    private void removeUsersFromGroup(HashMap<String, RoomsModel.Members> removeList, final String roomId) {
        String[] keyArray = removeList.keySet().toArray(new String[removeList.size()]);
        for (int i = 0; i < keyArray.length; i++) {
            this.getNewFireBase(ANCHOR_USERS, new String[]{keyArray[i], MESSAGE_ROOM, roomId}).setValue(null);
            this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, keyArray[i]}).setValue(null);
            this.getNewFireBase(ANCHOR_SOCIALS, new String[]{keyArray[i], GROUPS, roomId}).setValue(null);
            RoomsModel.Messages messages = new RoomsModel.Messages(keyArray[i], null, 4, removeList.get(keyArray[i]).name + " left", System.currentTimeMillis(), null, VIEW_FOR_A_DAY, null);
            this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES}).push().setValue(messages);
        }

        final DatabaseReference firebase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId});
        firebase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                HashMap<String, RoomsModel.Members> membersHashMap = roomsModel.members;
                String[] memberArray = membersHashMap.keySet().toArray(new String[membersHashMap.size()]);
                for (int i = 0; i < memberArray.length; i++) {
                    membersHashMap.get(memberArray[i]).type = ADMIN_REMOVED_MEMBER;
                    getNewFireBase(ANCHOR_USERS, new String[]{memberArray[i], MESSAGE_ROOM, roomId}).setPriority(getPriorityTimeStamp());
                }
                firebase.child(MEMBERS).setValue(membersHashMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void leaveGroupByUser(final String userId, final String roomId, final String userName) {
        this.getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM, roomId}).setValue(null);
        this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, userId}).setValue(null);
        this.getNewFireBase(ANCHOR_SOCIALS, new String[]{userId, GROUPS, roomId}).setValue(null);
        final DatabaseReference firebase = this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId});
        firebase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                if (roomsModel.members.size() == 0) {
                    firebase.setValue(null);
                } else {
                    RoomsModel.Messages messages = new RoomsModel.Messages(userId, null, 4, userName + " left", System.currentTimeMillis(), null, VIEW_FOR_A_DAY, null);
                    getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES}).push().setValue(messages);

                    HashMap<String, RoomsModel.Members> membersHashMap = roomsModel.members;
                    String[] memberArray = membersHashMap.keySet().toArray(new String[membersHashMap.size()]);
                    for (int i = 0; i < memberArray.length; i++) {
                        if (i == 0 && userId.equals(roomsModel.detail.adminId) && !userId.equals(memberArray[i])) {
                            getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, DETAIL, ADMIN}).setValue(memberArray[i]);
                        }
                        membersHashMap.get(memberArray[i]).type = USER_LEFT_GROUP;
                        getNewFireBase(ANCHOR_USERS, new String[]{memberArray[i], MESSAGE_ROOM, roomId}).setPriority(getPriorityTimeStamp());
                    }
                    firebase.child(MEMBERS).setValue(membersHashMap);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateUserMessageRoomOnGroupCreated(String roomId, long createdAt, String groupName) {
        UserModel.Rooms rooms = new UserModel.Rooms(GROUP_ROOM, createdAt, false);
        DatabaseReference firebase = this.getNewFireBase(ANCHOR_USERS, new String[]{myUserId, MESSAGE_ROOM, roomId});
        firebase.setValue(rooms);
        firebase.setPriority(getPriorityTimeStamp());
        updateUserSocialDataOnGroupCreation(roomId, groupName);
    }

    private void updateUserSocialDataOnGroupCreation(String roomId, String groupName) {
        SocialModel.Groups groups = new SocialModel.Groups(groupName, null, null, myUserId, roomId);
        this.getNewFireBase(ANCHOR_SOCIALS, new String[]{myUserId, GROUPS, roomId}).setValue(groups);
    }

    public String updatePendingUploads(final MediaModel mediaModel) {
        DatabaseReference localDataFireBase = getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{getMyUserId(), PENDING_MEDIA});
        final DatabaseReference pushedFireBase = localDataFireBase.push();
        String pendingMediaId = pushedFireBase.getKey();
        String mediaPath = mediaModel.url;
        int p = mediaPath.lastIndexOf(".");
        String extension = mediaPath.substring(p + 1);
        if (p == -1 || !extension.matches("\\w+")) {
            /* file has no extension */
            AppLibrary.log_d(TAG, "MediaPath does not have an extension!");
        }

        File oldFile = new File(mediaPath);
        File newFile = new File(AppLibrary.getFilesDirectory(mContext) + pendingMediaId + "." + extension);
        oldFile.renameTo(newFile);
        mediaModel.url = newFile.getAbsolutePath();
        if (mediaModel.addedTo != null && (mediaModel.addedTo.rooms != null || mediaModel.addedTo.moments != null))
            updateDownloadDetailsForMedia(pendingMediaId, mediaModel, DOWNLOADED_CHAT_MEDIA);
        if (mediaModel.addedTo != null && mediaModel.addedTo.rooms != null) {
            updateMessageRoomsOnPendingMedia(mediaModel, pendingMediaId);
        }
        pushedFireBase.setValue(mediaModel);
        return pendingMediaId;
    }

    public void updateOnCompletedUpload(Context context, String pendingMediaKey, MediaModel mediaModel,String finalUrl) {
        clearPendingMediasInList(pendingMediaKey);
        mediaModel.mediaId = null;
        mediaModel.createdAt = System.currentTimeMillis();
        HashMap<String, Integer> momentsMap = mediaModel.addedTo.moments;
        if (momentsMap != null) {
            String[] momentIds = momentsMap.keySet().toArray(new String[momentsMap.size()]);
            for (int i = 0; i < momentIds.length; i++) {
                if (momentsMap.get(momentIds[i]) == CUSTOM_MOMENT) {
                    updateCustomMoment(momentIds[i], pendingMediaKey, getFromSharedPreferences(context, AppLibrary.USER_LOGIN));
                } else if (momentsMap.get(momentIds[i]) == MY_MOMENT) {
                    updateMediaDataToMoments(context, mediaModel, pendingMediaKey,finalUrl);
                    updateUserFriendsData(mediaModel,true,mediaModel.createdAt);
                }
            }
        }
        if (mediaModel.addedTo != null && mediaModel.addedTo.rooms != null && mediaModel.addedTo.rooms.size() > 0)
            updateMessageRoomsOnUploadComplete(mediaModel, pendingMediaKey,finalUrl);

        this.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, PENDING_MEDIA, pendingMediaKey}).setValue(null);
        mediaModel.mediaText = null;
        mediaModel.privacy = null;
        DatabaseReference reference = this.getNewFireBase(ANCHOR_MEDIA, new String[]{pendingMediaKey});
        reference.setValue(mediaModel);
        reference.child(URL).setValue(finalUrl);
    }

    private void clearPendingMediasInList(String pendingMediaKey){
        if (pendingMediaInMyMoments != null && pendingMediaInMyMoments.size() > 0) {
            if (pendingMediaInMyMoments.containsKey(pendingMediaKey)) {
                pendingMediaInMyMoments.remove(pendingMediaKey);
                onMyStreamsLoadedCallback.onMediaInMomentUpdated(pendingMediaInMyMoments);
            }
        }
        if (pendingMediaInMessageRooms != null && pendingMediaInMessageRooms.size() > 0) {
            if (pendingMediaInMessageRooms.containsKey(pendingMediaKey)){
                pendingMediaInMessageRooms.remove(pendingMediaKey);
                onMyStreamsLoadedCallback.onMediaInMessageRoomsUpdated(pendingMediaInMessageRooms);
            }
        }
    }

    private void updateMessageRoomsOnUploadComplete(final MediaModel mediaModel, final String mediaId, final String finalUrl) {
        HashMap<String, Integer> rooms = mediaModel.addedTo.rooms;
        final long createdAt = System.currentTimeMillis();
        for (final String room : rooms.keySet()) {
            this.getNewFireBase(ANCHOR_ROOMS, new String[]{room}).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot roomDataSnapShot) {
                    roomDataSnapShot.child(MESSAGES).getRef().orderByChild(MEDIA_ID).equalTo(mediaId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                RoomsModel.Messages messages = snapshot.getValue(RoomsModel.Messages.class);
                                if (messages.type == MESSAGE_TYPE_MEDIA) {
                                    String messageId = snapshot.getKey();
                                    DatabaseReference firebase = getNewFireBase(ANCHOR_ROOMS, new String[]{room, MESSAGES, messageId});
                                    firebase.child(MEDIA_UPLOADING).setValue(MEDIA_UPLOADING_COMPLETE);
                                    firebase.child(CREATED_AT).setValue(createdAt);
                                    if (AppLibrary.getMediaType(mediaModel.url) == AppLibrary.MEDIA_TYPE_VIDEO) {
                                        firebase.child(IS_VIDEO).setValue(true);
                                    } else {
                                        firebase.child(IS_VIDEO).setValue(false);
                                    }
                                    firebase.setPriority(getPriorityTimeStamp());
                                    RoomsModel roomsModel = roomDataSnapShot.getValue(RoomsModel.class);
                                    if (roomsModel.type == FRIEND_ROOM) {
                                        for (String user : roomsModel.members.keySet()) {
                                            if (!user.equals(myUserId)) {
                                                updateMediaMessageRoomAndSendNotificationForFriend(myUserId, room, user, NEW_MEDIA, messageId, mediaId, finalUrl);
                                            }
                                        }
                                    } else {
                                        updateMediaMessageRoomAndSendNotificationForGroup(myUserId,room,NEW_MEDIA,messageId,mediaId,finalUrl);
                                    }
                                    if (onUploadStatusChangesCallback != null)
                                        onUploadStatusChangesCallback.onUploadingStatusModified(messageId,MEDIA_UPLOADING_COMPLETE);
                                    break;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public interface onUploadStatusChanges {
        void onUploadingStatusModified(String messageId, int status);
    }

    public void setOnUploadStatusChangesCallback(Fragment fragment) {
        onUploadStatusChangesCallback = (onUploadStatusChanges) fragment;
    }

    private void updateMessageRoomsOnPendingMedia(final MediaModel mediaModel, final String mediaId) {
        HashMap<String, Integer> rooms = mediaModel.addedTo.rooms;
        for (final String room : rooms.keySet()) {
            if (rooms.get(room) == FRIEND_ROOM) {
                this.getNewFireBase(ANCHOR_ROOMS, new String[]{room}).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                        for (String user : roomsModel.members.keySet()) {
                            if (!user.equals(myUserId)) {
                                RoomsModel.Messages messages;
                                messages = new RoomsModel.Messages(myUserId, mediaId, MESSAGE_TYPE_MEDIA, null, mediaModel.createdAt, null, mediaModel.expiryType, null);
                                messages.mediaUploadingStatus = MEDIA_UPLOADING_STARTED;
                                createMediaMessageForFriend(room, messages, user);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else if (rooms.get(room) == GROUP_ROOM) {
                RoomsModel.Messages messages;
                messages = new RoomsModel.Messages(myUserId, mediaId, MESSAGE_TYPE_MEDIA, null, mediaModel.createdAt, null,VIEW_FOR_A_DAY, null);
                messages.mediaUploadingStatus = MEDIA_UPLOADING_STARTED;
                createMediaMessageForGroup(room, messages);
            }
        }
    }

    public void updateCustomMoment(String momentId, String pendingMediaId, String myUserId) {
        DatabaseReference firebase = this.getNewFireBase(ANCHOR_MOMENTS, new String[]{momentId, CONTRIBUTED_MEDIA, pendingMediaId});
        MomentModel.ContributedMedia contributedMedia = new MomentModel.ContributedMedia(MEDIA_INACTIVE, myUserId);
        firebase.setValue(contributedMedia);
    }

    private void updateMediaDataToMoments(Context context, MediaModel mediaModel, String mediaId,String finalUrl) {
        String momentId = getMyUserModel().momentId;
        if (AppLibrary.checkStringObject(momentId) != null) {
            MomentModel.Media media = new MomentModel.Media(finalUrl, mediaModel.createdAt, mediaModel.type, 0, null, 0, null,mediaModel.mediaText);
            media.privacy = new MomentModel.Privacy(mediaModel.privacy.type, mediaModel.privacy.value);
            DatabaseReference firebase = this.getNewFireBase(ANCHOR_MOMENTS, new String[]{momentId, ANCHOR_MEDIA, mediaId});
            firebase.setValue(media);
            firebase.setPriority(getPriorityTimeStamp());
        }
    }

    private void updateUserFriendsData(MediaModel mediaModel,boolean incrementMomentCount,long updatedAt) {
        int actionType = mediaModel.privacy.type;
        HashMap<String, String> userListMap = mediaModel.privacy.value;
        SocialModel.Friends[] userFriendList = getFriendList();
        if (userFriendList != null) {
            for (int i = 0; i < userFriendList.length; i++) {
                SocialModel.Friends friends = userFriendList[i];
                if (actionType == SHARE_PRIVACY_ALL_FRIENDS) {
                    updateUserFriendRoom(friends.roomId, friends.friendId, updatedAt, incrementMomentCount);
                } else if (actionType == SHARE_PRIVACY_EXCEPT_FRIENDS) {
                    if (!userListMap.containsKey(friends.friendId))
                        updateUserFriendRoom(friends.roomId, friends.friendId, updatedAt, incrementMomentCount);
                } else if (actionType == SHARE_PRIVACY_CUSTOM_FRIEND_LIST) {
                    if (userListMap.containsKey(friends.friendId))
                        updateUserFriendRoom(friends.roomId, friends.friendId, updatedAt, incrementMomentCount);
                }
            }
        }
    }

    public void updateUserFriendRoom(final String roomId, final String friendId, final long updatedAt,boolean incrementMomentCount) {
        if (incrementMomentCount) {
            this.getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, friendId, MOMENT_COUNT}).runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {
                    if (mutableData.getValue() == null) {
                        mutableData.setValue(1);
                    } else {
                        mutableData.setValue((Long) mutableData.getValue() + 1);
                    }
                    return Transaction.success(mutableData);
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                }

            });
        }
        updateFriendsData(roomId, friendId, updatedAt,incrementMomentCount);
    }

    public void updateFriendsData(String roomId, String friendId, long updatedAt,boolean incrementMomentCount) {
        DatabaseReference firebase = this.getNewFireBase(ANCHOR_USERS, new String[]{friendId, MOMENT_ROOM, roomId});
        UserModel.Rooms rooms = new UserModel.Rooms(updatedAt, FRIEND_ROOM);
        firebase.setValue(rooms);
        if (incrementMomentCount)
            firebase.setPriority(-getPriorityTimeStamp());
    }

    public void fetchPendingUploadsForMedia(String mediaId) {
        DatabaseReference pendingDataFireBase = this.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, PENDING_MEDIA, mediaId});
        pendingDataFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                MediaModel model = dataSnapshot.getValue(MediaModel.class);
                mediaModelCallback.onPendingMediaListLoaded(model, dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void fetchPendingUploadsForMyMoments() {
        for (Map.Entry<String,MediaModel> entry : pendingMediaInMyMoments.entrySet()){
            mediaModelCallback.onPendingMediaListLoaded(entry.getValue(), entry.getKey());
        }
//        DatabaseReference pendingDataFireBase = this.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, PENDING_MEDIA});
//        pendingDataFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot mediaSnapshot : dataSnapshot.getChildren()) {
//                    MediaModel model = mediaSnapshot.getValue(MediaModel.class);
//                    mediaModelCallback.onPendingMediaListLoaded(model, mediaSnapshot.getKey());
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }

    public void fetchPendingUploadsForRoom(final String roomId) {
        mFireBase.child(ANCHOR_ROOMS).child(roomId).child(MESSAGES).orderByChild(MEDIA_UPLOADING).endAt(MEDIA_UPLOADING_STARTED).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    final RoomsModel.Messages messages = snapshot.getValue(RoomsModel.Messages.class);
                    String mediaId = messages.mediaId;
                    if (mediaId != null) {
                        getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, PENDING_MEDIA, mediaId}).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                MediaModel model = dataSnapshot.getValue(MediaModel.class);
                                if (model != null)
                                    mediaModelCallback.onPendingMediaForMessageLoaded(model, dataSnapshot.getKey(),roomId,messages.messageId);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void fetchPendingUploadsForAllRoom() {
        for (Map.Entry<String,MediaModel> entry : pendingMediaInMessageRooms.entrySet()){
            mediaModelCallback.onPendingMediaListLoaded(entry.getValue(),entry.getKey());
        }
    }

    public interface MediaModelCallback {
        void onPendingMediaListLoaded(MediaModel pendingList, String key);
        void onPendingMediaForMessageLoaded(MediaModel pendingList, String key,String roomId,String messageId);
    }

    public void setMediaModelCallback(Context context) {
        mediaModelCallback = (MediaModelCallback) context;
    }

    public Map<String, MomentModel> modelMap;

    public void fetchMomentList() {
        DatabaseReference userFireBase = this.getNewFireBase(ANCHOR_USERS, new String[]{myUserId});
        userFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                modelMap = new LinkedHashMap<>();
                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                modelMap.put(userModel.momentId, null);
                DatabaseReference momentFireBase = getNewFireBase(ANCHOR_MOMENTS, new String[]{userModel.momentId});
                momentFireBase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        MomentModel momentModel = dataSnapshot.getValue(MomentModel.class);
                        modelMap.put(dataSnapshot.getKey(), momentModel);
                        momentCallback.onMomentListLoaded(modelMap);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public interface MomentCallback {
        void onMomentListLoaded(Map<String, MomentModel> list);
    }

    public void setMomentCallback(Fragment fragment) {
        momentCallback = (MomentCallback) fragment;
    }

    private MomentsDataListener momentsDataListener;

    public interface MomentsDataListener {
        void onUnseenDataChanged(ArrayList<HomeMomentViewModel> unseenFriendMomentList);

        void onSeenDataChanged(ArrayList<HomeMomentViewModel> rooms);

        void onFavouritesDataChanged(ArrayList<HomeMomentViewModel> favouritesMomentsList);

        void onElementChanged(String momentId);
    }

    public void saveToSharedPreference(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(AppLibrary.APP_SETTINGS, 0);
        preferences.edit().putString(key, value).commit();
    }

    public String getFromSharedPreferences(Context context, String key) {
        return context.getSharedPreferences(AppLibrary.APP_SETTINGS, 0).getString(key, "");
    }

    public void setMomentsDataListener(MomentsDataListener momentsDataListener) {
        this.momentsDataListener = momentsDataListener;
        momentsDataListener.onSeenDataChanged(getSeenMomentRoomView());
        momentsDataListener.onUnseenDataChanged(getMomentRoomView());
        momentsDataListener.onFavouritesDataChanged(null);
    }


    /**
     * @param momentRoom the particular momentRoom
     * @return true if the user has checkedOut the moment already false otherwise
     */
    private boolean getMomentSeenStatus(RoomsModel momentRoom) {
        final HashMap<String, RoomsModel.Members> members = momentRoom.members;
        for (Map.Entry<String, RoomsModel.Members> membersEntry : members.entrySet()) {
            if (membersEntry.getKey().equals(getMyUserId())) {
                return membersEntry.getValue().momentCount == 0;
            }
        }
        throw new RuntimeException("getMomentSeenStatus: userNot found in members");
    }

    SliderMessageListListener mSliderMessageListListener;

    public void setSliderMessageListListener(SliderMessageListListener sliderMessageListListener) {
        this.mSliderMessageListListener = sliderMessageListListener;
        this.mSliderMessageListListener.onSliderListChanged(getSliderMessageView());
    }

    public void updateLocalPath(String momentId, String mediaId, String path) {
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(URL).setValue(path);
        mMoments.get(momentId).media.get(mediaId).url = path;
    }

    public void onMomentMediaDownloadFailed(String momentId, final String mediaId){
        mMoments.get(momentId).momentStatus = UNSEEN_MOMENT;
        if (momentsDataListener != null)
            momentsDataListener.onUnseenDataChanged(getMomentRoomView());
        if (momentsDataListener != null)
            momentsDataListener.onSeenDataChanged(getSeenMomentRoomView());
    }

    public void updateDownloadStatus(final String momentId, final String mediaId, final int mediaStatus) {
        mMoments.get(momentId).media.get(mediaId).status = mediaStatus;
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int status = dataSnapshot.getValue(Integer.class);
                if (status < MEDIA_DOWNLOAD_COMPLETE) {
                    mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).setValue(mediaStatus);
                }
                mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(MOMENT_ID).setValue(momentId);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        if (mediaStatus == MEDIA_DOWNLOAD_COMPLETE) {
            ArrayList<String> keys = new ArrayList<>();
            HashMap<String, MomentModel.Media> hMap = mMoments.get(momentId).media;
            Iterator<String> iterator = hMap.keySet().iterator();
            while (iterator.hasNext()) {
                keys.add(iterator.next());
            }
            int index = 0;
            for (String key : keys) {
                index++;
                if (key.equals(mediaId)) {
                    if (index < keys.size()) {
                        int currentIndex = index;
                        while (currentIndex < keys.size()) {
                            if (hMap.get(keys.get(currentIndex)).status != MEDIA_DOWNLOAD_COMPLETE) {
                                MediaDownloader mediaDownloader = new MediaDownloader(mContext);
                                mediaDownloader.startDownload(hMap.get(keys.get(currentIndex)));
                                break;
                            } else {
                                currentIndex++;
                            }
                        }
                        Log.d(TAG, " index to download " + currentIndex);
                    } else {
                        if (mMoments.get(momentId).momentStatus < SEEN_MOMENT)
                            mMoments.get(momentId).momentStatus = READY_TO_VIEW_MOMENT;
                        else if (mMoments.get(momentId).momentStatus == SEEN_BUT_DOWNLOADING)
                            mMoments.get(momentId).momentStatus = SEEN_MOMENT;
                        if (momentsDataListener != null)
                            momentsDataListener.onUnseenDataChanged(getMomentRoomView());
                        if (momentsDataListener != null)
                            momentsDataListener.onSeenDataChanged(getSeenMomentRoomView());
                        Log.d(TAG, " moment completely downloaded");
                    }
                    break;
                }
            }
        }
    }

    public void feedMediaToMyStreams(final MomentModel.Media media) {
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(media.mediaId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String localUrl = (String) dataSnapshot.child(URL).getValue();
                MediaModelView mediaModelView = new MediaModelView(localUrl, media.createdAt, media.type, media.totalViews, media.viewers,
                        media.screenShots, media.createdAtText, media.momentId, media.mediaId, MEDIA_UPLOADING_COMPLETE,media.mediaText);

                if (fileExists(localUrl)) {
                    mediaModelView.status = MEDIA_UPLOADING_COMPLETE;
                } else {
                    // download valid medias in my moment
                    mediaModelView.status = DOWNLOADING_MY_MOMENT_MEDIA;
                    mediaModelView.url = media.url;
                    MediaDownloader mediaDownloader = new MediaDownloader(mContext);
                    mediaDownloader.startDownload(media.mediaId,media.url, new OnMyMomentMediaDownloadCallback() {
                        @Override
                        public void onDownloadCallback(String mediaId,int status,String mediaPath) {
                            if (mediaPath != null){
                                myMedias.get(mediaId).url = mediaPath;
                            }
                            // pass status to my moment slider fragment
                            if (onMyMomentMediaDownloadStatusModified != null)
                                onMyMomentMediaDownloadStatusModified.onDownloadStatusChanges(mediaId,status);
                        }
                    });
                }
                myMedias.put(media.mediaId, mediaModelView);
                if (onMyStreamsLoadedCallback != null)
                    onMyStreamsLoadedCallback.onStreamsLoaded(myMedias, pendingMediaInMyMoments, pendingMediaInMessageRooms);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void fetchMyPendingStreams(){
        // fetch pending streams
        getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, PENDING_MEDIA}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot pendingMedia : dataSnapshot.getChildren()) {
                    MediaModel mediaModel = pendingMedia.getValue(MediaModel.class);
                    if (!fileExists(mediaModel.url)){
                        // remove from pending media
                        pendingMedia.getRef().setValue(null);
                    } else {
                        mediaModel.mediaId = pendingMedia.getKey();
                        if (mediaModel.addedTo.moments != null && !mediaModel.addedTo.moments.isEmpty()) {
                            pendingMediaInMyMoments.put(mediaModel.mediaId,mediaModel);
                            MediaModelView mediaModelView = new MediaModelView(mediaModel.url, mediaModel.createdAt, mediaModel.type, mediaModel.totalViews,
                                    mediaModel.viewers, 0, null, mFireBaseHelper.getMyUserModel().momentId, pendingMedia.getKey(), MEDIA_UPLOADING_FAILED,mediaModel.mediaText);
                            myMedias.put(mediaModel.mediaId, mediaModelView);
                        }
                        if (mediaModel.addedTo.rooms != null && !mediaModel.addedTo.rooms.isEmpty()) {
                            pendingMediaInMessageRooms.put(mediaModel.mediaId,mediaModel);
                        }
                    }
                }
                Log.d(TAG, " fetchMyStreams: returning " + myMedias + " of size " + myMedias.size());
                if (onMyStreamsLoadedCallback != null)
                    onMyStreamsLoadedCallback.onStreamsLoaded(myMedias, pendingMediaInMyMoments, pendingMediaInMessageRooms);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void onRetryMyMomentMediaDownload(String mediaId){
        if (myMedias != null && myMedias.containsKey(mediaId)) {
            MediaDownloader mediaDownloader = new MediaDownloader(mContext);
            if (onMyMomentMediaDownloadStatusModified != null)
                onMyMomentMediaDownloadStatusModified.onDownloadStatusChanges(mediaId, DOWNLOADING_MY_MOMENT_MEDIA);
            mediaDownloader.startDownload(mediaId, myMedias.get(mediaId).url, new OnMyMomentMediaDownloadCallback() {
                @Override
                public void onDownloadCallback(String mediaId, int status,String mediaPath) {
                    if (mediaPath != null){
                        myMedias.get(mediaId).url = mediaPath;
                    }
                    // pass status to my moment slider fragment
                    if (onMyMomentMediaDownloadStatusModified != null)
                        onMyMomentMediaDownloadStatusModified.onDownloadStatusChanges(mediaId, status);
                }
            });
        }
    }

    public interface onMyMomentMediaDownloadStatusModified{
        void onDownloadStatusChanges(String mediaId,int status);
    }

    public void setOnMyMomentMediaDownloadStatusModified(Fragment fragment){
        onMyMomentMediaDownloadStatusModified = (onMyMomentMediaDownloadStatusModified)fragment;
    }

    public interface OnMyMomentMediaDownloadCallback{
        void onDownloadCallback(String mediaId,int status,String mediaPath);
    }

    public LinkedHashMap<String,MediaModelView> getMyStreams(){
        return myMedias;
    }

    public HashMap<String,MediaModelView> getMyDownloadedStreams(){
        LinkedHashMap<String,MediaModelView> downloadedMedias = new LinkedHashMap<>();
        if (myMedias != null && myMedias.size() > 0){
            for (Map.Entry<String,MediaModelView> entry : myMedias.entrySet()){
                if (entry.getValue().status != DOWNLOAD_MY_MOMENT_MEDIA_FAILED && entry.getValue().status != DOWNLOADING_MY_MOMENT_MEDIA ){
                    downloadedMedias.put(entry.getKey(),entry.getValue());
                }
            }
        }
        return downloadedMedias;
    }

    public interface OnMyStreamsLoaded {
        void onStreamsLoaded(LinkedHashMap<String,MediaModelView> list,LinkedHashMap<String,MediaModel> pendingMyMomentMedia,LinkedHashMap<String,MediaModel> pendingMediaInMessageRoom);
        void onMediaInMessageRoomsUpdated(LinkedHashMap<String,MediaModel> pendingMediaInMessageRoom);
        void onMediaInMomentUpdated(LinkedHashMap<String,MediaModel> pendingMyMomentMedia);
    }

    public void setMyStreamListener(Fragment fragment) {
        onMyStreamsLoadedCallback = (OnMyStreamsLoaded) fragment;
    }

    public void downloadMoment(String momentId) {
        if (mMoments.get(momentId).momentStatus < SEEN_MOMENT)
            mMoments.get(momentId).momentStatus = DOWNLOADING_MOMENT;

        if (momentsDataListener != null) {
            momentsDataListener.onUnseenDataChanged(getMomentRoomView());
            if (momentsDataListener != null)
                momentsDataListener.onSeenDataChanged(getSeenMomentRoomView());
            Log.d(TAG, "updating moment status");
        }
        ArrayList<String> keys = new ArrayList<>();
        HashMap<String, MomentModel.Media> hMap = mMoments.get(momentId).media;
        Iterator<String> iterator = hMap.keySet().iterator();
        while (iterator.hasNext()) {
            keys.add(iterator.next());
        }
        int currentIndex = 0;
        while (currentIndex < keys.size()) {
            if (hMap.get(keys.get(currentIndex)).status < MEDIA_DOWNLOAD_COMPLETE) {
                MediaDownloader mediaDownloader = new MediaDownloader(mContext);
                mediaDownloader.startDownload(hMap.get(keys.get(currentIndex)));
                break;
            } else {
                currentIndex++;
            }
        }
    }

    public LinkedHashMap<String, MomentModel> openSeenMoment(String momentId) {
        LinkedHashMap<String, MomentModel> mLocalMoments = new LinkedHashMap<>();
        if (mMoments != null) {
            for (Map.Entry<String, MomentModel> entry : mMoments.entrySet()) {
                Log.d(TAG, "entry.getKey() = " + entry.getKey());
                if (entry.getKey().equals(momentId)) {
                    if (mMoments.get(entry.getKey()).momentStatus == SEEN_MOMENT) {
                        mLocalMoments.put(entry.getKey(), mMoments.get(entry.getKey()));
                        break;
                    }
                }
            }
        }
        return mLocalMoments;
    }

    public void viewCompleteMoment(String momentId) {
        if (mMoments != null) {
            mMoments.get(momentId).momentStatus = SEEN_MOMENT;
            momentsDataListener.onElementChanged(momentId);
            AppLibrary.log_d(TAG, "updating status for moment view completed for moment " + momentId);
        }
    }

    public LinkedHashMap<String, MomentModel> openMoment(String momentId) {
        LinkedHashMap<String, MomentModel> mLocalMoments = new LinkedHashMap<>();
        boolean found = false;
        int totalSize = mMoments.size();
        int momentIndex = 0;
        int currentIndex = -1;
        Log.d(TAG, "momentId = " + momentId);

        if (mMoments != null) {
            for (Map.Entry<String, MomentModel> entry : mMoments.entrySet()) {
                currentIndex++;
                Log.d(TAG, "entry.getKey() = " + entry.getKey());
                if (entry.getKey().equals(momentId)) {
                    if (mMoments.get(entry.getKey()).momentStatus == READY_TO_VIEW_MOMENT) {
                        mLocalMoments.put(entry.getKey(), mMoments.get(entry.getKey()));
                        found = true;
                        momentIndex = currentIndex;
                        populatePreviousUnseen(mLocalMoments, momentIndex, totalSize);
                    } else {
                        break;
                    }
                } else if (found) {
                    if (mMoments.get(entry.getKey()).momentStatus == READY_TO_VIEW_MOMENT) {
                        mLocalMoments.put(entry.getKey(), mMoments.get(entry.getKey()));
                    } else {
                        break;
                    }
                }
            }
        }
        return mLocalMoments;
    }

    void populatePreviousUnseen(LinkedHashMap<String, MomentModel> mLocalMoments, int foundIndex, int totalSize) {
        int currentIndex = -1;
        if (foundIndex < totalSize - 1) {
            for (Map.Entry<String, MomentModel> entry : mMoments.entrySet()) {
                currentIndex++;
                if (currentIndex < foundIndex) {
                    if (mMoments.get(entry.getKey()).momentStatus == READY_TO_VIEW_MOMENT) {
                        mLocalMoments.put(entry.getKey(), mMoments.get(entry.getKey()));
                    } else {
                        break;
                    }
                } else
                    break;
            }
        }
    }

    public interface SliderMessageListListener {
        void onSliderListChanged(LinkedHashMap<String,SliderMessageModel> roomsModels);
    }

    public void cleanMedia(final String mediaId) {
        // Fetch All AddedTo-moments in medias/mediaId
        // for each moment in moments
        // Remove media ref from moments/momentId/media/

        // Fetch all addedTo-rooms in medias/mediaId
        // for each room in rooms
        // Replace mediaId in all corresponding messageId with default mediaId( having expired media standard image)

        mFireBase.child(ANCHOR_MEDIA).child(mediaId).child(ADDED_TO).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean sentToFriend = false;
                boolean sentToRoom = false;
                for (DataSnapshot room : dataSnapshot.child(ROOMS).getChildren()) {
                    if (room.child(ROOM_TYPE).getValue() == (long) FRIEND_ROOM) {
                        replaceMediaFromFriendRoom(mediaId, room.getKey());
                        sentToFriend = true;
                        sentToRoom = true;
                    }
                }
                for (DataSnapshot room : dataSnapshot.child(ROOMS).getChildren()) {
                    if (room.child(ROOM_TYPE).getValue() == (long) GROUP_ROOM) {
                        replaceMediaFromGroupRoom(mediaId, room.getKey(), sentToFriend);
                        sentToRoom = true;
                    }
                }
                for (DataSnapshot moment : dataSnapshot.child(MOMENTS).getChildren()) {
                    if (moment.getValue() != CUSTOM_MOMENT)
                        removeMediaFromMoment(mediaId, moment.getKey(), sentToRoom);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private class MediaTransactionHandler implements Transaction.Handler {
        boolean isFinal;
        DataSnapshot mRoom;
        String userId;
        Callable completeFunc;

        @Override
        public Transaction.Result doTransaction(MutableData currentData) {
            if (currentData.getValue() != null) {
                long momentCount = (long) currentData.getValue();
                if (momentCount > 0) {
                    momentCount--;
                }
                currentData.setValue(momentCount);
            }
            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
            // If I get that it is final then
            /*if(isFinal){
                if(mMomentRoomHashMap == null)
                    mMomentRoomHashMap = new LinkedHashMap<>();
                mMomentRoomHashMap.put(mRoom.getKey(),mRoom.getValue(RoomsModel.class));
                mMomentRoomHashMap.get(mRoom.getKey()).configureDetails(userId);
            }*/

            // Update Count
            if (mMomentRoomHashMap != null) {
                if (mRoom != null) {
                    RoomsModel obj = mMomentRoomHashMap.get(mRoom.getKey());
                    if (obj != null) {
                        obj.members.get(userId).momentCount--;
                    }
                }
            }
        }

    }

    private class MessageTransactionHandler implements Transaction.Handler {
        boolean mRemove;
        boolean isFinal;
        DataSnapshot mRoom;
        DataSnapshot mMessage;
        String userId;
        int deductions = 0;

        @Override
        public Transaction.Result doTransaction(MutableData currentData) {
            if (currentData.getValue() != null) {
                long msgCount = (long) currentData.getValue();
                if (msgCount > 0) {
                    msgCount = msgCount - deductions;
                    if (msgCount < 0) {
                        msgCount = 0;
                    }
                }
                currentData.setValue(msgCount);
            }
            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
            if (mRemove) {
                mFireBase.child(ANCHOR_ROOMS).child(mRoom.getKey()).child(MESSAGES).child(mMessage.getKey()).removeValue();
            }
            if (mMessageRoomHashMap != null) {
                RoomsModel obj = mMessageRoomHashMap.get(mRoom.getKey());
                if (obj != null) {
                    obj.members.get(userId).msgCount--;
                }
            }
            if (isFinal) {
                // Trigger
                mSliderMessageListListener.onSliderListChanged(getSliderMessageView());
            }
        }

    }

    public ArrayList<ListRoomView> getListRoomView() {
        ArrayList<ListRoomView> listRoomViews = new ArrayList<>();
        if (mMessageRoomHashMap != null) {
            for (Map.Entry<String, RoomsModel> entry : mMessageRoomHashMap.entrySet()) {
                RoomsModel.Detail roomDetail = entry.getValue().detail;
                ListRoomView listRoomView = new ListRoomView(roomDetail.name, roomDetail.imageUrl, roomDetail.roomId, roomDetail.handle, roomDetail.momentId, roomDetail.userId, roomDetail.roomType);
                listRoomView.setFirstChar(roomDetail.name);
                listRoomViews.add(listRoomView);
            }
        }
        return listRoomViews;
    }

    public ArrayList<HomeMomentViewModel> getMomentRoomView() {
        ArrayList<HomeMomentViewModel> momentRoomViews = new ArrayList<>();
        if (mMomentRoomHashMap == null || mMoments == null)
            return null;
        for (Map.Entry<String, RoomsModel> entry : mMomentRoomHashMap.entrySet()) {
            RoomsModel.Detail roomDetail = entry.getValue().detail;
            if (mMoments.get(roomDetail.momentId) != null)
                Log.d(TAG, " moment status = " + mMoments.get(roomDetail.momentId).momentStatus);
            else {
                Log.e(TAG, " getMomentRoom View momentModel null returning");
                return null;
            }
            if (/*mMoments.get(roomDetail.momentId).media == null ||*/ mMoments.get(roomDetail.momentId).media.size() < 1)
                continue;//fixme null pointer here

            if (mMoments.get(roomDetail.momentId).momentStatus < SEEN_MOMENT) {
                long offset = entry.getValue().updatedAt + 24 * 60 * 60 * 1000 - System.currentTimeMillis();
                mMoments.get(roomDetail.momentId).name = roomDetail.name;
                mMoments.get(roomDetail.momentId).roomId = roomDetail.roomId;
                mMoments.get(roomDetail.momentId).updatedDate = AppLibrary.timeAccCurrentTime(entry.getValue().updatedAt);
                HomeMomentViewModel momentRoomView = new HomeMomentViewModel(roomDetail.roomId,
                        roomDetail.name, roomDetail.imageUrl,
                        AppLibrary.timeAccCurrentTime(entry.getValue().updatedAt), offset,
                        roomDetail.momentId, false, entry.getValue().type, MEDIA_DOWNLOAD_NOT_STARTED);
                momentRoomView.momentStatus = mMoments.get(roomDetail.momentId).momentStatus;
                momentRoomViews.add(momentRoomView);
            }
        }
        return momentRoomViews;
    }

    public ArrayList<HomeMomentViewModel> getSeenMomentRoomView() {
        ArrayList<HomeMomentViewModel> momentRoomViews = new ArrayList<>();
        if (mMomentRoomHashMap == null || mMoments == null || mMoments.size() <= 0)
            return null;
        for (Map.Entry<String, RoomsModel> entry : mMomentRoomHashMap.entrySet()) {
            RoomsModel.Detail roomDetail = entry.getValue().detail;
            if (mMoments.get(roomDetail.momentId) != null && mMoments.get(roomDetail.momentId).media != null && mMoments.get(roomDetail.momentId).media.size() < 1) {
                Log.d(TAG, " moment status = " + mMoments.get(roomDetail.momentId).momentStatus);
                if (mMoments.get(roomDetail.momentId).momentStatus >= SEEN_MOMENT) {
                    long offset = entry.getValue().updatedAt + 24 * 60 * 60 * 1000 - System.currentTimeMillis();
                    long offsetHours = (System.currentTimeMillis() - entry.getValue().updatedAt) / (1000 * 60 * 60);
                    long offsetMinutes = (System.currentTimeMillis() - entry.getValue().updatedAt) / (1000 * 60);
                    Log.d(TAG, " offset hours = " + offsetHours);
                    Log.d(TAG, " offset minutes = " + offsetMinutes);
                    Log.d(TAG, " offset = " + offset);

                    String timeString = "";
                    if (offsetHours > 0) {
                        timeString = offsetHours + (offsetHours > 1 ? " hours " : " hour ");
                    } else if (offsetMinutes > 2) {
                        timeString = "" + offsetMinutes + " min";
                    } else {
                        timeString = "Just now";
                    }
                    mMoments.get(roomDetail.momentId).name = roomDetail.name;
                    mMoments.get(roomDetail.momentId).roomId = roomDetail.roomId;
                    mMoments.get(roomDetail.momentId).updatedDate = timeString;
                    HomeMomentViewModel momentRoomView = new HomeMomentViewModel(roomDetail.roomId, roomDetail.name, roomDetail.imageUrl, timeString, offset, roomDetail.momentId, false, entry.getValue().type, MEDIA_DOWNLOAD_NOT_STARTED);
                    momentRoomView.momentStatus = mMoments.get(roomDetail.momentId).momentStatus;
                    momentRoomViews.add(momentRoomView);
                }
            }
        }
        return momentRoomViews;
    }

    public LinkedHashMap<String,SliderMessageModel> getSliderMessageView() {
        LinkedHashMap<String,SliderMessageModel> sliderMessageModels = new LinkedHashMap<>();
        if (mMessageRoomHashMap != null) {
            for (Map.Entry<String, RoomsModel> entry : mMessageRoomHashMap.entrySet()) {
                Log.d(TAG, " processing room with Id " + entry.getKey());
                RoomsModel.Detail roomDetail = entry.getValue().detail;

                String timeString = AppLibrary.timeAccCurrentTime(entry.getValue().updatedAt);

                if (entry.getValue().members == null || entry.getValue().members.get(getMyUserId()) == null) {
                    Log.e(TAG, " user itself not found in messageRoom ; skipping room: " + entry.getKey());
                    continue;
                }
                SliderMessageModel sliderMessageModel;
                if (entry.getValue().members.get(getMyUserId()) != null) {
                    sliderMessageModel = new SliderMessageModel(roomDetail.name, roomDetail.imageUrl, roomDetail.roomId, roomDetail.userId, roomDetail.roomType, entry.getValue().members.get(getMyUserId()).type, timeString, entry.getValue().members);
                } else {
                    sliderMessageModel = new SliderMessageModel(roomDetail.name, roomDetail.imageUrl, roomDetail.roomId, roomDetail.userId, roomDetail.roomType, 0, timeString, entry.getValue().members);
                }
                sliderMessageModels.put(roomDetail.roomId,sliderMessageModel);
            }
        }
        return sliderMessageModels;
    }

    private class MediaViewTransactionHandler implements Transaction.Handler {

        @Override
        public Transaction.Result doTransaction(MutableData currentData) {
            if (currentData.getValue() != null) {
                int mediaViews = currentData.getValue(Integer.class);
                mediaViews++;
                currentData.setValue(mediaViews);
            }
            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

        }

    }

    private class ExpiryTypeTransactionHandler implements Transaction.Handler {

        int finalValue = 0;
        String messageId = "";
        String roomId = "";

        @Override
        public Transaction.Result doTransaction(MutableData currentData) {
            if (currentData.getValue() != null) {
                if (currentData.getValue() != finalValue) {
                    currentData.setValue(finalValue);
                }
            } else {
                mFireBase.child(ANCHOR_ROOMS).child(roomId).child(messageId).removeValue();
            }
            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

        }

    }

    public void openMedia(String momentId, String mediaId, String roomId, String messageId, final int expiryType, final int roomType) {
        if (momentId != null) {
            // Update Moment/media Views and viewers
            MediaViewTransactionHandler transactionHandlerMomentMedia = new MediaViewTransactionHandler();
            mFireBase.child(ANCHOR_MOMENTS).child(momentId).child(MEDIA).child(mediaId).child(MEDIA_VIEWS).runTransaction(transactionHandlerMomentMedia);
            mFireBase.child(ANCHOR_MOMENTS).child(momentId).child(MEDIA).child(mediaId).child(VIEW_STATUS).child(getMyUserId()).setValue(1);
            mMoments.get(momentId).media.get(mediaId).status = MEDIA_VIEWED;
        }

        // Update View Status
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).setValue(MEDIA_VIEWED);
        // Update Media views and viewers
        MediaViewTransactionHandler transactionHandlerMedia = new MediaViewTransactionHandler();
        mFireBase.child(ANCHOR_MEDIA).child(mediaId).child(VIEW_STATUS).child(getMyUserId()).setValue(1);
        mFireBase.child(ANCHOR_MEDIA).child(mediaId).child(MEDIA_VIEWS).runTransaction(transactionHandlerMedia);

        if (roomId != null && messageId != null && expiryType != -1) {
            // Update room
            mFireBase.child(ANCHOR_ROOMS).child(roomId).child(MESSAGES).child(messageId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null){
                        RoomsModel.Messages message = dataSnapshot.getValue(RoomsModel.Messages.class);
                        if (message.viewers == null)
                            message.viewers = new HashMap<String, Integer>();
                        message.viewers.put(getMyUserId(),1);
                        if (roomType == FRIEND_ROOM) {
                            if (expiryType <= EXPIRY_TYPE_VIEW_ONCE) {
                                message.expiryType = VIEW_ONCE_AND_VIEWED;
                            } else if (expiryType == MESSAGE_EXPIRED_BUT_NOT_VIEWED) {
                                message.expiryType = REMOVED_UPON_ROOM_OPEN;
                            }
                        }
                        dataSnapshot.getRef().setValue(message);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }


    public void openNearbyMedia(String momentId, String mediaId) {
        if (momentId != null) {
            // Update Moment/media Views and viewers
            Log.d(TAG, "Updating data to Firebase for media " + mediaId + " moment id -" + momentId + " mMoments object -" + mMoments);
            MediaViewTransactionHandler transactionHandlerMomentMedia = new MediaViewTransactionHandler();
            mFireBase.child(ANCHOR_MOMENTS).child(momentId).child(MEDIA).child(mediaId).child(MEDIA_VIEWS).runTransaction(transactionHandlerMomentMedia);
            mFireBase.child(ANCHOR_MOMENTS).child(momentId).child(MEDIA).child(mediaId).child(VIEW_STATUS).child(getMyUserId()).setValue(1);
            if (NearbyMomentDownloader.getNearByMoment(momentId) != null) {
                NearbyMomentDownloader.getNearByMoment(momentId).media.get(mediaId).status = MEDIA_VIEWED;
            } else Log.e(TAG, "openNearbyMedia failed for momentId " + momentId);
        }

        // Update View Status
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).setValue(MEDIA_VIEWED);
        // Update Media views and viewers
        MediaViewTransactionHandler transactionHandlerMedia = new MediaViewTransactionHandler();
        mFireBase.child(ANCHOR_MEDIA).child(mediaId).child(VIEW_STATUS).child(getMyUserId()).setValue(1);
        mFireBase.child(ANCHOR_MEDIA).child(mediaId).child(MEDIA_VIEWS).runTransaction(transactionHandlerMedia);

    }

    public void removeViewOnceMessage(String roomId, String messageId) {
        mFireBase.child(ANCHOR_ROOMS).child(roomId).child(MESSAGES).child(messageId).setValue(null);
    }

    public void sendNotification(NotificationContent notificationContent) {
        mFireBase.child(ANCHOR_REQUEST).child(NOTIFICATION).push().setValue(notificationContent);
    }

    void removeMediaFromMoment(String mediaId, String momentId, boolean sentToRoom) {
        mFireBase.child(ANCHOR_MOMENTS).child(momentId).child(MEDIA).child(mediaId).removeValue();
        if (!sentToRoom)
            removeMediaFromDownloaded(getMyUserId(), mediaId);
    }

    void removeMediaFromDownloaded(String userId, String mediaId) {
        mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).child(mediaId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    if (dataSnapshot.hasChild(URL)) {
                        String filepath = (String) dataSnapshot.child(URL).getValue();
                        if (AppLibrary.checkStringObject(filepath) != null) {
                            File file = new File(filepath);
                            if (file.exists()) {
                                file.delete();
                            }
                        }
                    }
                }
                if (dataSnapshot != null){
                    dataSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void replaceMediaFromFriendRoom(final String mediaId, final String roomId) {
        mFireBase.child(ANCHOR_ROOMS).child(roomId).child(MESSAGES).orderByChild(MEDIA_ID).equalTo(mediaId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.child(EXPIRY_TYPE).getValue() != MESSAGE_EXPIRED_BUT_NOT_VIEWED) {
                    dataSnapshot.getRef().child(MEDIA_ID).setValue(DEFAULT_MEDIA);
                    removeMediaFromDownloaded(getMyUserId(), mediaId);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    void replaceMediaFromGroupRoom(final String mediaId, final String roomId, final boolean sentToFriend) {
        mFireBase.child(ANCHOR_ROOMS).child(roomId).child(MESSAGES).orderByChild(MEDIA_ID).equalTo(mediaId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                dataSnapshot.getRef().child(MEDIA_ID).setValue(DEFAULT_MEDIA);
                if (!sentToFriend)
                    removeMediaFromDownloaded(getMyUserId(), mediaId);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void fetchMomentRooms(final String userId) {
        mFireBase.child(ANCHOR_USERS).child(userId + "/momentRooms").addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                        boolean isExpired = isExpiredMoment(dataSnapshot);
                        if (!isExpired) {
                            mFireBase.child(ANCHOR_ROOMS).child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot roomSnapshot) {
                                    if (mMomentRoomHashMap == null)
                                        mMomentRoomHashMap = new LinkedHashMap<String, RoomsModel>();
                                    mMomentRoomHashMap.put(roomSnapshot.getKey(), roomSnapshot.getValue(RoomsModel.class));
                                    mMomentRoomHashMap.get(roomSnapshot.getKey()).configureDetails(userId, roomSnapshot.getKey());
                                    mMomentRoomHashMap.get(roomSnapshot.getKey()).setUpdatedAt((long) dataSnapshot.child(UPDATED_AT).getValue());
                                    HandleMomentRoom(roomSnapshot, userId);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } else {
                            if (mExpiredMomentRooms == null)
                                mExpiredMomentRooms = new HashMap<>();
                            mExpiredMomentRooms.put(dataSnapshot.getKey(), dataSnapshot.getValue(UserModel.Rooms.class));
                            mExpiredMomentRooms.get(dataSnapshot.getKey()).roomId = dataSnapshot.getKey();
                            cleanMomentRoom(dataSnapshot.getKey(),getMyUserId());
                        }
                }
            }

            @Override
            public void onChildChanged(final DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    boolean isExpired = isExpiredMoment(dataSnapshot);
                    if (!isExpired) {
                        mFireBase.child(ANCHOR_ROOMS).child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot roomSnapshot) {
                                if (mMomentRoomHashMap == null)
                                    mMomentRoomHashMap = new LinkedHashMap<String, RoomsModel>();
                                mMomentRoomHashMap.put(roomSnapshot.getKey(), roomSnapshot.getValue(RoomsModel.class));
                                mMomentRoomHashMap.get(roomSnapshot.getKey()).configureDetails(userId, roomSnapshot.getKey());
                                mMomentRoomHashMap.get(roomSnapshot.getKey()).setUpdatedAt((long) dataSnapshot.child(UPDATED_AT).getValue());
                                HandleMomentRoom(roomSnapshot, userId);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } else {
                        if (mExpiredMomentRooms == null)
                            mExpiredMomentRooms = new HashMap<>();
                        mExpiredMomentRooms.put(dataSnapshot.getKey(), dataSnapshot.getValue(UserModel.Rooms.class));
                        mExpiredMomentRooms.get(dataSnapshot.getKey()).roomId = dataSnapshot.getKey();
                        cleanMomentRoom(dataSnapshot.getKey(),getMyUserId());
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    void fetchMessageRooms(final String userId) {
        this.getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM}).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                processNonExpiredMessageRooms(dataSnapshot, userId);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                processNonExpiredMessageRooms(dataSnapshot, userId);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                if (mMessageRoomHashMap != null && mMessageRoomHashMap.containsKey(dataSnapshot.getKey())) {
                    RoomsModel model = mMessageRoomHashMap.remove(dataSnapshot.getKey());
                    LinkedHashMap<String, RoomsModel> roomsModelLinkedHashMap = new LinkedHashMap<String, RoomsModel>();
                    roomsModelLinkedHashMap.put(dataSnapshot.getKey(), model);
                    roomsModelLinkedHashMap.putAll(mMessageRoomHashMap);
                    mMessageRoomHashMap = roomsModelLinkedHashMap;
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void processNonExpiredMessageRooms(DataSnapshot dataSnapshot, String userId) {
        HandleMessageRoom(dataSnapshot, userId);
    }

    void HandleMessageRoom(final DataSnapshot room, final String userId) {
        final long roomType = (long) (room.child(ROOM_TYPE).getValue());
        final HashMap<String, Integer> transactionMap = new HashMap<>();
        // Fetch all messages which are expired and remove them
        mFireBase.child(ANCHOR_ROOMS).child(room.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    return;
                if (mMessageRoomHashMap == null)
                    mMessageRoomHashMap = new LinkedHashMap<>();
                cleanMediaMessageFromLocalData(dataSnapshot);
                mMessageRoomHashMap.put(dataSnapshot.getKey(), dataSnapshot.getValue(RoomsModel.class));
                mMessageRoomHashMap.get(dataSnapshot.getKey()).configureDetails(userId, room.getKey());
                mMessageRoomHashMap.get(dataSnapshot.getKey()).setUpdatedAt((long) room.child(UPDATED_AT).getValue());
                if (!dataSnapshot.hasChild(MESSAGES)) {
                    if (mSliderMessageListListener != null)
                        mSliderMessageListListener.onSliderListChanged(getSliderMessageView());
                } else {
                    long messageCount = dataSnapshot.child(MESSAGES).getChildrenCount();
                    long msgIndex = 0;
                    for (DataSnapshot message : dataSnapshot.child(MESSAGES).getChildren()) {
                        msgIndex++;
                        boolean isExpired = false;
                        try {
                            isExpired = isExpiredMessage(message);
                        } catch (NullPointerException e) {
                            Log.e(TAG, " rules issue pending fixAsap"/*fixme*/);
                            continue;
                        }
                        if (isExpired) {
                            long memberCount = (long) dataSnapshot.child(MEMBERS).getChildrenCount();
                            long i = 0;
                            boolean viewed = true;
                            MessageTransactionHandler transactionHandler = new MessageTransactionHandler();
                            transactionHandler.mMessage = message;
                            transactionHandler.mRoom = dataSnapshot;
                            for (DataSnapshot member : dataSnapshot.child(MEMBERS).getChildren()) {
                                i++;
                                transactionMap.put(member.getKey(), 0);
                                transactionHandler.isFinal = (i == (memberCount));
                                transactionHandler.mRemove = (i == (memberCount)) && viewed;
                                transactionHandler.userId = member.getKey();
                                if (message.child(VIEW_STATUS).child(member.getKey()).getValue() == null) {
                                    if (roomType == GROUP_ROOM) {
                                        transactionMap.put(member.getKey(), (transactionMap.get(member.getKey() + 1)));
                                    } else {
                                        Log.d(TAG, " message key " + message.getKey());
                                        if (message.child(EXPIRY_TYPE).getValue(Integer.class) != MESSAGE_EXPIRED_BUT_NOT_VIEWED) {
                                            Log.d(TAG, " inside and setting " + message.getKey());
                                            ExpiryTypeTransactionHandler expiryTypeTransactionHandler = new ExpiryTypeTransactionHandler();
                                            expiryTypeTransactionHandler.finalValue = MESSAGE_EXPIRED_BUT_NOT_VIEWED;
                                            expiryTypeTransactionHandler.roomId = room.getKey();
                                            expiryTypeTransactionHandler.messageId = message.getKey();
                                            message.child(EXPIRY_TYPE).getRef().runTransaction(expiryTypeTransactionHandler);
                                            viewed = false;
                                        }
                                    }
                                } else {
                                    transactionMap.put(member.getKey(), (transactionMap.get(member.getKey() + 1)));
                                }
                                if (i == memberCount) {
                                    // Trigger
                                    Log.d(TAG, "Trigger for messages ");
                                    if (mSliderMessageListListener != null)
                                        mSliderMessageListListener.onSliderListChanged(getSliderMessageView());
                                    runTransaction(room, transactionMap, message);
                                }
                            }
                        } else {
                            if ((msgIndex == messageCount)) {
                                // Trigger
                                Log.d(TAG, "Trigger for messages ");
                                if (mSliderMessageListListener != null)
                                    mSliderMessageListListener.onSliderListChanged(getSliderMessageView());
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public void runTransaction(DataSnapshot room, HashMap<String, Integer> map, DataSnapshot message) {
        int i = 0;
        for (Map.Entry<String, Integer> member : map.entrySet()) {
            i++;
            MessageTransactionHandler transactionHandler = new MessageTransactionHandler();
            transactionHandler.mMessage = message;
            transactionHandler.mRoom = room;
            if (map.get(member.getKey()) != null) {
                transactionHandler.deductions = map.get(member.getKey());
                if (transactionHandler.deductions > 0)
                    mFireBase.child(ANCHOR_ROOMS).child(room.getKey()).child(MESSAGES).child(member.getKey()).child(MESSAGE_COUNT).runTransaction(transactionHandler);
            }
        }
    }

    public void saveMoment(MomentModel moment, final String userId) {
        for (Map.Entry<String, MomentModel.Media> media : moment.media.entrySet()) {
            checkAndSaveMediaDownload(moment.media.get(media.getKey()), userId, moment.momentId, null);
        }
        checkForReadyToView(moment, userId);
    }

    public void checkForReadyToView(final MomentModel moment, final String userId) {
        mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).orderByChild(MOMENT_ID).equalTo(moment.momentId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean hasAny = dataSnapshot.hasChildren();
                Log.d(TAG, "children " + dataSnapshot.getChildren());
                Log.d(TAG, " hasAny = " + hasAny);
                boolean isReady = true;
                if (hasAny) {
                    for (DataSnapshot media : dataSnapshot.getChildren()) {
                        Log.d(TAG, " media id = " + media.getKey());
                        Log.d(TAG, " media status = " + media.child(LOCAL_MEDIA_STATUS).getValue());
                        if (media.child(LOCAL_MEDIA_STATUS).getValue(Integer.class) < MEDIA_DOWNLOAD_COMPLETE) {
                            isReady = false;
                            mMoments.get(moment.momentId).momentStatus = UNSEEN_MOMENT;
                            break;
                        } else {
                            // check for existing file on device
                            boolean doesFileExists = fileExists((String) media.child(URL).getValue());
                            if (doesFileExists)
                                mMoments.get(moment.momentId).media.get(media.getKey()).url = (String) media.child(URL).getValue();
                            else {
                                if (mMoments.get(moment.momentId).momentStatus == SEEN_MOMENT) {
                                    mMoments.get(moment.momentId).momentStatus = SEEN_BUT_DOWNLOADING;
//                                    if(momentsDataListener != null)
//                                        momentsDataListener.onSeenDataChanged(getSeenMomentRoomView());
                                    downloadMoment(moment.momentId);
                                }
                                // start download moment
                                isReady = false;
                                break;
                            }
                        }
                    }
                } else {
                    isReady = false;
                }
                if (isReady) {
                  if (mMoments.get(moment.momentId).momentStatus == SEEN_BUT_DOWNLOADING) {
                        mMoments.get(moment.momentId).momentStatus = SEEN_MOMENT;
                    } else if (mMoments.get(moment.momentId).momentStatus != SEEN_MOMENT) {
                        mMoments.get(moment.momentId).momentStatus = READY_TO_VIEW_MOMENT;
                    }
                }
                if (momentsDataListener != null)
                    momentsDataListener.onUnseenDataChanged(getMomentRoomView());
                if (momentsDataListener != null)
                    momentsDataListener.onSeenDataChanged(getSeenMomentRoomView());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public boolean fileExists(String value) {
        return new File(value).exists();
    }

    void checkAndSaveMediaDownload(final MomentModel.Media mediaData, final String userId, final String momentId, final String roomId) {
        final String mediaId = mediaData.mediaId;
        mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot media : dataSnapshot.getChildren()) {
                    if (media.getKey().equals(mediaId) && media.child(LOCAL_MEDIA_STATUS).getValue(Integer.class) > MEDIA_DOWNLOADING) {
                        // Already Downloaded
                        boolean doesFileExists = fileExists((String) media.child(URL).getValue());
                        if (doesFileExists) {
                            int myStatus = 0;
                            if (mMoments.get(momentId).media.get(mediaId).viewers != null && mMoments.get(momentId).media.get(mediaId).viewers.containsKey(userId)){
                                myStatus = mMoments.get(momentId).media.get(mediaId).viewers.get(userId);
                            }

                            if (myStatus > 0) {
                                mMoments.get(momentId).media.get(mediaId).status = MEDIA_VIEWED;
                            } else {
                                mMoments.get(momentId).media.get(mediaId).status = MEDIA_DOWNLOAD_COMPLETE;
                            }
                        }
                        return;
                    }
                }
                mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).setValue(MEDIA_DOWNLOAD_NOT_STARTED);
                if (momentId != null)
                    mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).child(mediaId).child(MOMENT_ID).setValue(momentId);
                if (roomId != null)
                    mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).child(mediaId).child(ROOM_ID).setValue(roomId);

                mFireBase.child(ANCHOR_LOCAL_DATA).child(userId).child(MEDIA_DOWNLOAD).child(mediaId).child(URL).setValue(mediaData.url);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void HandleMomentRoom(final DataSnapshot dataSnapshotRoom, final String userId) {
        Long type = (Long) dataSnapshotRoom.child(ROOM_TYPE).getValue();
        String momentId = "";
        if (type == FRIEND_ROOM) {
            for (DataSnapshot user : dataSnapshotRoom.child(MEMBERS).getChildren()) {
                if (!user.getKey().equals(userId)) {
                    momentId = (String) user.child(MOMENT_ID).getValue();
                    break;
                }
            }
        }
        final String momentIdFinal = momentId;
        mFireBase.child(ANCHOR_MOMENTS).child(momentId).child(MEDIA).keepSynced(true);
        mFireBase.child(ANCHOR_MOMENTS).child(momentId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                final int[] momentStatus = {SEEN_MOMENT};
                final LinkedHashMap<String, MomentModel.Media> medias = new LinkedHashMap<>();
                MomentModel momentFinal = dataSnapshot.getValue(MomentModel.class);
                for (DataSnapshot media : dataSnapshot.child(MEDIA).getChildren()){
                    MomentModel.Media mediaObj = media.getValue(MomentModel.Media.class);
                    mediaObj.mediaId = media.getKey();
                    mediaObj.momentId = momentIdFinal;
                    boolean isExpired = isExpiredMedia(media);
                    if (isExpired) {
                        cleanMedia(media.getKey());
                    } else {
                        medias.put(media.getKey(), mediaObj);
                        if (!media.hasChild(MEDIA_VIEWS_USERS) || !media.child(MEDIA_VIEWS_USERS).hasChild(userId)) {
                            momentStatus[0] = UNSEEN_MOMENT;
                        }
                    }
                }
                HashMap<String,MomentModel.Media> oldMedias = null;
                if (mMoments != null && mMoments.containsKey(momentIdFinal)){
                    oldMedias = mMoments.get(momentIdFinal).media;
                }
                cleanMomentMediaFromLocalData(momentIdFinal,oldMedias,medias);
                momentFinal.media = medias;
                momentFinal.momentStatus = momentStatus[0];
                momentFinal.momentId = dataSnapshot.getKey();
                if (mMoments == null) {
                    mMoments = new LinkedHashMap<String, MomentModel>();
                }
                if (medias.isEmpty()){
                    if (mMoments.containsKey(momentIdFinal)){
                        mMoments.remove(momentIdFinal);
                        cleanMomentRoom(dataSnapshotRoom.getKey(),getMyUserId());
                    }
                } else {
                    mMoments.put(dataSnapshot.getKey(), momentFinal);
                    saveMoment(momentFinal, userId);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void cleanMediaMessageFromLocalData(DataSnapshot roomSnapshot){
        RoomsModel model = roomSnapshot.getValue(RoomsModel.class);
        final HashMap<String,RoomsModel.Messages> messagesHashMap = model.messages;
        if (messagesHashMap != null && !messagesHashMap.isEmpty()) {
            mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).orderByChild("rooms/" + roomSnapshot.getKey()).equalTo(1).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getValue() != null && dataSnapshot.getChildrenCount() > 0) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            String mediaId = snapshot.getKey();
                            boolean isMediaFound = false;
                            for (Map.Entry<String, RoomsModel.Messages> entry : messagesHashMap.entrySet()) {
                                if (entry.getValue().type == MESSAGE_TYPE_MEDIA && entry.getValue().mediaId.equals(mediaId)) {
                                    isMediaFound = true;
                                    break;
                                }
                            }
                            if (!isMediaFound) {
                                // delete media for local db
                                removeMediaFromDownloaded(getMyUserId(), mediaId);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    private void cleanMessageMediaFromLocalData(String userId){
        this.getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    getNewFireBase(ANCHOR_ROOMS,new String[]{snapshot.getKey()}).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot roomSnapshot) {
                            RoomsModel model = roomSnapshot.getValue(RoomsModel.class);
                            final HashMap<String,RoomsModel.Messages> messagesHashMap = model.messages;
                            if (messagesHashMap != null && !messagesHashMap.isEmpty()) {
                                mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).orderByChild("rooms/" + roomSnapshot.getKey()).equalTo(1).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            String mediaId = snapshot.getKey();
                                            boolean isMediaFound = false;
                                            for (Map.Entry<String,RoomsModel.Messages> entry : messagesHashMap.entrySet()){
                                                if (entry.getValue().type == MESSAGE_TYPE_MEDIA && entry.getValue().mediaId.equals(mediaId)){
                                                    isMediaFound = true;
                                                    break;
                                                }
                                            }
                                            if (!isMediaFound){
                                                // delete media for local db
                                                removeMediaFromDownloaded(getMyUserId(),mediaId);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void cleanMomentMediaFromLocalData(String momentId, HashMap<String,MomentModel.Media> previousMediaList, HashMap<String,MomentModel.Media> newMediaList){
        if (previousMediaList != null && newMediaList != null) {
            for (Map.Entry<String, MomentModel.Media> entry : previousMediaList.entrySet()) {
                if (!newMediaList.containsKey(entry.getKey())) {
                    cleanMedia(entry.getKey());
                }
            }
        } else {
            cleanLocalData(momentId,newMediaList);
        }
    }

    public void cleanLocalData(final String momentId, final HashMap<String,MomentModel.Media> currentMedias){
        if (currentMedias == null)
            return;
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).orderByChild(MOMENT_ID).equalTo(momentId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (currentMedias != null && !currentMedias.isEmpty()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (!currentMedias.containsKey(snapshot.getKey())){
                            removeMediaFromDownloaded(getMyUserId(),snapshot.getKey());
                        }
                    }
                } else {
                    mFireBase.child(MOMENTS).child(momentId).child(MEDIA).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot medias) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                if (!medias.hasChild(snapshot.getKey())){
                                    removeMediaFromDownloaded(getMyUserId(),snapshot.getKey());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void cleanMomentRoom(final String roomId, final String userId) {
        // Fetch Room's momentId
        mFireBase.child(ANCHOR_ROOMS).child(roomId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long type = (Long) dataSnapshot.child(ROOM_TYPE).getValue();
                String momentId = "";
                if (type == FRIEND_ROOM) {
                    for (DataSnapshot user : dataSnapshot.child(MEMBERS).getChildren()) {
                        if (!user.getKey().equals(userId)) {
                            momentId = (String) user.child(MOMENT_ID).getValue();
                            break;
                        }
                    }
                } else if (type == GROUP_ROOM) {
                    momentId = (String) dataSnapshot.child(DETAIL).child(MOMENT_ID).getValue();
                }
                mFireBase.child(ANCHOR_MOMENTS).child(momentId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot media : dataSnapshot.child(MEDIA).getChildren()) {
                            cleanMedia(media.getKey());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mFireBase.child(ANCHOR_USERS).child(userId).child(MOMENT_ROOM).child(roomId).removeValue();
    }

    public boolean isExpiredMedia(DataSnapshot media) {
        long now = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
        Long dtStart = (Long) media.child(CREATED_AT).getValue();
        if(dtStart==null)
            Log.e(TAG," crashing on  "+media);
        if (now > dtStart) {
            return true;
        }
        return false;
    }

    boolean isExpiredMoment(DataSnapshot momentRoom) {
        long now = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
        Long dtStart = (Long) momentRoom.child(UPDATED_AT).getValue();
        if (now > dtStart) {
            return true;
        }
        return false;
    }

    boolean isExpiredMessage(DataSnapshot message) {
        long now = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
        Long dtStart = (Long) message.child(CREATED_AT).getValue();
        if (now > dtStart) {
            return true;
        }
        return false;
    }

    void cleanData(String userId) {
        fetchMomentRooms(userId);
        fetchMessageRooms(getMyUserId());
//        cleanMessageMediaFromLocalData(userId);
    }

    void loadData() {
        if (mUserModel == null)
            loadMyUserModel();
        if (getSocialModel() == null)
            loadMySocialModel(getMyUserId());
        if (mFacebookSuggestedFriendList == null)
            loadFriendSuggestions();
    }

    int myMomentStatus = SEEN_MOMENT;
    void loadMyMoment() {
        mFireBase.child(ANCHOR_MOMENTS).child(mUserModel.momentId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myMoment = dataSnapshot.getValue(MomentModel.class);
                myMoment.momentId = mUserModel.momentId;
                final LinkedHashMap<String, MomentModel.Media> medias = new LinkedHashMap<>();
                dataSnapshot.getRef().child(MEDIA).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot media, String s) {
                        MomentModel.Media mediaObj = media.getValue(MomentModel.Media.class);
                        mediaObj.mediaId = media.getKey();
                        mediaObj.momentId = mUserModel.momentId;
                        boolean isExpired = isExpiredMedia(media);
                        if (isExpired) {
                            cleanMedia(media.getKey());
                        } else {
                            medias.put(media.getKey(), mediaObj);
                            if (!media.child(MEDIA_VIEWS_USERS).hasChild(getMyUserId())) {
                                myMomentStatus = UNSEEN_MOMENT;
                            }
                            feedMediaToMyStreams(mediaObj);
                        }
                        myMoment.momentStatus = myMomentStatus;
                        myMoment.media = medias;
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        MomentModel.Media mediaObj = dataSnapshot.getValue(MomentModel.Media.class);
                        mediaObj.mediaId = dataSnapshot.getKey();
                        mediaObj.momentId = mUserModel.momentId;
                        String mediaId = dataSnapshot.getKey();
                        if (myMedias.containsKey(mediaId)){
                            medias.put(mediaId, mediaObj);
                            if (!dataSnapshot.child(MEDIA_VIEWS_USERS).hasChild(getMyUserId())) {
                                myMomentStatus = UNSEEN_MOMENT;
                            }
                            feedMediaToMyStreams(mediaObj);
                        }
                        myMoment.momentStatus = myMomentStatus;
                        myMoment.media = medias;
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                fetchMyPendingStreams();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public ArrayList<SliderMessageModel> getAllMessageRoom() {
        if (mMessageRoomHashMap == null)
            return null;
        ArrayList<SliderMessageModel> arrayList = new ArrayList<>();
        for (Map.Entry<String, RoomsModel> roomEntry : mMessageRoomHashMap.entrySet()) {
            RoomsModel roomsModel = roomEntry.getValue();
            if (roomsModel == null) continue;
            String displayName = null, imageUrl = null;
            String friendId = null;
            if (roomsModel.type == FRIEND_ROOM) {
                HashMap<String, RoomsModel.Members> members = roomsModel.members;
                for (Map.Entry<String, RoomsModel.Members> membersEntry : members.entrySet()) {
                    if (!membersEntry.getKey().equals(myUserId)) {
                        displayName = membersEntry.getValue().name;
                        friendId = membersEntry.getKey();
                        imageUrl = membersEntry.getValue() == null ? null : membersEntry.getValue().imageUrl;
                        break;
                    }
                }
            } else if (roomsModel.type == GROUP_ROOM) {
                displayName = roomsModel.detail == null ? null : roomsModel.detail.name;
                imageUrl = roomsModel.detail == null ? null : roomsModel.detail.imageUrl;
            }
            SliderMessageModel model = new SliderMessageModel(displayName, imageUrl, roomEntry.getKey(), friendId, roomsModel.type, 000, null, roomsModel.members);
            model.setFirstChar(displayName);
            arrayList.add(model);
        }
        return arrayList;
    }

    /**
     * 9. Accept ` Request
     * <p/>
     * STEP 1:
     * UPDATE rooms/<roomId>/members as
     * <reqUser._id> : {
     * name: String,
     * imageUrl: String,
     * handle: String,
     * momentId : String,
     * type: 12 // you joined
     * }
     * <p/>
     * STEP 2
     * REMOVE rooms/<roomId>/pendingMembers/<reqUser._id>
     * REMOVE socials/<reqUser._id>/pendingGroupRequest/<roomId>
     * }
     */
    public void acceptGroupRequest(final String reqUser, final String roomId, final SocialModel.PendingGroupRequest requestReceived) {
        Log.d(TAG, " accepting group request with roomId: " + roomId);

        //STEP 1
        final RoomsModel.Members members = new RoomsModel.Members();
        members.name = getMyUserModel().name;
        members.imageUrl = getMyUserModel().imageUrl;
        members.handle = getMyUserModel().handle;
        members.momentId = getMyUserModel().momentId;
        members.type = YOU_JOIN;
        getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, reqUser}).setValue(members);

        //STEP 2
        getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, PENDING_MEMBERS, reqUser}).setValue(null);
        getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUser, PENDING_GROUP_REQUEST, roomId}).setValue(null);
        /**
         * STEP 3
         STEP 3.1
         PUSH rooms/<roomId>/messages
         {
         type: 4,
         text: <reqUser Name> Joined
         createdAt: Date,
         expiryType: 3
         }

         STEP 3.2 Iterate over each members except reqUser
         STEP 3.2.1
         UPDATE rooms/<roomId>/members/<userId> as {type : 11}
         STEP 3.2.3
         SET Priority of users/<userId>/messageRooms/<roomId> with -ve timestamp

         */
        //STEP3
        //3.1
        RoomsModel.Messages message = new RoomsModel.Messages();
        message.type = SYSTEM_MESSAGE;
        message.createdAt = getCreatedAt();
        message.expiryType = EXPIRY_TYPE_24_HOURS;
        message.text = getMyUserModel().name + " joined";
        getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MESSAGES}).push().setValue(message);

        //3.2
        getNewFireBase(ANCHOR_ROOMS, new String[]{roomId}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RoomsModel roomsModel = dataSnapshot.getValue(RoomsModel.class);
                final HashMap<String, RoomsModel.Members> roomMembers = roomsModel.members;
                for (Map.Entry<String, RoomsModel.Members> memberEntry : roomMembers.entrySet()) {
                    if (memberEntry.getKey().equals(reqUser)) continue;
                    String userId = memberEntry.getKey();
                    getNewFireBase(ANCHOR_ROOMS, new String[]{roomId, MEMBERS, userId, TYPE}).setValue(MEMBER_JOIN);
                    getNewFireBase(ANCHOR_USERS, new String[]{userId, MESSAGE_ROOM, roomId}).setPriority(getPriorityTimeStamp());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        //STEP 4
        //4.1
        UserModel.Rooms room = new UserModel.Rooms();
        room.updatedAt = getUpdatedAt();
        room.type = GROUP_ROOM;
        room.interacted = false;
        getNewFireBase(ANCHOR_USERS, new String[]{reqUser, MESSAGE_ROOM, roomId}).setValue(room);

        //4.2
        getNewFireBase(ANCHOR_USERS, new String[]{reqUser, MESSAGE_ROOM, roomId}).setPriority(getPriorityTimeStamp());

        //4.3
//        public Groups(String name, String imageUrl, String handle, String admin,String roomId){
        SocialModel.Groups groups = new //fixme admin??
                SocialModel.Groups(requestReceived.name, requestReceived.imageUrl, null, null, roomId);
        getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUser, GROUPS, roomId}).setValue(groups);
    }

    /**
     * Group Request
     * INPUT : reqUser, roomId
     * <p/>
     * STEP 1:
     * Remove socials/<reqUser._id>/pendingGroupRequest/<roomId>
     * <p/>
     * STEP 2:
     * UPDATE socials/<reqUser._id>/groupRequestIgnored with key as <roomId> and value as 1
     */
    public void ignoreGroupRequest(String reqUser, String roomId) {

        Log.d(TAG, " ignoring group request roomId: " + roomId);
        //STEP 1
        getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUser, PENDING_GROUP_REQUEST, roomId}).setValue(null);

        //STEP 2
        getNewFireBase(ANCHOR_SOCIALS, new String[]{reqUser, GROUP_REQUEST_IGNORED, roomId}).setValue(1);
    }

    public void registerChatMediaForDownload(final String mediaId) {
        getNewFireBase(ANCHOR_MEDIA, new String[]{mediaId}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                MediaModel mediaModel = dataSnapshot.getValue(MediaModel.class);
                if (mediaModel == null) {
                    Log.d(TAG, " registerChatMediaForDownload failed returning");
                    return;
                }
                MediaDownloader downloader = new MediaDownloader(mContext);
                downloader.startDownload(dataSnapshot.getKey(), mediaModel, new DownloadStatusCallbacks() {
                    @Override
                    public void updateDownloadingStatus(String mediaId, int status) {
                        if (status != DOWNLOADED_CHAT_MEDIA) {
                            ChatMediaModel chatMediaModel = chatMediaMap.get(mediaId);
                            chatMediaModel.status = status;
                            chatMediaStatusChangeListener.onMediaChanged();
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public interface DownloadStatusCallbacks {
        void updateDownloadingStatus(String mediaId, int status);
    }

    public void updateLocalPathForChatMedia(MediaModel mediaModel,String mediaId, String path, int status) {
        final ChatMediaModel chatMediaModel = chatMediaMap.get(mediaId);
        chatMediaModel.uri = path;
        chatMediaModel.status = DOWNLOADED_CHAT_MEDIA;
        chatMediaStatusChangeListener.onMediaChanged();
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(URL).setValue(path);
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).setValue(status);
        if (mediaModel != null && mediaModel.addedTo != null && mediaModel.addedTo.rooms != null){
            mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(ROOMS).setValue(mediaModel.addedTo.rooms);
        }
    }

    public void updateDownloadDetailsForMedia(String mediaId, MediaModel mediaModel, int status) {
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(URL).setValue(mediaModel.url);
        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(LOCAL_MEDIA_STATUS).setValue(status);
        if (mediaModel.addedTo != null){
            if (mediaModel.addedTo.moments != null){
                HashMap<String,Integer> moments = mediaModel.addedTo.moments;
                for (Map.Entry<String,Integer> entry : moments.entrySet()){
                    if (entry.getValue() == MY_MOMENT){
                        mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(MOMENT_ID).setValue(entry.getKey());
                        break;
                    }
                }
            }
            if (mediaModel.addedTo.rooms != null){
                mFireBase.child(ANCHOR_LOCAL_DATA).child(getMyUserId()).child(MEDIA_DOWNLOAD).child(mediaId).child(ROOMS).setValue(mediaModel.addedTo.rooms);
            }
        }
    }

    public LinkedHashMap<String, ChatMediaModel> getChatMediaStatusMap() {
        return chatMediaMap;
    }

    private LinkedHashMap<String, ChatMediaModel> chatMediaMap = new LinkedHashMap<>();

    public void checkChatMediaDownloadStatus(final RoomsModel.Messages message) {
        final String mediaId = message.mediaId;
        ChatMediaModel model = new ChatMediaModel();
        model.mediaId = mediaId;
        model.expiryType = message.expiryType;
        model.messageId = message.messageId;
        model.memberId = message.memberId;
        chatMediaMap.put(mediaId, model);
        getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{getMyUserId(), MEDIA_DOWNLOAD, mediaId}).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    ChatMediaModel model = chatMediaMap.get(mediaId);
                    int status = dataSnapshot.child(LOCAL_MEDIA_STATUS).getValue(Integer.class);
                    if (status >= DOWNLOADED_CHAT_MEDIA) {
                        if (!fileExists(dataSnapshot.child(URL).getValue().toString())) {
                            if (message.memberId.equals(getMyUserId()) && message.mediaUploadingStatus != MEDIA_UPLOADING_COMPLETE){
                                // delete the media message
                                chatMediaStatusChangeListener.deleteMediaMessage(message.messageId);
                            } else {
                                status = UNSEEN_CHAT_MEDIA;
                                model.status = status;
                                registerChatMediaForDownload(mediaId);
                            }
                        } else {
                            model.uri = dataSnapshot.child(URL).getValue().toString();
                            model.status = status;
                            chatMediaStatusChangeListener.onMediaChanged();
                        }
                    }
                } else {
                    ChatMediaModel model = chatMediaMap.get(mediaId);
                    model.status = UNSEEN_CHAT_MEDIA;
                    chatMediaStatusChangeListener.onMediaChanged();
                    registerChatMediaForDownload(mediaId);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public void retryDownloadingChatMedia(RoomsModel.Messages messages) {
        if (messages.mediaId != null)
            registerChatMediaForDownload(messages.mediaId);
    }

    private ChatMediaStatusChangeListener chatMediaStatusChangeListener;

    public void setChatMediaStatusChangeListener(ChatMediaStatusChangeListener chatMediaStatusChangeListener) {
        this.chatMediaStatusChangeListener = chatMediaStatusChangeListener;
    }

    public interface ChatMediaStatusChangeListener {
        void onMediaChanged();
        void deleteMediaMessage(String messageId);
    }
}
