package com.instalively.android.downloader;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.instalively.android.MasterClass;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.models.MediaModel;
import com.instalively.android.models.MomentModel;
import com.instalively.android.util.AppLibrary;

import java.io.File;

/**
 * Created by user on 5/10/2016.
 */
public class MediaDownloader implements FireBaseKEYIDS {

    private static final String TAG = "MediaDownloader";
    private Context mContext;
    private HandlerThread downloaderThread;
    private Handler downloadHandler;
    private String mediaUrl;
    private TransferObserver download;
    private int transferId;
    private String mediaStoragePath;
    private String mediaId;


    public MediaDownloader(Context context) {
        mContext = context;
//        this.downloaderThread = new HandlerThread("downloadMediaThread");
//        this.downloaderThread.start();
//        this.downloadHandler = new Handler(this.downloaderThread.getLooper());
        validateFileSystem();
    }

    private void validateFileSystem() {
        mediaStoragePath = AppLibrary.getFilesDirectory(mContext);
        File mediaDir = new File(mediaStoragePath);
        if (!mediaDir.exists())
            mediaDir.mkdirs();
    }

    // FOR MOMENT MEDIA DOWNLOAD
    public void startDownload(final MomentModel.Media media) {
        this.mediaUrl = media.url;
        mediaId = media.mediaId;
        int p = mediaUrl.lastIndexOf(".");
        String extension = mediaUrl.substring(p + 1);
        if (p == -1 || !extension.matches("\\w+")) {
            /* file has no extension */
            return;
        }
        mediaStoragePath = mediaStoragePath + mediaId + "." + extension;
        File file = new File(mediaStoragePath);
        String key = this.mediaUrl.split("pulse.resources/")[1];
        if (AppLibrary.getMediaType(mediaUrl) == AppLibrary.MEDIA_TYPE_IMAGE) {
            this.download = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file); //Starting upload
        } else {
            this.download = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file);
        }
        this.transferId = this.download.getId();
        this.download.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int i, TransferState transferState) {
                AppLibrary.log_d(TAG, "onStateChanged -" + transferState.toString());
                if (transferState.toString().equals("IN_PROGRESS")) {
                    AppLibrary.log_d(TAG, "MEDIA DOWNLOAD IN_PROGRESS");
                    FireBaseHelper.getInstance(mContext).updateDownloadStatus(media.momentId, media.mediaId, MEDIA_DOWNLOADING);
                } else if (transferState.toString().equals("COMPLETED")) {
                    AppLibrary.log_d(TAG, "MEDIA DOWNLOAD COMPLETED");
                    FireBaseHelper.getInstance(mContext).updateLocalPath(media.momentId, mediaId, mediaStoragePath);
                    FireBaseHelper.getInstance(mContext).updateDownloadStatus(media.momentId, media.mediaId, MEDIA_DOWNLOAD_COMPLETE);
                } else if (transferState == TransferState.FAILED){
                    FireBaseHelper.getInstance(mContext).onMomentMediaDownloadFailed(media.momentId,media.mediaId);
                }
            }

            @Override
            public void onProgressChanged(int i, long l, long l1) {

            }

            @Override
            public void onError(int i, Exception e) {
                FireBaseHelper.getInstance(mContext).onMomentMediaDownloadFailed(media.momentId,media.mediaId);
                MasterClass.getTransferUtility().cancel(transferId);
                e.printStackTrace();
            }
        });
    }

    // FOR CHAT MEDIA DOWNLOAD
    public void startDownload(final String mediaId, final MediaModel mediaModel, final FireBaseHelper.DownloadStatusCallbacks statusCallbacks) {
        String mediaUrl = mediaModel.url;
        int p = mediaUrl.lastIndexOf(".");
        String extension = mediaUrl.substring(p + 1);
        if (p == -1 || !extension.matches("\\w+")) {
            /* file has no extension */
            return;
        }
        mediaStoragePath = mediaStoragePath + mediaId + "." + extension;
        File file = new File(mediaStoragePath);
        String key = mediaUrl.split("pulse.resources/")[1];
        if (AppLibrary.getMediaType(mediaUrl) == AppLibrary.MEDIA_TYPE_IMAGE) {
            this.download = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file); //Starting upload
        } else {
            this.download = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file);
        }
        this.transferId = this.download.getId();
        this.download.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int i, TransferState transferState) {
                AppLibrary.log_d(TAG, "onStateChanged -" + transferState.toString());
                if (transferState.toString().equals("IN_PROGRESS")) {
                    AppLibrary.log_d(TAG, "MEDIA DOWNLOAD IN_PROGRESS");
                    statusCallbacks.updateDownloadingStatus(mediaId,DOWNLOADING_CHAT_MEDIA);
                } else if (transferState.toString().equals("COMPLETED")) {
                    AppLibrary.log_d(TAG, "MEDIA DOWNLOAD COMPLETED");
                    FireBaseHelper.getInstance(mContext).updateLocalPathForChatMedia(mediaModel,mediaId, mediaStoragePath,DOWNLOADED_CHAT_MEDIA);
                    statusCallbacks.updateDownloadingStatus(mediaId,DOWNLOADED_CHAT_MEDIA);
                } else if (transferState == TransferState.FAILED){
                    statusCallbacks.updateDownloadingStatus(mediaId,DOWNLOAD_CHAT_MEDIA_FAILED);
                }
            }

            @Override
            public void onProgressChanged(int i, long l, long l1) {

            }

            @Override
            public void onError(int i, Exception e) {
                statusCallbacks.updateDownloadingStatus(mediaId,DOWNLOAD_CHAT_MEDIA_FAILED);
                MasterClass.getTransferUtility().cancel(transferId);
                e.printStackTrace();
            }
        });
    }

    // FOR MY MOMENT MEDIA DOWNLOAD
    public void startDownload(final String mediaId, String mediaUrl, final FireBaseHelper.OnMyMomentMediaDownloadCallback statusCallbacks) {
        int p = mediaUrl.lastIndexOf(".");
        String extension = mediaUrl.substring(p + 1);
        if (p == -1 || !extension.matches("\\w+")) {
            /* file has no extension */
            return;
        }
        mediaStoragePath = mediaStoragePath + mediaId + "." + extension;
        File file = new File(mediaStoragePath);
        String key = mediaUrl.split("pulse.resources/")[1];
        if (AppLibrary.getMediaType(mediaUrl) == AppLibrary.MEDIA_TYPE_IMAGE) {
            this.download = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file); //Starting upload
        } else {
            this.download = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file);
        }
        this.transferId = this.download.getId();
        this.download.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int i, TransferState transferState) {
                AppLibrary.log_d(TAG, "onStateChanged -" + transferState.toString());
                if (transferState.toString().equals("IN_PROGRESS")) {
                    AppLibrary.log_d(TAG, "MEDIA DOWNLOAD IN_PROGRESS");
                    statusCallbacks.onDownloadCallback(mediaId,DOWNLOADING_MY_MOMENT_MEDIA,null);
                } else if (transferState.toString().equals("COMPLETED")) {
                    AppLibrary.log_d(TAG, "MEDIA DOWNLOAD COMPLETED");
                    statusCallbacks.onDownloadCallback(mediaId,DOWNLOADED_MY_MOMENT_MEDIA,mediaStoragePath);
                } else if (transferState == TransferState.FAILED){
                    statusCallbacks.onDownloadCallback(mediaId,DOWNLOAD_MY_MOMENT_MEDIA_FAILED,null);
                }
            }

            @Override
            public void onProgressChanged(int i, long l, long l1) {

            }

            @Override
            public void onError(int i, Exception e) {
                statusCallbacks.onDownloadCallback(mediaId,DOWNLOAD_MY_MOMENT_MEDIA_FAILED,null);
                MasterClass.getTransferUtility().cancel(transferId);
                e.printStackTrace();
            }
        });
    }

}