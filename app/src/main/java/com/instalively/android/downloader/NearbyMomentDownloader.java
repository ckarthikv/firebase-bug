package com.instalively.android.downloader;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.instalively.android.MasterClass;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.models.MomentModel;
import com.instalively.android.util.AppLibrary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 5/10/2016.
 */
@SuppressWarnings("AccessStaticViaInstance")
public class NearbyMomentDownloader implements FireBaseKEYIDS {

    private static final String TAG = NearbyMomentDownloader.class.getSimpleName();
    private static Context mContext;
    private static String myUserId;
    private static DataSnapshot localMediaSnap;
    private static List<NearbyMomentDownloader> instanceList;
    private static String mediaStoragePath;
    private HashMap<Integer, MomentModel.Media> downloadIdMediaIdMap;
    private MomentModel nearByMoment;
    private LinkedHashMap<String, MomentModel.Media> pendingDownloadMediaMap;
    private FireBaseHelper fireBaseHelper;
    private String momentId;
    private HashMap<String, Integer> downloadRetryMap;

    public enum DownloaderState {
        LOADING_NOT_STARTED, LOADING_IN_PROGRESS, LOADING_COMPLETE
    }

    private DownloaderState downloadState;

    public static NearbyMomentDownloader getNearbyMediaDownloader(Context context, MomentModel nearByMoment) {
        if (!downloaderReady)
            throw new RuntimeException(" downloader not ready; local data not loaded");
        if (instanceList == null) {
            NearbyMomentDownloader downloader = new NearbyMomentDownloader(context, nearByMoment);
            instanceList.add(downloader);
            return downloader;

        } else {
            for (NearbyMomentDownloader downloader : instanceList) {
                if (downloader.momentId.equals(nearByMoment.momentId))
                    return downloader;
            }
            NearbyMomentDownloader downloader = new NearbyMomentDownloader(context, nearByMoment);
            instanceList.add(downloader);
            return downloader;
        }
    }


    /**
     * @return when the local download data is ready and
     * hence the class is ready for use
     */
    public static boolean isDownloaderReady(Context context) {
        if (!downloaderReady)
            refreshLocalData(context);
        return downloaderReady;
    }


    public DownloaderState getDownloadState() {
        return downloadState;
    }

    private NearbyMomentDownloader(Context context, MomentModel nearByMoment) {
        this.mContext = context;
        this.fireBaseHelper = FireBaseHelper.getInstance(context);
        this.myUserId = fireBaseHelper.getMyUserId();
        this.downloadState = DownloaderState.LOADING_NOT_STARTED;
        this.validateFileSystem();
        if (instanceList == null)
            this.instanceList = new ArrayList<>();
        this.nearByMoment = nearByMoment;
        this.momentId = nearByMoment.momentId;
        this.pendingDownloadMediaMap = new LinkedHashMap<>();
        this.downloadIdMediaIdMap = new HashMap<>();
        this.fireBaseHelper = FireBaseHelper.getInstance(context);
        this.downloadRetryMap = new HashMap<>();
        this.checkIfMediasPresentInDisk();
        Log.d(TAG, " constructed new downloader " + localMediaSnap);
    }


    private void checkIfMediasPresentInDisk() {
        if (nearByMoment.media == null) {
            Log.e(TAG, "no media found in this moment");
            return;
        }
        int totalMedia = nearByMoment.media.size();
        int filesPresentInDisk = 0;
        for (Map.Entry<String, MomentModel.Media> mediaEntry : nearByMoment.media.entrySet()) {
            final MomentModel.Media media = mediaEntry.getValue();
            media.mediaId = mediaEntry.getKey();
            String uri = getLocalURIFromMediaModel(media);
            if (new File(uri).exists()) {
                ++filesPresentInDisk;
            }
        }
        if (filesPresentInDisk == totalMedia) {
            this.downloadState = DownloaderState.LOADING_COMPLETE;
            Log.d(TAG, " allMedias present for " + nearByMoment.momentId + " calling start download");
            this.startNearByMomentDownload();//all media will be loaded from disk itself
        }
    }


    public static MomentModel getNearByMoment(String momentId) {
        for (NearbyMomentDownloader downloader : instanceList) {
            if (downloader.momentId.equals(momentId))
                return downloader.nearByMoment;
        }
        return null;
    }

    private void validateFileSystem() {
        mediaStoragePath = AppLibrary.getFilesDirectory(mContext);
        File mediaDir = new File(mediaStoragePath);
        if (!mediaDir.exists()) {
            final boolean mkdirs = mediaDir.mkdirs();
            if (!mkdirs) Log.e(TAG, " directory not made");
        }
    }

    /**
     * Must be validated before using this class
     */

    private static boolean downloaderReady;
    private static DatabaseReference localDataReference;

    private static void refreshLocalData(Context mContext) {
        if (localDataReference != null) return;
        if (myUserId == null) myUserId = FireBaseHelper.getInstance(mContext).getMyUserId();
        localDataReference = FireBaseHelper.getInstance(mContext).getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MEDIA_DOWNLOAD});
        localDataReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                localMediaSnap = dataSnapshot;//keeping it fresh all the time
                if (!downloaderReady) {
                    downloaderReady = true;
                    Log.d(TAG, " localMediaSnap loaded first time !" + localMediaSnap);
                    if (momentDownloadListener != null)
                        momentDownloadListener.onDownloaderReady();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void startNearByMomentDownload() {
        if (downloadState == DownloaderState.LOADING_NOT_STARTED)
            downloadState = DownloaderState.LOADING_IN_PROGRESS;
        //else if downloadState==loading complete; no downloading will take place
        //as all the media exists locally only the uri will be populated
        if (nearByMoment.media == null) {
            Log.e(TAG, "nearByMoment.media==null returning");
            return;
        }
        for (Map.Entry<String, MomentModel.Media> mediaEntry : nearByMoment.media.entrySet()) {
            String mediaId = mediaEntry.getKey();
            MomentModel.Media media = mediaEntry.getValue();
            if (media == null) {
                Log.e(TAG, " media found null in nearByMoment: " + momentId);
                continue;
            }
            media.mediaId = mediaId;

            if (localMediaSnap.child(mediaId).exists()) {
                DataSnapshot mediaSnap = localMediaSnap.child(mediaId);

                Integer downloadStatus = mediaSnap.child(LOCAL_MEDIA_STATUS).getValue(Integer.class);
                if (downloadStatus == null)
                    downloadStatus = MEDIA_DOWNLOAD_NOT_STARTED;
                String mediaSnapUri = mediaSnap.child(URL).getValue(String.class);
                boolean doesFileExists = false;
                if (mediaSnapUri != null)
                    doesFileExists = fireBaseHelper.fileExists(mediaSnapUri);

                if (downloadStatus == MEDIA_DOWNLOAD_NOT_STARTED) {
                    addToPendingDownloads(media);
                    continue;
                }
                if (downloadStatus == MEDIA_DOWNLOADING) {
                    addToPendingDownloads(media);
                    continue;
                }
                if (downloadStatus == MEDIA_DOWNLOAD_COMPLETE || downloadStatus == MEDIA_VIEWED) {
                    if (!doesFileExists) {
                        Log.e(TAG, "fireBase says file downloaded but file not present in disk for: " + mediaSnapUri);
                        addToPendingDownloads(media);
                    } else {//everything OK; file exists; continuing
                        media.url = mediaSnapUri;
                        notifyMediaLoadedFromDisk();
                        if (media.viewers != null)
                            if (media.viewers.get(myUserId) != null && media.viewers.get(myUserId) == 1)
                                media.status = MEDIA_VIEWED;
                    }
                }
            } else {//mediaId doesn't exists in local data
                addToPendingDownloads(media);
            }
        }
        exhaustPendingDownloadList();
    }


    @SuppressWarnings("LoopStatementThatDoesntLoop")
    private void exhaustPendingDownloadList() {
        if (pendingDownloadMediaMap.size() > 0) {
            for (Map.Entry<String, MomentModel.Media> mediaEntry : pendingDownloadMediaMap.entrySet()) {
                String mediaId = mediaEntry.getKey();
                DatabaseReference reference = fireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MEDIA_DOWNLOAD, mediaId});

                reference.child(LOCAL_MEDIA_STATUS).setValue(MEDIA_DOWNLOADING);
                reference.child(URL).setValue(getLocalURIFromMediaModel(mediaEntry.getValue()));
                reference.child(MOMENT_ID).setValue(momentId);
                String[] keysSet = pendingDownloadMediaMap.keySet().toArray(new String[pendingDownloadMediaMap.size()]);
                startDownload(pendingDownloadMediaMap.remove(keysSet[0]));
                break;//does not loop only starts first media item to download
            }
        } else {//no more items in the list
            Log.d(TAG, " download list exhausted; do some stuff");
            checkIfAllMediaLoaded();
        }
    }

    private int mediaLoadedFromDisk, mediaLoadedFromURL = 0;

    private void notifyMediaDownloaded(int transferId) {
        mediaLoadedFromURL++;
        final MomentModel.Media media = downloadIdMediaIdMap.get(transferId);
        String mediaIdJustDownloaded = media.mediaId;
        pendingDownloadMediaMap.remove(mediaIdJustDownloaded);
        checkIfAllMediaLoaded();
    }


    private void notifyMediaLoadedFromDisk() {
        mediaLoadedFromDisk++;
        checkIfAllMediaLoaded();

    }

    private void checkIfAllMediaLoaded() {
        Log.d(TAG, " total media to download " + nearByMoment.media.size() + " media loaded from disk " + mediaLoadedFromDisk +
                " media loaded from URL " + mediaLoadedFromURL);
        Log.d(TAG, " downloadState " + this.downloadState);
        if ((nearByMoment.media.size() == mediaLoadedFromURL + mediaLoadedFromDisk) ||
                (this.downloadState == DownloaderState.LOADING_COMPLETE)) {
            this.downloadState = DownloaderState.LOADING_COMPLETE;
            if (momentDownloadListener != null)
                momentDownloadListener.onMomentDownloaded(momentId);
        }
    }

    private String getLocalURIFromMediaModel(MomentModel.Media media) {
        String url = media.url;
        String extension;
        int i = url.lastIndexOf('.');
        extension = url.substring(i + 1);

        return mediaStoragePath + media.mediaId + "." + extension;
    }

    private void addToPendingDownloads(MomentModel.Media media) {
        DatabaseReference localMedia = fireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MEDIA_DOWNLOAD, media.mediaId});
        localMedia.child(LOCAL_MEDIA_STATUS).setValue(MEDIA_DOWNLOAD_NOT_STARTED);
        localMedia.child(MOMENT_ID).setValue(momentId);
        localMedia.child(URL).setValue(media.url);
        pendingDownloadMediaMap.put(media.mediaId, media);

        Log.d(TAG, "addToPendingDownloads new size= " + pendingDownloadMediaMap.size());
        String s = "";
        for (Map.Entry<String, MomentModel.Media> asd : pendingDownloadMediaMap.entrySet()) {
            s += asd.getKey() + " ";
        }
        Log.d(TAG, "addToPendingDownloads" + s);
    }


    private void startDownload(MomentModel.Media mediaToDownload) {
        String mediaUrl = mediaToDownload.url;
        String mediaId = mediaToDownload.mediaId;

        Log.d(TAG, "startDownload Id " + mediaId);
        Log.d(TAG, "startDownload Url " + mediaUrl);

        int p = mediaUrl.lastIndexOf(".");
        String extension = mediaUrl.substring(p + 1);
        if (p == -1 || !extension.matches("\\w+")) {
            /* file has no extension */
            Log.e(TAG, " start download; no extension found for a file returning");
            return;
        }
        String currentMediaStoragePath = mediaStoragePath + mediaId + "." + extension;
        File file = new File(currentMediaStoragePath);
        String key = mediaUrl.split("pulse.resources/")[1];

        TransferObserver transferObserver = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file);

        Log.d(TAG, " downloadId " + transferObserver.getId());
        downloadIdMediaIdMap.put(transferObserver.getId(), mediaToDownload);
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int i, TransferState transferState) {
                Log.d(TAG, "onStateChanged -" + transferState.toString() + " for mediaId" + i);
                if (transferState.toString().equals("IN_PROGRESS")) {
                    Log.d(TAG, "MEDIA DOWNLOAD IN_PROGRESS for " + i);
                    updateStickerDownloadProgress(i, MEDIA_DOWNLOADING);
                } else if (transferState.toString().equals("COMPLETED")) {
                    Log.d(TAG, "MEDIA DOWNLOAD COMPLETED for mediaId" + i);
                    updateStickerDownloadProgress(i, MEDIA_DOWNLOAD_COMPLETE);
                }
            }

            @Override
            public void onProgressChanged(int i, long l, long l1) {
                Log.d(TAG, " onProgressChanged i: " + i + " l " + l + " l1 " + l1);
            }

            @Override
            public void onError(int i, Exception e) {
                e.printStackTrace();
                Log.d(TAG, "onError " + e);
                Log.d(TAG, " downloading for mediaId " + i);
                updateStickerDownloadProgress(i, ERROR_DOWNLOADING_MEDIA);
            }
        });
    }


    private void updateStickerDownloadProgress(int transferId, int downloadProgress) {
        final MomentModel.Media media = downloadIdMediaIdMap.get(transferId);
        Log.d(TAG, "updateStickerDownloadProgress for media " + media.mediaId);
        DatabaseReference mediaRef = fireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, MEDIA_DOWNLOAD, media.mediaId});

        mediaRef.child(LOCAL_MEDIA_STATUS).setValue(downloadProgress);
        mediaRef.child(MOMENT_ID).setValue(momentId);

        if (downloadProgress == MEDIA_DOWNLOAD_COMPLETE) {
            //updating from url to uri on download completion
            media.url = getLocalURIFromMediaModel(media);
            mediaRef.child(URL).setValue(media.url);

            notifyMediaDownloaded(transferId);
            exhaustPendingDownloadList();
        } else if (downloadProgress == MEDIA_DOWNLOADING) {
            //do nothing already downloading
        } else if (downloadProgress == ERROR_DOWNLOADING_MEDIA) {
            if (checkAndRetryMediaDownload(media))
                startDownload(media);
            else exhaustPendingDownloadList();
        }
    }


    private boolean checkAndRetryMediaDownload(MomentModel.Media media) {
        Integer retryCount = downloadRetryMap.get(media.mediaId);
        if (retryCount == null) {
            downloadRetryMap.put(media.mediaId, 1);
            return true;
        }
        if (retryCount <= 3) {
            downloadRetryMap.put(media.mediaId, ++retryCount);
            return true;
        }
        Log.e(TAG, " downloading failed permanently for mediaId " + media.mediaId);
        return false;
    }

    private static NearbyMomentDownloadListener momentDownloadListener;

    public static void setMomentDownloadListener(NearbyMomentDownloadListener momentDownloadListener) {
        NearbyMomentDownloader.momentDownloadListener = momentDownloadListener;
    }

    public interface NearbyMomentDownloadListener {
        void onMomentDownloaded(String momentId);

        void onDownloaderReady();
    }
}