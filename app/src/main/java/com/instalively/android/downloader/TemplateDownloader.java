package com.instalively.android.downloader;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.instalively.android.MasterClass;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.models.LocationTemplateModel;
import com.instalively.android.models.StickerLocalStatusModel;
import com.instalively.android.stickers.ParentStickerFragment;
import com.instalively.android.util.AppLibrary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by deepankur on 5/31/16.
 */
public class TemplateDownloader implements FireBaseKEYIDS {
    private static TemplateDownloader mTemplateDownloader;
    String TEMPLATE_STICKERS_DIR_NAME = "templateStickers";
    private Context mContext;
    private String templateStoragePath;
    private String TAG = this.getClass().getSimpleName();
    private TransferObserver transferObserver;
    private FireBaseHelper fireBaseHelper;
    private DatabaseReference templateFireBase;
    private LinkedHashMap<String, LocationTemplateModel> templateLinkedMap;
    private String myUserId;
    private boolean mSetterCalled;
    private ParentStickerFragment parentStickerFragment;
    private HashMap<Integer, StickerLocalStatusModel> downloadIdStickerMap;
    /**
     * ArrayList of sticker ids that were written to local fireBase node earlier
     * irrespective of their download status
     */
    private ArrayList<String> previouslyEncounteredStickersList = new ArrayList<>();
    private com.google.firebase.database.ValueEventListener localDataEventListener = new com.google.firebase.database.ValueEventListener() {
        @Override
        public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
            if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                //node not found populating all the stickers
                populateAllTemplateLocalData();
                return;
            }
            previouslyEncounteredStickersList.clear();
            for (com.google.firebase.database.DataSnapshot child : dataSnapshot.getChildren()) {
                StickerLocalStatusModel model = child.getValue(StickerLocalStatusModel.class);
                previouslyEncounteredStickersList.add(child.getKey());
                if (model.downloadStatus == DEFAULT_INTEGER_VALUE || model.downloadStatus == STICKER_NOT_DOWNLOADED
                        || model.downloadStatus == STICKER_ERROR_DOWNLOADING) {
                    model.stickerId = child.getKey();
                    if (needToDownloadStickerInThisSession(child.getKey()))
                        startDownload(child.getKey(), model.pictureUrl);
                    else {
                        //consider deleting this file
                    }
                }
                if (model.downloadStatus == STICKER_DOWNLOAD_IN_PROGRESS)
                    Log.d(TAG, " download already progress ");//do nothing
                if (model.downloadStatus == STICKER_DOWNLOADED) {
                    Log.d(TAG, " already have sticker with id: " + model.stickerId);
                    if (!new File(model.localUri).exists()) {
                        Log.e(TAG, " file status for stickerId: " +
                                model.stickerId + "is Downloaded on fireBase but not found in internal Directory starting download");
                        startDownload(model.stickerId, model.pictureUrl);
                    } else {
                        Log.d(TAG, "everyThing Ok");
                    }
                    fillUriInModel(model);
                }
            }
            checkForNewTemplateStickers();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }

    };

    public TemplateDownloader(Context context) {
        mContext = context;
        validateFileSystem();
        fireBaseHelper = FireBaseHelper.getInstance(context);
        myUserId = fireBaseHelper.getMyUserId();
        downloadIdStickerMap = new HashMap<>();
        Log.d(TAG, "status before transverse");
        traverse(new File(mContext.getFilesDir().getAbsolutePath() + File.separator));

    }

    public static TemplateDownloader getInstance(Context context) {
        if (mTemplateDownloader == null)
            mTemplateDownloader = new TemplateDownloader(context);
        return mTemplateDownloader;
    }

    //to be called only once during application lifecycle
    public void setCurrentTemplates(LinkedHashMap<String, LocationTemplateModel> templateModelLinkedHashMap) {
        if (mSetterCalled) /*throw new RuntimeException("multiple calls to this setter not allowed");*/ {
            Log.e(TAG, " multiple calls to setter !! returning ");
            return;
        }
        mSetterCalled = true;
        this.templateLinkedMap = templateModelLinkedHashMap;
        this.templateFireBase = fireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, TEMPLATE_DOWNLOAD});
        this.templateFireBase.addListenerForSingleValueEvent(localDataEventListener);
    }

    public LinkedHashMap<String, LocationTemplateModel> getTemplateLinkedMap() {
        return templateLinkedMap;
    }

    public LocationTemplateModel getLocationModelByIndex(int index) {
        String[] keySet = templateLinkedMap.keySet().toArray(new String[templateLinkedMap.size()]);
        return templateLinkedMap.get(keySet[index]);
    }

    public void setFragmentReference(ParentStickerFragment parentStickerFragment) {
        this.parentStickerFragment = parentStickerFragment;
    }

    private boolean needToDownloadStickerInThisSession(String key) {
        for (Map.Entry<String, LocationTemplateModel> templateModelEntry : templateLinkedMap.entrySet()) {
            if (templateModelEntry.getValue() == null) {
                Log.d(TAG, "needToDownloadStickerInThisSession nullTemplateFound returning");
                continue;
            }
            HashMap<String, LocationTemplateModel.LocationSticker> stickerHashMap = templateModelEntry.getValue().stickers;
            if (stickerHashMap == null) {
                Log.e(TAG, "needToDownloadStickerInThisSession null stickerHashMapFound returning");
                continue;
            }
            for (Map.Entry<String, LocationTemplateModel.LocationSticker> stickerEntry : stickerHashMap.entrySet()) {
                if (stickerEntry.getKey().equals(key))
                    return true;
            }
        }
        return false;
    }

    private void populateAllTemplateLocalData() {
        for (Map.Entry<String, LocationTemplateModel> templateEntry : templateLinkedMap.entrySet()) {
            LocationTemplateModel templateModel = templateEntry.getValue();
            if (templateModel == null || templateModel.stickers == null) continue;
            for (Map.Entry<String, LocationTemplateModel.LocationSticker> stickerEntry : templateModel.stickers.entrySet()) {
                String stickerId = stickerEntry.getKey();
                LocationTemplateModel.LocationSticker sticker = stickerEntry.getValue();
                StickerLocalStatusModel model = new StickerLocalStatusModel(stickerId, sticker.pictureUrl, null, STICKER_NOT_DOWNLOADED);
                fireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, TEMPLATE_DOWNLOAD, stickerId}).setValue(model);
                this.startDownload(stickerId, sticker.pictureUrl);
            }
        }
    }

    public String getTemplateFilesDirectory(Context mContext) {
        return mContext.getFilesDir().getAbsolutePath() + File.separator + TEMPLATE_STICKERS_DIR_NAME + File.separator;
    }

    private void validateFileSystem() {
        templateStoragePath = this.getTemplateFilesDirectory(mContext);
        File mediaDir = new File(templateStoragePath);
        if (!mediaDir.exists())
            mediaDir.mkdirs();
    }

    public void startDownload(final String mediaId, String pictureUrl) {

        int p = pictureUrl.lastIndexOf(".");
        String extension = pictureUrl.substring(p + 1);
        if (p == -1 || !extension.matches("\\w+")) {
            /* file has no extension */
            Log.e(TAG, " start download returning");
            return;
        }
        String currentMediaStoragePath = templateStoragePath + mediaId + "." + extension;
        Log.d(TAG, " mediaStoragePath " + templateStoragePath);
        File file = new File(currentMediaStoragePath);
        Log.d(TAG, " pictureUrl " + pictureUrl);
        String key = pictureUrl.split("pulse.resources/")[1];
        Log.d(TAG, " key " + key);
        if (AppLibrary.getMediaType(pictureUrl) == AppLibrary.MEDIA_TYPE_IMAGE) {
            this.transferObserver = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file); //Starting upload
        } else {
            this.transferObserver = MasterClass.getTransferUtility().download(AppLibrary.MediaHostBucket, key, file);
        }
        Log.d(TAG, " downloadId " + transferObserver.getId());
        StickerLocalStatusModel model = new StickerLocalStatusModel(mediaId, pictureUrl, currentMediaStoragePath, STICKER_NOT_DOWNLOADED);
        downloadIdStickerMap.put(transferObserver.getId(), model);
        this.transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int i, TransferState transferState) {
                Log.d(TAG, "onStateChanged -" + transferState.toString() + " for mediaId" + i);
                if (transferState.toString().equals("IN_PROGRESS")) {
                    Log.d(TAG, "MEDIA DOWNLOAD IN_PROGRESS for " + i);
                    updateStickerDownloadProgress(i, STICKER_DOWNLOAD_IN_PROGRESS);
                } else if (transferState.toString().equals("COMPLETED")) {
                    Log.d(TAG, "MEDIA DOWNLOAD COMPLETED for mediaId" + i);
                    updateStickerDownloadProgress(i, STICKER_DOWNLOADED);
                }
            }

            @Override
            public void onProgressChanged(int i, long l, long l1) {
                Log.d(TAG, " onProgressChanged i: " + i + " l " + l + " l1 " + l1);
            }

            @Override
            public void onError(int i, Exception e) {
                e.printStackTrace();
                Log.d(TAG, "onError " + e);
                Log.d(TAG, " downloading failed permanently for mediaId " + i);
                updateStickerDownloadProgress(i, STICKER_ERROR_DOWNLOADING);
            }
        });
    }

    private void updateStickerDownloadProgress(int downloadId, int status) {
        StickerLocalStatusModel model = downloadIdStickerMap.get(downloadId);
        model.downloadStatus = status;
        String stickerId = model.stickerId;
        fireBaseHelper.getNewFireBase(ANCHOR_LOCAL_DATA, new String[]{myUserId, TEMPLATE_DOWNLOAD, stickerId}).setValue(model);

        if (status == STICKER_ERROR_DOWNLOADING) {
            this.startDownload(model.stickerId, model.pictureUrl);
            Log.w(TAG, "download failed recursively calling startDownload");
        }
        if (status == STICKER_DOWNLOADED) {
            fillUriInModel(model);
        }
    }

    private void fillUriInModel(StickerLocalStatusModel model) {
        String uri = model.localUri;
        String stickerId = model.stickerId;
        for (Map.Entry<String, LocationTemplateModel> locationTemplateModelEntry : templateLinkedMap.entrySet()) {
            LocationTemplateModel locationModel = locationTemplateModelEntry.getValue();
            if (locationModel == null) continue;
            HashMap<String, LocationTemplateModel.LocationSticker> stickerHashMap = locationModel.stickers;
            for (Map.Entry<String, LocationTemplateModel.LocationSticker> stickerEntry : stickerHashMap.entrySet()) {
                LocationTemplateModel.LocationSticker sticker = stickerEntry.getValue();
                String currentPictureId = stickerEntry.getKey();
                if (stickerId.equals(currentPictureId)) {
                    sticker.localUri = uri;
                    notifyDataChanged();
                    return;
                }
            }
        }
        //throw new RuntimeException("stickerId not found in linked map");
        Log.d(TAG, " something wrong stickerId not found in linked map; delete it from Local fireBase");
    }

    private void notifyDataChanged() {

        if (parentStickerFragment.adapter != null)
            parentStickerFragment.adapter.notifyDataSetChanged();
    }

    public void traverse(File dir) {
        Log.d(TAG, " transversing");
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; ++i) {
                File file = files[i];
                if (file.isDirectory()) {
                    Log.d(TAG, " dir " + file);
                    traverse(file);
                } else {
                    Log.d(TAG, " file " + file);
                    Log.d(TAG, " file path " + file.getPath());
                }
            }
        }
    }

    private void checkForNewTemplateStickers() {
        for (Map.Entry<String, LocationTemplateModel> templateEntry : templateLinkedMap.entrySet()) {
            if (templateEntry.getValue() == null) continue;
            HashMap<String, LocationTemplateModel.LocationSticker> sticker = templateEntry.getValue().stickers;
            for (Map.Entry<String, LocationTemplateModel.LocationSticker> stickerEntry : sticker.entrySet()) {

                if (!previouslyEncounteredStickersList.contains(stickerEntry.getKey())) {
                    LocationTemplateModel.LocationSticker locationSticker = stickerEntry.getValue();
                    Log.d(TAG, "new StickerId found in templates starting Download");
                    startDownload(stickerEntry.getKey(), stickerEntry.getValue().pictureUrl);
                }
            }
        }
    }

    public void destroy(Context context) {
        mTemplateDownloader = null;
    }
}