package com.instalively.android.util;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import com.instalively.android.MasterClass;
import com.instalively.android.signals.BroadCastSignals;

/**
 * Created by admin on 1/29/2015.
 */
public abstract class BaseActivity extends AppCompatActivity {
    
    private boolean registerForEvents = false;
    
    public abstract void onEvent(BroadCastSignals.BaseSignal eventSignal);
    
    protected void registerForInAppSignals(boolean flag) {
        this.registerForEvents = flag;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.registerForEvents) {
            MasterClass.getEventBus().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        if (this.registerForEvents) {
            MasterClass.getEventBus().unregister(this);
        }
        super.onDestroy();
    }
}
