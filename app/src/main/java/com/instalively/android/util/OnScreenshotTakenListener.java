package com.instalively.android.util;

import android.net.Uri;

/**
 * Created by user on 5/27/2016.
 */
public interface OnScreenshotTakenListener {
    void onScreenshotTaken(Uri uri);
}
