package com.instalively.android.util;

import android.net.Uri;
import android.os.Environment;
import android.os.FileObserver;

import java.io.File;

/**
 * Created by user on 5/27/2016.
 */
public class SnapshotDetector extends FileObserver {

    private static final String TAG = "SnapshotDetector";
    private static final String PATH = Environment.getExternalStorageDirectory().toString() + "/Pictures/Screenshots/";
    private OnScreenshotTakenListener mListener;
    private String mLastTakenPath;
    private boolean deleteScreenshot = false;

    public SnapshotDetector(String path) {
        super(PATH, FileObserver.CLOSE_WRITE);
    }

    public SnapshotDetector(OnScreenshotTakenListener listener) {
        super(PATH, FileObserver.CLOSE_WRITE);
        mListener = listener;
    }

    @Override
    public void onEvent(int event, String path) {
        AppLibrary.log_i(TAG, "Event:" + event + "\t" + path);

        if (path == null || event != FileObserver.CLOSE_WRITE)
            AppLibrary.log_i(TAG, "Not important");
        else if (mLastTakenPath != null && path.equalsIgnoreCase(mLastTakenPath))
            AppLibrary.log_i(TAG, "This event has been observed before.");
        else {
            mLastTakenPath = path;
            File file = new File(PATH + path);

            if (deleteScreenshot) {
                if (file != null)
                    file.delete();

                /*
                * A null uri is returned to listener once screenshot
                * has been deleted.
                * */
                if (mListener != null)
                    mListener.onScreenshotTaken(null);

            } else {

                if (mListener != null)
                    mListener.onScreenshotTaken(Uri.fromFile(file));
            }
        }
    }

    public void setWatcher(OnScreenshotTakenListener listener) {
        mListener = listener;
    }

    public void start() {
        super.startWatching();
    }

    public void stop() {
        super.stopWatching();
    }

    public void deleteScreenshot(boolean deleteScreenshot) {
        this.deleteScreenshot = deleteScreenshot;
    }
}
