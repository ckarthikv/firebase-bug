package com.instalively.android.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.instalively.android.R;

/**
 * Created by user on 5/10/2016.
 */
public class SettingsModelLayout extends LinearLayout {

    private RadioButton radioButton;
    private ImageView updateImageView;
    private ViewControlsCallback viewControlsCallback;
    private View itemRowView;
    private String id;
    private TextView descTextView;

    public SettingsModelLayout(Context context) {
        super(context);
        init(context);
    }

    public SettingsModelLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SettingsModelLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.settings_model_view, this, true);
        this.itemRowView = this;
        this.radioButton = (RadioButton)itemRowView.findViewById(R.id.contentName);
        this.descTextView = (TextView) itemRowView.findViewById(R.id.desc);
        this.updateImageView = (ImageView) itemRowView.findViewById(R.id.updateImageView);
    }

    public void setViewControlsCallback(Fragment fragment){
        viewControlsCallback = (ViewControlsCallback) fragment;
    }

    public void initializeViewObjects(String settingName, final String id) {
        this.id = id;
        this.radioButton.setText(settingName);
        this.updateImageView = (ImageView) itemRowView.findViewById(R.id.updateImageView);
        this.updateImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewControlsCallback.onUpdateListClicked(id);
                radioButton.setChecked(true);
            }
        });
        this.itemRowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLibrary.log_d("SettingsLayout","clicked");
                viewControlsCallback.onSettingRowItemClicked(id);
                radioButton.setChecked(true);
            }
        });
    }

    public void setRadioChecked(boolean check){
        this.radioButton.setChecked(check);
    }

    public void setDescText(String descText){
        descTextView.setText(descText);
        descTextView.setVisibility(View.VISIBLE);
    }

    public interface ViewControlsCallback{
        void onUpdateListClicked(String id);
        void onSettingRowItemClicked(String id);
    }

    public View getUpdateView(){
        return updateImageView;
    }
}