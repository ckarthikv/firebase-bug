package com.instalively.android.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.instalively.android.uploader.mediaUpload;

/**
 * Created by Karthik on 1/25/16.
 */
public class UploadReceiver extends BroadcastReceiver {

    private NotificationManager mNotificationManager;

    public UploadReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // This method is called when this BroadcastReceiver receives an Intent broadcast.
        if (intent.getExtras()!=null) {
            int transferId;
            int notificationId;

            if (intent.hasExtra("TransferId")) {
                transferId = intent.getExtras().getInt("TransferId");
                notificationId = intent.getExtras().getInt("NotificationId");
//                if (notificationList.isEmpty() || !notificationList.contains(notificationId))
//                    notificationList.add(notificationId);
                mediaUpload.resumeUpload(transferId);
            }
            else {
                Log.e("UploadReceiver", "Cancelling Notifications");
                mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancelAll();
//                SharedPreferences prefs = context.getSharedPreferences(AppLibrary.APP_SETTINGS, 0);
//                String picUrl = prefs.getString(AppLibrary.USER_PROFILE_PIC_URL, null);
//
//                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
//                Notification notification = null;
//
//                String title = "Upload Interrupted";
//                String msg = "Please open InstaLively to continue upload";
//                notification = mBuilder.setSmallIcon(R.drawable.public_event_notification)
//                        .setContentTitle(title)
//                        .setAutoCancel(true)
//                        .setTicker(title)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
//                        .setOngoing(false)
//                        .setVibrate(new long[] {1000, 1000}) //delay + vibrate + delay +... and so on
//                        .setProgress(0, 0, false)
//                        .setColor(context.getResources().getColor(R.color.notification_icon_background))
//                        .setContentText(msg).setContentIntent(contentIntent).build();
            }
        }
    }
}

