package com.instalively.android.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.instalively.android.R;
import com.instalively.android.apihandling.RequestManager;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.fragments.ChooseUserNameFragment;
import com.instalively.android.fragments.OnBoardingAddFriendFragment;
import com.instalively.android.fragments.SelectInstitutionFragment;
import com.instalively.android.util.AppLibrary;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepankur on 6/22/16.
 */
public class OnBoardingActivity extends AppCompatActivity implements FireBaseKEYIDS {

    private String TAG = this.getClass().getSimpleName();
    private String userName;
    private SharedPreferences prefs;
    private static final int PERMISSION_ACCESS_CAMERA_MICROPHONE = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences(AppLibrary.APP_SETTINGS, 0);

        setContentView(R.layout.activity_on_boarding);
        userName = getIntent().getStringExtra(AppLibrary.USER_NAME);
        int onBoardStatus = getIntent().getIntExtra(AppLibrary.USER_ONBOARDING_STATUS, 0);
        if (onBoardStatus == ON_BOARDING_NOT_STARTED)
            loadSelectUserNameFragment();
        if (onBoardStatus == USER_NAME_SELECTION_DONE)
            loadAddFriendsFragment();
        if (onBoardStatus == FRIENDS_INVITING_DONE)
            loadAddInstituteFragment();
        if (onBoardStatus == INSTITUTION_PROVING_DONE) { // Never used
            Intent intent = new Intent(OnBoardingActivity.this, CameraActivity.class);
            startActivity(intent);
        }
    }


    public void loadSelectUserNameFragment() {//pos not required; data has complete info
        Bundle b = new Bundle();
        b.putString(AppLibrary.USER_NAME, userName);
        ChooseUserNameFragment chooseUserNameFragment = new ChooseUserNameFragment();
        chooseUserNameFragment.setArguments(b);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.onBoardingFrameHolder, chooseUserNameFragment, ChooseUserNameFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void onUserNameSelectedSuccessfully() {
        updateOnBoardingStatusInPreference(USER_NAME_SELECTION_DONE);
        updateOnBoardingStatusInServer(USER_NAME_SELECTION_DONE);
        loadAddFriendsFragment();
    }

    private void loadAddFriendsFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.onBoardingFrameHolder, new OnBoardingAddFriendFragment(), ChooseUserNameFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void onFriendsSelectionDone() {
        updateOnBoardingStatusInPreference(FRIENDS_INVITING_DONE);
        updateOnBoardingStatusInServer(FRIENDS_INVITING_DONE);
        loadAddInstituteFragment();
    }

    private void loadAddInstituteFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.onBoardingFrameHolder, new SelectInstitutionFragment(), ChooseUserNameFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    /**
     * @param hasSelected whether anything selected on not
     * @param instituteId -1 if not selected ; instituteId otherwise
     * @param query       the last status of the Edit text
     */
    public void onInstitutionSelectionComplete(boolean hasSelected, int instituteId, String query) {
        updateOnBoardingStatusInPreference(INSTITUTION_PROVING_DONE);
        requestCameraPermissionsAndProceed(); //Request camera permissions before starting activity
        updateOnBoardingStatusInServer(INSTITUTION_PROVING_DONE);
    }

    private void requestCameraPermissionsAndProceed() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                    PERMISSION_ACCESS_CAMERA_MICROPHONE);
        } else {
            Intent mIntent = new Intent(OnBoardingActivity.this, CameraActivity.class);
            startActivity(mIntent);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_CAMERA_MICROPHONE:
                if (AppLibrary.verifyPermissions(grantResults)) {
                    Intent mIntent = new Intent(OnBoardingActivity.this, CameraActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    requestCameraPermissionsAndProceed();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @SuppressLint("CommitPrefEdits")
    void updateOnBoardingStatusInPreference(int onBoardStatus) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(AppLibrary.USER_ONBOARDING_STATUS, onBoardStatus);
        editor.commit();
    }


    @SuppressWarnings("deprecation")
    void updateOnBoardingStatusInServer(int onBoardingStatus) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("userId", FireBaseHelper.getInstance(this).getMyUserId()));
        pairs.add(new BasicNameValuePair("onboarding_status", String.valueOf(onBoardingStatus)));
        RequestManager.makePostRequest(this, RequestManager.UPDATE_ON_BOARDING_STATUS_REQUEST, RequestManager.UPDATE_ONBOARDING_STATUS_RESPONSE,
                null, pairs, onBoardingStatusCallback);
    }

    private RequestManager.OnRequestFinishCallback onBoardingStatusCallback = new RequestManager.OnRequestFinishCallback() {
        @Override
        public void onBindParams(boolean success, Object response) {
            try {
                Log.d(TAG, " server says " + response);
                final JSONObject object = (JSONObject) response;
                if (success) {
                    Log.d(TAG, object.getString("value"));
                } else {
                    Log.e(TAG, "onBoardingStatusCallback Error, response -" + object);
                    // request failed
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "onBoardingStatusCallback JsonException " + e);

            }
        }

        @Override
        public boolean isDestroyed() {
            return isDestroyed;
        }
    };

    boolean isDestroyed;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
    }
}
