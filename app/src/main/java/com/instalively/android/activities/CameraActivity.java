package com.instalively.android.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.instalively.android.MasterClass;
import com.instalively.android.R;
import com.instalively.android.Splash;
import com.instalively.android.adapters.ChatAdapter;
import com.instalively.android.adapters.LetterTileProvider;
import com.instalively.android.adapters.MainFragmentPagerAdapter;
import com.instalively.android.adapters.SliderMessageListAdapter;
import com.instalively.android.adapters.SliderMyMomentAdapter;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.fragments.AddFriendFragment;
import com.instalively.android.fragments.AllChatListFragment;
import com.instalively.android.fragments.AroundYouFragment;
import com.instalively.android.fragments.CanvasFragment;
import com.instalively.android.fragments.ChatFragment;
import com.instalively.android.fragments.CreateManageGroupFragment;
import com.instalively.android.fragments.DashBoardFragment;
import com.instalively.android.fragments.MomentListFragment;
import com.instalively.android.fragments.ShareMediaFragment;
import com.instalively.android.fragments.ViewChatMediaFragment;
import com.instalively.android.fragments.ViewMomentsFragment;
import com.instalively.android.fragments.ViewMyMediaFragment;
import com.instalively.android.modelView.ChatMediaModel;
import com.instalively.android.modelView.SliderMessageModel;
import com.instalively.android.models.MediaModel;
import com.instalively.android.signals.BroadCastSignals;
import com.instalively.android.stickers.EmoticonFragment;
import com.instalively.android.uploader.mediaUpload;
import com.instalively.android.util.AppLibrary;
import com.instalively.android.util.BaseActivity;
import com.instalively.android.util.BitmapWorkerClass;
import com.instalively.android.util.CustomViewPager;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

/**
 * Created by karthik on 2/29/2016
 * This is the broadcast page: base to all actions in this screen
 *
 * @hint press ctrl+shift+NumPad- and look for functions
 */
@SuppressWarnings("NewApi")
public class CameraActivity extends BaseActivity implements
        ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener,

        OnClickListener, OnPageChangeListener, FragmentManager.OnBackStackChangedListener,
        ShareMediaFragment.ViewControlsCallback, FireBaseHelper.MediaModelCallback, FireBaseKEYIDS,
        EmoticonFragment.SwipeListener, mediaUpload.UploadStatusCallbacks,
        SliderMessageListAdapter.ViewControlsCallback, ChatAdapter.ViewControlCallbacks,
        SliderMyMomentAdapter.ViewControlsCallback, DashBoardFragment.ViewControlsCallback,
        ViewMyMediaFragment.ViewControlsCallback, ChatFragment.ViewControlsCallback
        ,ViewChatMediaFragment.ViewControlsCallback,MomentListFragment.ViewControlsCallback
        ,AroundYouFragment.ViewControlsCallback,ViewMomentsFragment.ViewControlsCallback {

    private static final String TAG = "CameraActivity";
    public static final int CUTOFF_SDK = 18;
    public static String eventThumbnailUrl;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    public static boolean flashMode = false;
    public static boolean PreRecordFilters = false;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec //1 hr
    private static int FATEST_INTERVAL = 5000; // 5 sec //1 min
    private static int DISPLACEMENT = 100; // 10 meters //1 km

    public static final boolean IGNORE_BACK_STACK_LISTENER = false;

    //For using application's context globally
    public static Context applicationContext = null;

    private boolean VERBOSE = false;

    private ImageView filterImageView;
    private String addressText;

    LetterTileProvider tileProvider;
    int tileSize;
    Resources res;

    /* Event Object Data to access required params */
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private SharedPreferences prefs;
    private Display display;

    LocationManager lm;
    private Location mLastLocation;
    private double mlatitude, mlongitude;

    // Google client to interact with Google API
    public GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
//    private RecordingActionControllerKitkat controller;

    private CustomViewPager viewPager;
    private ImageView cameraSwapButton, flashLayout;

    public boolean isNearByMoment;
    private int mPreviousFragment;
    private int mCurrentFragment;

    int mCurrentFragmentInView;
    public boolean isScrolling = false;
    public static Object mVideoLock = new Object();

    private final int DASHBOARD_FRAGMENT_SELECTED = 0;
    private final int CAMERA_FRAGMENT_SELECTED = 1;

    private static final int FRAGMENT_CAMERA = 0;
    private static final int FRAGMENT_CANVAS = 1;
    private static final int FRAGMENT_VIDEO_EDITOR = 2;
    private static final int FRAGMENT_SHARE_MEDIA = 3;
    private static final int FRAGMENT_NEW_GROUP = 4;
    private static final int FRAGMENT_DASHBOARD = 5;

    private static final int FRAGMENT_CHAT = 6;
    private static final int FRAGMENT_VIEW_CHAT_MEDIA = 8;
    private static final int FRAGMENT_ALL_CHAT_LIST = 9;
    private static final int FRAGMENT_VIEW_MY_MEDIA = 10;
    private static final int FRAGMENT_CREATE_MANAGE_GROUP=11;
    private static final int FRAGMENT_ADD_FRIEND=12;


    private FireBaseHelper fireBaseHelper;
    private GestureDetector gestureDetector;
    private boolean isCanvasFragmentActive = false;

    public CameraActivity() {
        super.registerForInAppSignals(true);
    }

    private static Vector<CameraActivity> allInstances = new Vector<CameraActivity>();
    // Vector<MomentModel> allInstances= new Vector<MomentModel>();

    public static synchronized Vector getAllInstances() {
        return ((Vector) allInstances.clone());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        Vector v = this.getAllInstances();
//        Iterator itr = v.iterator();
//
//        Log.d(TAG, " number of instances=" + v.size());
//        Log.d(TAG, "Iterating through Vector elements");
//        System.out.println();
//        while (itr.hasNext())
//            Log.d(TAG, " instance " + itr.next());

        boolean fromIntent = false;
        if (intent.getExtras() != null && AppLibrary.checkStringObject(intent.getExtras().getString("action")) != null) {
            fromIntent = true;
        }

        setContentView(R.layout.camera_activity);
        initResources();
        initializeViewObjects(fromIntent); //Passing fromIntent as true prevents camera from opening, and opens dashboard directly
        initControllers();

        /* instantiating sticker downloader here */
//        ChatCameraStickerDownloader.getChatCameraStickerDownloader(this);
        super.onCreate(savedInstanceState);
        mFireBaseHelper = FireBaseHelper.getInstance(this);

        if (fromIntent) {
            switch (intent.getExtras().getString("action")) {
                case "friendRequestReceived":
                    requestAddFriendFragmentOpen();
                    break;
                case "friendRequestAccepted":
                    //ToDo - Show Popup of friend on the dashboard
                    break;
                case "groupRequestReceived":
                    requestAddFriendFragmentOpen();
                    break;
                case "mediaMessage":
                    SliderMessageModel model = intent.getParcelableExtra("model");
                    loadChatFragment(model);
                    break;
                case "chatMessage":
                    model = intent.getParcelableExtra("model");
                    loadChatFragment(model);
                    break;
            }
        }
    }

    private FireBaseHelper mFireBaseHelper;


    private void initControllers() {
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    private void initResources() {
        prefs = getSharedPreferences(AppLibrary.APP_SETTINGS, 0);
        this.applicationContext = getApplicationContext();
        res = this.getResources();
        tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);
        tileProvider = new LetterTileProvider(this);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        fireBaseHelper = FireBaseHelper.getInstance(this);
        mCurrentFragment = 0;
    }

    private void initializeViewObjects(boolean fromIntent) {
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(2);//Need to keep this 2 to avoid canvas fragment
        //from getting garbage collected
        viewPager.setAdapter(new MainFragmentPagerAdapter(getSupportFragmentManager(), fromIntent));
        viewPager.setOnPageChangeListener(this);
//        if (fromIntent)
//            viewPager.setCurrentItem(0);
//        else
        viewPager.setCurrentItem(0);
        viewPager.setCanSwipe(false);

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) { //By default, all events are intercepted by this listener first
                CanvasFragment canvasFragment = getCanvasFragment();

                if (canvasFragment != null && isCanvasFragmentActive) {
                    canvasFragment.dispatchTouchEvent(event);
                    if (canvasFragment.isCanvasLocked()) //If locked, then prevent swipe
                        return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, " onResumeCalled ");
        toggleFullScreen(mCurrentFragment != DASHBOARD_FRAGMENT_SELECTED);
        AppsFlyerLib.onActivityResume(this);
        AppEventsLogger.activateApp(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Splash.isCameraOpenChecker = false;
        checkPlayServices();

//        try {
//            if (controller != null) {
//                if (getVideoEditorFragment()!=null) //Set can play to true only if its in video editor fragment
//                    controller.setCanPlay(true);
//
//                controller.resumeView();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, " onPauseCalled ");

//        if (controller != null) {
//            controller.pauseView();
//            controller.setCanPlay(false);
//        }

        AppsFlyerLib.onActivityPause(this);
        AppEventsLogger.deactivateApp(this);
    }


    @Override
    public void onBackPressed() {
        //chatFragment
        if (chatFragment != null && getFragmentInBackStack(ViewChatMediaFragment.class.getSimpleName()) == null
                && chatFragment.onBackPressed()) {
            Log.d(TAG, " chat fragment consumed backPress");
            return;
        }

        //chatListFragment
        if (allChatListFragment != null && allChatListFragment.onBackPressed()) {
            Log.d(TAG, " closed search with this back event");
            return;
        }

        //dashboard
        FragmentManager fm = getSupportFragmentManager();
        Log.d(TAG, " backStack entry count " + fm.getBackStackEntryCount());
        if (fm.getBackStackEntryCount() == 0) {//no back stack entry only dashboard visible
            if (getDashboardFragment() != null && getDashboardFragment().onBackPressed()) {
                Log.d(TAG, "onBackPressed ; dashboard fragment consumed back press ");
                return;
            }
        } else if (fm.getBackStackEntryCount() == 1) {
            Log.d(TAG, " entry == 1 " + fm.getBackStackEntryAt(0).getName());
            if (fm.getBackStackEntryAt(0).getName() != null)
                if (fm.getBackStackEntryAt(0).getName().equals(ChatFragment.class.getSimpleName()) ||
                        fm.getBackStackEntryAt(0).getName().equals(AllChatListFragment.class.getSimpleName()) ||
                        fm.getBackStackEntryAt(0).getName().equals(AddFriendFragment.class.getSimpleName()) ||
                        fm.getBackStackEntryAt(0).getName().equals(ViewChatMediaFragment.class.getSimpleName())) {
                    Log.d(TAG, " coming back to dashboard fragment ");
                    mCurrentFragment = FRAGMENT_DASHBOARD;
                }
        }


        if (IGNORE_BACK_STACK_LISTENER) {
            super.onBackPressed();
            return;
        }

        if (mCurrentFragment == FRAGMENT_VIDEO_EDITOR) {
            mPreviousFragment = mCurrentFragment;
            mCurrentFragment = FRAGMENT_CAMERA;
//            checkAndRemoveEditorFragment();
        } else if (isCanvasFragmentActive && getCanvasFragment().isCanvasLocked()) {

            getCanvasFragment().undoEditChanges();

        } else if (mCurrentFragment == FRAGMENT_NEW_GROUP) {
            mPreviousFragment = mCurrentFragment;
            mCurrentFragment = FRAGMENT_CAMERA; // Not necessary.. but not being used right now.
            getSupportFragmentManager().popBackStack();
        } else if (mCurrentFragment == FRAGMENT_SHARE_MEDIA) {
            mPreviousFragment = mCurrentFragment;
            mCurrentFragment = FRAGMENT_VIDEO_EDITOR;
            ShareMediaFragment shareMediaFragment = getShareMediaFragment();
            if (shareMediaFragment != null) {
                int activeView = shareMediaFragment.getCurrentActiveView();
                if (activeView == 0) {
                    // not active popups or view
                    mPreviousFragment = mCurrentFragment;
                    getSupportFragmentManager().popBackStack();
                } else {
                    shareMediaFragment.updateCurrentActiveView();
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    public void minimizeEditor() {
////        VideoEditorFragment fragment = getVideoEditorFragment();
//
//        if (fragment != null && getEmoticonFragment() != null && getEmoticonFragment().textInFocus == true) {
//            getEmoticonFragment().minimizeEditor();
//        }
    }


//    private boolean checkAndRemoveEditorFragment() {
//        VideoEditorFragment fragment = getVideoEditorFragment();
//        if (fragment != null)
//            fragment.destroyRenderer();
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.popBackStack();
////        viewPager.setCurrentItem(1);
//        return true;
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, " onDestroyCalled ");

//        if (controller != null) {
//            controller = null;
//        }

//        if (!CameraFragment.mVisible) { //Reset visibility if onDestroy is called
//            CameraFragment.mVisible = true;
//        }
    }

    @Override
    public void onEvent(BroadCastSignals.BaseSignal event) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //Should never be called as the app is locked at portrait
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, " onStopCalled ");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, " onStartCalled ");

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

//    private void resumePendingUploads() {  //Retrieve and Resume pending uploads
//        if (fireBaseHelper != null) {
//            fireBaseHelper.setMediaModelCallback(this);
//            fireBaseHelper.fetchPendingUploads();
//        }
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Add code to pass the activity result to fragments

//        Fragment fragment = (Fragment) getVideoEditorFragment();
//        if (fragment != null) {
//            fragment.onActivityResult(requestCode, resultCode, data);
//        }

    }

    // generic function to check if GPS is enabled or not
    public boolean isGpsEnabled() {
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }

    public void getAddress(final Location location) {
        Geocoder geo = new Geocoder(this, Locale.getDefault());

        if (Geocoder.isPresent()) {
            try {
                List<Address> addresses = geo.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    addressText = String.format("%s", address.getLocality());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     */
    public void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this,
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     */
    @SuppressWarnings("MissingPermission")
    public void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            AppLibrary.log_d(TAG, "GoogleLocationClient Connected");
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                mlatitude = mLastLocation.getLatitude();
                mlongitude = mLastLocation.getLongitude();
                AppLibrary.log_d(TAG, "Location : " + "latitude- " + mlatitude + " longitude- " + mlongitude);

                getAddress(mLastLocation); //If address is required

            } else {
                //location object null
                //default location is previous location
                AppLibrary.log_d(TAG, "Location object null");
            }
        } else {
            AppLibrary.log_d(TAG, "GoogleLocationClient not Connected, or is Streaming");
        }
    }

    public Location getLastLocation() {
        return mLastLocation;
    }

    public double getLatitude() {
        return mlatitude;
    }

    public double getLongitude() {
        return mlongitude;
    }

    /* Stopping location updates */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        AppLibrary.log_i("Connection", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        //startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

        //Not sure if we should use new lat long if location changed
    }

    public void startMediaUpload(String pendingMediaId, MediaModel mediaModel) {
        String mediaPath = mediaModel.url;
        if (mediaPath != null && !mediaPath.isEmpty()) {
            String videoPath = mediaPath;
            String name = "media";
            File file = new File(videoPath);
            String uniqueUploadName = prefs.getString(AppLibrary.USER_LOGIN, "Name") + System.currentTimeMillis(); //Always unique
            mediaUpload mediaUploader = new mediaUpload(CameraActivity.this, uniqueUploadName, pendingMediaId, mediaModel);

            if (file != null && file.exists()) {
                AppLibrary.log_e(TAG, "Starting media upload");
                mediaUploader.startUpload(file, name);
            } else {
                AppLibrary.log_e(TAG, "Media upload - file doesn't exist");
                //ToDo - Handle case when file doesn't exist
            }
        } else {
            AppLibrary.log_e(TAG, "Parse object is null in startMediaUpload");
            // something went wrong
        }
    }

    private void saveToLocalData(int action_type, HashMap<String, String> selectedRoomsForMoment, String mediaPath, HashMap<String, Integer> momentList, HashMap<String, Integer> roomList, int expiryType, String mediaText) {
        long createdAt = FireBaseHelper.getInstance(this).getUpdatedAt();
        MediaModel mediaModel = new MediaModel(mediaText, prefs.getString(AppLibrary.USER_LOGIN, ""), mediaPath, AppLibrary.getMediaType(mediaPath), true, false, createdAt);
        mediaModel.expiryType = expiryType;
        mediaModel.privacy = new MediaModel.Privacy(action_type, selectedRoomsForMoment);
        mediaModel.addedTo = new MediaModel.AddedTo(momentList, roomList);
        String pendingMediaId = fireBaseHelper.updatePendingUploads(mediaModel);
        startMediaUpload(pendingMediaId, mediaModel);
    }

    public static Context getApplicationContextPublic() {
        return applicationContext;
    }

    public void putNewProfileImage(Bitmap bitmap, String userid, boolean triggerLike) {
        //Add new bitmap fetched from background
    }

    public void createBitmapInBackground(Bitmap profileBitmap, String userId) {
        if (VERBOSE)
            AppLibrary.log_d(TAG, "Inside createLikeBitmap, Bitmap is -" + profileBitmap);

        //Creates like bitmap in the background - Any form of background bitmap creation can be done in this function
        BitmapWorkerClass task = new BitmapWorkerClass(profileBitmap, this, userId, false);
        task.execute();
    }

//    private CameraFragment getCameraFragment() {
//        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + 1);
//        // based on the current position you can then cast the page to the correct
//        // class and call the method:
//        if (page != null) {
//            return (CameraFragment) page;
//        } else {
//            return null;
//        }
//    }

    private DashBoardFragment getDashboardFragment() {
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + 0);
        // based on the current position you can then cast the page to the correct
        // class and call the method:
        if (page != null) {
            return (DashBoardFragment) page;
        } else {
            return null;
        }
    }

//    private VideoEditorFragment getVideoEditorFragment() {
//        return (VideoEditorFragment) getSupportFragmentManager().findFragmentByTag("videoEditorFragment");
//    }

    private EmoticonFragment getEmoticonFragment() {
        Fragment page = getSupportFragmentManager().findFragmentByTag("emoticonFragment");

        if (page != null)
            return (EmoticonFragment) page;
        else
            return null;
    }


    private CanvasFragment getCanvasFragment() {
//        CameraFragment cameraFragment = getCameraFragment();
//        if (cameraFragment != null) {
//            Fragment fragment = cameraFragment.getChildFragmentManager().findFragmentByTag("canvasFragment");
//            if (fragment != null)
//                return (CanvasFragment) fragment;
//            else
//                return null;
//        } else {
            return null;
//        }
    }

    private ShareMediaFragment getShareMediaFragment() {
        return (ShareMediaFragment) getSupportFragmentManager().findFragmentByTag("shareMediaFragment");
    }

    private void updateDashboardViews() {
        findViewById(R.id.topLayerLayout).setVisibility(View.GONE);
//        findViewById(R.id.bottomLayout).setVisibility(View.GONE);
//        findViewById(R.id.cameraControlsLayout).setVisibility(View.GONE);
//        findViewById(R.id.textOptionLayout).setVisibility(View.GONE);
    }

//    @Override
//    public void setViewPagerGestureDetector(GestureDetector gestureDetector) {
//        this.gestureDetector = gestureDetector;
//    }

    private void updateTextEditorViews() {
        findViewById(R.id.topLayerLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.cameraControlsLayout).setVisibility(View.GONE);
        findViewById(R.id.textOptionLayout).setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }

//    @Override
//    public void launchVideoEditorFragment(Bitmap bitmap) {
//        findViewById(R.id.topLayerLayout).setVisibility(View.GONE);
////        mCurrentFragment = FRAGMENT_VIDEO_EDITOR;
////        mPreviousFragment = mCurrentFragment - 1;
////        CameraFragment cameraFragment = getCameraFragment();
/////*        if (cameraFragment != null)
////            cameraFragment.pauseCameraPreview();*/
////        if (cameraFragment != null) {
////            cameraFragment.releaseCamera();
////        }
////        CapturedMediaController.getInstance().setCapturedBitmap(bitmap);
////        FragmentManager fragmentManager = getSupportFragmentManager();
////        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////        fragmentTransaction.add(R.id.fragmentContainer, new VideoEditorFragment(), "videoEditorFragment");
////        fragmentTransaction.addToBackStack(VideoEditorFragment.class.getSimpleName());
////        fragmentTransaction.commit();
//    }
//
//    @Override
//    public void launchVideoEditorFragment(String uri) {
//        findViewById(R.id.topLayerLayout).setVisibility(View.GONE);
//        mCurrentFragment = FRAGMENT_VIDEO_EDITOR;
//        mPreviousFragment = mCurrentFragment - 1;
//
//        CameraFragment cameraFragment = getCameraFragment();
//        if (uri != null && (uri.endsWith(".jpg") || uri.endsWith(".jpeg"))) {
///*            if (cameraFragment != null)
//                cameraFragment.pauseCameraPreview();*/
//            if (cameraFragment != null)
//                cameraFragment.releaseCamera();
//        } else if (uri != null && uri.endsWith(".mp4")) {
//            if (cameraFragment != null)
//                cameraFragment.releaseCamera();
//        }
//
//        toggleFullScreen(true);
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        VideoEditorFragment fragment = new VideoEditorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(AppLibrary.SELECTED_MEDIA_PATH, uri);
//        fragment.setArguments(bundle);
//        fragmentTransaction.add(R.id.fragmentContainer, fragment, "videoEditorFragment");
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
//    }

/*    @Override
    public void launchVideoEditorFragment(String uri) {
        CameraFragment cameraFragment = getCameraFragment();
        if (cameraFragment != null)
            cameraFragment.releaseCamera();
        mCurrentFragment = FRAGMENT_VIDEO_EDITOR;
        mPreviousFragment = mCurrentFragment - 1;
        MainFragmentPagerAdapter.uri = uri;
        Log.d(TAG, "URI: " + uri);
        viewPager.setCurrentItem(3);
    }*/

//    @Override
//    public void onLaunchCanvasFragment() {
//        isCanvasFragmentActive = true;
//        updateTextEditorViews();
////        startActivity(new Intent(this, ViewMomentActivity.class));
//    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentFragment = position; // the positions must correspond to the Fragment Enumeration.
        Log.d(TAG, "viewPager onPageSelected " + position);
        this.mCurrentFragmentInView = position;
    }


    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d(TAG, "viewPager onPageScrollStateChanged " + state);
        if (state == ViewPager.SCROLL_STATE_DRAGGING) {
            isScrolling = true;

            if (mCurrentFragmentInView == DASHBOARD_FRAGMENT_SELECTED) {
                toggleFullScreen(true);
//                if (!isCanvasFragmentActive)
//                    startCameraPreview(CAMERA_PREVIEW_START_DELAY);
            }

        } else if (state == ViewPager.SCROLL_STATE_IDLE) {

            isScrolling = false;

            if (mCurrentFragmentInView == DASHBOARD_FRAGMENT_SELECTED) {
                toggleFullScreen(false);
//                if (!isCanvasFragmentActive)
//                    stopCameraPreview();
                //Even if we're using interpolator, use here as it has be synchronous after full screen toggle - else animation becomes jerky
//                if (!dashBoardFragmentOpened) {
//                    getDashboardFragment().addInterpolator();
//                    dashBoardFragmentOpened = true;
//                }
            }
        }
    }

    void setMCurrentFragment(int backCount){
        if (backCount == 0) {
            if (viewPager.getCurrentItem() == 0)
                mCurrentFragment = FRAGMENT_DASHBOARD;
            else mCurrentFragment = FRAGMENT_CAMERA;

        } else { // backCount > 0 , quering top most element
            String tag = (getSupportFragmentManager().getBackStackEntryAt(backCount-1).getName());
            if (tag==null){
                Log.e(TAG," fragment tag not provided during transaction");
                return;
            }
            if (tag.equals(ChatFragment.class.getSimpleName())) {
                mCurrentFragment = FRAGMENT_CHAT;
                mPreviousFragment = -1;
            } else if (tag.equals(AllChatListFragment.class.getSimpleName())) {
                mCurrentFragment=FRAGMENT_ALL_CHAT_LIST;
                mPreviousFragment = -1;
            } else if (tag.equals(ViewChatMediaFragment.class.getSimpleName())) {
                mCurrentFragment=FRAGMENT_VIEW_CHAT_MEDIA;
                mPreviousFragment = -1;
            } else if (tag.equals(CreateManageGroupFragment.class.getSimpleName())) {
                mCurrentFragment=FRAGMENT_CREATE_MANAGE_GROUP;
                mPreviousFragment = -1;
            } else if (tag.equals(AddFriendFragment.class.getSimpleName())) {
                mCurrentFragment=FRAGMENT_ADD_FRIEND;
                mPreviousFragment = -1;
            } else if (tag.equals(ViewMyMediaFragment.class.getSimpleName())) {
                mCurrentFragment=FRAGMENT_VIEW_MY_MEDIA;
                mPreviousFragment =- 1;
            }
        }
    }
    @Override
    public void onBackStackChanged() {
        if (IGNORE_BACK_STACK_LISTENER)
            return;

        int backCount = getSupportFragmentManager().getBackStackEntryCount();
        Log.d(TAG, "onBackStackChanged: backStack count " + backCount);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            Log.d(TAG, "onBackStackChanged: fragment " + getSupportFragmentManager().getBackStackEntryAt(i).getName());
        }

        setMCurrentFragment(backCount);
        Log.d(TAG," setting mCurrentFragment to .. " + mCurrentFragment);


        Log.d(TAG, "OnBackStackChanged :: " + "previous frag: " + mPreviousFragment + " current frag: " + mCurrentFragment);
        if (mPreviousFragment >= mCurrentFragment) {
//            CameraFragment cameraFragment = getCameraFragment();
//            VideoEditorFragment videoEditorFragment = getVideoEditorFragment();
            switch (mCurrentFragment) {
                case FRAGMENT_CAMERA:
//                    if (cameraFragment != null) {
//                        cameraFragment.setVisibility(true);
//                        cameraFragment.onResume();
////                        cameraFragment.resumeCameraPreview();
//                    }
                    break;
                case FRAGMENT_VIDEO_EDITOR:
//                    if(videoEditorFragment != null) {
//                        videoEditorFragment.setCanPlay(true);
//                        videoEditorFragment.resumeMediaView();
//                    }
                    break;
                case FRAGMENT_SHARE_MEDIA:
                    mCurrentFragment = FRAGMENT_VIDEO_EDITOR;
                    break;
                case FRAGMENT_NEW_GROUP:
                    mCurrentFragment = FRAGMENT_SHARE_MEDIA;
                    break;
                default:
                    Log.d(TAG, "Unknown fragment");
            }
        } else {
//            CameraFragment cameraFragment = getCameraFragment();
//            VideoEditorFragment videoEditorFragment = getVideoEditorFragment();
            switch (mCurrentFragment) {
                case FRAGMENT_CAMERA:
//                    if (cameraFragment != null) {
//                        cameraFragment.setVisibility(true);
//                        cameraFragment.onResume();
//                    }
                    break;
                case FRAGMENT_VIDEO_EDITOR:
//                    if (cameraFragment != null) {
//                        cameraFragment.setVisibility(false);
//                        cameraFragment.translatePreview();
//                    }
                    break;
                case FRAGMENT_SHARE_MEDIA:
//                    if(videoEditorFragment != null) {
//                        videoEditorFragment.setCanPlay(false);
//                    }
            }
        }
    }

//    @Override
//    public void clearEditorFragment() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                onBackPressed();
//            }
//        });
//        checkAndRemoveEditorFragment();
//    }

    public void toggleFullScreen(final boolean goFullScreen) {

//        if (true)return;
        if (goFullScreen) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        } else {
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }


//    @Override
//    public void launchShareMediaFragment(String mediaPath, String mediaText) {
//        mPreviousFragment = mCurrentFragment;
//        mCurrentFragment = FRAGMENT_SHARE_MEDIA;
//        Bundle bundle = new Bundle();
//        bundle.putString("mediaPath", mediaPath);
//        bundle.putString(AppLibrary.MEDIA_TEXT, mediaText);
//        ShareMediaFragment fragment = new ShareMediaFragment();
//        fragment.setArguments(bundle);
//        getSupportFragmentManager().beginTransaction()
//                .add(R.id.fragmentContainer, fragment, "shareMediaFragment").addToBackStack(null).commitAllowingStateLoss();
//    }

//    @Override
//    public void launchShareFragment(String absolutePath, String s) {
//        launchShareMediaFragment(absolutePath, s);
//    }
//
//    @Override
//    public void setViewpagerSwipable(boolean enable) {
//        viewPager.setCanSwipe(enable);
//    }

//    @Override
//    public CustomViewPager getParentViewPager() {
//        return viewPager;
//    }
//
//    @Override
//    public void onCanvasFragmentHide() {
//        isCanvasFragmentActive = false;
//    }

//    @Override
//    public void launchEditorFragment(String uri) {
//        findViewById(R.id.topLayerLayout).setVisibility(View.GONE);
//        mPreviousFragment = mCurrentFragment;
//        mCurrentFragment = FRAGMENT_VIDEO_EDITOR;
//        CameraFragment cameraFragment = getCameraFragment();
//        if (cameraFragment != null)
//            cameraFragment.releaseCamera();
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        VideoEditorFragment fragment = new VideoEditorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(AppLibrary.SELECTED_MEDIA_PATH, uri);
//        fragment.setArguments(bundle);
//        fragmentTransaction.add(R.id.fragmentContainer, fragment, "videoEditorFragment");
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commitAllowingStateLoss();
//    }

    @Override
    public void onNewGroupButtonClicked() {
        mCurrentFragment = FRAGMENT_NEW_GROUP;
    }

    @Override
    public void uploadMediaToFireBase(final int action_type, final HashMap<String, String> selectedRoomsForMoment, final String mediaPath, final HashMap<String, Integer> momentList, final HashMap<String, Integer> roomList, final int expiryType, final String mediaText) {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        viewPager.setCurrentItem(0);
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    synchronized (mVideoLock) {
//                                if (controller!=null && !controller.isPublishing()) {
//                                    // Don't wait, video already encoded or picture already created
//                                    Log.d(TAG, "Item ready before sharing");
//                                } else if (controller!=null && controller.isPublishing() && mediaPath.endsWith(".mp4")){
//                                        mVideoLock.wait(10000); //Wait for at max 10 seconds
//                                    Log.d(TAG, "Video encoding ongoing before sharing");
//                                }
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }

                            File savedFile = new File(mediaPath); //Being doubly sure that the file now exists at the path, else will crash
                            if (savedFile != null && savedFile.exists())
                                saveToLocalData(action_type, selectedRoomsForMoment, mediaPath, momentList, roomList, expiryType, mediaText);

//                        if (controller != null) { //As we need to do this post encoding the video
//                            Log.e(TAG, "Destroying Movie Player and renderer");
//                            controller.pauseView();
//                            controller.setRenderTargetType(0);
//                            controller.stopAndCleanupPlayer(true);
//                            if (controller.isPublishing())
//                                controller.actionPublish();
//
//                            controller.destroy();
//                            controller = null;
//                            MoviePlayer.ENABLE_AUDIO = true; //Reset any audio changes
//                        }
                    }
                }
            };
            MasterClass.uploadHandler.post(r);
    }

    private ChatFragment chatFragment;
    public static SliderMessageModel currentRoomBeingOpened;

    public void loadChatFragment(SliderMessageModel data) {//pos not required; data has complete info
        /*if (chatFragment != null){
            Log.e(TAG, "@@@@@@");
            chatFragment = null;
        }*/
        if (currentRoomBeingOpened != null) {
            Log.e(TAG, " ignoring multiple request to openChat Fragment");
            return;
        }
//        if (chatFragment == null)
        chatFragment = new ChatFragment();
        if (data == null)
            throw new RuntimeException(" cannot openChats without sliderModel ");

        currentRoomBeingOpened = data;

        if (getDashboardFragment() != null)
            getDashboardFragment().dismissSliderOnChatOpen();

        Log.d(TAG, " opening room with Id " + data);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left,
                R.anim.slide_out_right, R.anim.slide_in_right);
        fragmentTransaction.add(R.id.chatFrame, chatFragment, ChatFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(ChatFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public ChatFragment getChatFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ChatFragment.class.getSimpleName());
        if (fragment != null)
            return (ChatFragment) fragment;
        else
            return null;
    }

//    public void setRecordingActionControllerReference(RecordingActionControllerKitkat controller) {
//        this.controller = controller;
//    }

    private AllChatListFragment allChatListFragment;

    public void openFriendListFragment() {
        allChatListFragment = new AllChatListFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, allChatListFragment, AllChatListFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(AllChatListFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void loadCreateGroupFragment() {
        CreateManageGroupFragment fragment = new CreateManageGroupFragment();
        Bundle data = new Bundle();
        data.putInt(AppLibrary.GROUP_ACTION, AppLibrary.GROUP_ACTION_CREATE);
        fragment.setArguments(data);

        Log.d(TAG, " opening create group fragment ");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, fragment, CreateManageGroupFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(CreateManageGroupFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void loadViewMyMediaFragment(String mediaId) {
        Bundle bundle = new Bundle();
        bundle.putString(AppLibrary.MEDIA_ID, mediaId);
        ViewMyMediaFragment fragment = new ViewMyMediaFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, fragment, ViewMyMediaFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(ViewMyMediaFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onCloseViewMyMediaFragment() {
    /*    Fragment fragment = getSupportFragmentManager().findFragmentByTag(ViewMyMediaFragment.class.getSimpleName());
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }*/ //not being removed from back stack right away, sometimes fixme

        if (getFragmentInBackStack(ViewMyMediaFragment.class.getSimpleName()) != null) {
            boolean popped = getSupportFragmentManager().popBackStackImmediate();
            Log.d(TAG, "onCloseViewMyMediaFragment: popped " + popped);
        } else Log.d(TAG, "onCloseViewMyMediaFragment: fragment is null");
    }

    @Override
    public void onPendingMediaListLoaded(MediaModel mediaModel, String pendingId) {
        startMediaUpload(pendingId, mediaModel);
    }

    @Override
    public void onPendingMediaForMessageLoaded(MediaModel mediaModel, String pendingId, String roomId, String messageId) {
        startMediaUpload(pendingId, mediaModel);
        mFireBaseHelper.updateMediaMessageUploadStarting(roomId, pendingId);
    }

    public void scrollToCameraFragment() {
//        viewPager.setCurrentItem(1, true);
//        this.toggleFullScreen(true);
//        startCameraPreview(CAMERA_PREVIEW_START_DELAY);
    }

    final int CAMERA_PREVIEW_START_DELAY = 0;//millisecs

//    public void stopCameraPreview() {
//        CameraFragment cameraFragment = (CameraFragment) getSupportFragmentManager().
//                findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + CAMERA_FRAGMENT_SELECTED);
//        cameraFragment.pauseCameraPreview();
//    }
//
//    public void startCameraPreview(int delay) {
//        final CameraFragment cameraFragment = (CameraFragment) getSupportFragmentManager().
//                findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + CAMERA_FRAGMENT_SELECTED);
//        cameraFragment.resumeCameraPreview();
//    }

    public void requestAddFriendFragmentOpen() {
        final DashBoardFragment dashBoardFragment = (DashBoardFragment) getSupportFragmentManager().
                findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + DASHBOARD_FRAGMENT_SELECTED);
        dashBoardFragment.loadAddFriendFriendFragment();
    }

    @Override
    public void onSwipeLeft() {
//        VideoEditorFragment videoEditorFragment = getVideoEditorFragment();
//        if (videoEditorFragment != null) {
//            videoEditorFragment.filterSwipeLeft();
//        }
    }

    @Override
    public void onSwipeRight() {
//        VideoEditorFragment videoEditorFragment = getVideoEditorFragment();
//        if (videoEditorFragment != null) {
//            videoEditorFragment.filterSwipeRight();
//        }
    }

    @Override
    public void updateUploadingStatus(final MediaModel mediaModel, final String mediaId, final int status) {
        DashBoardFragment fragment = getDashboardFragment();
        if (fragment != null) {
            fragment.updateMediaUploadStatus(mediaModel, mediaId, status);
        }
    }

    @Override
    public void onResumeUpload(String roomId) {
        // resume failed upload of only that media which the user clicked to retry
        if (fireBaseHelper != null && roomId != null) {
            fireBaseHelper.setMediaModelCallback(this);
            fireBaseHelper.fetchPendingUploadsForRoom(roomId);
        }
    }

    @Override
    public void onUploadRetryClicked(String mediaId) {
        if (fireBaseHelper != null && mediaId != null) {
            fireBaseHelper.setMediaModelCallback(this);
            fireBaseHelper.fetchPendingUploadsForMedia(mediaId);
        }
    }

    @Override
    public void onUploadRetryClickedForAllMediaInMoments() {
        if (fireBaseHelper != null) {
            fireBaseHelper.setMediaModelCallback(this);
            fireBaseHelper.fetchPendingUploadsForMyMoments();
        }
    }

    @Override
    public void onUploadRetryClickedForAllMediaMessages() {
        if (fireBaseHelper != null) {
            fireBaseHelper.setMediaModelCallback(this);
            fireBaseHelper.fetchPendingUploadsForAllRoom();
        }
    }

    @Override
    public void onLoadViewChatMediaFragment(String roomId,int roomType,String mediaId) {
        Bundle bundle = new Bundle();
        bundle.putString(AppLibrary.MEDIA_ID, mediaId);
        bundle.putString(AppLibrary.ROOM_ID, roomId);
        bundle.putInt(AppLibrary.ROOM_TYPE,roomType);
        ViewChatMediaFragment viewChatMediaFragment = new ViewChatMediaFragment();
        viewChatMediaFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.viewChatMomentContainer, viewChatMediaFragment, ViewChatMediaFragment.class.getSimpleName()).
                addToBackStack(ViewChatMediaFragment.class.getSimpleName()).commitAllowingStateLoss();
    }

    @Override
    public void onRoomOpen(String roomId) {
        DashBoardFragment fragment = getDashboardFragment();
        if (fragment != null){
            fragment.updateOnRoomOpen(roomId);
        }
    }

    @Override
    public LinkedHashMap<String, ChatMediaModel> getDownloadedChatMediaMap(String mediaId) {
        ChatFragment fragment = getChatFragment();
        if (fragment != null) {
            return fragment.getDownloadedChatMediaMap(mediaId);
        } else {
            return null;
        }
    }

    @Override
    public void onCloseViewChatMediaFragment() {
        /*ChatFragment fragment = getChatFragment();
        if (fragment != null){
            onBackPressed();
        }*/

/*        FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(this.getSupportFragmentManager().getBackStackEntryCount() - 1);
        String str = backEntry.getName();
        if (str != null && str.equals(ViewChatMediaFragment.class.getSimpleName())) {
            FragmentManager fm = getSupportFragmentManager();
            boolean popped = fm.popBackStackImmediate();
            Log.d(TAG, "onCloseViewChatMediaFragment popped? " + popped);
        }*/

        Log.d(TAG, "onCloseViewChatMediaFragment requested close ViewChatMediaFragment ");

        try {
            if (getFragmentInBackStack(ViewChatMediaFragment.class.getSimpleName()) != null) {
                boolean popped = getSupportFragmentManager().popBackStackImmediate();
                Log.d(TAG, "onCloseViewChatMediaFragment: popped " + popped);
            } else Log.d(TAG, "onCloseViewChatMediaFragment: fragment is null");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public boolean popFragmentInBackStack(String tag) {
        if (getFragmentInBackStack(tag) != null) {
            boolean popped = getSupportFragmentManager().popBackStackImmediate();
            Log.d(TAG, "popFragmentInBackStack: popped " + popped);
            return popped;
        } else {
            Log.d(TAG, "popFragmentInBackStack: fragment is null");
            return false;
        }
    }

    /**
     * @param fragmentTag the String supplied while fragment transaction
     * @return
     */
    private Fragment getFragmentInBackStack(String fragmentTag) {
        return getSupportFragmentManager().findFragmentByTag(fragmentTag);

    }

    @Override
    public void onOpenMedia(String mediaId) {
        ChatFragment fragment = getChatFragment();
        if (fragment != null) {
            chatFragment.onOpenMedia(mediaId);
        }
    }

    @Override
    public void onLoadViewMomentFragment(String momentId, int momentStatus) {
        ViewMomentsFragment viewMomentsFragment = new ViewMomentsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppLibrary.MOMENT_ID,momentId);
        bundle.putInt(AppLibrary.MOMENT_STATUS,momentStatus);
        viewMomentsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.viewMomentsFragmentContainer, viewMomentsFragment, ViewMomentsFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(ViewMomentsFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onLoadViewMomentFragment(String momentId, int momentStatus, String momentType) {
        ViewMomentsFragment viewMomentsFragment = new ViewMomentsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppLibrary.MOMENT_ID,momentId);
        bundle.putInt(AppLibrary.MOMENT_STATUS,momentStatus);
        bundle.putString(AppLibrary.MOMENT_TYPE,momentType);
        viewMomentsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.viewMomentsFragmentContainer, viewMomentsFragment, ViewMomentsFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(ViewMomentsFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onCloseViewMomentsFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ViewMomentsFragment.class.getSimpleName());
        getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
    }
}