package com.instalively.android.broadcast;

import android.hardware.Camera;

import com.instalively.android.activities.CameraActivity;
//import com.instalively.android.fragments.CameraFragment;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by admin on 1/20/2015.
 */
public class FilterManager implements Serializable {

    private String colorEffect;
    private String flashMode;

    /* can be static, these values wont change */
    private static ArrayList<String> availColorEffects = new ArrayList<>();
    private static ArrayList<String> availFlashModes = new ArrayList<>();

    private static ArrayList<String> defaultAvailColorEffects = new ArrayList<>();

    static {
        defaultAvailColorEffects.add("NATURAL");
        defaultAvailColorEffects.add("RICH");
        defaultAvailColorEffects.add("MONO");
        defaultAvailColorEffects.add("NIGHT");
        defaultAvailColorEffects.add("VIGNETTE");
        defaultAvailColorEffects.add("MIRROR");
        defaultAvailColorEffects.add("BULGE");
        defaultAvailColorEffects.add("SKETCH");
        defaultAvailColorEffects.add("TOON");
    }

    public static final int FILTER_NATURAL = 0;
    public static final int FILTER_RICH = 1;
    public static final int FILTER_MONO = 2;
    public static final int FILTER_NIGHT = 3;
    public static final int FILTER_VIGNETTE = 4;
    public static final int FILTER_MIRROR = 5;
    public static final int FILTER_BULGE = 6;
    public static final int FILTER_SKETCH = 7;
    public static final int FILTER_TOON = 8;

    public static final int FILTER_COUNT = 9;

    private static int FILTER_RUNNING_COUNT = 9;

    public static final int FILTER_BLUR = ++FILTER_RUNNING_COUNT;
    public static final int FILTER_SHARP= ++FILTER_RUNNING_COUNT;

    public static void initialize(Camera.Parameters params) {
        
        availColorEffects.clear();
        for(String val : defaultAvailColorEffects) {
            availColorEffects.add(val);
        }

        availFlashModes.clear();

        availFlashModes.add("torch");
        availFlashModes.add("off");

    }
    
    public static void destroy() {
        availColorEffects.clear();

        availFlashModes.clear();
    }

    public static FilterManager getInstance (Camera.Parameters params) {
        initialize(params);
        return new FilterManager(params);
    }

    public static FilterManager getInstance(FilterManager filterManager) {
        if (filterManager == null)
            throw new NullPointerException("filterManager should not be null");
        return new FilterManager(filterManager);
    }

    private FilterManager (Camera.Parameters params) {

        this.colorEffect  =   defaultAvailColorEffects.get(0);

        if(CameraActivity.flashMode)
            this.flashMode    =   params.getFlashMode();
    }
    
    private FilterManager (FilterManager fm) {
        this.colorEffect   = fm.colorEffect;
        this.flashMode     = fm.flashMode;
    }

    public void setColorEffect(String colorEffect) {
        if (!availColorEffects.contains(colorEffect)) {
            throw new IllegalStateException(colorEffect + " is unknown, should be selected from available settings");
        }
        this.colorEffect = colorEffect;
    }

    public void setFlashMode(String flashMode) {
        if (!availFlashModes.contains(flashMode)) {
            throw new IllegalStateException(flashMode + " is unknown, should be selected from available settings");
        }
        this.flashMode = flashMode;
    }

    public String rotateFlashMode () {
        int index = availFlashModes.indexOf(flashMode);
        index = (index + 1)%availFlashModes.size();
        setFlashMode(availFlashModes.get(index));
        return flashMode;
    }

    public String getColorEffect() {
        return colorEffect;
    }

    public String getFlashMode() {
        return flashMode;
    }

    public ArrayList<String> getAvailColorEffects () {
        return (ArrayList<String>) availColorEffects.clone();
    }

    public static String getColorEffectName(int index) {
        if(availColorEffects == null || availColorEffects.size() == 0) return null;
        return availColorEffects.get(index);
    }

    public ArrayList<String> getAvailFlashModes () {
        return (ArrayList<String>) availFlashModes.clone();
    }
}
