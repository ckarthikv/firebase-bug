package com.instalively.android.stickers;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;

import com.instalively.android.R;
import com.instalively.android.customViews.SquareImageView;

/**
 * Created by deepankur on 15/2/16.
 */
public class EmojiGridAdapter extends BaseAdapter {
    private final Context context;
    private final Bitmap[] bitmaps;
    private LayoutInflater inflater;


    public EmojiGridAdapter(Context context, Bitmap[] bitmaps) {
        this.context = context;
        this.bitmaps = bitmaps;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return bitmaps.length;
    }

    @Override
    public Object getItem(int position) {
        return bitmaps[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.emojicell, null);
        }
        SquareImageView imageView = (SquareImageView) convertView.findViewById(R.id.emoji);
        imageView.setImageBitmap(bitmaps[position]);
        addViewTreeObserver(imageView);
        return convertView;
    }

    private void addViewTreeObserver(final View view) {
        final String TAG = "Adapter";
        final ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int initIVHeight = view.getMeasuredHeight();
                    int initIVWidth = view.getMeasuredWidth();
                    int top = view.getTop();
                    int left = view.getLeft();
                    int[] dimens = {initIVWidth, initIVHeight, top, left};
                    Log.d(TAG, "VTO " + initIVWidth + "<-width height->" + initIVHeight);
                    view.setTag(dimens);
                    int[] posXY = new int[2];
                    view.getLocationOnScreen(posXY);
                    int x = posXY[0];
                    int y = posXY[1];
                    Log.d(TAG, "xycor" + " x :" + x + " y :" + y);
                }
            });
        }

    }
}
