package com.instalively.android.stickers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.instalively.android.models.LocationTemplateModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by deepankur on 5/27/16.
 */
public class LocationThemeLoader {
    //    private SparseArray<LocationTemplateModel> templates;
    private LinkedHashMap<String, LocationTemplateModel> templates;
    private Context context;
    private static LocationThemeLoader themeLoader;
    private Set<Target> protectedFromGarbageCollectorTargets;
    private String TAG = this.getClass().getSimpleName();

    public static LocationThemeLoader getInstance(Context context, LinkedHashMap<String, LocationTemplateModel> templates) {
        if (themeLoader == null)
            themeLoader = new LocationThemeLoader(context, templates);
        return themeLoader;
    }

    private LocationThemeLoader(Context context, LinkedHashMap<String, LocationTemplateModel> templates) {
        this.context = context;
        this.templates = templates;
        protectedFromGarbageCollectorTargets = new HashSet<>();
        syncBitmapFromFireBase();
    }

    public LocationTemplateModel getThemeOfThisFragment(int index) {
        String[] keys = templates.keySet().toArray(new String[templates.size()]);
        return templates.get(keys[index]);
    }
/*    public static void destroy() {
        if (sThemeModelMap != null) {
            for (int i = 0; i < sThemeModelMap.size(); i++) {
                int key = sThemeModelMap.keyAt(i);
                ThemeModel themeModel = sThemeModelMap.get(key);
                for (int j = 0; j < themeModel.getAssets().length; j++) {
                    Bitmap bitmap = themeModel.getAssets()[j].mBitmap;
                    if (bitmap != null) {
                        bitmap.recycle();
                    }
                }
            }
            sThemeModelMap = null;
        }
    }*/

    private class UrlTarget implements Target {
        String TAG = "ThemeLoader";
        //        ThemeAsset themeAsset;
        Context context;
        LocationTemplateModel.LocationSticker sticker;

        public UrlTarget(LocationTemplateModel.LocationSticker sticker, Context context) {
            this.sticker = sticker;
            this.context = context;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.d(TAG, "onBitmapLoaded");
            this.sticker.mBitmap = bitmap;
            writeThisBitmapToInternalStorage(String.valueOf(this.sticker.mStickerId), bitmap);
            protectedFromGarbageCollectorTargets.remove(this);
        }

        private void writeThisBitmapToInternalStorage(String stickerId, Bitmap bitmap) {
            File file = new File(context.getFilesDir(), stickerId);
            FileOutputStream fOutputStream;
            try {
                fOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOutputStream);
                fOutputStream.flush();
                fOutputStream.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.d(TAG, "exception FNF" + e);
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "exception IO" + e);
            }
        }


        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.d(TAG, "onBitmapFailed");
            protectedFromGarbageCollectorTargets.remove(this);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            Log.d(TAG, "onPrepareLoad");
        }
    }

    String IMAGES_HOST_URL = "";

    private Bitmap readFromInternalDirectory(String stickerId) {
        String filePath = context.getFilesDir() + "/" + stickerId;
        File file = new File(filePath);
        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
            return bitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void syncBitmapFromFireBase() {
        boolean clearScreen = true;
        for (Map.Entry<String, LocationTemplateModel> template : templates.entrySet()) {
            if (clearScreen) {
                clearScreen = false;
                continue;
            }
            HashMap<String, LocationTemplateModel.LocationSticker> stickerMap = template.getValue().stickers;
            for (Map.Entry<String, LocationTemplateModel.LocationSticker> stickersEntry : stickerMap.entrySet()) {
                String stickerId = stickersEntry.getKey();
                LocationTemplateModel.LocationSticker sticker = stickersEntry.getValue();
                sticker.mStickerId = stickerId;
                Bitmap bitmap = readFromInternalDirectory(stickerId);
                if (bitmap == null) {
                    Log.d(TAG, "bitmap not found internally");
                    UrlTarget urlTarget = new UrlTarget(sticker, context);
                    protectedFromGarbageCollectorTargets.add(urlTarget);
                    Picasso.with(context).load(/*IMAGES_HOST_URL + */sticker.pictureUrl).into(urlTarget);
                } else sticker.mBitmap = bitmap;

            }
        }
    }
}
