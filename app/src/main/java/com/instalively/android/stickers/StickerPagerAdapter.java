package com.instalively.android.stickers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by abc on 12/1/2015.
 */
public class StickerPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 3;
    private static ChildStickerFragment[] childStickerFragments = new ChildStickerFragment[3];

    public StickerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {
        ChildStickerFragment childStickerFragment = new ChildStickerFragment();
        Bundle data = new Bundle();
        data.putInt("current_page", arg0);
        childStickerFragment.setArguments(data);
        childStickerFragments[arg0] = childStickerFragment;
        return childStickerFragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    public static ChildStickerFragment getChildStickerFragment(int position) {
        return childStickerFragments[position];
    }

}
