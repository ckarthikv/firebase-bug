package com.instalively.android.modelView;

/**
 * Created by deepankur on 6/23/16.
 */
public class FaceBookFriendsModel {
    public String userId;
    public String imageUrl;
    public String handle;
    public String name;
    public boolean isChecked;

    public FaceBookFriendsModel(String userId,String name, String imageUrl, String handle, boolean isChecked) {
        this.userId = userId;
        this.name=name;
        this.imageUrl = imageUrl;
        this.handle = handle;
        this.isChecked = isChecked;
    }
}
