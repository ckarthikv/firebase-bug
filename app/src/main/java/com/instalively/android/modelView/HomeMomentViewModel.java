package com.instalively.android.modelView;

import android.util.Log;

import java.util.Random;

/**
 * Created by deepankur on 6/5/16.
 */
public class HomeMomentViewModel {// internal reference

    public enum ClickType {SINGLE_TAP, DOUBLE_TAP}

//    public boolean watchedMomentInCurrentSession;
    public ClickType clickType;//internal reference
    public String roomId;
    public String name;
    public String imageUrl;
    public String momentId;
    public long updatedAt;
    public String updatedAtText;//formatted in "hours and minutes"
    public long timeLeftToExpire;
    public boolean checkedOut;//already checked out that moment
    public int roomType;//friend or group
    public int momentStatus;//download status
    public float angle;//representing the time left in expiry angle is 0 is about to expire 360 if its a fresh moment
    private String TAG = this.getClass().getSimpleName();


    public HomeMomentViewModel() {
    }

    public HomeMomentViewModel(String roomId, String name, String imageUrl, String updatedAtText, long timeLeftToExpire, String momentId, boolean checkedOut, int roomType, int downloadStatus) {
        this.roomId = roomId;
        this.name = name;
        this.imageUrl = imageUrl;
        this.timeLeftToExpire = timeLeftToExpire;
        this.updatedAtText = updatedAtText;
        this.checkedOut = checkedOut;
        this.roomType = roomType;
        this.momentStatus = downloadStatus;
        this.momentId = momentId;
//        initRandomAngle();
        initExpireAngle();
    }

/*
    private void initRandomAngle() {
        Random r = new Random();
        int Low = 0;
        int High = 360;
        this.angle = r.nextInt(High - Low) + Low;
    }
*/

    public class Media {
        public String url;
        public long status;
    }

    private void initExpireAngle() {
        this.angle = ((this.timeLeftToExpire + 0.0f) / (24 * 60 * 60 * 1000)) * 360;
        Log.d(TAG, " angle is " + angle);
    }
}
