package com.instalively.android.modelView;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by user on 6/8/2016.
 */
public class MediaModelView implements Serializable {
    public String url;
    public long createdAt;
    public int type;
    public int totalViews;
    public HashMap<String,Integer> viewers;
    public int screenShots;
    public String createdAtText;
    public String momentId;
    public String mediaId;
    public int status;
    public String mediaText;

    public MediaModelView(String url,long createdAt,int type,int totalViews,HashMap<String,Integer> viewers,
                          int screenShots,String createdAtText,String momentId,String mediaId,int status,String mediaText){
        this.url = url;
        this.createdAt = createdAt;
        this.type = type;
        this.totalViews = totalViews;
        this.viewers = viewers;
        this.screenShots = screenShots;
        this.createdAtText = createdAtText;
        this.momentId = momentId;
        this.mediaId = mediaId;
        this.status = status;
        this.mediaText = mediaText;
    }
}
