package com.instalively.android.modelView;

import android.util.Log;

import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.models.RoomsModel;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by deepankur on 8/5/16.
 */
public class SliderMessageModel implements FireBaseKEYIDS, Serializable {
    public String roomId;
    public int roomType;//friend; group
    public String displayName;
    public String imageUrl;//shown at top of message window
    public String updatedAtText;
    public String status;
    public int messageType;
    public String friendId;
    public HashMap<String, RoomsModel.Members> members;
    private int normalizedFirstChar;//for internal reference; this will make organising a list based on alphabets faster

    public int getNormalizedFirstChar() {
        return normalizedFirstChar;
    }

    public void setFirstChar(String displayName) {
        int letter = displayName.charAt(0);
        if (letter > 96 && letter < 123) {
            this.normalizedFirstChar = letter - 32;
        } else this.normalizedFirstChar = letter;

        Log.d("SliderMessageModel ", " name " + displayName + " char " + letter);
    }

    @Override
    public String toString() {
        return (super.toString() + "\n roomId " + roomId + " roomType " + roomType + " displayName " + displayName
                + " updatedAtText " + updatedAtText + " status " + status + " messageType " + messageType +
                " friendId " + friendId);
    }

    public SliderMessageModel() {
    }

    public SliderMessageModel(String displayName, String imageUrl, String roomId, String friendId, int roomType, int messageType, String updatedAtText, HashMap<String, RoomsModel.Members> members) {
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.roomId = roomId;
        this.roomType = roomType;
//        this.status = status;
        this.updatedAtText = updatedAtText;
        this.messageType = messageType;
        this.friendId = friendId;
        this.members = members;
        this.status = setStatus(this.messageType);
    }

    public String setStatus(final int messageType) {
        switch (messageType) {
            case SENDING_FAILED:
                return SENDING_FAILED_TEXT;
            case SENDING_MEDIA:
                return "Sending...";
            case NO_STATUS:
                return "";
            case SENT_MEDIA:
                return "SENT_MEDIA";
            case SENT_CHAT:
                return "SENT_CHAT";
            case SEEN_MEDIA:
                return "SEEN_MEDIA";
            case SEEN_CHAT:
                return "SEEN_CHAT";
            case SCREEN_SHOTTED_CHAT:
                return "SCREEN_SHOTTED_CHAT";
            case SCREEN_SHOTTED_MEDIA:
                return "SCREEN_SHOTTED_MEDIA";
            case NEW_MEDIA:
                return "NEW_MEDIA";
            case NEW_TEXT:
                return "NEW_TEXT";
            case MEDIA_WITH_TEXT:
                return "MEDIA_WITH_TEXT";
            case GROUP_CREATED:
                return "GROUP_CREATED";
            case MEMBER_JOIN:
                return "MEMBER_JOIN";
            case YOU_JOIN:
                return "YOU_JOI;N";
            case ADMIN_REMOVED_MEMBER:
                return "ADMIN_REMOVED_MEMBER";
            case USER_LEFT_GROUP:
                return "USER_LEFT_GROUP";
            default:
                return null;
        }
    }


}
