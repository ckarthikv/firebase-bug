package com.instalively.android;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.appsflyer.AppsFlyerLib;
import com.instalively.android.activities.CameraActivity;
import com.instalively.android.activities.FacebookLogin;
import com.instalively.android.activities.OnBoardingActivity;
import com.instalively.android.firebase.FireBaseHelper;
import com.instalively.android.firebase.FireBaseKEYIDS;
import com.instalively.android.models.SocialModel;
import com.instalively.android.models.UserModel;
import com.instalively.android.util.AppLibrary;

/**
 * Created by Karthik on 2/29/2016
 */
public class Splash extends AppCompatActivity implements FireBaseKEYIDS {

    private static final String TAG = "I/Splash";

    private SharedPreferences prefs;
    public static boolean isCameraOpenChecker;
    public static boolean isActivityLaunched;
    private static final int PERMISSION_ACCESS_CAMERA_MICROPHONE = 0;
    private static FireBaseHelper fireBaseHelper;

    @SuppressLint("CommitPrefEdits")
    @SuppressWarnings("PointlessBooleanExpression")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCameraOpenChecker = true;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            setContentView(R.layout.splash);
    }

    private void requestCameraPermissionsAndProceed() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                    PERMISSION_ACCESS_CAMERA_MICROPHONE);
        } else {
            Intent mIntent = new Intent(Splash.this, CameraActivity.class);
            startActivity(mIntent);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_CAMERA_MICROPHONE:
                if (AppLibrary.verifyPermissions(grantResults)) {
                    Intent mIntent = new Intent(Splash.this, CameraActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    requestCameraPermissionsAndProceed();
                }
            break;

            default: super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void startOnBoardingActivity(int onBoardingStatus, String username) {
        Intent intent = new Intent(Splash.this, OnBoardingActivity.class);
        intent.putExtra(AppLibrary.USER_ONBOARDING_STATUS, onBoardingStatus);
        intent.putExtra(AppLibrary.USER_NAME, username);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        prefs = getSharedPreferences(AppLibrary.APP_SETTINGS, 0);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppLibrary.USER_LOGIN, "577294cb4e55002340ee1952");//putting userId in prefs
        editor.putInt(AppLibrary.USER_ONBOARDING_STATUS, 3);//putting on board status in prefs
        editor.commit();

        final int onBoardingStatus = prefs.getInt(AppLibrary.USER_ONBOARDING_STATUS, 0);
        /**
         * if the userId  is  there start activity on basis of onboarding status
         */
        if ((prefs.getString(AppLibrary.USER_LOGIN, null) != null)) {
            AppsFlyerLib.setCustomerUserId(prefs.getString(AppLibrary.USER_LOGIN, null)); //To cross-reference using UserID

            Log.d(TAG, "user Id already there in prefs " + prefs.getString(AppLibrary.USER_LOGIN, null));
            FireBaseHelper.myUserId = prefs.getString(AppLibrary.USER_LOGIN, null);

            if (fireBaseHelper==null || fireBaseHelper.getSocialModel() == null || fireBaseHelper.getMyUserModel()==null) {
            fireBaseHelper = FireBaseHelper.getInstance(getApplicationContext());
            fireBaseHelper.setFireBaseReadyListener(new FireBaseHelper.FireBaseReadyListener() {
                    @Override
                    public void onDataLoaded(SocialModel socialModel, UserModel userModel) {
                        Log.d(TAG, " socialModel " + socialModel + " userModel " + userModel);

                        if (socialModel != null && userModel != null && !isActivityLaunched) {
                            isActivityLaunched = true;
                            if (onBoardingStatus == INSTITUTION_PROVING_DONE) {//startCameraActivity; onboarding already done
                                requestCameraPermissionsAndProceed(); //Request camera permissions before starting activity
                            } else {//startOnBoardingActivity
                                startOnBoardingActivity(onBoardingStatus, userModel.name);
                            }
                        }
                    }
                });
            } else {
                //ToDo - Tell Firebase to Refresh UserModel, SocialModel, etc
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (onBoardingStatus == INSTITUTION_PROVING_DONE) {//startCameraActivity; onboarding already done
                            requestCameraPermissionsAndProceed(); //Request camera permissions before starting activity
                        } else {//startOnBoardingActivity
                            startOnBoardingActivity(onBoardingStatus, fireBaseHelper.getMyUserModel().name);
                        }
                    }
                }, 700);
            }
        }
        else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent mIntent = new Intent(Splash.this, FacebookLogin.class);
                    startActivity(mIntent);
                    finish();
                }
            }, 1500);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}